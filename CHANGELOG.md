# Changelog

<https://keepachangelog.com/>

Types of changes

- 'Added' for new features.
- 'Changed' for changes in existing functionality.
- 'Deprecated' for soon-to-be removed features.
- 'Removed' for now removed features.
- 'Fixed' for any bug fixes.
- 'Security' in case of vulnerabilities.

## [Unreleased]

## 0.3.3, 2025-02-24

### Changed

- project is now managed with uv

## 0.3.2 2024-07-29

- revise 'find_files', resolution 'full' is now 'highest' and falls back to '10s' if no non-10s file is found
- dissect_filename now treats version prefix 'v' and 'V' identically (didn't like lower-case v)
- use pyfuppes 0.5

## 0.3.1 2024-07-16

- use tagged version of pyfuppes
- update examples

## 0.3.0 2024-05-03

- MA tools update
- update license to GPLv3

## 0.2.25 2024-01-12

- add WSM DOI

## 0.2.24 2024-01-11

- na2nc: revise handling of na file header; set it as a global attr instead of var

## 0.2.23 2024-01-10

- na2nc: include NASA Ames file header as a dimension-less variable instead of a variable with a dimension

## 0.2.22 2024-01-08

- update WSM variable config

## 0.2.21 2024-01-05

- expose modules via `__all__`

## 0.2.20 (2024-01-05)

- check CARIBIC NASA Ames: make ChangeLog check optional
- check CARIBIC NASA Ames: add typing for result

## 0.2.19 (2023-11-30)

- revise netCDF global attributes and attributes of variables

## 0.2.18 (2023-11-29, no tag)

- add script to merge whole air sampler data in netCDF format
- carfileutils.increment_version: allow pathlib.Path as input filename
- carfileutils: add new prefix INTtps (CARIBIC data file names)

## 0.2.17 (2023-11-16)

- add script to convert CARIBIC-1 NASA Ames files to netCDF

## 0.2.16 (2023-09-26)

- NASA Ames to netCDF: add contributors field

## 0.2.15 (2023-09-26)

- NASA Ames to netCDF: add "T" (time), "X" (lon), "Y" (lat) and "Z" (pressure alt) tags to coordinate variables according to CF convention

## 0.2.14 (2023-09-25, no tag)

- NASA Ames to netCDF: add license and DOI as nc var

## 0.2.13 (2023-09-20)

- more verbose output / checks for SU make script

## 0.2.12 (2023-09-18)

- export Header_Defaults in `caribic2dp.check_caribic_na`, a dict with the content of `Caribic2_DataProc/config/NA_header_cfg.toml`

## 0.2.11 (2023-07-26)

- add na to nc converter tests

## 0.2.10 (2023-07-24)

- make nasa ames header defaults a config file (check_caribic_na.py/revise_header)

## 0.2.9 (2023-06-29)

- bugfix SU and MS file name generation (was: replaced all 'MA' characters...)
- bugfix ExceptionToConsider line number
- revised search for model data (MS file generation)

## 0.2.8 (2023-06-28)

- introduce changelog for SU and MS files

## 0.2.7 (2023-06-27)

- revised SU merge file processing, adjusted to changes in pyfuppes.na1001
- updated header lines to check with NASA Ames file validator

## 0.2.6 (2023-02-20)

- add CARIBIC netCDF to CARIBIC NASA Ames converter (alpha)
- add number format check in na2nc converter

## 0.2.5 (2022-12-28)

- add CARIBIC NASA Ames to CARIBIC netCDF converter (alpha)

## 0.2.4 (2022-12-21)

- NASA Ames checker is alpha-complete, including example!

## 0.2.3 (2022-11-25)

- add example : usage of find_files and gather_data

## 0.2.2 (2022-11-19)

- API change! : revise find_files function in carfileutils

## 0.2.1 (2022-11-16)

- draft NA checker and NA-nc4 conversion

## 0.2.0 (2022-11-16)

- API change! : revise `carfiletools` module, is now `carfileutils`
- sphinx docs : switch to html for readability
- updated pyproject.toml so that poetry can build a wheel file

## 0.1.2

- add sphinx docs (markdown)

## 0.1.1

- revised structure, updated paths in scripts

## 0.1.0

- initial release
