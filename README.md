# Caribic2_DataProc

Process IAGOS-CARIBIC (CARIBIC-2) data.

## Installation

Via `pip`, from a specific [tag](https://gitlab.kit.edu/FObersteiner/Caribic2dp/-/tags) of this repository:

```sh
pip install git+https://gitlab.kit.edu/FObersteiner/Caribic2dp.git@[TAG-NAME]
```

...or editable, from cloned repo:

```sh
pip install -e .
```

## License

GPLv3 - see LICENSE file in the root directory of the repository.
