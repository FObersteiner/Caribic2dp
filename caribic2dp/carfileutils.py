# -*- coding: utf-8 -*-
"""CARIBIC file server tools, revised version (was: carfiletools)."""
import datetime
import functools
import pathlib
import re
import typing

#: PREFIXES set contains the allowed file prefixes.
PREFIXES = {
    "AN_",  # ex: AN_20060220_138_FRA_GRU_10s_V02.txt     ; PTRMS / Acetonitril
    "Ac_",  # ex: Ac_20060220_138_FRA_GRU_10s_V02.txt     ; PTRMS / Acetone
    "C2_",  # ex: C2_20051115_131_CAN_MNL_10s_V25.txt     ; LiCor CO2
    "C2I_",  # ex: C2I_20070306_182_FRA_CAN_V11.txt
    "C4I_",  # ex: C4I_20050519_110_FRA_CAN_10s_V26.txt   ; IAGOS server summary files
    "CM_",  # ex: CM_20101020_313_FRA_CCS_10s_V02.txt
    "CN_",  # ex: CN_20050519_110_FRA_CAN_10s_V08.txt
    "CO_",  # ex: CO_20050519_110_FRA_CAN_10s_V03.txt     ; carbon monoxide continuous measurements
    "DD_",  # ex: DD_20100420_294_FRA_FRA_V13.txt
    "DIS_",  # ex: DIS_20070522_190_FRA_CAN_V01.txt
    "DN_",  # ex: DN_20051005_127_GRU_SCL_10s_V03.txt
    "DU_",  # ex: DU_20110118_325_FRA_JNB_10s_V02.txt
    "GHG_",  # ex: GHG_20051004_126_FRA_GRU_V11.txt       ; whole air sampler greenhouse gases
    "HCF_",  # ex: HCF_20180322_544_MUC_DEN_V05.txt       ; halocarbons, Uni Frankfurt
    "HG_",  # ex: HG_20050519_110_FRA_CAN_10s_V08.txt     ; mercury
    "INT_",  # ex: INT_20050519_110_FRA_CAN_V15.txt       ; additional data for whole air sampler intervals
    "INTtpc_",  # ex: INTtpc_20050519_110_FRA_CAN_V15.txt ; TPChange data for whole air sampler intervals
    "MA_",  # ex: MA_20050519_110_FRA_CAN_10s_V05.txt     ; master data
    "ME_",  # ex: ME_20140819_472_MUC_SFO_10s_V01.txt
    "MS_",  # ex: MS_20050519_110_FRA_CAN_10s_V29.txt     ; meteo summary files, 10 s bins, continuous data
    "NO_",  # ex: NO_20050519_110_FRA_CAN_10s_V03.txt     ; NO / NOy data
    "OM_",  # ex: OM_20050620_114_FRA_GRU_10s_V04.txt     ; ozone data, UV photometer
    "P2C_",  # ex: P2C_20180729_550_MUC_HND_10s_V01.txt   ; IAGOS package 2C (aerosol)
    "PIC_",  # ex: PIC_20180729_550_MUC_HND_10s_V03.txt   ; Picarro CO2, CH4, H2O
    "PTR_",  # ex: PTR_20171211_537_MUC_ICN_10s_V01.txt   ; PTRMS data (replaces AN, Ac)
    "SD_",  # ex: SD_20100420_294_FRA_FRA_10s_V14.txt
    "SP2_",  # ex: SP2_20140819_472_MUC_SFO_10s_V02.txt   ; SP2 / soot aerosol
    "SU_",  # ex: SU_20050519_110_FRA_CAN_10s_V26.txt     ; summary files, 10 s bins, continuous data
    "WA_",  # ex: WA_20050519_110_FRA_CAN_10s_V02.txt     ; water vapour, FPH and photo-acoustic
    "WAS_",  # ex: WAS_20050519_110_FRA_CAN_V11.txt       ; whole air sampler summary files (includes INT)
    "WB_",  # ex: WB_20050519_110_FRA_CAN_V02.txt         ; water vapour, frost point hygrometer (FPH)
    "WI_",  # ex: WI_20100622_297_FRA_CCS_10s_V02.txt     ; ice water
}

#: PREFIXES_NC are netCDF-specific prefixes. A prefix might be part of the
#:  'standard' PREFIXES, but doesn't have to.
PREFIXES_NC = {
    "MA_",  # MAster data, same as NASA Ames
    "MS_",  # MeteoSummary file, same as NASA Ames
    "SU_",  # SUmmary file, same as NASA Ames
    "GHG_",  # whole air sampler greenhouse gas data, same as NASA Ames
    "WSM_",  # whole air sampler merge file
}

#: EXTS set contains allowed file extension.
EXTS = {"txt", "na", "nas"}

FlightDirTuple = typing.NamedTuple(
    "result", [("flight_no", int), ("flight_date", datetime.datetime)]
)


def dissect_flightdirname(name: str, _delimiter: str = "_") -> FlightDirTuple:
    """
    Extract flight number and flight date from flight directory name.

    Parameters
    ----------
    name : str
        Name of flight directory.

    Returns
    -------
    collections.namedtuple
        ('flight_no': int, 'flight_date': datetime.datetime).
    """
    if isinstance(name, pathlib.Path):  # a fallback: name can also be a Path object
        name = name.name

    assert name.startswith("Flight"), "name must start with prefix 'Flight'"

    n_characters = len(name)
    assert (
        n_characters == 18 or n_characters == 19
    ), f"need 18 or 19 characters in flight directory name, have {n_characters:d}"

    parts = name.split(_delimiter)
    assert len(parts) == 2, f"split on {_delimiter} must result in 2 parts, have {len(parts):d}"

    parts[0] = parts[0].strip("Flight")
    assert (
        len(parts[0]) == 3 or len(parts[0]) == 4
    ), f"flight number must have 3 or 4 characters, got {len(parts[0]):d}"

    assert len(parts[1]) == 8, f"date must have 8 characters, got {len(parts[1]):d}"

    flight_no = int(parts[0])
    flight_date = datetime.datetime.strptime(parts[1], "%Y%m%d").replace(
        tzinfo=datetime.timezone.utc
    )

    return FlightDirTuple(flight_no, flight_date)


DissectedFilename = typing.NamedTuple(
    "result",
    [
        ("type", str),
        ("flight_date", datetime.datetime),
        ("flight_no", int),
        ("departure_ap", str),
        ("destination_ap", str),
        ("resolution", str),
        ("version", int),
    ],
)


def dissect_filename(
    name: typing.Union[str, pathlib.Path], _delimiter: str = "_"
) -> DissectedFilename:
    """
    Extract file parameters from CARIBIC file name.

    Parameters
    ----------
    name : str
        Name of file.

    Returns
    -------
    collections.namedtuple
         'type': str,
         'flight_date': datetime.datetime,
         'flight_no': int,
         'departure_ap': str,
         'destination_ap': str,
         'resolution': str,
         'version': int
    """
    if isinstance(name, pathlib.Path):  # a fallback: name can also be a Path object
        name = name.name

    parts = name.split(".")
    assert len(parts) == 2, "name must be in the form 'filename.extension'"
    assert parts[-1] in EXTS, f"filename ends with '{parts[-1]}' but must end with {EXTS}"

    parts = parts[0].split(_delimiter)
    assert (
        len(parts) == 6 or len(parts) == 7
    ), f"name must contain 6 or 7 parts, have {len(parts):d}"
    assert (
        parts[0] + "_" in PREFIXES
    ), f"filename starts with '{parts[0]}_' but must start with {PREFIXES}"

    flight_date = datetime.datetime.strptime(parts[1], "%Y%m%d").replace(
        tzinfo=datetime.timezone.utc
    )
    flight_no = int(parts[2])
    resolution = "10s" if parts[5] == "10s" else "full"
    vers = int(parts[-1].strip("V").strip("v"))

    return DissectedFilename(parts[0], flight_date, flight_no, parts[3], parts[4], resolution, vers)


# since this function could be called often, it can be useful to cache results:
@functools.lru_cache()
def get_all_flightdirs(flights_dir: pathlib.Path) -> list:
    """
    Collect all directories with flight data on the CARIBIC server.

    Parameters
    ----------
    flights_dir : pathlib.Path
        Directory with subdirectories that contain the data for each flight.

    Returns
    -------
    list
        List with paths to flight directories in ascending order.
    """
    if isinstance(flights_dir, str):  # fall-back: path might be string
        flights_dir = pathlib.Path(flights_dir)

    return sorted(d for d in flights_dir.glob("Flight*") if d.is_dir())


def flight_no_to_flightdir(flight_no: int, flights_dir: pathlib.Path) -> pathlib.Path:
    """
    Get directory with data for given flight number.

    Parameters
    ----------
    flight_no : int
        Flight number.
    flights_dir : pathlib.Path
        Directory with subdirectories that contain the data for each flight..

    Raises
    ------
    FileNotFoundError
        No directory found for specified flight number.

    Returns
    -------
    pathlib.Path
        Path to directory with flight data.
    """
    if isinstance(flights_dir, str):  # fall-back: path might be string
        flights_dir = pathlib.Path(flights_dir)

    result = sorted(d for d in flights_dir.glob(f"Flight{flight_no:03d}_*") if d.is_dir())
    if not result:
        raise FileNotFoundError(f"no directory found for flight no. {flight_no}")

    return result[0]


def flight_no_to_modeldir(flight_no: int, model_dir: pathlib.Path) -> pathlib.Path:
    """
    Get directory with model data for given flight number.

    Parameters
    ----------
    flight_no : int
        Flight number.
    model_dir : pathlib.Path
        Directory with subdirectories that contain the data for each flight..

    Raises
    ------
    FileNotFoundError
        No directory found for specified flight number.

    Returns
    -------
    pathlib.Path
        Path to directory with flight data.
    """
    if isinstance(model_dir, str):  # fall-back: path might be string
        model_dir = pathlib.Path(model_dir)

    result = sorted(d for d in model_dir.glob(f"*_{flight_no:03d}") if d.is_dir())
    if not result:
        raise FileNotFoundError(f"no directory found for flight no. {flight_no}")

    return result[0]


def find_files(
    flight_no: int,
    prfx: str,
    flights_dir: pathlib.Path,
    file_dir: pathlib.Path = pathlib.Path(""),
    resolution: str = "all",
    raise_filenotfound: bool = False,
    ext: str = "txt",
    caribic: int = 2,
) -> list:
    """
    Find NASA Ames file in flights directory (CARIBIC server folder structure).

    Parameters
    ----------
    flight_no : int
        Flight number.
    prfx : str
        File name prefix specifying the data origin, e.g. 'MA' for master data.
    flights_dir : pathlib.Path
        Directory with subdirectories containing flight data.
    file_dir : pathlib.Path, optional
        Specific directory where to look for NASA Ames file(s). Overrides lookup in flights_dir! The default is "". The default is pathlib.Path("").
    resolution : str, optional
        Data resolution. '10s' - 10s binned data, 'highest' - max. available resolution, 'all' - all availlable. The default is 'all'.
    raise_filenotfound : bool, optional
        Raise an error if no matching data is found. The default is False.
    ext : str, optional
        File name extension, to be supplied without the dot. The default is "txt".
    caribic : int, optional
        Use CARIBIC-1 or CARIBIC-2 file pattern. The default is 2.

    Raises
    ------
    FileNotFoundError
        No matching data found.

    Returns
    -------
    list
        List with paths to all files that match the search criteria.
    """
    if resolution not in ("10s", "highest", "all"):
        raise ValueError(
            f"got keyword resolution='{resolution}', but must be '10s', 'highest' or 'all'"
        )

    if isinstance(flights_dir, str):  # fall-back: path might be string
        flights_dir = pathlib.Path(flights_dir)
    if isinstance(file_dir, str):  # fall-back: path might be string
        file_dir = pathlib.Path(file_dir)

    # if empty string was supplied to file_dir, its .name is empty.
    if not file_dir.name:
        try:  # we need to catch a possible exception raised by flight_no_to_flightdir
            file_dir = flight_no_to_flightdir(flight_no, flights_dir)
        except FileNotFoundError as err:
            if raise_filenotfound:
                raise err
            return []

    pattern_full = rf"{prfx}_[0-9]{{8}}_{flight_no:03d}_[A-Z]{{3}}_[A-Z]{{3}}_V[0-9]{{2}}\.{ext}"
    pattern_10s = rf"{prfx}_[0-9]{{8}}_{flight_no:03d}_[A-Z]{{3}}_[A-Z]{{3}}_10s_V[0-9]{{2}}\.{ext}"

    if caribic == 1:
        # flight number does not always have 3 digits for caribic 1
        # 'full' should not contain "10s" substring
        pattern_full = rf"^(?!.*10s){prfx}_[0-9]{{8}}_0?{flight_no:02d}.*\.{ext}"
        # '10s' must contain "10s" substring, with an optional two characters"MV" after it
        pattern_10s = rf"{prfx}_[0-9]{{8}}_0?{flight_no:02d}_[A-Z]{{3}}_[A-Z]{{3}}_10s(.{{2}})?_v[0-9]{{2}}\.{ext}"

    files = []
    if resolution == "10s":
        files = [f for f in file_dir.glob("*") if re.match(pattern_10s, f.name, re.IGNORECASE)]

    if resolution == "highest":
        files = [f for f in file_dir.glob("*") if re.match(pattern_full, f.name, re.IGNORECASE)]
        if not files:  # if 'highest' yields no result, there still might be a 10s file
            files = [f for f in file_dir.glob("*") if re.match(pattern_10s, f.name, re.IGNORECASE)]

    if resolution == "all":
        files = [
            f
            for f in file_dir.glob("*")
            if re.match(pattern_full, f.name, re.IGNORECASE)
            or re.match(pattern_10s, f.name, re.IGNORECASE)
        ]

    if not files and raise_filenotfound:
        raise FileNotFoundError(f"nothing found for prefix {prfx} and flight {flight_no}")

    return files


def find_flights_range(lower: int, upper: int, flights_dir: pathlib.Path) -> list:
    """
    Find flight data directories given a range of flight numbers, both included.

    Parameters
    ----------
    lower : int
        Lower limit flight number.
    upper : int
        Upper limit flight number.
    flights_dir : pathlib.Path
        Path where to look for flight sub-directories.

    Raises
    ------
    ValueError
        Lower limit must be <= upper limit.

    Returns
    -------
    list
        List with paths to flight directories.

    """
    if lower > upper:
        raise ValueError(f"lower limit must be <= upper limit, have {lower} > {upper}")

    if isinstance(flights_dir, str):
        flights_dir = pathlib.Path(flights_dir)

    allflightdirs = get_all_flightdirs(flights_dir)

    result = (
        d
        for d in allflightdirs
        if dissect_flightdirname(d.name).flight_no >= lower
        and dissect_flightdirname(d.name).flight_no <= upper
    )

    return sorted(result)


def find_flights_daterange(
    lower: datetime.datetime, upper: datetime.datetime, flights_dir: pathlib.Path
) -> list:
    """
    Find flight data directories given a range of dates, both included.

    Parameters
    ----------
    lower : datetime.datetime
        Lower limit flight date.
    upper : datetime.datetime
        Upper limit flight date.
    flights_dir : pathlib.Path
        Path where to look for flight sub-directories.

    Raises
    ------
    ValueError
        Path where to look for flight sub-directories.

    Returns
    -------
    list
        List with paths to flight directories.

    """
    if lower > upper:
        raise ValueError(f"lower limit must be <= upper limit, have {lower} > {upper}")

    if lower.tzinfo is None:
        lower = lower.replace(tzinfo=datetime.timezone.utc)
    if upper.tzinfo is None:
        upper = upper.replace(tzinfo=datetime.timezone.utc)

    if isinstance(flights_dir, str):
        flights_dir = pathlib.Path(flights_dir)

    allflightdirs = get_all_flightdirs(flights_dir)

    result = (
        d
        for d in allflightdirs
        if dissect_flightdirname(d.name).flight_date >= lower
        and dissect_flightdirname(d.name).flight_date <= upper
    )

    return sorted(result)


def gather_data(
    prefixes: list,
    flights_dir: pathlib.Path,
    flightrange: tuple = (),
    daterange: tuple = (),
    resolution: str = "all",
    ext: str = "txt",
    caribic: int = 2,
) -> list:
    """
    Gather paths to all files matching the supplied prefixes and flight number range and/or flight date range.

    Parameters
    ----------
    prefixes : list
        List of file prefixes to look for. See carfileutils.PREFIXES.
    flights_dir : pathlib.Path
        Path where to look for flight sub-directories.
    flightrange : tuple, optional
        Range of flights to search. The default is ().
    daterange : tuple, optional
        Range of flight dates to search. The default is ().
    resolution : str, optional
        Data resolution. '10s' - 10s binned data, 'highest' - max. available resolution, 'all' - all availlable. The default is 'all'.
    ext : str, optional
        File name extension, to be supplied without the dot. The default is "txt".
    caribic : int, optional
        Mission number, [1..2]

    Returns
    -------
    list
        List with paths to all files that match the search criteria.
    """
    if not flightrange and not daterange:  # if neither is provided, use all flights available
        candidates = get_all_flightdirs(flights_dir)
    else:
        if not flightrange:  # no flight range, so all directories could have suitable data
            by_flightrange = set(get_all_flightdirs(flights_dir))
        else:
            by_flightrange = set(find_flights_range(flightrange[0], flightrange[1], flights_dir))
        if not daterange:  # no date range, so all directories could have suitable data
            by_daterange = set(get_all_flightdirs(flights_dir))
        else:
            by_daterange = set(find_flights_daterange(daterange[0], daterange[1], flights_dir))
        # candidates are directories which are selected by both flight number and date,
        # so we can use the intersection of those sets.
        candidates = by_flightrange.intersection(by_daterange)

    result = []

    for directory in candidates:
        flight_no = dissect_flightdirname(directory).flight_no
        for prfx in prefixes:
            have = find_files(
                flight_no,
                prfx,
                flights_dir=pathlib.Path(),
                file_dir=directory,
                resolution=resolution,
                ext=ext,
                caribic=caribic,
            )
            if have:
                result += have

    return result


def increment_version(
    filename: typing.Union[str, pathlib.Path], increment_by: int = 1, specify_versnum: int = -1
) -> str:
    """
    Increment the version tag in a CARIBIC filename. Allowed range is [0..99].

    Parameters
    ----------
    filename : str
        name of the file.
    increment_by : int, optional
        How many versions to bump up. The default is 1.
    specify_versnum : int, optional
        If greater than -1, overrides default bumping and just sets specified version. The default is -1.

    Raises
    ------
    ValueError
        New version number would be out of range [0..99].

    Returns
    -------
    str
        filename with updated version tag.
    """
    if isinstance(filename, pathlib.Path):  # a fallback: name can also be a Path object
        filename = filename.name

    components = dissect_filename(filename)
    new_version = components.version + increment_by if specify_versnum < 0 else specify_versnum

    if new_version < 0 or new_version > 99:
        raise ValueError(
            f"New version number would be {new_version}, which is out of allowed range [0..99]"
        )

    return (
        "_".join(
            filter(
                None,
                (
                    components.type,
                    components.flight_date.strftime("%Y%m%d"),
                    f"{components.flight_no:03d}",
                    components.departure_ap,
                    components.destination_ap,
                    "10s" if components.resolution == "10s" else None,
                    f"V{new_version:02d}",
                ),
            )
        )
        + "."
        + filename.split(".")[-1]
    )
