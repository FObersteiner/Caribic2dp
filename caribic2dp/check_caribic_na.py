# -*- coding: utf-8 -*-
"""Check CARIBIC NASA Ames FFI 1001 format."""
import pathlib
import re
import shutil
import typing
import warnings
from datetime import datetime, timezone

import numpy as np
from pyfuppes.na1001 import FFI1001
from pyfuppes.txt2dict import txt_2_dict as t2d

from caribic2dp.carfileutils import dissect_filename, flight_no_to_flightdir

try:
    import tomllib as toml  # Python 3.11
except ImportError:
    # https://github.com/hukkin/tomli
    import tomli as toml  # let's assume this works the same as the 3.11 lib...

try:
    from .utils.cfnames_loader import load_names
except ImportError:  # relative import might not work...
    from utils.cfnames_loader import load_names

# cfunits can be a bit tricky to set up, or might not work in all IDE configurations...
_override_cf = False
try:
    from cfunits import Units
except AssertionError:
    warnings.warn("failed to load cfunits package; CF checks are disabled")
    # If importing the package fails, this disables CF checks:
    _override_cf = True

try:
    wd = pathlib.Path(__file__).parent
except NameError:
    wd = pathlib.Path.cwd()
assert wd.is_dir(), "faild to obtain working directory"

# -----------------------------------------------------------------------------

# CARIBIC defaults
VNAME_SEP = "; "
COL_SEP = "\t"

# CF Standard names reference
CFSTDNAMES_URL = (
    "http://cfconventions.org/Data/cf-standard-names/current/src/cf-standard-name-table.xml"
)
# TODO : collect some version of that xml and use it if server is not reachable
cf_names = load_names(CFSTDNAMES_URL)

# Header defaults
Header_Defaults = {}
with open((wd / "../config/NA_header_cfg.toml").resolve(), "rb") as fp:
    Header_Defaults = toml.load(fp)


def _cmp(have, want, msg, ok, tag="", regex=False):
    """Help validate function to compare strings in file header."""
    if regex:
        if not re.match(want, have):
            msg += f"\n    nok {tag}:\n        have '{have}'\n        want '{want}'"
            ok = False
    else:
        if have != want:
            msg += f"\n    nok {tag}:\n        have '{have}'\n        want '{want}'"
            ok = False
    return msg, ok


def _cfunit_valid(unit: str) -> bool:
    """Help validate a CF unit."""
    return Units(unit).isvalid


def _cfname_valid(name: str) -> bool:
    """Help validate a CF name"""
    return cf_names.get(name) is not None


CheckResult = typing.NamedTuple("result", [("msg", str), ("na1001", typing.Optional[FFI1001])])


def validate(
    filepath: pathlib.Path,
    check_caribic2: bool = True,
    airports_file: pathlib.Path = pathlib.Path(),
    check_changelog: bool = False,
    cf_checks: bool = False,
) -> CheckResult:
    """
    Validate that file fulfills CARIBIC NASA Ames convention.

    Parameters
    ----------
    filepath : pathlib.Path
        Path to file to check.
    check_caribic2 : bool, optional
        Check CARIBIC format specific properties. The default is True.
    airports_file : pathlib.Path, optional
        Path to CARIBIC airports config file. The default is pathlib.Path().
    check_changelog : bool, optional
        analyse the "ChangeLog" line if there is an entry for each file-version.
        The default is False.
    cf_checks : bool, optional
        Enable/disable CF convention - specific tests. The default is False.

    Raises
    ------
    ValueError
        if CARIBIC checks are enabled but no CARIBIC airports config file path provided.

    Returns
    -------
    namedtuple
        .msg field: result message. .na field: NASA Ames FFI 1001 object.
    """
    if _override_cf and cf_checks:
        warnings.warn("cannot run CF checks since module cfunits could not be imported")
        cf_checks = False

    if check_caribic2 and airports_file == pathlib.Path():
        raise ValueError(
            "check CARIBIC-2 is enabled; must provide path to CARIBIC airports config file"
        )

    if not isinstance(filepath, pathlib.Path):
        filepath = pathlib.Path(filepath)

    # I) file can be loaded to pyFuppes.na1001.FFI1001 class.
    # FFI1001 has a bunch of assertions which will abort the loading process.
    # If an assertion error occurs, return an ERROR message.
    try:
        na = FFI1001(filepath, allow_emtpy_data=False)
    except Exception as e:
        return CheckResult(
            f"check {filepath.name}: ERROR {e} (Further errors potentially skipped)",
            None,
        )

    # if not additional constraints are supplied, we can return early
    if not check_caribic2:
        return CheckResult(
            f"check {filepath.name}: OK (no CARIBIC-specific constraints checked)", na
        )

    # II) FFI1001 attributes fulfill CARIBIC format conventions.
    msg, ok = f"check {filepath.name}:", True

    # TODO : check file name; must have e.g. zero-padded (02d) version number

    # MNAME
    have, want = (
        na.MNAME,
        Header_Defaults["MNAME"],
    )
    msg, ok = _cmp(have, want, msg, ok, tag="MNAME")

    # XNAME
    have, want = (
        na.XNAME,
        Header_Defaults["XNAME"],
    )
    msg, ok = _cmp(have, want, msg, ok, tag="XNAME")

    # VNAME
    vname_ok = True
    for idx, name in enumerate(na.VNAME):
        name_stem = name.split("#")[0].strip()  # remove possible comment at the end of the line
        parts = name_stem.split(VNAME_SEP)
        if len(parts) != 3:
            ok, vname_ok = False, False
            msg += f"\n    nok VNAME {idx+1}:\n        have '{name}'\n        could not split on '{VNAME_SEP}' into 3 parts after removing possible comment at the end of the line (# ...)"
        elif not parts[-1].startswith("[") or not parts[-1].endswith("]"):
            ok, vname_ok = False, False
            msg += f"\n    nok VNAME {idx+1}:\n        have last part '{parts[-1]}'\n        last part must resemble '[*]'"
        if cf_checks and len(parts) == 3:
            have = parts[-1].strip("[]")
            if not _cfunit_valid(have):
                ok = False
                msg += (
                    f"\n    nok unit for variable {idx+1}:\n        '{have}' is not a valid CF unit"
                )

        # TODO : add Caribic-2 specific check; if vname exists in definition file

    # SCOM
    have, want = (
        na.SCOM[0],
        r"FileFormatVersionInfo: CARIBIC_NAmes_v[0-9]{2} standard # This file was created according to the conventions for CARIBIC NASA Ames data files, given by 'Info_Nasa_Ames_Format_[0-9]{2}.zip'.",
    )
    msg, ok = _cmp(have, want, msg, ok, tag="SCOM", regex=True)

    req_labels, labels_ok = {
        label: -1
        for label in [
            "FlightNo",
            "FlightRoute",
            "DepartureAirport",
            "ArrivalAirport",
            "FileName",
        ]
    }, True
    for idx, line in enumerate(na.SCOM):
        start = line.split(":")[0]
        if req_labels.get(start):
            req_labels[start] = idx
    missing = [k for k, v in req_labels.items() if v < 0]
    if len(missing) != 0:
        ok, labels_ok = False, False
        msg += f"\n    nok SCOM:\n        missing labels {missing}"

    ## ignore (in)valid in file name, used in testing
    fname, fname_components = re.sub("_[in]*valid[0-9]*", "", filepath.name), None
    try:
        fname_components = dissect_filename(fname)
    except Exception as e:
        ok = False
        msg += f"\n    nok filename:\n        '{fname}'\n        got '{e}'"

    # the next checks can only be done if we could dissect the file name successfully,
    # and all the labels were found
    if fname_components and labels_ok:
        have, want = fname_components.flight_date, datetime(*na.DATE, tzinfo=timezone.utc)
        if have != want:
            ok = False
            msg += f"\n    nok DATE:\n        filename: '{str(have.date())}'\n        NAmes header: '{str(want.date())}'"

        try:
            have = int(na.SCOM[req_labels["FlightNo"]].split(":")[-1].split("#")[0].strip())
        except ValueError:
            have = na.SCOM[req_labels["FlightNo"]].split(":")[-1].split("#")[0].strip()
        want = fname_components.flight_no
        msg, ok = _cmp(have, want, msg, ok, tag="label 'FlightNo'")

        have = na.SCOM[req_labels["FlightRoute"]].split(":")[-1].split("#")[0].strip()
        want = " ".join((fname_components.departure_ap, fname_components.destination_ap))
        if have != want:
            ok = False
            msg += f"\n    nok FlightRoute:\n        filename: '{want}'\n        NAmes header: '{have}'"

        airports = t2d(airports_file).data
        dep_ap, dest_ap = have.split(" ")
        dep_ap_idx = airports["IATA"].index(dep_ap) if dep_ap in airports["IATA"] else -1
        dest_ap_idx = airports["IATA"].index(dest_ap) if dest_ap in airports["IATA"] else -1
        do_ap_checks = True
        if dep_ap_idx == -1 or dest_ap_idx == -1:
            do_ap_checks, ok = False, False
            msg += f"\n    nok Airports:\n        please check: '{have}'"

        if do_ap_checks:
            have = na.SCOM[req_labels["DepartureAirport"]].split(":")[-1].split("#")[0].strip()
            want = airports["Name"][dep_ap_idx].split(",")[0]
            if not have.startswith(want):
                ok = False
                msg += f"\n    nok DepartureAirport:\n        have: '{have}'\n        but IATA code '{dep_ap}' is: '{want}'"
            have = na.SCOM[req_labels["ArrivalAirport"]].split(":")[-1].split("#")[0].strip()
            want = airports["Name"][dest_ap_idx].split(",")[0]
            if not have.startswith(want):
                ok = False
                msg += f"\n    nok ArrivalAirport:\n        have: '{have}'\n        but IATA code '{dest_ap}' is: '{want}'"

        have = na.SCOM[req_labels["FileName"]].split(":")[-1].split("#")[0].strip()
        msg, ok = _cmp(have, fname, msg, ok, tag="FileName, header vs. actual")

    opt_labels, labels_ok = {
        label: -1
        for label in [  # TODO:
            "ExceptionToConsider",  # actually, this is not optional, as it is checked in the NCOM section
            "ChangeLog",  # this might as well be a required tag
        ]
    }, True

    for idx, line in enumerate(na.SCOM):
        start = line.split(":")[0]
        if opt_labels.get(start):
            opt_labels[start] = idx
    missing = [k for k, v in opt_labels.items() if v < 0]
    if len(missing) != 0:
        ok, labels_ok = False, False
        msg += f"\n    SCOM:\n        missing optional labels {missing}"
    if fname_components and labels_ok and check_changelog:
        have = na.SCOM[opt_labels["ChangeLog"]]
        if fname_components.version > 1 and have == "ChangeLog: 0":
            ok = False
            msg += f"\n    nok ChangeLog:\n        have: '{have}', but filename specifies version '{fname_components.version}'"

    # NCOM - last lines
    further_test_ncom = True
    if len(na.NCOM) < 8:
        ok, further_test_ncom = False, False
        msg += "\n    nok NCOM:\n        needs at least 8 lines to include recommended information"

    # if NCOM is too short, skip the following tests (further_test_ncom == False)
    if further_test_ncom:
        # info string
        have = na.NCOM[-8]
        want = Header_Defaults[
            "NCOM_CFinfo"
        ]  # "The following two lines contain standard names and units according to the 'NetCDF Climate and Forecast (CF) Metadata Convention', see http://cfconventions.org/. Each entry (name or unit) corresponds to a certain column; the entry before the first tab belongs to the independent column."
        msg, ok = _cmp(have, want, msg, ok, tag="NCOM")
        have = na.NCOM[-5]
        want = Header_Defaults[
            "NCOM_final_words"
        ]  # "The following four lines contain: Start date like in line 7 (formatted as YYYYMMDD), followed by the scaling factors like in line 11; Line number containing the KeyLabel 'ExceptionToConsider' (0 if no such exception is mentioned), followed by the NaN-values like in line 12; Units like in lines 13ff; Column headers."
        msg, ok = _cmp(have, want, msg, ok, tag="NCOM")

        # validate CF_StandardNames
        if na.NCOM[-7].startswith("CF_StandardNames:"):
            names = [n.strip() for n in na.NCOM[-7].replace("CF_StandardNames:", "").split(COL_SEP)]
            if len(names) != na.NV + 1:
                ok = False
                msg += f"\n    nok CF_StandardNames:\n        have {len(names)}, want {na.NV+1}"
            elif len(names) == na.NV + 1 and cf_checks:
                for idx, n in enumerate(names):
                    if not _cfname_valid(n):
                        ok = False
                        msg += f"\n    warning: CF name for variable {idx+1}:\n        '{n}' is not a CF standard name"
        else:
            ok = False
            msg += f"\n    nok CF name tag; want 'CF_StandardNames:',\n        have '{na.NCOM[-7]}'"

        # validate CF_Units
        if na.NCOM[-6].startswith("CF_Units:"):
            units = [u.strip() for u in na.NCOM[-6].replace("CF_Units:", "").split(COL_SEP)]
            if len(units) != na.NV + 1:
                ok = False
                msg += f"\n    nok CF_Units:\n        have {len(units)}, want {na.NV+1}"
            elif len(units) == na.NV + 1 and cf_checks:
                for idx, u in enumerate(units):
                    if not _cfunit_valid(u.strip("[]")):
                        ok = False
                        msg += f"\n    nok CF unit for variable {idx+1}:\n        '{u}' is not a valid CF unit"
        else:
            ok = False
            msg += f"\n    nok CF name tag; want 'CF_StandardNames:',\n        have '{na.NCOM[-7]}'"

        # verify start date and vscale , na.NCOM[-4]
        want = "".join(f"{i:02d}" for i in na.DATE) + COL_SEP + COL_SEP.join(map(str, na.VSCAL))
        if want != na.NCOM[-4]:
            ok = False
            msg += f"\n    nok NCOM date/vscal line; want '{want}',\n        have '{na.NCOM[-4]}'"

        # verify exceptions line no. and vmiss , na.NCOM[-3]
        except_line = opt_labels["ExceptionToConsider"]
        if not except_line:
            ok = False
            msg += "\n    nok NCOM cannot check ExceptionToConsider as not defined in SCOM"
        else:
            prefix = "0"
            if len(na.SCOM[except_line]) > 23:
                prefix = str(na.NLHEAD - na.NNCOML - na.NSCOML + except_line)
            want = prefix + COL_SEP + COL_SEP.join(map(str, na.VMISS))
            if na.NCOM[-3] != want:
                ok = False
                msg += f"\n    nok NCOM exceptions/vmiss line; want '{want}',\n        have '{na.NCOM[-3]}'"

        # verify units as in vname , na.NCOM[-2]
        parts = na.NCOM[-2].split(COL_SEP)
        if len(parts) != na.NV + 1:
            ok = False
            msg += f"\n    nok NCOM units line; want {na.NV+1} components, have {len(parts)}"
        elif (
            vname_ok
        ):  # if VNAMES weren't specified correctly, we cannot determine what NCOM[-2] should look like
            want = (
                na.XNAME.split(VNAME_SEP)[2]
                + COL_SEP
                + COL_SEP.join(name.split(VNAME_SEP)[2].split("#")[0].strip() for name in na.VNAME)
            )
            # units_as_vname = True
            if na.NCOM[-2] != want:  ## equal to VNAME
                ok = False  # units_as_vname = False
                msg += f"\n    nok NCOM units line; want '{want}',\n        have '{na.NCOM[-2]}'"
            ### skip cf checks here, as this is already covered in the VNAME section

        # verify col header , na.NCOM[-1]
        if vname_ok:
            want = (
                na.XNAME.split(VNAME_SEP)[0]
                + COL_SEP
                + COL_SEP.join(name.split(VNAME_SEP)[0] for name in na.VNAME)
            )
            if na.NCOM[-1] != want:
                ok = False
                msg += f"\n    nok NCOM column header line; want '{want}',\n        have '{na.NCOM[-1]}'"

    # TODO : add check for variables, e.g.
    #   all < VMISS if VMISS is positive
    #   10s files: bins correctly set (t % 5 == 0; t % 10 != 0)

    if ok:  # if everything went well so far
        msg += " OK"

    return CheckResult(msg, na)


# ------------------------------------------------------------------------------


def revise_header(
    src: pathlib.Path,
    dst: pathlib.Path,
    cfg_path: pathlib.Path,
    frange: tuple,
    overwrite=True,
    prefix=False,
    copy_wo_changes=False,
    backup_copy_dst=False,
    sepd="\t",
    _suffix=(".txt", ".na", ".ames"),
):
    r"""
    Revise the header section of a NASA Ames 1001 file to fulfill CARIBIC convention.

    Parameters
    ----------
    src : pathlib.Path
        source directory, where to look for files to revies.
    dst : pathlib.Path
        destination directory, where to dump revised data.
    cfg_path : pathlib.Path
        path to config file that specifies defaults for MNAME etc.
    frange : tuple
        (first_flight_number, last_flight_number).
    overwrite : bool, optional
        Overwrite data if exists in dst directory. The default is True.
    prefix : str, optional
        A specific file prefix to check. The default is False.
    copy_wo_changes : bool, optional
        Just check files, but copy without changes from src to dst. The default is False.
    backup_copy_dst : bool or pathlib.Path, optional
        Make a backup copy of the un-modified file to backup_copy_dst. The default is False.
    sepd : str, optional
        data section / column separator. The default is "\t".

    Raises
    ------
    ValueError
        ...if relevant info not found in header.

    Returns
    -------
    na_1001 : pyfuppes.na1001.FFI1001
        FFI1001 object with revised data.
    """
    with open(cfg_path, "rb") as fp:
        cfg = toml.load(fp)

    ### header lines that should be equal for all caribic 2 nasa ames files:
    DEF_MNAME = cfg["MNAME"]
    DEF_XNAME = cfg["XNAME"]
    DEF_CFINFO = cfg["NCOM_CFinfo"]
    DEF_NCOMCDESC = cfg["NCOM_final_words"]

    ### begin check...
    PREFIX = prefix if prefix else ""
    SUFFIX = _suffix
    PATTERNS = [PREFIX + "*" + s for s in SUFFIX]

    print("collecting files to check...")
    chk_files = []
    if src:
        search_dir = src
    for fno in range(frange[0], frange[1] + 1):
        if not src:
            search_dir = flight_no_to_flightdir(fno)
        chk_files += [src for p in PATTERNS for src in search_dir.glob(p) if f"_{fno}_" in src.name]

    na_1001 = None

    if chk_files:
        for f in chk_files:
            print(f"\nchecking: {str(f)}")
            write = 0
            na_1001 = FFI1001(f, sep_data=sepd)

            if na_1001.XNAME != DEF_XNAME:
                print(f"updating xname in {f.name}")
                na_1001.XNAME = DEF_XNAME
                write = 1

            for idx, col in enumerate(na_1001.V):
                try:
                    _ = [float(v) for v in col]
                except ValueError:
                    print(f"{f}\n --> non-numeric value in {na_1001.VNAME[idx]}!")

            # check na 1001 DX:
            dt = np.unique(np.diff(np.array(na_1001.X, dtype=float)))
            dx = 0 if dt.size != 1 else dt[0]
            if not np.isclose(na_1001.DX, dx):
                write = 1
                print(f"updating DX in {f.name}")
                na_1001.DX = dx

            if na_1001.MNAME != DEF_MNAME:
                write = 1
                print(f"updating mission name in {f.name}")
                na_1001.MNAME = DEF_MNAME

            idx_DEF_CFINFO, idx_DEF_NCOMCDESC = None, None
            for ix, line in enumerate(na_1001.NCOM):
                if line.startswith("The following two lines contain standard names and units"):
                    idx_DEF_CFINFO = ix
                if line.startswith("The following four lines contain:"):
                    idx_DEF_NCOMCDESC = ix

            if idx_DEF_CFINFO is None or idx_DEF_NCOMCDESC is None:
                raise ValueError(
                    f"{f.name}:\nlines not found, idx_DEF_CFINFO is {idx_DEF_CFINFO}, idx_DEF_NCOMCDESC is {idx_DEF_NCOMCDESC}"
                )

            if "FileName" in na_1001.SCOM[5] and f.name not in na_1001.SCOM[5]:
                na_1001.SCOM[5] = f"FileName: {f.name}"
                print(f"updating FileName in {f.name}")
                write = 1

            if na_1001.NCOM[idx_DEF_CFINFO] != DEF_CFINFO:
                print(f"updating CF info in {f.name}")
                write = 1
                na_1001.NCOM[idx_DEF_CFINFO] = DEF_CFINFO

            if na_1001.NCOM[idx_DEF_NCOMCDESC] != DEF_NCOMCDESC:
                print(f"updating column description in {f.name}")
                write = 1
                na_1001.NCOM[idx_DEF_NCOMCDESC] = DEF_NCOMCDESC

            if write:
                if backup_copy_dst:
                    shutil.copy(f, backup_copy_dst / f.name)

                # adjust revision date:
                now_utc = datetime.now(tz=timezone.utc)
                na_1001.RDATE = [now_utc.year, now_utc.month, now_utc.day]

                io = na_1001.to_file(
                    dst / f.name,
                    # sep=" ",
                    # sep_com=";",
                    sep_data="\t",
                    overwrite=overwrite,
                )
                if io:
                    print(f"wrote: {str(dst/f.parent.name/f.name)}")
                else:
                    print(f" ! FAILED TO WRITE: {str(dst/f.parent.name/f.name)}")
            else:
                if copy_wo_changes:
                    shutil.copy(f, dst / f.name)
                    print(f"copied w/o changes: {str(dst/f.parent.name/f.name)}")
                else:
                    print(f"skipped, no changes: {f.name}")
    else:
        print("no files selected.")
        return None

    print("\ndone.")
    return na_1001
