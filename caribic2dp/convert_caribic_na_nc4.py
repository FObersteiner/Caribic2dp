# -*- coding: utf-8 -*-
#
# ~~~ na_to_nc4 ~~~
# => CARIBIC_NC4_FORMAT_VERS changelog <=
# 0.6, xxxx-xx-xx
#   - ?
# 0.5, xxxx-xx-xx
#   - add geospatial bounding-box
# 0.4, 2023-10-30
#   - change time long name to "CARIBIC master computer time, UTC"
# 0.3, 2023-09-26
#   - remove "reference_time" coordinate as this might be misleading
#   - add lat/lon/alt coordinates
#   - add contributors field
# 0.2, 2023-09-25
#   - add license URL
# 0.1, 2023-07-26
#   - transfer from Armin's converter to this Python code
#
# ------------------------------------------------------------------------------
"""Convert between CARIBIC NASA Ames FFI 1001 and netCDF4."""
import pathlib
import warnings
from importlib.metadata import version
from io import StringIO
from typing import Optional, Union

import numpy as np
import pandas as pd
import xarray as xr
from pyfuppes.na1001 import FFI1001
from pyfuppes.numberstring import NumStr
from pyfuppes.timeconversion import xrtime_to_mdns

try:
    wd = pathlib.Path(__file__).parent
except NameError:
    wd = pathlib.Path.cwd()
assert wd.is_dir(), "faild to obtain working directory"

# ------------------------------------------------------------------------------

CARIBIC_NC4_FORMAT_DATE = "2024-01-11"
CARIBIC_NC4_FORMAT_VERS = "0.6"
CARIBIC_NC4_CFCONVENTION = "CF-1.10"
SW_SPEC = f"caribic2dp {version('caribic2dp')}, https://gitlab.kit.edu/FObersteiner/Caribic2dp"

# data license
LICENSE = "https://creativecommons.org/licenses/by/4.0/deed.en"

CARIBIC_IVAR = "TimeCRef"  # CARIBIC-2 only
CARIBIC_IVAR_long = "CARIBIC master computer time, UTC"

CARIBIC_DVAR_lat = "PosLat"
CARIBIC_DVAR_lon = "PosLong"
CARIBIC_DVAR_presAlt = "BaroAltit"

# ------------------------------------------------------------------------------


def na_to_nc4(
    src: pathlib.Path,
    dst: pathlib.Path,
    params_file: pathlib.Path,
    params: str = "__all__",
    write_to_disk: bool = True,
    use_float32: bool = True,
    doi: Optional[str] = None,
    contributors: Optional[str] = None,
    _software_spec: str = SW_SPEC,
    _warnings_enabled: bool = True,
    _license=LICENSE,
) -> xr.Dataset:
    """
    Convert CARIBIC-2 NASA Ames 1001 to netCDF4.

    Parameters
    ----------
    src : str or pathlib.Path
        path to input file.
    dst : str or pathlib.Path
        path to output file.
    params_file : pathlib.Path
        path to CARIBIC parameters config file.
    params : str, optional
        parameters to include in the netCDF output. The default is "all". To
        specify multiple parameters, separate them e.g. by a space;
        'param1 param2 param3'.
    write_to_disk : bool, optional. The default is True.
        if set False, only the xarray.DataSet is returned.
    use_float32 : bool, optional: The default is True.
        if set False, uses 64 bit floating point precision.

    Raises
    ------
    ValueError
        path to CARIBIC parameters config file must be provided.
    KeyError
        missing / invalid parameters.

    Returns
    -------
    ds : xarray.Dataset
        a dataset with the input NASA Ames file content.
    """

    specs = pd.read_csv(params_file, sep=";").set_index("VNAME")

    src, dst = pathlib.Path(src), pathlib.Path(dst)

    # we need a valid caribic nasa ames format
    na = FFI1001(src)
    vnames = [name.split(";")[0] for name in na.VNAME]

    dvars = na.to_dict_nparray(vdtype=np.float32 if use_float32 else np.float64)
    dvars.pop(CARIBIC_IVAR)  # we just want dependent variables, so pop ivar

    # export all variables or only export selected params
    params_to_process = list(dvars.keys()) if params == "__all__" else params.split()

    # CARIBIC-2 has some special time columns...
    special_units = {
        "CRefTime": "days since 1899-12-30",
        "UTC_ARINC": "days since 1899-12-30",
        "LocalTime": "days since " + pd.Timestamp(*na.DATE).strftime("%Y-%m-%d"),  # type: ignore
    }

    coords = {
        "time": pd.Timestamp(*na.DATE)
        + pd.to_timedelta(
            np.array(na.X, float), unit="s"
        ),  # NOTE : xarray does not want to handle time zones, so do not set UTC explicitly here
    }

    # verify specified parameters exist in data
    param_index = {}
    for p in params_to_process:
        if p not in dvars:
            raise KeyError(f"could not find parameter '{p}' in data")
        try:
            param_index[p] = specs.index.get_loc(p)
        except KeyError:
            raise KeyError(f"could not find parameter '{p}' in parameters config")

    data = {k: (["time"], v) for k, v in dvars.items() if k in params_to_process}

    ds = xr.Dataset(data, coords=coords)

    # add NASA Ames file header
    ds.attrs["na_file_header"] = np.array(na._HEADER, dtype=str)

    # general (global) attributes
    # TODO : add Exceptions from NA file !
    ds.attrs["title"] = "IAGOS-CARIBIC netCDF4 data file"
    ds.attrs["date_created"] = (
        pd.Timestamp.now(tz="UTC").isoformat(timespec="seconds").replace("+00:00", "Z")
    )
    ds.attrs["summary"] = "merged IAGOS-CARIBIC data (CARIBIC-2), on a 10s grid"  # new in v0.5
    ds.attrs["keywords"] = (
        "greenhouse gases, aerosols, trace gases, aircraft observations, in-situ, monitoring, long-term"  # new in v0.5
    )
    ds.attrs["conventions"] = "CF-1.10"
    ds.attrs["mission"] = na.MNAME
    ds.attrs["data_description"] = na.SNAME
    ds.attrs["data_institute"] = na.ORG
    ds.attrs["data_owners"] = (
        "A. Zahn; H. Boenisch; T. Gehrlein; F. Obersteiner; contact: andreas.zahn@kit.edu"
    )
    if contributors:
        ds.attrs["data_contributors"] = contributors
    ds.attrs["license"] = _license  # added in v0.2
    if doi:
        ds.attrs["doi"] = doi  # added in v0.2
    ds.attrs["format_date"] = CARIBIC_NC4_FORMAT_DATE
    ds.attrs["format_version"] = CARIBIC_NC4_FORMAT_VERS
    ds.attrs["history"] = (
        "Converted from NASA Ames format with na_to_nc4 from caribic2dp.convert_caribic_na_nc4 module. "
        "Might contain only a subset of the parameters from the original NASA Ames file. "
    ) + _software_spec

    # to be able to recreate the input NASA Ames file, we need time format
    fmt = NumStr(na.X[0]).analyse_format()[0]
    ds.attrs["ivar_C_format"] = "%" + fmt

    ds["time"].attrs["name"] = CARIBIC_IVAR
    ds["time"].attrs["standard_name"] = "time"
    ds["time"].attrs["long_name"] = CARIBIC_IVAR_long

    # the index where to check parameter format (decimal places etc.)
    check_index = int(len(na.X) / 2)

    # attributes of variables
    for p in params_to_process:
        ds[p].attrs["standard_name"] = specs.iloc[param_index[p]].CF_StandardName
        ds[p].attrs["long_name"] = specs.iloc[param_index[p]].Long_Name
        ds[p].attrs["unit"] = specs.iloc[param_index[p]].Unit.strip("[]")
        if p in special_units:
            ds[p].attrs["unit"] = special_units[p]
        ds[p].attrs["standard_unit"] = specs.iloc[param_index[p]].CF_Unit.strip("[]")
        if ds[p].attrs["standard_name"].endswith("standard_error"):
            ds[p].attrs["coverage_content_type"] = "qualityInformation"  # ISO 19115-1
        else:
            ds[p].attrs["coverage_content_type"] = "physicalMeasurement"  # ISO 19115-1

        # verify parameter format
        vname_idx = vnames.index(p)
        want = specs.iloc[param_index[p]].py_format.strip("{}").replace(":", "")
        have = NumStr(na.V[vname_idx][check_index]).analyse_format()[0]
        if want.strip("%") != have and _warnings_enabled:
            warnings.warn(
                f"Warning: format specifier mismatch for parameter '{p}', want '{want}'"
                f" but found '%{have}' (e.g. value is {na.V[vname_idx][check_index]})"
            )
        # set the format from params_file
        ds[p].attrs["C_format"] = "%" + want

    # mark time coordinate as T
    ds["time"].attrs["axis"] = "T"
    ds["time"].attrs["coverage_content_type"] = "coordinate"

    # try to add lat/lon/alt as additional coordinates # added in v0.3
    try:
        ds.coords[CARIBIC_DVAR_lat] = ds.data_vars[CARIBIC_DVAR_lat]
        ds[CARIBIC_DVAR_lat].attrs["axis"] = "Y"
        ds[CARIBIC_DVAR_lat].attrs["coverage_content_type"] = "coordinate"
        ds.coords[CARIBIC_DVAR_lon] = ds.data_vars[CARIBIC_DVAR_lon]
        ds[CARIBIC_DVAR_lon].attrs["axis"] = "X"
        ds[CARIBIC_DVAR_lon].attrs["coverage_content_type"] = "coordinate"
        ds.coords[CARIBIC_DVAR_presAlt] = ds.data_vars[CARIBIC_DVAR_presAlt]
        ds[CARIBIC_DVAR_presAlt].attrs["axis"] = "Z"
        ds[CARIBIC_DVAR_presAlt].attrs["coverage_content_type"] = "coordinate"

        # add bounding box (new in v0.5)
        ds.attrs["geospatial_bounds_crs"] = "EPSG:4326"
        ds.attrs["geospatial_bounds_vertical_crs"] = "EPSG:5829"
        ds.attrs["geospatial_lat_min"] = ds[CARIBIC_DVAR_lat].values.min()
        ds.attrs["geospatial_lat_max"] = ds[CARIBIC_DVAR_lat].values.max()
        ds.attrs["geospatial_lon_min"] = ds[CARIBIC_DVAR_lon].values.min()
        ds.attrs["geospatial_lon_max"] = ds[CARIBIC_DVAR_lon].values.max()
        ds.attrs["geospatial_lat_units"] = "degree_north"
        ds.attrs["geospatial_lon_units"] = "degree_east"
        ds.attrs["geospatial_vertical_min"] = ds[CARIBIC_DVAR_presAlt].values.min()
        ds.attrs["geospatial_vertical_max"] = ds[CARIBIC_DVAR_presAlt].values.max()
        ds.attrs["geospatial_vertical_positive"] = "up"
        ds.attrs["geospatial_vertical_units"] = "m"
        ds.attrs["time_coverage_start"] = coords["time"].min().isoformat().replace("+00:00", "Z")
        ds.attrs["time_coverage_end"] = coords["time"].max().isoformat().replace("+00:00", "Z")
    except KeyError:
        if _warnings_enabled:
            warnings.warn("failed to add lat/lon/alt coordinates")

    if write_to_disk:
        n = ds.to_netcdf(dst)
        if n == 0 and _warnings_enabled:
            warnings.warn("no data written")

    return ds


def nc4_to_na(src: Union[pathlib.Path, str], dst: Union[pathlib.Path, str] = ""):
    """
    Convert CARIBIC netCDF4 to CARIBIC NASA Ames 1001.

    Parameters
    ----------
    src : str
        path to source.
    dst : str, optional
        desired output path. If not provided, source path is used, with file
        extension being replaced with '.nc'.

    Returns
    -------
    na : pyfuppes.na1001.FFI1001
        NASA Ames FFI 1001 class instance.

    """
    # this does not work with arbitrary nc files.
    # the nc file must have ben created with na_to_nc4.
    src, dst = pathlib.Path(src), pathlib.Path(dst)
    if dst == pathlib.Path(""):
        dst = src.parent / (src.stem + ".nc")

    ds = xr.open_dataset(src)

    # ds must have an attribute "na_file_header"
    assert "na_file_header" in ds.attrs, "variable 'na_file_header' not found in dataset attributes"

    # also check for CARIBIC na-2-nc specific attributes
    required = [
        "conventions",
        "date_created",
        "summary",
        "keywords",
        "data_description",
        "data_institute",
        "data_owners",
        "format_date",
        "format_version",
        "history",
        "mission",
        "title",
    ]
    has_attrs = [a.lower() for a in ds.attrs]
    for attr in required:
        assert attr in has_attrs, f"attribute '{attr}' not found in netCDF"

    # read header into ffi1001 instance
    na = FFI1001(
        StringIO("\n".join(ds.attrs["na_file_header"])),
        allow_emtpy_data=True,
    )

    # all variables specified in file header must be found as data arrays
    na_vars = [v.split(";")[0] for v in na.VNAME]
    na_V = []

    for idx, v in enumerate(na_vars):
        assert (var := ds.get(v)) is not None, f"missing variable {v} in netCDF"
        # extract data and fill NaN
        arr = var.values
        arr[~np.isfinite(arr)] = na.VMISS[idx]
        # get format code for each variable
        fmt = getattr(var, "C_format").strip("%")
        # array must be casted to integer for 'd' formatting directive to work
        arr = arr.astype(int) if "d" in fmt else arr
        # to string, for na.V
        na_V.append([f"{v:{fmt}}" for v in arr])

    # make ivar and dvars as a list of lists of dtype str
    fmt = ds.attrs["ivar_C_format"].strip("%")
    na.X = [f"{v:{fmt}}" for v in xrtime_to_mdns(ds["time"], dim_name="time").astype(int)]
    na.V = na_V

    return na
