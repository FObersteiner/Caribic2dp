# -*- coding: utf-8 -*-
"""Find the closest CARIBIC airport to a lat/lon tuple."""
from pathlib import Path
from typing import NamedTuple

from pyfuppes.txt2dict import txt_2_dict as t2d
from scipy import spatial as _sc_spatial

# ------------------------------------------------------------------------------

AirportSpecs = NamedTuple(
    "ap_data",
    [
        ("name", str),
        ("lat", float),
        ("lon", float),
        ("masl", float),
        ("continent", str),
        ("col", str),
    ],
)


def get_ap_data(iata_code: str, ap_file: Path) -> AirportSpecs:
    """
    Get info for given airport IATA code
    """
    data = t2d(ap_file).data
    apidx = data["IATA"].index(iata_code)
    assert apidx != -1, f"could not find IATA code '{iata_code}' in given airports file"

    return AirportSpecs(
        data["Name"][apidx],
        float(data["PosLat"][apidx]),
        float(data["PosLon"][apidx]),
        float(data["masl"][apidx]),
        data["Continent"][apidx],
        data["Pltflght_col"][apidx],
    )


def get_closest_AP(lat: float, lon: float, ap_file: Path) -> list:
    """
    Find the closest airport in CARIBIC_Airports.csv for a given set of lat/lon coordinates.

    search algorithm based on
    https://stackoverflow.com/questions/39107896/efficiently-finding-the-closest-coordinate-pair-from-a-set-in-python
    2018-08-08 10:29 UTC

    Parameters
    ----------
    lat : float
        latitude.
    lon : float
        longitude.
    ap_file : Path
        airport data csv.

    Raises
    ------
    FileNotFoundError
        ap_file not a file or not found.
    ValueError
        distance to nearest airport greater than one degree: airport not in ap_file!

    Returns
    -------
    list
        [IATA-Name, Name, masl].
    """
    ap_file = Path(ap_file)

    if not ap_file.is_file():
        raise FileNotFoundError(f"not found: {ap_file}")

    lat, lon = float(lat), float(lon)

    ap_dict = t2d(ap_file).data

    ap_lat_lon = [
        tuple(map(float, (la, lo))) for la, lo in zip(ap_dict["PosLat"], ap_dict["PosLon"])
    ]

    tree = _sc_spatial.KDTree(ap_lat_lon)
    ix = tree.query([(lat, lon)])[1][0]

    delta_latlon = (
        abs(float(ap_dict["PosLat"][ix]) - lat),
        abs(float(ap_dict["PosLon"][ix]) - lon),
    )

    if any(d > 1 for d in delta_latlon):
        raise ValueError("delta to selected airport greater than 1 deg")

    return [ap_dict["IATA"][ix], ap_dict["Name"][ix].split(",")[0], ap_dict["masl"][ix]]
