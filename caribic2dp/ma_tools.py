# -*- coding: utf-8 -*-
"""Master data (MA) tools."""
import os
import math
from datetime import datetime, timezone

import numpy as np
import matplotlib.pyplot as plt

from pyfuppes.monotonicity import strictly_increasing
from pyfuppes.filters import filter_jumps


# ------------------------------------------------------------------------------


def MA_get_pstat_palt_Tpot(StdAltitu, StcAirTmp, vmiss):
    """Helper to calculate pstat and palt and Tpot."""
    if StdAltitu == vmiss:
        pstat, palt = vmiss, vmiss
    else:
        pstat = (
            math.exp((1 / 0.190284) * math.log(1 - StdAltitu * 0.3048 * 0.0065 / 288.15)) * 1013.246
        )
        palt = StdAltitu * 0.3048
    if StcAirTmp == vmiss or StdAltitu == vmiss:
        Tpot = vmiss
    else:
        Tpot = (StcAirTmp + 273.15) * math.exp(0.2857 * math.log(1000.0 / pstat))
    return (pstat, palt, Tpot)


# ------------------------------------------------------------------------------
def MA_write_sortfiles(
    data,
    flight_info,
    ts_fmts,
    outpath,
    pc_time="PCUTC",
    merge_keys=["ari", "ilt", "snd", "prc"],
    delimiter="\t",
):
    """
    Merge master PC log data in flight-wise text files.
    """
    flights = flight_info["flights"]
    t_dep, t_arr = [], []
    for i in range(len(flights)):
        t_dep.append(data["ari"][pc_time][flight_info["ix"][0][i]])
        t_arr.append(data["ari"][pc_time][flight_info["ix"][1][i]])

    t_dep = [datetime.strptime(t, ts_fmts["ari"]) for t in t_dep]
    t_arr = [datetime.strptime(t, ts_fmts["ari"]) for t in t_arr]

    for mkey in merge_keys:
        Time_ref = [datetime.strptime(t, ts_fmts[mkey]) for t in data[mkey][pc_time]]
        for f in range(len(flights)):
            delta_dep = [abs(t - t_dep[f]) for t in Time_ref]
            ix_dep = delta_dep.index(min(delta_dep))
            delta_arr = [abs(t - t_arr[f]) for t in Time_ref]
            ix_arr = delta_arr.index(min(delta_arr))

            fname = outpath / ("Flight_" + str(flights[f]) + "_merged." + mkey)

            keys = list(data[mkey].keys())
            n_keys = len(keys)

            deli_list = [delimiter] * n_keys
            deli_list[-1] = "\n"
            header = ""
            for i in range(n_keys):
                header += keys[i] + deli_list[i]

            if mkey == "prc":
                header = "PCUTC\tAddr\tTime\tCommand\tvalue1\tvalue2\n"

            try:
                os.stat(os.path.dirname(fname))
            except OSError:
                os.mkdir(os.path.dirname(fname))

            with open(fname, "w", encoding="UTF-8") as file_obj:  # write  merge file
                file_obj.write(header)
                for i in range(ix_dep, ix_arr):
                    line = ""
                    for j in range(n_keys):
                        line += str(data[mkey][keys[j]][i]) + deli_list[j]
                    file_obj.write(line)


# ------------------------------------------------------------------------------
def MA_check_Arinc_essential(
    data,
    key_ari,
    ts_fmt="%d.%m.%y %H:%M:%S.%f",
    errorlog=False,
    logpath="",
    delimiter="\t",
    verbose=False,
    flightseries=[-1],
):
    """
    check essential ARINC parameters in Master PC log files.
        essential == if an entry is missing, the whole line is removed.
    """
    keys = list(data[key_ari].keys())
    n_keys = len(keys)
    nel = len(data[key_ari]["UTC"])

    ix_del, e_code = [], []
    t_mpc, t_ari = [], []

    for i, tstr in enumerate(data[key_ari]["UTC"]):  # arinc utc time
        tmp0 = datetime.strptime(data[key_ari]["PCUTC"][i], ts_fmt)
        tmp0 = tmp0.replace(tzinfo=timezone.utc)

        tmp1 = datetime(year=tmp0.year, month=tmp0.month, day=tmp0.day, tzinfo=timezone.utc)

        # check if A/C timestamp is valid
        if float(tstr) >= 2360 or float(tstr) <= 0:
            pass  # assume arinc date == master computer date, ignore time!
        else:
            if len(tstr) < 7:  # format is not hhmm.ss; -> add missing zeros
                tstr = "0" * (7 - len(tstr)) + tstr
            hour, minute, second = int(tstr[:2]), int(tstr[2:4]), int(tstr[5:7])
            if second > 59:  # let's hope this doesn't change the hour!
                print("    invalid A/C time:", hour, minute, second)
                minute, second = minute + 1, second - 60
            # will crash if min>59:
            tmp1 = tmp1.replace(hour=hour, minute=minute, second=second)

        t_mpc.append(tmp0)
        t_ari.append(tmp1)

    ### check: absolute value ok?
    for i in range(nel):
        v = float(data[key_ari]["UTC"][i])
        if v < 0.0 or v > 2400.0:  # code 2 UTC out of range
            ix_del.append(i)
            e_code.append(2)
            if verbose:
                print("-+- found error code #2 in line " + str(i) + " -+-")

        v1 = data[key_ari]["FlightPhas"][i]
        if v == 0.0 and v1 == "PowerOn":  # code 7 not UTC
            ix_del.append(i)
            e_code.append(7)
            if verbose:
                print("-+- found error code #7 in line " + str(i) + " -+-")

        if len(v1) < 2 or not v1[0].isupper():  # code 3 invalid flight state
            ix_del.append(i)
            e_code.append(3)
            if verbose:
                print("-+- found error code #3 in line " + str(i) + " -+-")

        v = float(data[key_ari]["PosLat"][i])
        if v < -50.0 or v > 90.0:  # code 4 latitude out of range
            ix_del.append(i)
            e_code.append(4)
            if verbose:
                print("-+- found error code #4 in line " + str(i) + " -+-")

        v1 = float(data[key_ari]["PosLong"][i])
        if v1 < -180.0 or v1 > 180.0:  # code 5 longitude out of range
            ix_del.append(i)
            e_code.append(5)
            if verbose:
                print("-+- found error code #5 in line " + str(i) + " -+-")

        if v == 0.0 and v1 == 0.0:  # code 6 no lat/lon position
            ix_del.append(i)
            e_code.append(6)
            if verbose:
                print("-+- found error code #6 in line " + str(i) + " -+-")

        t0 = datetime.timestamp(t_mpc[i])
        if i < nel - 1:
            t1 = datetime.timestamp(t_mpc[i + 1])
            if t1 - t0 < 0.5:  # code 8 delta t in PCUTC less than .5 sec
                ix_del.append(i)
                e_code.append(8)
                if verbose:
                    print("-+- found error code #8 in line " + str(i) + " -+-")

        t_ac = datetime.timestamp(t_ari[i])
        if abs(t0 - t_ac) >= 60.0:
            ix_del.append(i)
            e_code.append(9)  # code 9: Arinc and Master time not in agreement
            if verbose:
                print("-+- found error code #9 in line " + str(i) + " -+-")

    ### check: delta ok?
    v_lat = np.array(data[key_ari]["PosLat"], dtype="float64")
    v_lon = np.array(data[key_ari]["PosLong"], dtype="float64")
    # simple diff filter...
    dlat = np.absolute(np.diff(v_lat, prepend=v_lat[0]))
    dlon = np.absolute(np.diff(v_lon, prepend=v_lon[0]))
    # get out of range (oor) indices...
    dlat_max = 0.015  # -90:90 degr; 1200 km/h -> ~0.0015 degr/s
    dlon_max = 0.03  # -180:180 degr; 1200 km/h -> ~0.003 degr/s
    w_lat_oor = np.where(dlat > dlat_max)
    w_lon_oor = np.where(dlon > dlon_max)

    if w_lat_oor[0].size > 0:
        ix_del.extend(w_lat_oor[0])
        e_code.extend([11] * w_lat_oor[0].size)  # delta of latitudes is too big
        if verbose:
            print("-+- found error(s), code #11 -+-")

    if w_lon_oor[0].size > 0:
        ix_del.extend(w_lon_oor[0])
        e_code.extend([12] * w_lon_oor[0].size)  # delta of longitudes is too big
        if verbose:
            print("-+- found error(s), code #12 -+-")

    ### sort deletion list and calculate statistics

    ix_sort = sorted(range(len(ix_del)), key=lambda k: ix_del[k])
    ix_del = [ix_del[ix] for ix in ix_sort]
    e_code = [e_code[ix] for ix in ix_sort]
    e_stat = [[], [], []]
    tmp = np.unique(np.array(e_code, dtype="int32"))
    e_stat[0] = list(tmp)
    for e in e_stat[0]:
        e_stat[1].append((e_code == e).sum())
        e_stat[2].append(100.0 * (e_stat[1][-1] / len(e_code)))

    ### write log if desired
    if errorlog and len(ix_del) > 0:
        fileheader = [
            "ERROR_Code\tExplanation\n",
            "-------------------\n",
            #  "1\tline incomplete\n", # ESSENTIAL
            "2\t(UTC < 0) or (UTC > 2400)\n",  # ESSENTIAL
            "3\tFlightPhase does not start with a capital letter (A..Z), e.g. is a number\n",  # ESSENTIAL
            "4\t(PosLat < -50) or (PosLat > 90)\n",  # ESSENTIAL
            "5\t(PosLong < -180) or (PosLong > 180)\n",  # ESSENTIAL
            "6\t(PosLat = 0) and (PosLong = 0), i.e. no position data on the ARINC429 bus\n",  # ESSENTIAL
            "7\t(UTC = 0) and (FlightPhase = 'PowerOn'), i.e. no time on ARINC429 bus\n"  # ESSENTIAL,
            "8\tTime between two data points less than 0.5 sec (mostly double lines)\n"  # ESSENTIAL
            "9\tArinc UTC time is off by more than 60 seconds from Master PC time\n",  # OPTIONAL
            "11\tPosLat is wrong (difference to the previous PosLat larger than 0.015°)\n",  # ESSENTIAL
            "12\tPosLong is wrong  (difference to the previous PosLong larger than 0.03°)\n",  # ESSENTIAL
            #  "13\tTotPres is wrong (difference to the previous TotPres corresponds to a change of more than 10 hPa/s)\n", # OPTIONAL
            #  "14\tStdAltitu is wrong (difference to the previous StdAltitu corresponds to a change of more than 150 feet/s)\n", # OPTIONAL
            #  "15\tBaroAltit is wrong (difference to the previous BaroAltit corresponds to a change of more than 150 feet/s)\n", # OPTIONAL
            #  "16\tStcAirTmp is wrong (difference to the previous StcAirTmp corresponds to a change of more than 5 K/s)\n", # OPTIONAL
            #  "17\tTrAiSpeed >550 knots\n", # OPTIONAL
            #  "18\tStcAirTmp < -80 or StcAirTmp > 45\n", # OPTIONAL
        ]

        fname = logpath / ("Flightseries_" + str(flightseries[0]) + "_DeleteLog.txt")
        try:
            os.stat(os.path.dirname(fname))
        except OSError:
            os.mkdir(os.path.dirname(fname))

        deli_list = [delimiter] * (n_keys + 1)
        deli_list[-1] = "\n"
        header = "ERROR_Code\t"
        for i in range(n_keys):
            header += keys[i] + deli_list[i]

        with open(fname, "w", encoding="UTF-8") as file_obj:  # write  merge file
            file_obj.write("### " + "Flightseries_" + str(flightseries[0]) + " deleted lines ###\n")
            file_obj.write("###\n")

            for element in fileheader:
                file_obj.write(element)
                file_obj.write("###\n")
            file_obj.write("ERROR_Code" + delimiter + "n_occurances" + delimiter + "rel_abd_%\n")

            for ix, e in enumerate(e_stat[0]):
                file_obj.write(
                    str(e) + delimiter + str(e_stat[1][ix]) + delimiter + str(e_stat[2][ix]) + "\n"
                )

            file_obj.write("###\n")
            file_obj.write(header)

            for i in range(len(ix_del)):
                if ix_del[i - 1] != ix_del[i]:
                    line = str(e_code[i]) + delimiter
                    for j in range(n_keys):
                        line += str(data[key_ari][keys[j]][ix_del[i]]) + deli_list[j + 1]
                    file_obj.write(line)

            if verbose:
                print("-+++- created error log: " + str(fname) + " -+++-")

        ### remove lines from data
        deletes = 0
        for n in range(len(ix_del)):
            if ix_del[n - 1] != ix_del[n]:
                for key in keys:
                    data[key_ari][key].pop(ix_del[n] - deletes)
                deletes += 1

        return data


# ------------------------------------------------------------------------------
def MA_check_Arinc_essential_v2(
    data,
    key_ari,
    ts_fmt="%d.%m.%y %H:%M:%S.%f",
    errorlog=False,
    logpath="",
    delimiter="\t",
    flightseries=[-1],
):
    """
    check essential ARINC parameters in Master PC log files.
        essential == if an entry is missing, the whole line is removed.
    v2: revised to make processing faster
    """
    # master pc time
    data[key_ari]["PCUTC_datetime"] = []
    for t in data[key_ari]["PCUTC"]:
        data[key_ari]["PCUTC_datetime"].append(datetime.strptime(t, ts_fmt))
        data[key_ari]["PCUTC_datetime"][-1] = data[key_ari]["PCUTC_datetime"][-1].replace(
            tzinfo=timezone.utc
        )
    t_mpc_ts = np.array([t.timestamp() for t in data[key_ari]["PCUTC_datetime"]], dtype="float64")
    dt_mpc = np.diff(t_mpc_ts, prepend=t_mpc_ts[0])

    # general properties, output variables
    keys, n_el = list(data[key_ari].keys()), len(data[key_ari]["UTC"])
    n_keys = len(keys)
    ix_del, e_code = [], []

    v_lat = np.array(data[key_ari]["PosLat"], dtype="float64")
    v_lon = np.array(data[key_ari]["PosLong"], dtype="float64")
    v_ACUTC = np.array(data[key_ari]["UTC"], dtype="float32")
    dt_acutc = np.diff(v_ACUTC, prepend=v_ACUTC[0])

    w_oor = np.where((v_ACUTC < 0.0) | (v_ACUTC > 2360.0))
    if w_oor[0].shape[0] > 0:
        ix_del.extend(w_oor[0])
        e_code.extend([2] * w_oor[0].shape[0])

    w_FlSt_invd = [
        i
        for i in range(n_el)
        if not data[key_ari]["FlightPhas"][i].istitle() or len(data[key_ari]["FlightPhas"][i]) < 2
    ]
    if w_FlSt_invd:
        ix_del.extend(w_FlSt_invd)
        e_code.extend([3] * len(w_FlSt_invd))

    w_oor = np.where((v_lat < -50.0) | (v_lat > 90.0))
    if w_oor[0].shape[0] > 0:
        ix_del.extend(w_oor[0])
        e_code.extend([4] * w_oor[0].shape[0])

    w_oor = np.where((v_lon < -180.0) | (v_lat > 180.0))
    if w_oor[0].shape[0] > 0:
        ix_del.extend(w_oor[0])
        e_code.extend([5] * w_oor[0].shape[0])

    w_oor = np.where((v_lat == 0.0) & (v_lon == 0.0))
    if w_oor[0].shape[0] > 0:
        ix_del.extend(w_oor[0])
        e_code.extend([6] * w_oor[0].shape[0])

    w_noUTC = [
        i for i in range(n_el) if data[key_ari]["FlightPhas"][i] == "PowerOn" and v_ACUTC[i] == 0.0
    ]
    if w_noUTC:
        ix_del.extend(w_noUTC)
        e_code.extend([7] * len(w_noUTC))

    # check negative dt mpc (not allowed)
    w_oor = np.where(dt_mpc < 0.5)
    if w_oor[0].shape[0] > 0:
        ix_del.extend(w_oor[0])
        e_code.extend([8] * w_oor[0].shape[0])

    # check positive dt arinc time (3 minutes max.)
    # replaces check for timedelta MasterPC-ARINC
    w_oor = np.where(np.absolute(dt_acutc) > 180)  # 3h max allowed delta t
    if w_oor[0].shape[0] > 0:
        w_oor_vd = []
        for ix in w_oor[0]:
            if ix - 1 > 0 and ix + 1 < v_ACUTC.shape[0]:
                t = v_ACUTC[ix - 1 : ix + 2]
                if not all(((t[0] > t[1]), (t[1] < t[2]), (t[0] > t[2]))):
                    w_oor_vd.append(ix)

        if w_oor_vd:
            ix_del.extend(w_oor_vd)
            e_code.extend([9] * len(w_oor_vd))

    ### removed: check for timedelta MasterPC-ARINC since this can give
    ### inappropriate detection at date change!

    # simple diff filter for lat and long
    dlat = np.absolute(np.diff(v_lat, prepend=v_lat[0]))
    dlon = np.absolute(np.diff(v_lon, prepend=v_lon[0]))
    # get out of range (oor) indices...
    dlat_max = 0.015  # -90:90 degr; 1200 km/h -> ~0.0015 degr/s
    dlon_max = 0.03  # -180:180 degr; 1200 km/h -> ~0.003 degr/s
    w_lat_oor = np.where(dlat > dlat_max)
    w_lon_oor = np.where(dlon > dlon_max)
    if w_lat_oor[0].size > 0:
        ix_del.extend(w_lat_oor[0])
        e_code.extend([11] * w_lat_oor[0].size)  # delta of latitudes is too big
    if w_lon_oor[0].size > 0:
        ix_del.extend(w_lon_oor[0])
        e_code.extend([12] * w_lon_oor[0].size)  # delta of longitudes is too big

    ### sort deletion list and calculate statistics
    ix_sort = sorted(range(len(ix_del)), key=lambda k: ix_del[k])
    ix_del = [ix_del[ix] for ix in ix_sort]
    e_code = [e_code[ix] for ix in ix_sort]
    e_stat = [[], [], []]
    tmp = np.unique(np.array(e_code, dtype="int32"))
    e_stat[0] = list(tmp)
    for e in e_stat[0]:
        e_stat[1].append((e_code == e).sum())
        e_stat[2].append(100.0 * (e_stat[1][-1] / len(e_code)))

    ### write log if desired
    if errorlog and len(ix_del) > 0:
        fileheader = [
            "ERROR_Code\tExplanation\n",
            "-------------------\n",
            # "1\tline incomplete\n", # ESSENTIAL
            "2\t(UTC < 0) or (UTC > 2400)\n",  # ESSENTIAL
            "3\tFlightPhase does not start with a large letter (A..Z), e.g. is a number\n",  # ESSENTIAL
            "4\t(PosLat < -50) or (PosLat > 90)\n",  # ESSENTIAL
            "5\t(PosLong < -180) or (PosLong > 180)\n",  # ESSENTIAL
            "6\t(PosLat = 0) and (PosLong = 0), i.e. no position data on the ARINC429 bus\n",  # ESSENTIAL
            "7\t(UTC = 0) and (FlightPhase = 'PowerOn'), i.e. no time on ARINC429 bus\n"  # ESSENTIAL,
            "8\tTime between two data points less than 0.5 sec (mostly double lines)\n"  # ESSENTIAL
            "9\tArinc UTC time is off (jump forward)\n",  # ESSENTIAL
            "11\tPosLat is wrong (difference to the previous PosLat larger than 0.015°)\n",  # ESSENTIAL
            "12\tPosLong is wrong  (difference to the previous PosLong larger than 0.03°)\n",  # ESSENTIAL
            # "13\tTotPres is wrong (difference to the previous TotPres corresponds to a change of more than 10 hPa/s)\n", # OPTIONAL
            # "14\tStdAltitu is wrong (difference to the previous StdAltitu corresponds to a change of more than 150 feet/s)\n", # OPTIONAL
            # "15\tBaroAltit is wrong (difference to the previous BaroAltit corresponds to a change of more than 150 feet/s)\n", # OPTIONAL
            # "16\tStcAirTmp is wrong (difference to the previous StcAirTmp corresponds to a change of more than 5 K/s)\n", # OPTIONAL
            # "17\tTrAiSpeed >550 knots\n", # OPTIONAL
            # "18\tStcAirTmp < -80 or StcAirTmp > 45\n", # OPTIONAL
        ]

        fname = logpath / ("Flightseries_" + str(flightseries[0]) + "_DeleteLog.txt")
        try:
            os.stat(os.path.dirname(fname))
        except OSError:
            os.mkdir(os.path.dirname(fname))

        deli_list = [delimiter] * (n_keys + 1)
        deli_list[-1] = "\n"
        header = "ERROR_Code\t"
        for i in range(n_keys):
            header += keys[i] + deli_list[i]

        with open(fname, "w", encoding="UTF-8") as file_obj:  # write  merge file
            file_obj.write("### " + "Flightseries_" + str(flightseries[0]) + " deleted lines ###\n")
            file_obj.write("###\n")
            for element in fileheader:
                file_obj.write(element)
            file_obj.write("###\n")
            file_obj.write("ERROR_Code" + delimiter + "n_occurances" + delimiter + "rel_abd_%\n")
            for ix, e in enumerate(e_stat[0]):
                file_obj.write(
                    str(e) + delimiter + str(e_stat[1][ix]) + delimiter + str(e_stat[2][ix]) + "\n"
                )
            file_obj.write("###\n")
            file_obj.write(header)
            for i in range(len(ix_del)):
                if ix_del[i - 1] != ix_del[i]:
                    line = str(e_code[i]) + delimiter
                    for j in range(n_keys):
                        line += str(data[key_ari][keys[j]][ix_del[i]]) + deli_list[j + 1]
                    file_obj.write(line)

    ### remove lines from data
    ix_del_uniq = set(ix_del)  # ix_del is already sorted
    ix_all = set(range(n_el))
    ix_keep = ix_del_uniq.symmetric_difference(ix_all)
    for key in keys:
        data[key_ari][key] = [data[key_ari][key][i] for i in ix_keep]

    return data


# ------------------------------------------------------------------------------
def MA_check_Arinc_optional(
    MA_data,
    flightseries,
    vmiss="999999",
    reset_after=1,
    errorlog=False,
    logpath="",
    delimiter="\t",
    verbose=False,
):
    """
    check optional parameter in ARINC data.
        optional == if an entry is missing, vmiss is inserted but the whole
                    line is not deleted.
    """
    repl = {
        "flight": [],  # store replacements
        "ix_repl": [],
        "e_code": [],
        "del_parm": [],
        "del_val": [],
        "del_val_t": [],
    }

    for flight in flightseries:
        repl["flight"].append(flight)

        ix_repl = []
        del_parm = []
        del_val = []
        del_val_t = []
        e_code = []

        #        v_TrueHead = np.array(MA_data[flight]['TrueHead'], dtype='float64')
        v_WindSpeed = np.array(MA_data[flight]["WindSpeed"], dtype="float64")
        #        v_WindDirTr = np.array(MA_data[flight]['WindDirTr'], dtype='float64')
        v_PitchAng = np.array(MA_data[flight]["PitchAng"], dtype="float64")
        v_RollAng = np.array(MA_data[flight]["RollAng"], dtype="float64")
        #        v_DistToGo = np.array(MA_data[flight]['DistToGo'], dtype='float64')
        #        v_TimeToGo = np.array(MA_data[flight]['TimeToGo'], dtype='float64')
        #        v_LeftPres = np.array(MA_data[flight]['LeftPres'], dtype='float64')
        #        v_RightPres = np.array(MA_data[flight]['RightPres'], dtype='float64')
        v_StdAltitu = np.array(MA_data[flight]["StdAltitu"], dtype="float64")
        v_BaroAltit = np.array(MA_data[flight]["BaroAltit"], dtype="float64")
        v_TrAiSpeed = np.array(MA_data[flight]["TrAiSpeed"], dtype="float64")

        # ToAirTmp: key is wrong in master pc logfiles (ToAiSpeed is actually temperature):
        v_ToAirTmp = np.array(MA_data[flight]["ToAiSpeed"], dtype="float64")
        v_AltitRate = np.array(MA_data[flight]["AltitRate"], dtype="float64")
        v_StcAirTmp = np.array(MA_data[flight]["StcAirTmp"], dtype="float64")
        v_AngOAttck = np.array(MA_data[flight]["AngOAttck"], dtype="float64")
        v_TotPres = np.array(MA_data[flight]["TotPres"], dtype="float64")
        v_VertSpeed = np.array(MA_data[flight]["VertSpeed"], dtype="float64")
        v_GndSpeed = np.array(MA_data[flight]["GndSpeed"], dtype="float64")

        ### check absolute values
        w_oor = np.where((v_TrAiSpeed < 0.0) | (v_TrAiSpeed > 550.0))
        if w_oor[0].shape[0] > 0:
            for ix in w_oor[0]:
                ix_repl.append(ix)
                e_code.append(17)  # code 17 speed out of range
                del_parm.append("TrAiSpeed")
                del_val.append(MA_data[flight]["TrAiSpeed"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["TrAiSpeed"][ix] = vmiss
                if verbose:
                    print("-+- error code #17, flight " + flight + ", line " + str(ix) + " -+-")

        w_oor = np.where((v_StcAirTmp < -90.0) | (v_StcAirTmp > 90.0))
        if w_oor[0].shape[0] > 0:
            for ix in w_oor[0]:
                ix_repl.append(ix)
                e_code.append(18)  # code 18 air temperature out of range
                del_parm.append("StcAirTmp")
                del_val.append(MA_data[flight]["StcAirTmp"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["StcAirTmp"][ix] = vmiss
                if verbose:
                    print("-+- error code #18, flight " + flight + ", line " + str(ix) + " -+-")

        w_oor = np.where((v_ToAirTmp < -90.0) | (v_ToAirTmp > 90.0))
        if w_oor[0].shape[0] > 0:
            for ix in w_oor[0]:
                ix_repl.append(ix)
                e_code.append(18)  # code 18 air temperature out of range
                del_parm.append("ToAirTmp")
                del_val.append(MA_data[flight]["ToAiSpeed"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                # ToAirTmp: key is wrong in master pc logfiles (ToAiSpeed):
                MA_data[flight]["ToAiSpeed"][ix] = vmiss
                if verbose:
                    print("-+- error code #18, flight " + flight + ", line " + str(ix) + " -+-")

        w_oor = np.where((v_VertSpeed < -6000.0) | (v_VertSpeed > 6000.0))
        if w_oor[0].shape[0] > 0:
            for ix in w_oor[0]:
                ix_repl.append(ix)
                e_code.append(26)  # code 26 vertical speed out of range
                del_parm.append("VertSpeed")
                del_val.append(MA_data[flight]["VertSpeed"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["VertSpeed"][ix] = vmiss
                if verbose:
                    print("-+- error code #26, flight " + flight + ", line " + str(ix) + " -+-")

        ### check delta values
        _, m = filter_jumps(v_TotPres, 10.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(13)  # delta of TotPres is too big
                del_parm.append("TotPres")
                del_val.append(MA_data[flight]["TotPres"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["TotPres"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #13, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_StdAltitu, 300.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(14)  # delta of StdAltitu is too big
                del_parm.append("StdAltitu")
                del_val.append(MA_data[flight]["StdAltitu"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["StdAltitu"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #14, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_BaroAltit, 150.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(15)  # delta of BaroAltit is too big
                del_parm.append("BaroAltit")
                del_val.append(MA_data[flight]["BaroAltit"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["BaroAltit"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #15, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_StcAirTmp, 5.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                if MA_data[flight]["StcAirTmp"][ix] != vmiss:  # avoid double-checking
                    ix_repl.append(ix)
                    e_code.append(16)  # delta of StcAirTmp is too big
                    del_parm.append("StcAirTmp")
                    del_val.append(MA_data[flight]["StcAirTmp"][ix])
                    del_val_t.append(MA_data[flight]["PCUTC"][ix])
                    MA_data[flight]["StcAirTmp"][ix] = vmiss
                    if verbose:
                        print(f"-+- error code #16, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_VertSpeed, 10000.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                if MA_data[flight]["VertSpeed"][ix] != vmiss:  # avoid double-checking
                    ix_repl.append(ix)
                    e_code.append(19)  # delta of VertSpeed is too big
                    del_parm.append("VertSpeed")
                    del_val.append(MA_data[flight]["VertSpeed"][ix])
                    del_val_t.append(MA_data[flight]["PCUTC"][ix])
                    MA_data[flight]["VertSpeed"][ix] = vmiss
                    if verbose:
                        print(f"-+- error code #19, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_WindSpeed, 50.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(20)  # delta of WindSpeed is too big
                del_parm.append("WindSpeed")
                del_val.append(MA_data[flight]["WindSpeed"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["WindSpeed"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #20, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_GndSpeed, 1000.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(21)  # delta of GndSpeed is too big
                del_parm.append("GndSpeed")
                del_val.append(MA_data[flight]["GndSpeed"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["GndSpeed"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #21, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_PitchAng, 30.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(22)  # delta of PitchAng is too big
                del_parm.append("PitchAng")
                del_val.append(MA_data[flight]["PitchAng"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["PitchAng"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #22, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_RollAng, 30.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(23)  # delta of RollAng is too big
                del_parm.append("RollAng")
                del_val.append(MA_data[flight]["RollAng"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["RollAng"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #23, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_AngOAttck, 5.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(24)  # delta of AngOAttck is too big
                del_parm.append("AngOAttck")
                del_val.append(MA_data[flight]["AngOAttck"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["AngOAttck"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #24, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_AltitRate, 10000.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                ix_repl.append(ix)
                e_code.append(25)  # delta of AltitRate is too big
                del_parm.append("AltitRate")
                del_val.append(MA_data[flight]["AltitRate"][ix])
                del_val_t.append(MA_data[flight]["PCUTC"][ix])
                MA_data[flight]["AltitRate"][ix] = vmiss
                if verbose:
                    print(f"-+- error code #25, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_TrAiSpeed, 200.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                if MA_data[flight]["TrAiSpeed"][ix] != vmiss:  # avoid double-checking
                    ix_repl.append(ix)
                    e_code.append(27)  # delta of TrAiSpeed is too big
                    del_parm.append("TrAiSpeed")
                    del_val.append(MA_data[flight]["TrAiSpeed"][ix])
                    del_val_t.append(MA_data[flight]["PCUTC"][ix])
                    MA_data[flight]["TrAiSpeed"][ix] = vmiss
                    if verbose:
                        print(f"-+- error code #27, flight {flight}, line {ix} -+-")

        _, m = filter_jumps(v_ToAirTmp, 50.0, reset_after, abs_delta=True)
        ix_del = np.where(~m)[0]
        if ix_del.shape[0] > 0:
            for ix in ix_del:
                if MA_data[flight]["ToAiSpeed"][ix] != vmiss:
                    ix_repl.append(ix)
                    e_code.append(28)  # delta of ToAirTmp is too big
                    del_parm.append("ToAiSpeed")  # Note: bad tag in MA logfiles!
                    del_val.append(MA_data[flight]["ToAiSpeed"][ix])
                    del_val_t.append(MA_data[flight]["PCUTC"][ix])
                    MA_data[flight]["ToAiSpeed"][ix] = vmiss
                    if verbose:
                        print(f"-+- error code #28, flight {flight}, line {ix} -+-")

        repl["ix_repl"].append(ix_repl)
        repl["e_code"].append(e_code)
        repl["del_parm"].append(del_parm)
        repl["del_val"].append(del_val)
        repl["del_val_t"].append(del_val_t)

    ### write log if desired
    if errorlog:
        fileheader = [
            "Error\tExplanation\n",
            "-------------------\n",
            #        "1\tline incomplete\n", # ESSENTIAL
            #        "2\t(UTC < 0) or (UTC > 2400)\n", # ESSENTIAL
            #        "3\tFlightPhase does not start with a large letter (A..Z), e.g. is a number\n", # ESSENTIAL
            #        "4\t(PosLat < -50) or (PosLat > 90)\n", # ESSENTIAL
            #        "5\t(PosLong < -180) or (PosLong > 180)\n", # ESSENTIAL
            #        "6\t(PosLat = 0) and (PosLong = 0), i.e. no position data on the ARINC429 bus\n", # ESSENTIAL
            #        "7\t(UTC = 0) and (FlightPhase = 'PowerOn'), i.e. no time on ARINC429 bus\n" # ESSENTIAL,
            #        "8\tTime between two data points less than 0.5 sec (mostly double lines)\n" # ESSENTIAL
            #        "9\tUTC time (3. column in *.ari file) is wrong, i.e. does not agree with PCUTC (first 2 lines)\n", # OPTIONAL
            #        "11\tPosLat is wrong (difference to the previous PosLat larger than 3°)\n", # ESSENTIAL
            #        "12\tPosLong is wrong  (difference to the previous PosLong larger than 3°)\n", # ESSENTIAL
            "13\tTotPres is wrong (difference to the previous TotPres corresponds to a change of more than 10 hPa/s)\n",  # OPTIONAL
            "14\tStdAltitu is wrong (difference to the previous StdAltitu corresponds to a change of more than 150 feet/s)\n",  # OPTIONAL
            "15\tBaroAltit is wrong (difference to the previous BaroAltit corresponds to a change of more than 150 feet/s)\n",  # OPTIONAL
            "16\tStcAirTmp is wrong (difference to the previous StcAirTmp corresponds to a change of more than 5 K/s)\n",  # OPTIONAL
            "17\tTrAiSpeed >550 knots\n",  # OPTIONAL
            "18\tStcAirTmp < -80 or StcAirTmp > 45\n",  # OPTIONAL
            "19\tdelta in VertSpeed > 10000\n",  # OPTIONAL, new in v181030
            "20\tdelta in WindSpeed > 50\n",  # OPTIONAL, new in v181030
            "21\tdelta in GndSpeed > 1000\n",  # OPTIONAL, new in v181030
            "22\tdelta in PitchAng > 30\n",  # OPTIONAL, new in v181030
            "23\tdelta in RollAng > 30\n",  # OPTIONAL, new in v181030
            "24\tdelta in AngOAttck > 5\n",  # OPTIONAL, new in v181030
            "25\tdelta in AltitRate > 10000\n",  # OPTIONAL, new in v181030
            "26\tVertSpeed out of range (+-6000)\n",  # OPTIONAL, new in v181030
            "27\tdelta in TrAiSpeed > 200\n",  # OPTIONAL, new in v181030
            "27\tdelta in v_ToAirTmp > 50\n",  # OPTIONAL, new in v200113
        ]

        fname = logpath / ("Flightseries_" + str(flightseries[0]) + "_ReplaceLog.txt")
        try:
            os.stat(os.path.dirname(fname))
        except OSError:
            os.mkdir(os.path.dirname(fname))

        with open(fname, "w", encoding="UTF-8") as file_obj:
            file_obj.write(f"### Flightseries_{str(flightseries[0])} replaced values ###\n")
            file_obj.write("###\n")
            for element in fileheader:
                file_obj.write(element)
            file_obj.write("###\n")
            file_obj.write(f"values replaced with:\t{str(vmiss)}\n")
            file_obj.write(f"jump filter lookahead:\t{str(reset_after)}\n")
            file_obj.write("###\n")

            header = "Flight\t"
            header += "ERROR_Code\t"
            header += "PCUTC\t"
            header += "Parameter\t"
            header += "replaced_value\n"

            file_obj.write(header)

            for f in range(len(repl["flight"])):
                if len(repl["e_code"][f]) == 0:  # no replacements in current flight
                    line = repl["flight"][f] + delimiter
                    line += "NaN" + delimiter
                    line += "NaN" + delimiter
                    line += "NaN" + delimiter
                    line += "NaN" + "\n"
                    file_obj.write(line)
                else:
                    for g in range(len(repl["e_code"][f])):
                        line = repl["flight"][f] + delimiter
                        line += str(repl["e_code"][f][g]) + delimiter
                        line += str(repl["del_val_t"][f][g]) + delimiter
                        line += str(repl["del_parm"][f][g]) + delimiter
                        line += str(repl["del_val"][f][g]) + "\n"
                        file_obj.write(line)
            if verbose:
                print("-+++- created error log: " + str(fname) + " -+++-")

    return MA_data


# ------------------------------------------------------------------------------


def MA_data_chk_time(masdata, ts_fmts, timekey="PCUTC"):
    """
    check if time vectors in ARINC data are strictly increasing over all
    entries of the masdata dictionary.
    """
    for key in masdata:
        if masdata[key]:  # make sure the key actually has data...
            Time = [datetime.strptime(t, ts_fmts[key]) for t in masdata[key][timekey]]
            sort_ix = sorted(range(len(Time)), key=lambda k: Time[k])
            if not strictly_increasing(sort_ix):
                print("-+- found irregular time vector in " + key + ", " + timekey + " -+-")
                for key0 in masdata[key]:
                    masdata[key][key0] = [masdata[key][key0][sort_ix[n]] for n in range(len(Time))]

    return masdata


# ------------------------------------------------------------------------------


def MA_get_flight_ix(
    data,
    key_ari,
    flightseries,
    key_flph="FlightPhas",
    states=[
        "nn",
        "PowerOn",
        "EngineStart",  # 0, 1, 2
        "TaxiOut",
        "TakeOff",
        "Climb",  # 3, 4, 5
        "Cruise",
        "Approach",
        "RollOut",  # 6, 7, 8
        "TaxiIn",
        "EngineStop",
    ],  # 9, 10
    write_log=False,
    key_PCUTC="PCUTC",
    ts_fmt="%d.%m.%y %H:%M:%S.%f",
    dt_max=1800.0,
    save_diagn_pl=False,
    plt_keep=True,
    plot_ts=True,
):
    """
    scan ARINC data for beginning and end of CARIBIC flights.
    function also writes "flights_log.txt".
    """
    trig_stats = [2, 7]

    FlightPhas = data[key_ari][key_flph]  # extract flight phase as string

    # replace TakeOffRoll with TakeOff to ensure consitency
    FlightPhas = ["TakeOff" if s == "TakeOffRoll" else s for s in FlightPhas]

    # extract master time to check for gaps
    PCutc = [datetime.strptime(x, ts_fmt).timestamp() for x in data[key_ari][key_PCUTC]]
    PCutc = np.array(PCutc, dtype="float64")

    t_delta = np.diff(PCutc, prepend=PCutc[0])

    ix_beg, ix_end = [], []

    FlState, logical_flight = [], []

    flight = False
    for i, value in enumerate(FlightPhas):
        # extract flight state
        fail = False
        try:
            _ = states.index(value)
        except ValueError as e:
            print(e, value, i)
            fail = True

        if fail:  # no value received...
            if i > 0:
                FlState.append(FlState[-1])  # ...replace with previous
            else:
                FlState.append(0)
            # now it could still be that the container was switched off in e.g.
            # flight state "climb" and switched on again in the same state...
        elif t_delta[i] > dt_max and states.index(value) >= 5:  # climb
            FlState.append(
                0
            )  # insert one value with flight state = 0 to trigger the recognition of a new flight
        else:
            FlState.append(states.index(value))

        if not flight and FlState[-1] > trig_stats[0] and FlState[-1] < trig_stats[1]:
            flight = True
            ix_beg.append(i)

        if flight and (FlState[-1] <= trig_stats[0] or FlState[-1] > trig_stats[1]):
            flight = False
            ix_end.append(i - 1)

        logical_flight.append(int(flight))

    if save_diagn_pl:
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title("Flightseries " + str(flightseries) + ": detection diagnostic")
        ax1.set_xlabel("POSIX[s]", color="k")
        p0 = ax1.plot(PCutc, FlState, color="b", lw=1.5, label="state")
        p1 = ax1.plot(PCutc, logical_flight, color="g", lw=4.0, label="logical_flight")
        ax1.set_xlim([PCutc[0] - 10.0, PCutc[-1] + 10.0])
        plots = p0 + p1
        lbls = [elem.get_label() for elem in plots]
        plt.legend(plots, lbls, loc=0, framealpha=1, facecolor="white")
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        fname = save_diagn_pl
        try:
            os.stat(os.path.dirname(save_diagn_pl))
        except OSError:
            os.mkdir(os.path.dirname(save_diagn_pl))
        fig.savefig(
            fname,
            dpi=300,
            facecolor="w",
            edgecolor="w",
            orientation="portrait",
            transparent=False,
            bbox_inches=None,
            pad_inches=0.1,
        )
        if not plt_keep:
            plt.close()

    # work-around: sometimes ARINC data becomes incomplete after a flight
    # when the flight state changes from Approach to e.g. PowerOn.
    # MA_check_Arinc_essential will remove these data - detection of the end
    # of the last flight is not possible -> use max index of flstate vector
    # as the end index of the last flight.
    if len(ix_end) == len(ix_beg) - 1:
        ix_end.append(len(FlightPhas) - 1)

    flightseries = list(range(flightseries, flightseries + len(ix_beg)))

    flight_info = {
        "ix": [ix_beg, ix_end],
        "state_nbr": FlState,
        "flights": flightseries,
    }

    if write_log:
        fname = write_log / ("Flightseries_" + str(flightseries[0]) + "_FlightsLog.txt")
        try:
            os.stat(os.path.dirname(fname))
        except OSError:
            os.mkdir(os.path.dirname(fname))

        with open(fname, "w", encoding="UTF-8") as file_obj:
            file_obj.write("### " + "Flightseries " + str(flightseries[0]) + " Flight Times ###\n")
            file_obj.write("###\n")
            file_obj.write("Flight_No\tt_start[UTC]\tt_end[UTC]\n")
            for fl_ix, flight in enumerate(flightseries):
                line = str(flight) + "\t"
                line += (data[key_ari][key_PCUTC])[flight_info["ix"][0][fl_ix]] + "\t"
                line += (data[key_ari][key_PCUTC])[flight_info["ix"][1][fl_ix]] + "\n"
                file_obj.write(line)

    return flight_info


# ------------------------------------------------------------------------------


def MA_load_fileheader(fname):
    """
    What?
        load information from a config file for the MA nasa ames file header.
    Returns?
        Dictionary, with "cleaned" entries, corresponding the nasa ames 1001
        keys.
    """
    with open(fname, "r", encoding="UTF-8") as file_obj:
        na_config = file_obj.readlines()  # read file content to string list

    header_dict = {}

    for ix, line in enumerate(na_config):
        if line.find("### vers_tag") > -1:
            header_dict["vers_tag"] = na_config[ix + 1].strip()
        if line.find("### ONAME") > -1:
            header_dict["ONAME"] = na_config[ix + 1].strip()
        if line.find("### ORG") > -1:
            header_dict["ORG"] = na_config[ix + 1].strip()
        if line.find("### SNAME") > -1:
            header_dict["SNAME"] = na_config[ix + 1].strip()
        if line.find("### MNAME") > -1:
            header_dict["MNAME"] = na_config[ix + 1].strip()
        if line.find("### XNAME") > -1:
            header_dict["XNAME"] = na_config[ix + 1].strip().rsplit(";")
        if line.find("### NV") > -1:
            header_dict["NV"] = int(na_config[ix + 1].strip())
        if line.find("### VSCAL") > -1:
            header_dict["VSCAL"] = int(na_config[ix + 1].strip())
        if line.find("### VMISS") > -1:
            header_dict["VMISS"] = int(na_config[ix + 1].strip())
        if line.find("### VNAME") > -1:
            header_dict["VNAME"] = [
                vname.strip() for vname in na_config[ix + 1 : ix + 1 + header_dict["NV"]]
            ]
        if line.find("### NCOM") > -1:
            nl = int(na_config[ix + 1])
            header_dict["NCOM"] = na_config[ix + 2 : ix + 2 + nl]
            for i in range(len(header_dict["NCOM"])):
                header_dict["NCOM"][i] = header_dict["NCOM"][i].strip()

        if line.find("### SCOM ffmt_info") > -1:
            header_dict["ffmt_info"] = na_config[ix + 1].strip()

        if line.find("### SCOM terminal block") > -1:
            nl = int(na_config[ix + 1])
            header_dict["scom_term"] = na_config[ix + 2 : ix + 2 + nl]
            for i in range(len(header_dict["scom_term"])):
                header_dict["scom_term"][i] = header_dict["scom_term"][i].strip()

        if line.find("### VAR_FORMATS") > -1:
            header_dict["v_fmt"] = na_config[ix + 1].strip()
            header_dict["v_fmt"] = header_dict["v_fmt"].rsplit(sep=";")

    return header_dict


# ------------------------------------------------------------------------------
