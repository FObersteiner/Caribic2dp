# -*- coding: utf-8 -*-
"""
Master Data (MA) diagnostics
"""
from datetime import datetime, timezone
from fnmatch import fnmatch as fnm
from pathlib import Path
from math import sin, cos, sqrt, atan2, radians
import os

import numpy as np
from pandas.plotting import register_matplotlib_converters


from matplotlib import pyplot as plt
from matplotlib import dates as mpl_dates
from matplotlib.backends.backend_pdf import PdfPages

try:
    from mpl_toolkits.basemap import Basemap  # TODO : replace this
except ValueError:
    print("ma_tools_diagnostics.py: intercepted Basemap import error...")

from pyfuppes.plottools import get_plot_range
from pyfuppes.timeconversion import mdns_2_dtobj
from pyfuppes.na1001 import FFI1001 as na

from caribic2dp.carfileutils import dissect_filename

register_matplotlib_converters()

# ------------------------------------------------------------------------------


def diag_plots_ov(data: dict, flightseries: list, outpath: str, show_plots=False, plot_ts=True):
    """
    overview plots for the whole flight series.

    Parameters
    ----------
    data : dict
        DESCRIPTION.
    flightseries : list
        DESCRIPTION.
    outpath : str
        DESCRIPTION.
    show_plots : TYPE, optional
        DESCRIPTION. The default is False.
    plot_ts : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.
    """
    outpath = Path(outpath)

    fltseries = str(flightseries[0]) + "-" + str(flightseries[-1])

    key_t = "PCUTC"
    ts_fmt = "%d.%m.%y %H:%M:%S.%f"
    key0, key_v = "ari", "TotPres"
    v_lim = [100, 1100]
    key_state = "FlightPhas"

    x = [datetime.strptime(element, ts_fmt) for element in data[key0][key_t]]
    y = np.array(data[key0][key_v], dtype="float32")

    fig, ax = plt.subplots(figsize=(16, 10))  # call plot object
    ax.plot_date(x, y, "b+")  # fill plot

    days = mpl_dates.DayLocator()  # config x axis
    hours = mpl_dates.HourLocator()
    #    minutes  =  mpl.dates.MinuteLocator()
    #    sec =  mpl.dates.SecondLocator()
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_minor_locator(hours)
    majorFormatter = mpl_dates.DateFormatter("%d.%m.%y")
    ax.xaxis.set_major_formatter(majorFormatter)
    ax.autoscale_view()

    plt.xticks(rotation=45)  # config x axis tick marks
    plt.minorticks_on()

    title = "Flight(s) " + fltseries + ": " + key0 + " / " + key_v
    plt.title(title, fontsize=20, fontweight="bold")  # title and labels
    plt.xlabel(key_t, fontweight="bold")
    plt.ylabel(key_v, fontweight="bold")

    axes = plt.gca()  # set y axis limits
    axes.set_ylim(v_lim)

    ax.grid(True, which="both")  # call plot grid

    plt.tight_layout()  # set layout to small margins
    if plot_ts:
        ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
        plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)

    if show_plots:
        plt.show()  # show the plot!

    fig.savefig(
        outpath / (key0 + "_" + key_v + ".png"),
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )

    if not show_plots:
        plt.close()

    ##### LAT LON PLOT #####
    key0 = "ari"
    key_lat = "PosLat"
    key_lon = "PosLong"
    key_state = "FlightPhas"

    lat = np.array(data[key0][key_lat], dtype="float32")
    lon = np.array(data[key0][key_lon], dtype="float32")
    state = np.array(data[key0][key_state], dtype="int")

    w_cruise = np.where((state == 6) | (state == 5))
    lat = lat[w_cruise]
    lon = lon[w_cruise]

    reduce_size = 10
    lat = lat[0::reduce_size]
    lon = lon[0::reduce_size]

    #    plt.style.use("seaborn")
    fig = plt.figure(figsize=(16, 10))
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])

    m = Basemap(
        llcrnrlon=-170.0,
        llcrnrlat=-50.0,
        urcrnrlon=170.0,
        urcrnrlat=80.0,
        rsphere=(6378137.00, 6356752.3142),
        resolution="l",
        projection="merc",
        lat_0=50.0,
        lon_0=12.0,
        lat_ts=0.0,
    )

    m.drawcoastlines()
    m.fillcontinents()

    m.drawparallels(np.arange(-90, 90, 30), labels=[1, 1, 0, 1])
    m.drawmeridians(np.arange(-180, 180, 30), labels=[1, 1, 0, 1])

    lat, lon = np.array(lat, dtype="float32"), np.array(lon, dtype="float32")
    x, y = m(lon, lat)

    plt.plot(x, y, "-", color="b", linewidth=2.0)

    title = "Flight(s) " + fltseries + ": Flighttracks"
    ax.set_title(title, fontsize=20, fontweight="bold")
    if plot_ts:
        ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
        plt.text(0.89, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)

    if show_plots:
        plt.show()  # show the plot!

    fig.savefig(
        outpath / (key0 + "_" + key_lat + "_" + key_lon + ".png"),
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )

    if not show_plots:
        plt.close()


# ------------------------------------------------------------------------------


def diag_plots_indiv_flights(
    na_1001: dict,
    filename: str,
    diagn_save_dir: str,
    call_plots=True,
    save_close_dpi_pdf=[True, True, 300, False],
    call_stat=True,
    plot_ts=True,
):
    """
    diagnostic plots for individual flights, based on Nasa Ames 1001 dict.

    Parameters
    ----------
    na_1001 : dict
        DESCRIPTION.
    filename : str
        DESCRIPTION.
    diagn_save_dir : str
        DESCRIPTION.
    call_plots : TYPE, optional
        DESCRIPTION. The default is True.
    save_close_dpi_pdf : TYPE, optional
        DESCRIPTION. The default is [True, True, 300, False].
    call_stat : TYPE, optional
        DESCRIPTION. The default is True.
    plot_ts : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.
    """
    plt.style.use("classic")
    ylim_dgr = [-180.0, 180.0]

    if not na_1001:  # set na_1001 to False to load data from file
        na_1001 = na(filename)

    f_no = "F" + str(dissect_filename(filename).flight_no)
    print("+++ running diagnostics for " + f_no + " +++")

    if save_close_dpi_pdf[3]:
        pp = PdfPages(diagn_save_dir / (f_no + "_ARI_Parms.pdf"))

    vmiss = float(na_1001.VMISS[0])

    t_pc_key = "CRefTime"  # Master PC time
    t_ac_key = "UTC_ARINC"  # Arinc time

    all_vars = {}
    units = {}
    for ix, element in enumerate(na_1001.VNAME):
        unt_tmp = (element.rsplit(sep=";")[2]).strip()
        if "#" in unt_tmp:
            unt_tmp = unt_tmp[0 : unt_tmp.find("#") - 1]
        units[element.rsplit(sep=";")[0]] = unt_tmp
        all_vars[element.rsplit(sep=";")[0]] = [ix]
        if fnm((element.rsplit(sep=";")[0]), t_pc_key):
            t_pc = np.array(na_1001.V[ix], dtype="float64")
        if fnm((element.rsplit(sep=";")[0]), t_ac_key):
            t_ac = np.array(na_1001.V[ix], dtype="float64")

    x = np.array(na_1001.X, dtype="float64")

    dx = x[1:] - x[0:-1]

    dt = (t_ac - t_pc) * 86400.0  # t_pc and t_ac unit is [days-since]

    xrange = get_plot_range(x, add_percent=3.0)

    for key in all_vars:
        all_vars[key] = np.array(na_1001.V[all_vars[key][0]], dtype="float64")
        # replace vmiss with NaN so plots look nicer
        w_invd = np.where(all_vars[key] == vmiss)
        if len(all_vars[key][w_invd]) > 0:
            #            print(f_no + " found " + str(len(all_vars[key][w_invd])) + " NaN in " + key)
            all_vars[key][w_invd] = np.nan

    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------
    #     PLOTS     #
    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------
    # begin with timing diagnostics
    if call_plots:
        title = f_no + " MasterPC dt to previous"
        yrange = get_plot_range(dx, add_percent=5.0)
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x[1:], dx, color="b", label="dt")
        #        ax1.set_yscale("log", nonposy='clip')
        ax1.set_xlabel("UTC[s]", color="k", fontweight="bold")
        ax1.set_ylabel("dt[s]", color="b", fontweight="bold")
        ax1.set_ylim(yrange)
        ax1.tick_params("y", colors="b")
        plots = p0
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        title = f_no + " timedelta ARINC-MasterPC"
        yrange = get_plot_range(dt, add_percent=5.0)
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, dt, color="r", label="timedelta")
        ax1.set_xlabel("UTC[s]", color="k", fontweight="bold")
        ax1.set_ylabel("dt[s]", color="r", fontweight="bold")
        ax1.set_ylim(yrange)
        ax1.tick_params("y", colors="r")
        plots = p0
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        # now the rest...
        l0, v0 = "PosLat" + units["PosLat"], all_vars["PosLat"]
        l1, v1 = "PosLong" + units["PosLong"], all_vars["PosLong"]
        title = f_no + "_" + "PosLat" + "_" + "PosLong"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "pstatic" + units["pstatic"], all_vars["pstatic"]
        l1, v1 = "TotPres" + units["TotPres"], all_vars["TotPres"]
        title = f_no + "_" + "pstatic" + "_" + "TotPres"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "Altitude" + units["Altitude"], all_vars["Altitude"]
        l11, v11 = "BaroAltit" + units["BaroAltit"], all_vars["BaroAltit"]
        l12, v12 = "StdAltitu" + units["StdAltitu"], all_vars["StdAltitu"]
        title = f_no + "_" + "Altitude" + "_" + "BaroAltit" + "_" + "StdAltitu"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0 + ", left y")
        #        ax1.set_yscale("log", nonposy='clip')
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v11, color="g", label=l11 + ", right y")
        p2 = ax2.plot(x, v12, color="r", label=l12 + ", right y")
        ax2.set_ylim(get_plot_range(np.append(v11, v12), add_percent=5.0))
        #        ax2.set_yscale("log", nonposy='clip')
        ax2.tick_params("y", colors="k")
        plots = p0 + p1 + p2
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "PitchAng" + units["PitchAng"], all_vars["PitchAng"]
        l1, v1 = "RollAng" + units["RollAng"], all_vars["RollAng"]
        title = f_no + "_" + "PitchAng" + "_" + "RollAng"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "CRefTime" + units["CRefTime"], all_vars["CRefTime"]
        l1, v1 = "LST" + units["LocalTime"], all_vars["LocalTime"]
        title = f_no + "_" + "CRefTime" + "_" + "LocalTime"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=0.0005))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l01, v01 = "StcAirTmp" + units["StcAirTmp"], all_vars["StcAirTmp"]
        l02, v02 = "ToAirTmp" + units["ToAirTmp"], all_vars["ToAirTmp"]
        l1, v1 = "Tpot", all_vars["Tpot"]
        title = f_no + "_" + "StcAirTmp" + "_" + "ToAirTmp" + "_" + "Tpot"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v01, color="b", label=l01 + ", left y")
        p1 = ax1.plot(x, v02, color="r", label=l02 + ", left y")
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(np.append(v01, v02), add_percent=5.0))
        ax1.tick_params("y", colors="k")
        ax2 = ax1.twinx()
        p2 = ax2.plot(x, v1, color="g", label=l1 + ", right y")
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1 + p2
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "TrueHead" + units["TrueHead"], all_vars["TrueHead"]
        l1, v1 = "WindDirTr" + units["WindDirTr"], all_vars["WindDirTr"]
        title = f_no + "_" + "TrueHead" + "_" + "WindDirTr"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(ylim_dgr)
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(ylim_dgr)
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "WindSpeed" + units["WindSpeed"], all_vars["WindSpeed"]
        l1, v1 = "TrAirSpeed" + units["TrAirSpeed"], all_vars["TrAirSpeed"]
        title = f_no + "_" + "WindSpeed" + "_" + "TrAirSpeed"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "AltitRate" + units["AltitRate"], all_vars["AltitRate"]
        l1, v1 = "AngOAttck" + units["AngOAttck"], all_vars["AngOAttck"]
        title = f_no + "_" + "AltitRate" + "_" + "AngOAttck"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        # ------------------------------------------------------------------------------
        l0, v0 = "VertSpeed" + units["VertSpeed"], all_vars["VertSpeed"]
        l1, v1 = "GndSpeed" + units["GndSpeed"], all_vars["GndSpeed"]
        title = f_no + "_" + "VertSpeed" + "_" + "GndSpeed"
        fig, ax1 = plt.subplots(figsize=(16, 10))
        plt.title(title)
        ax1.set_xlim(xrange)
        p0 = ax1.plot(x, v0, color="b", label=l0)
        ax1.set_xlabel("UTC[s]", color="k")
        ax1.set_ylim(get_plot_range(v0, add_percent=5.0))
        ax1.tick_params("y", colors="b")
        ax2 = ax1.twinx()
        p1 = ax2.plot(x, v1, color="g", label=l1)
        ax2.set_ylim(get_plot_range(v1, add_percent=5.0))
        ax2.tick_params("y", colors="g")
        plots = p0 + p1
        lbls = [l.get_label() for l in plots]
        plt.legend(plots, lbls, loc=0)
        plt.tight_layout()
        if plot_ts:
            ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
            plt.text(0.875, 0.01, ts, fontsize=8, transform=plt.gcf().transFigure)
        plt.show(block=False)
        if save_close_dpi_pdf[0]:
            if save_close_dpi_pdf[3]:
                pp.savefig(fig)
            else:
                fname = diagn_save_dir / title.replace(" ", "_")
                fig.savefig(
                    fname,
                    dpi=save_close_dpi_pdf[2],
                    facecolor="w",
                    edgecolor="w",
                    orientation="portrait",
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                )
            if save_close_dpi_pdf[1]:
                plt.close()

        if save_close_dpi_pdf[3]:
            pp.close()

    # ------------------------------------------------------------------------------
    #     STATISTICS     #
    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------
    if call_stat:
        # flight route
        for entry in na_1001.SCOM:
            if fnm(entry, "FlightRoute*"):
                fl_route = entry[0 : entry.find("#")].strip()
            # flight duration
            fl_dur = x[-1] - x[0]
            fl_dur_h = int((fl_dur - fl_dur % 3600) / 3600)
            fl_dur_min = int((fl_dur / 3600 - fl_dur_h) * 60)
            fl_dur_sec = int(round((((fl_dur / 3600 - fl_dur_h) * 60) - fl_dur_min) * 60))
        # container power on / shutdown
        con_po_sd = [-1, -1]
        con_po_sd[0] = (
            mdns_2_dtobj(float(x[0]), na_1001.DATE, posix=False)
            .replace(tzinfo=timezone.utc)
            .isoformat()
        )
        con_po_sd[1] = (
            mdns_2_dtobj(float(x[-1]), na_1001.DATE, posix=False)
            .replace(tzinfo=timezone.utc)
            .isoformat()
        )
        # flight distance
        R = 6373.0  # approximate radius of earth in km
        fl_dist_km = 0.0  # travelled distance
        for j in range(len(all_vars["PosLat"]) - 1):
            lat0 = radians(all_vars["PosLat"][j])
            lat1 = radians(all_vars["PosLat"][j + 1])
            lon0 = radians(all_vars["PosLong"][j])
            lon1 = radians(all_vars["PosLong"][j + 1])
            dlon = lon1 - lon0
            dlat = lat1 - lat0
            a = sin(dlat / 2) ** 2 + cos(lat0) * cos(lat1) * sin(dlon / 2) ** 2
            c = 2 * atan2(sqrt(a), sqrt(1 - a))
            fl_dist_km += R * c

        # prepare logfile
        crlf = "\n"
        sep = "\t"
        log = [
            "### Flight " + f_no[1:] + " Statistics ###" + crlf,
            "###" + crlf,
            fl_route + crlf,
            "Flight distance (km): " + "{:.1f}".format(fl_dist_km) + crlf,
            (
                "CARIBIC online (hh:mm:ss): "
                + "{:02d}".format(fl_dur_h)
                + ":"
                + "{:02d}".format(fl_dur_min)
                + ":"
                + "{:02d}".format(fl_dur_sec)
                + crlf
            ),
            "Container power on: " + con_po_sd[0] + crlf,
            "Container shut down: " + con_po_sd[1] + crlf,
            "Log entries total: " + str(len(x)) + crlf,
            "Log entries invalid/discarded: " + str(int(x[-1] - x[0]) - (len(x) - 1)) + crlf,
            "###" + crlf,
            "Parameter"
            + sep
            + "Min"
            + sep
            + "Max"
            + sep
            + "Mean"
            + sep
            + "Median"
            + sep
            + "n_valid"
            + sep
            + "completeness(%)"
            + crlf,
        ]

        vd_keys = [
            "PosLat",
            "PosLong",
            "TrueHead",
            "WindSpeed",
            "WindDirTr",
            "PitchAng",
            "RollAng",
            "StdAltitu",
            "BaroAltit",
            "TrAirSpeed",
            "ToAirTmp",
            "AltitRate",
            "StcAirTmp",
            "AngOAttck",
            "TotPres",
            "VertSpeed",
            "GndSpeed",
            "pstatic",
            "Tpot",
            "Altitude",
        ]

        for key in all_vars:
            if key in vd_keys:
                nvd = np.count_nonzero(np.isfinite(all_vars[key]))
                cmpl = nvd / len(x)
                log.append(
                    key
                    + units[key]
                    + sep
                    + "{:.3f}".format(np.nanmin(all_vars[key]))
                    + sep
                    + "{:.3f}".format(np.nanmax(all_vars[key]))
                    + sep
                    + "{:.3f}".format(np.nanmean(all_vars[key]))
                    + sep
                    + "{:.3f}".format(np.nanmedian(all_vars[key]))
                    + sep
                    + str(nvd)
                    + sep
                    + str(cmpl * 100)
                    + crlf
                )

        fname = diagn_save_dir / ("Flight_" + f_no[1:] + "_Statistics.txt")
        try:
            os.stat(os.path.dirname(fname))
        except OSError:
            os.mkdir(os.path.dirname(fname))

        with open(fname, "w", encoding="UTF-8") as file_obj:
            for line in log:
                file_obj.write(line)
