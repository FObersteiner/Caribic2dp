# -*- coding: utf-8 -*-
"""Meteo Summary (MS) tools."""
from pathlib import Path
import re

import numpy as np
from scipy.interpolate import interp1d

from pyfuppes.avgbinmap import bin_t_10s, bin_y_of_t


# ------------------------------------------------------------------------------


def load_moddata(dir_moddata, sep="\t", interp_10s=True, vmiss=9999999):
    r"""
    Load KNMI model data.

    Parameters
    ----------
    dir_moddata : TYPE
        DESCRIPTION.
    sep : TYPE, optional
        DESCRIPTION. The default is "\t".
    interp_10s : TYPE, optional
        DESCRIPTION. The default is True.
    vmiss : TYPE, optional
        DESCRIPTION. The default is 9999999.

    Raises
    ------
    FileNotFoundError
        DESCRIPTION.

    Returns
    -------
    moddata : TYPE
        DESCRIPTION.
    """
    # a mapping of possible input and output definition
    name_change = {
        "CC": "cc",
        "CC(0-1)": "cc",
        "CLWC(g/kg)": "clwc_g_kg_",
        "CLIC(g/kg)": "clic_g_kg_",
        "Temp(K)": "temp__k_",
        "PV(PVU)": "pv__pvu_",
        "Pot.Temp.(K)": "pot_temp__k_",
        "Eq.Pot.Temp.(K)": "eq_pott_temp__k_",
        "Eq.Pott.Temp.(K)": "eq_pott_temp__k_",
        "Eq.latitude(degN)": "eq_latitude_deg_n_",
        "Spec.Hum.(g/kg)": "spec_hum__g_kg_",
        "Spec.Hum.(e-3g/kg)": "spec_hum__g_kg_",
        "U(m/s)": "u__m_s_",
        "V(m/s)": "v__m_s_",
        "W(mubar/s)": "w__mubar_s_",
        "Windspeed(m/s)": "wind_speed__m_s_",
        "Winddir(deg)": "wind_dir__deg_",
        "H2O(ppmv)": "h2o__ppmv_",
        "RH(%)": "rh___",
        "Z(0.1*gm)": "z__0_1_g_m_",
        "P_STrop(hPa)": "p_strop__hpa_",
        "P_DTrop(hPa)": "p_dtrop__hpa_",
        "T_STrop(K)": "t_strop__k_",
        "trop(K)": "t_strop__k_",
        "tstrop(K)": "t_strop__k_",
        "T_DTrop(K)": "t_dtrop__k_",
        "tdtrop(K)": "t_dtrop__k_",
        "PT_STrop(K)": "pt_strop__k_",
        "PT_DTrop(K)": "pt_dtrop__k_",
        "PV_STrop(PVU)": "pv_strop__pvu_",
        "Z_STrop(0.1*gm)": "z_strop__01grav_m_",
        "Z_STrop(0.1gravm)": "z_strop__01grav_m_",
        "Z_DTrop(0.1*gm)": "z_dtrop__01grav_m_",
        "Z_DTrop(0.1gravm)": "z_dtrop__01grav_m_",
        "DP_STrop(hPa)": "dp_strop__hpa_",
        "DP_DTrop(hPa)": "dp_dtrop__hpa_",
    }

    patterns = {
        "search_str": (
            r"trackCLcl_(.*).dat",
            r"trackMLpd_(.*).dat",
            r"trackMLpd_(.*).dat_eqlat",
            r"trackTP_(.*).dat",
        ),
        "datatype": ("trackCLcl", "trackMLpd", "trackMLpd_eqlat", "trackTP"),
        "input": [None, None, None, None],
        "output": "",
    }

    try:
        dir_test = dir_moddata.is_dir()
    except AttributeError:
        dir_moddata = Path(dir_moddata)
        dir_test = dir_moddata.is_dir()

    if not dir_test:
        raise FileNotFoundError(f"not found: {dir_moddata}")

    content = list(dir_moddata.glob("*"))

    for i, p in enumerate(patterns["search_str"]):
        for j, c in enumerate(content):
            if re.fullmatch(p, c.name):
                patterns["input"][i] = c

    if not any(patterns["input"]):
        return None

    moddata, moddata["source_info"] = {}, patterns

    for i, file in enumerate(patterns["input"]):
        if not file:
            continue

        with open(file, "r", encoding="UTF-8") as file_obj:
            tmp = file_obj.readlines()

        # datatype is the type of model data; CL, ML TP or eqLat
        datatype = patterns["datatype"][i]
        moddata[datatype] = {}

        # see if first two lines of model data file split with same result
        line0, line1 = tmp[0].strip().rsplit(sep=sep), tmp[1].strip().rsplit(sep=sep)
        if len(line0) == len(line1):
            # header line seems to contain correct number of tabs, so we can use it
            header = line0
        else:
            # header line is messed up;
            # clean header: leave only spaces and make sure there is a space after each closing bracket:
            header = tmp[0].strip().replace(")", ") ").replace("\t", " ")
            # now a regex should be able to extract variable names...
            header = [header.split(" ")[0]] + re.findall(r"\w+\ \(\S+\ ?\w*\)", header)
            assert len(header) == len(
                line1
            ), "number of varnames doesn't match with number of parameters"

        # now we have the names of the variables in the file; remove spaces
        variables = [varname.replace(" ", "") for varname in header]

        for varname in variables:
            moddata[datatype][varname] = []

        for ix, line in enumerate(tmp[1:]):
            line = line.strip().rsplit(sep=sep)
            for ix_el, element in enumerate(line):
                if ix_el > 0:  # first col should stay string
                    element = float(element)
                moddata[datatype][variables[ix_el]].append(element)

        # make sure we do not have duplicate entries for independent variable
        u, cts = np.unique(np.array(moddata[datatype]["UT(h)"]), return_counts=True)
        if u.size != len(moddata[datatype]["UT(h)"]):
            for ix_dupe in np.where(cts != 1)[0]:
                for varname in variables:
                    moddata[datatype][varname].pop(ix_dupe)

        moddata[datatype]["UTC[s]"] = [x * 3600 for x in moddata[datatype]["UT(h)"]]
        moddata[datatype]["10s_info"] = bin_t_10s(
            np.array(moddata[datatype]["UTC[s]"], dtype=float), drop_empty=False
        )

        varnames = list(moddata[datatype].keys())
        # change dict keys to the variable names used in nasa ames file
        if name_change:
            for have, want in name_change.items():
                if have in varnames:
                    moddata[datatype][want] = moddata[datatype].pop(have)
            # for j in range(0, len(name_change), 2):
            #     if name_change[j] in varnames:
            #         name = varnames[varnames.index(name_change[j])]
            #         moddata[datatype][name_change[j + 1]] = moddata[datatype].pop(name)
            varnames = list(moddata[datatype].keys())

        # list of keys that should not appear in the dictionary that contains the
        # interpolated or binned values:
        v_ivd = [
            "UTC[s]",
            "UT(min)",
            "UT(h)",
            "yymmdd",
            "10s_info",
            "10s_binned",
            "10s_interp",
            "EMPTYcolumn",
        ]
        for v in v_ivd:
            if v in varnames:
                varnames.pop(varnames.index(v))

        # bin to 10s intervals if interp_10s is False, else interpolate linearly
        if not interp_10s:  # use binning
            patterns["output"], moddata[datatype]["10s_binned"] = "10s_binned", {}
            moddata[datatype]["10s_binned"]["UTC[s]"] = moddata[datatype]["10s_info"]["t_binned"]
            for name in varnames:
                moddata[datatype]["10s_binned"][name] = bin_y_of_t(
                    np.array(moddata[datatype][name], dtype=float),
                    moddata[datatype]["10s_info"],
                )
        else:
            patterns["output"], moddata[datatype]["10s_interp"] = "10s_interp", {}
            moddata[datatype]["10s_interp"]["UTC[s]"] = moddata[datatype]["10s_info"]["t_binned"]
            for name in varnames:
                f_ip = interp1d(
                    moddata[datatype]["UTC[s]"],
                    moddata[datatype][name],
                    kind="linear",
                    fill_value=vmiss,
                    bounds_error=False,
                )
                moddata[datatype]["10s_interp"][name] = f_ip(
                    moddata[datatype]["10s_interp"]["UTC[s]"]
                )

    return moddata
