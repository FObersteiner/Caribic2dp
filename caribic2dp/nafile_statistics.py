# -*- coding: utf-8 -*-
"""Collect NASA Ames file statistics and write to log files."""
import os
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.timeconversion import mdns_2_dtobj

from caribic2dp.carfileutils import find_files, dissect_filename


# -----------------------------------------------------------------------------


def naFileLog(file, crlf="\n", sep="\t"):
    r"""
    TODO.

    Parameters
    ----------
    file : TYPE
        DESCRIPTION.
    crlf : TYPE, optional
        DESCRIPTION. The default is "\n".
    sep : TYPE, optional
        DESCRIPTION. The default is "\t".

    Returns
    -------
    log : TYPE
        DESCRIPTION.
    """
    na_1001 = na1001(file)
    all_vars = {}
    units = {}
    for ix, element in enumerate(na_1001.VNAME):
        unt_tmp = (element.rsplit(sep=";")[2]).strip()
        if "#" in unt_tmp:
            unt_tmp = unt_tmp[0 : unt_tmp.find("#") - 1]
        units[element.rsplit(sep=";")[0]] = unt_tmp
        all_vars[element.rsplit(sep=";")[0]] = np.array(na_1001.V[ix], dtype="float64")
        w_invd = np.where(all_vars[element.rsplit(sep=";")[0]] == float(na_1001.VMISS[ix]))
        if len(all_vars[element.rsplit(sep=";")[0]][w_invd]) > 0:
            all_vars[element.rsplit(sep=";")[0]][w_invd] = np.nan

    log = [
        "### NA file Statistics ###" + crlf,
        "### fname: " + os.path.basename(file) + crlf,
        (
            "Parameter"
            + sep
            + "Min"
            + sep
            + "Max"
            + sep
            + "Mean"
            + sep
            + "Median"
            + sep
            + "n_valid"
            + sep
            + "completeness(%)"
            + crlf
        ),
    ]

    for key in all_vars:
        nvd = np.count_nonzero(np.isfinite(all_vars[key]))
        cmpl = 0 if nvd == 0 else nvd / len(na_1001.X)

        log.append(
            key
            + units[key]
            + sep
            + "{:.3f}".format(np.nanmin(all_vars[key]))
            + sep
            + "{:.3f}".format(np.nanmax(all_vars[key]))
            + sep
            + "{:.3f}".format(np.nanmean(all_vars[key]))
            + sep
            + "{:.3f}".format(np.nanmedian(all_vars[key]))
            + sep
            + str(nvd)
            + sep
            + str(cmpl * 100.0)
            + crlf
        )

    return log


# -----------------------------------------------------------------------------


def multiNaFileStat(
    file_prefix,
    f0=537,
    f1=537,
    flights_dir="//IMK-ASF-CARFS/Caribic/extern/Caribic2data/Flights",
):
    """
    TODO misses handling of 10s vs. full res files

    Parameters
    ----------
    file_prefix : TYPE
        DESCRIPTION.
    f0 : TYPE, optional
        DESCRIPTION. The default is 537.
    f1 : TYPE, optional
        DESCRIPTION. The default is 537.
    flights_dir : TYPE, optional
        DESCRIPTION. The default is "//IMK-ASF-CARFS/Caribic/extern/Caribic2data/Flights".

    Returns
    -------
    appended : TYPE
        DESCRIPTION.
    """
    # exclude_str = ''
    flights_dir = Path(flights_dir)

    flightrange = [f0, f1]

    appended = {"flightrange": flightrange}

    files = []
    for i in range(flightrange[0], flightrange[1] + 1):
        file = find_files(i, file_prefix, flights_dir, resolution="all")
        if file:
            files.append(file[0])

    for i, file in enumerate(files):
        print("processing: ", str(file))
        na_1001 = na1001(file)
        flarr = np.full(len(na_1001.X), dissect_filename(file).flight_no)
        ixarr = np.linspace(0, len(na_1001.X) - 1, len(na_1001.X), dtype="int64")
        dtarr = mdns_2_dtobj(na_1001._X, na_1001.DATE)

        all_vars = {}
        units = {}
        for j, element in enumerate(na_1001.VNAME):
            unt_tmp = (element.rsplit(sep=";")[2]).strip()
            if "#" in unt_tmp:
                unt_tmp = unt_tmp[0 : unt_tmp.find("#") - 1]
            units[element.rsplit(sep=";")[0]] = unt_tmp
            all_vars[element.rsplit(sep=";")[0]] = np.array(na_1001.V[j], dtype="float64")
            w_invd = np.where(all_vars[element.rsplit(sep=";")[0]] == float(na_1001.VMISS[j]))
            if len(all_vars[element.rsplit(sep=";")[0]][w_invd]) > 0:
                all_vars[element.rsplit(sep=";")[0]][w_invd] = np.nan

        if i == 0:
            appended["vars"] = all_vars
            appended["units"] = units
            appended["flno"] = flarr
            appended["ix"] = ixarr
            appended["t"] = dtarr
        else:
            appended["flno"] = np.append(appended["flno"], flarr)
            appended["ix"] = np.append(appended["ix"], ixarr)
            appended["t"] += dtarr
            for key in appended["vars"]:
                appended["vars"][key] = np.append(appended["vars"][key], all_vars[key])

    print("Parameter | Min (fl_no) | Max (fl_no) | Mean | Median")
    for var in appended["vars"]:
        vmax = np.nanmax(appended["vars"][var])
        vmin = np.nanmin(appended["vars"][var])
        w_vmax = np.where(appended["vars"][var] == vmax)
        w_vmin = np.where(appended["vars"][var] == vmin)
        fl_vmax = appended["flno"][w_vmax]
        fl_vmin = appended["flno"][w_vmin]
        print(
            var + appended["units"][var] + " |",
            "{:.3f}".format(vmin),
            "(" + str(fl_vmin[0]) + ") |",
            "{:.3f}".format(vmax),
            "(" + str(fl_vmax[0]) + ") |",
            "{:.3f}".format(np.nanmean(appended["vars"][var])) + " |",
            "{:.3f}".format(np.nanmedian(appended["vars"][var])),
        )

    return appended

    # t_select = appended['t'][np.asscalar(w_vmax[0])]
    # dt_arr = np.array([abs((t-t_select).total_seconds()) for t in appended['t']])
    # ix_select = np.argmin(dt_arr)


#############

#    fig, ax1 = plt.subplots(figsize=(16, 9))
#
#    v0 = np.linspace(0, len(appended['t'])-1, len(appended['t']))
#    v1 = appended['vars']['Ozone']
#
#    cm = plt.cm.get_cmap('jet')
#
#    p0 = ax1.scatter(v0, v1, s=1, c=v1, cmap=cm,
#                     label='O3')
#
#    ax1.set_xlabel('n', color='k')
#    ax1.set_ylabel('O3[ppb]', color='k')
#    ax1.set_ylim([0, 1100])
#    ax1.set_xlim([0, max(v0)])
#
#    plt.tight_layout()
#    plt.grid()
#    plt.show(block=False)
#

############## Flight Level Histogram


# -----------------------------------------------------------------------------


def plot_flightdata_hist(data):
    """
    TODO.

    Parameters
    ----------
    data : TYPE
        DESCRIPTION.

    Returns
    -------
    None.
    """
    flightrange = data["flightrange"]
    # fllv = data['vars']['StdAltitu'] / 100
    alt_km = data["vars"]["StdAltitu"] * 0.3048 / 1000

    fig, ax1 = plt.subplots(figsize=(16, 9))
    # n, bins, patches = plt.hist(fllv, range(5, int(max(fllv))+10, 10),
    #                             density=1, facecolor='b', alpha=0.75,
    #                             edgecolor='black', linewidth=1)
    n, bins, patches = plt.hist(
        alt_km,
        range(5, int(max(alt_km)) + 1, 1),
        density=1,
        facecolor="b",
        alpha=0.75,
        edgecolor="black",
        linewidth=1,
    )

    plt.xlim([0, int(max(alt_km)) + 1])
    # plt.xlabel('Flight Level')
    plt.xlabel("Flight Altitude [km]", fontsize=12, fontweight="bold")
    plt.ylabel("Probability", fontsize=12, fontweight="bold")
    plt.title(
        f"Caribic F{flightrange[0]}-{flightrange[1]}, Median: {np.nanmedian(alt_km):.0f} km",
        fontsize=18,
        fontweight="bold",
    )
    ax1.tick_params(labelcolor="k", labelsize=12, width=3)

    # for tick in ax1.xaxis.get_major_ticks():
    #     tick.label.set_fontsize(12)

    #    plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    fname = f"D:/KIT/Dataprocessing_Caribic/MA_data/flight_statistics/F{flightrange[0]}to{flightrange[1]}_FL_ALT_Hist.png"
    fig.savefig(
        fname,
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        papertype=None,
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )


# -----------------------------------------------------------------------------


def plot_latlon_mesh(data):
    """
    TODO.

    Parameters
    ----------
    data : TYPE
        DESCRIPTION.

    Returns
    -------
    None.
    """
    flightrange = data["flightrange"]

    vd_ix = np.nonzero(np.isfinite(data["vars"]["PosLat"]) & np.isfinite(data["vars"]["PosLong"]))
    lat = data["vars"]["PosLat"][vd_ix]
    lng = data["vars"]["PosLong"][vd_ix]

    fig, ax = plt.subplots()
    ax.set_title(f"F{flightrange[0]}-{flightrange[1]} lat/lon coverage (10s data)")

    nbins = 30
    h = ax.hist2d(lng, lat, bins=nbins, norm=LogNorm())

    ax.set_xlim((min(lng) - 10, max(lng) + 10))
    ax.set_ylim((min(lat) - 5, max(lat) + 5))

    ax.set_xlabel("longitude")
    ax.set_ylabel("latitude")

    plt.colorbar(h[3], ax=ax)

    plt.colorbar()
    plt.show()

    # hist, xedges, yedges = np.histogram2d(lng, lat, bins=(xedges, yedges))
    # hist[hist < 0.0001] = np.nan
    # fig, ax = plt.subplots()
    # ax.imshow(hist)#, extent=[min(lng), max(lng), min(lat), max(lat)])
    # plt.show()

    # from scipy import stats
    # ret = stats.binned_statistic_2d(lat, lng, lat, 'count', bins=[xedges,yedges])
    # fig, ax = plt.subplots()
    # ax.imshow(ret[0])#, extent=[min(lng), max(lng), min(lat), max(lat)])
    # plt.show()


#     from mpl_toolkits.basemap import Basemap, cm
#     import matplotlib.pyplot as plt

#     import numpy as np

#     m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
#                 llcrnrlon=-180,urcrnrlon=180,resolution='c')

#     m.drawcoastlines()
#     m.drawlsmask(land_color='white',ocean_color='white',lakes=True)
#     m.drawparallels([-90,-60,-30,0,30,60,90],labels=[1,0,0,0],fontsize=10)
#     m.drawmeridians([-180,-120,-60,0,60,120,180],labels=[0,0,0,1],fontsize=10)
#     m.drawmapboundary(fill_color='aqua')

#     img = m.imshow(h[0], cmap='jet' )

#     m.colorbar(img)
#     plt.show()
