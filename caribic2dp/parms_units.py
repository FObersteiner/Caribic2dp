# -*- coding: utf-8 -*-
"""CARIBIC parameters and units."""
from pyfuppes.txt2dict import txt_2_dict as t2d

try:
    import tomllib as _toml  # Python 3.11+
except ImportError:
    # https://github.com/hukkin/tomli
    import tomli as _toml  # let's assume this works the same as the 3.11 lib...

# ------------------------------------------------------------------------------


def vname_2_ncom_tblock(
    vnames,
    param_file,
    header_config_file,
    out_type="MS",
    exception_line="0",
    set_vmiss=False,
) -> list[str]:
    r"""
    From a list of variable names (VNAMEs), generate the terminal text block of the normal comment line field (NCOM).

    Parameters
    ----------
    vnames : list of string
        list if parameters.
    header_config_file : pathlib.Path
        CARIBIC NASA Ames file header config (defaults)
    param_file : pathlib.
        Parameters config file. The default is "".
    out_type : str, optional
        SU or MS. The default is "MS".
    set_vmiss : any except boolean, optional
        Override missing value. The default is False (use value from parameters config file).

    Returns
    -------
    ncom_terminal_block : list of string
        CARIBIC NASA Ames file NCOM last block.
    """
    with open(header_config_file, "rb") as fp:
        hdr_cfg = _toml.load(fp)

    if ";" in vnames[0]:
        for ix, name in enumerate(vnames):
            vnames[ix] = name.split(";")[0].strip()

    pdict = t2d(param_file, delimiter=";").data
    if set_vmiss is not False:  # could use None ?!
        for i, _ in enumerate(pdict["VMISS"]):  # type: ignore
            pdict["VMISS"][i] = str(set_vmiss)  # type: ignore

    pdict_ix = []  # index of variable in param_file
    for name in vnames:
        if name in pdict["VNAME"]:  # type: ignore
            pdict_ix.append(pdict["VNAME"].index(name))  # type: ignore
        else:
            raise ValueError(f"CARIBIC_parms_units -> variable '{name}' not found")

    if len(vnames) != len(pdict_ix):
        raise ValueError("CARIBIC_parms_units -> Failed: could not find all variables!")

    allowed_outtypes = ("MA", "MS", "SU")
    if out_type not in allowed_outtypes:
        raise ValueError("invalid output type {out_type}, must be one of {allowed_outtypes}")

    ncom_terminal_block = []

    ### master computer file
    if out_type in ["MA"]:
        ncom_terminal_block = [
            "this file was generated using dedicated Python code, introduced in August 2018 #version#.",
            "# ",
            hdr_cfg["NCOM_CFinfo"],
            "CF_StandardNames: time\t",  # x-axis is pre-set
            "CF_Units: [s]\t",  # x-axis is pre-set
            hdr_cfg["NCOM_final_words"],
            "#fldate#\t",  # VSCAL, x-axis is pre-set
            f"{exception_line}\t",  # VMISS, x-axis is pre-set
            "[s]\t",  # Unit, x-axis is pre-set
            "TimeCRef\t",  # VNAME, x-axis is pre-set
        ]

        repl_ix = [3, 4, 6, 7, 8, 9]
        repl_keys = [
            "CF_StandardName",
            "CF_Unit",
            "VSCAL",
            "VMISS",
            "Unit",
            "VNAME",
        ]

    ### summary and meteo summary files
    if out_type in ["MS", "SU"]:
        ncom_terminal_block = [
            "#software_spec#",  # 0 # this line has to be updated by calling script
            "# ",  # 1
            "The following line contains information about the lower detection limit of the instrument"
            " for the respective column: A number represents the lower detection limit (given in the units"
            " of the column, unaffected by the column's scaling factor). A column name refers to the column containing the"
            ' lower detection limit. "[]" means that no lower detection limit is given.',  # 2
            "LowerDetectionLimit:\t",  # 3
            "Size bin information: The next three lines contain (all values in nanometers): lower limit"
            " of the respective size channel; upper limit of the respective size channel; channel center of the"
            " respective size channel (it is calculated as square root(BinLowLimit*BinUpLimit))."
            ' "[]" means that no limit is given.',  # 4
            "BinUpLimit:\t",  # 5
            "BinCenter:\t",  # 6
            "BinLowLimit:\t",  # 7
            hdr_cfg["NCOM_CFinfo"],  # 8
            "CF_StandardNames: time\t",  # 9 # x-axis is pre-set
            "CF_Units: [s]\t",  # 10 # x-axis is pre-set
            hdr_cfg["NCOM_final_words"],  # 11
            "#fldate#\t",  # 12 # VSCAL, x-axis is pre-set # this line has to be updated by calling script
            f"{exception_line}\t",  # 13 # VMISS, x-axis is pre-set
            "[s]\t",  # 14 # Unit, x-axis is pre-set
            "TimeCRef\t",  # 15 # VNAME, x-axis is pre-set
        ]

        repl_ix = [3, 5, 6, 7, 9, 10, 12, 13, 14, 15]
        repl_keys = [
            "LowerDetectionLimit",
            "BinUpLimit",
            "BinCenter",
            "BinLowLimit",
            "CF_StandardName",
            "CF_Unit",
            "VSCAL",
            "VMISS",
            "Unit",
            "VNAME",
        ]

    if ncom_terminal_block:
        # for each line in NCOM which should be extended...
        for idx_NCOM, repl_key in zip(repl_ix, repl_keys):  # type: ignore
            # for each variable (VNAME)...
            # print(f"updating {repl_key} at index {idx_NCOM} in ncom")
            ncom_terminal_block[idx_NCOM] += "\t".join(
                pdict[repl_key][idx_var] for idx_var in pdict_ix  # type: ignore
            )

    return ncom_terminal_block
