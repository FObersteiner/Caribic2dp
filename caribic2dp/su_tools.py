# -*- coding: utf-8 -*-
"""Aux functions to process summary data (SU)."""
from pyfuppes.geo import sza, sza_pysolar
from pyfuppes.timeconversion import mdns_2_dtobj


# ------------------------------------------------------------------------------


def calc_SZA(data: dict, vmiss_MA=999999, vmiss_SU=9999999, use_pysolar=False) -> list:
    """
    Calculate solar zenith angle for CARIBIC SU files.

    Parameters
    ----------
    data : dict
        dict with master data.
    vmiss_MA : int, optional
        master VMISS. The default is 999999.
    vmiss_SU : int, optional
        SU VMISS. The default is 9999999.
    use_pysolar : bool, optional
        The default is False.

    Raises
    ------
    ValueError
        latitude and/or longitued not found in data.

    Returns
    -------
    list
        SZA for all lat/lon tuples.
    """
    MA_data = data["MA"]
    names = MA_data.VNAME
    ix_lat, ix_lon = False, False

    for ix, name in enumerate(names):
        if name.startswith("PosLat"):
            ix_lat = ix
        if name.startswith("PosLong"):
            ix_lon = ix

    if not (ix_lat and ix_lon):
        raise ValueError("lat/lon not found!")

    latitude = [float(v) for v in MA_data.V[ix_lat]]
    longitude = [float(v) for v in MA_data.V[ix_lon]]
    time = mdns_2_dtobj(MA_data.X, MA_data.DATE)

    result = []

    for lat, lon, t in zip(latitude, longitude, time):  # type: ignore
        if lat == vmiss_MA or lon == vmiss_MA:
            result.append(vmiss_SU)
        else:
            if use_pysolar:
                result.append(sza_pysolar(UTC=t, latitude=lat, longitude=lon))
            else:
                result.append(sza(UTC=t, latitude=lat, longitude=lon))

    return result
