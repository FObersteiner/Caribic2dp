# -*- coding: utf-8 -*-
from functools import lru_cache
from xml.sax import ContentHandler, make_parser


def normalize_whitespace(text):
    """Remove redundant whitespace from a string."""
    return " ".join(text.split())


class ConstructDict(ContentHandler):
    """Parse the xml standard_name table, reading all entries into a dictionary;
    storing standard_name and units.

    Code adopted from https://github.com/cedadev/cf-checker
    """

    def __init__(
        self,
    ):
        self.inUnitsContent = 0
        self.inEntryIdContent = 0
        self.inVersionNoContent = 0
        self.inLastModifiedContent = 0
        self.current = False
        self.dict = {}

    def startElement(self, name, attrs):
        # If it's an entry element, save the id
        if name == "entry":
            id = normalize_whitespace(attrs.get("id", ""))
            self.this_id = str(id)

        # If it's the start of a canonical_units element
        elif name == "canonical_units":
            self.inUnitsContent = 1
            self.units = ""

        elif name == "alias":
            id = normalize_whitespace(attrs.get("id", ""))
            self.this_id = str(id)

        elif name == "entry_id":
            self.inEntryIdContent = 1
            self.entry_id = ""

        elif name == "version_number":
            self.inVersionNoContent = 1
            self.version_number = ""

        elif name == "last_modified":
            self.inLastModifiedContent = 1
            self.last_modified = ""

    def characters(self, ch):
        if self.inUnitsContent:
            self.units = self.units + ch

        elif self.inEntryIdContent:
            self.entry_id = self.entry_id + ch

        elif self.inVersionNoContent:
            self.version_number = self.version_number + ch

        elif self.inLastModifiedContent:
            self.last_modified = self.last_modified + ch

    def endElement(self, name):
        # If it's the end of the canonical_units element, save the units
        if name == "canonical_units":
            self.inUnitsContent = 0
            self.units = normalize_whitespace(self.units)
            self.dict[self.this_id] = self.units

        # If it's the end of the entry_id element, find the units for the self.alias
        elif name == "entry_id":
            self.inEntryIdContent = 0
            self.entry_id = str(normalize_whitespace(self.entry_id))
            try:
                self.dict[self.this_id] = self.dict[self.entry_id]
            except KeyError:
                self._add_warn(
                    "Error in standard_name table:  entry_id '%s' not found. "
                    "Please contact Rosalyn Hatcher (r.s.hatcher@reading.ac.uk)" % self.entry_id
                )

        # If it's the end of the version_number element, save it
        elif name == "version_number":
            self.inVersionNoContent = 0
            self.version_number = normalize_whitespace(self.version_number)

        # If it's the end of the last_modified element, save the last modified date
        elif name == "last_modified":
            self.inLastModifiedContent = 0
            self.last_modified = normalize_whitespace(self.last_modified)


@lru_cache(maxsize=2)
def load_names(url: str):
    std_names = ConstructDict()
    parser = make_parser()
    parser.setContentHandler(std_names)

    # TODO : collect some version of that xml and use it if URL is not reachable
    parser.parse(url)
    return std_names.dict
