36 1001
Dieter Scharffe; Carl Brenninkmeijer; Franz Slemr; Claus Koeppel; d.scharffe@mpic.de
Max-Planck-Institut fuer Chemie; Chemie der Atmosphaere; Hahn-Meitner-Weg 1; 55128 Mainz; Germany
Fictitious Data of Fictitious ExampleInstrument
Project CARIBIC2 (since 2013 IAGOS-CARIBIC)
1 1
2013 11 09 2013 11 12
10
TimeCRef; CARIBIC_reference_time_since_0_hours_UTC_on_first_date_in_line_7; [s] # Due to delay times in the communication with the Master PC, the time given here might deviate from CARIBIC reference time by up to 3 seconds.
4
1 100 1 1
9 9.99 999.9 999.9
IStatus; Instrument status; [1] # status of the instrument. Possible are the numbers 1, 2, 3 with the following meaning: 1 -> instrument was in standby-mode because no measurement permission by the Master; 2 -> instrument was measuring properly; 3 -> some error occured
Temperature; absolute temperature inside the instrument; [K] # The sensor is mounted inside the instrument, close to the readout electronics
Species1MR; MolarMixingRatio of Species1; [ppb] # The mixing ratio is given in NanoMol per Mol.
Species1Err; StatisticalError of Species1MR; [ppb] # Error estimation based on lab studies, cf. page 25ff of logbook for details.
8
FileFormatVersionInfo: CARIBIC_NAmes_v02 standard # This file was created according to the conventions for CARIBIC NASA Ames data files, given by 'Info_Nasa_Ames_Format_02.zip'.
FlightNo: 452
FlightRoute: BKK FRA
DepartureAirport: Bangkok
ArrivalAirport: Frankfurt
FileName: ExampleFile2.txt # name of this file; e.g. "EX_20131109_452_BKK_FRA_10s_V01.txt"
ExceptionToConsider: no # Nothing special happened, although the measured mixing ratios seem to be a bit lower than during the previous flights.
ChangeLog: 0
10
Data have been averaged over 10s intervals; the given time is the respective center the interval.
# 
The following two lines contain standard names and units according to the 'NetCDF Climate and Forecast (CF) Metadata Convention', see 'cf-pcmdi.llnl.gov'. Each entry (name or unit) corresponds to a certain column; the entry before the first tab belongs to the independent column.
CF_StandardNames: time	sensor_for_Species1 status_flag	temperature_of_sensor_for_Species1	mole_fraction_of_Species1_in_air	mole_fraction_of_Species1_in_air standard_error
CF_Units: [s]	[1]	[K]	[1e-9]	[1e-9]
The following four lines contain:  Start date like in line 7 (formatted as YYYYMMDD), followed by the multiplicators like in line 11;  Line number containing the KeyLabel 'ExceptionToConsider' (0 if no such exception is mentioned), followed by the NaN-values like in line 12;  Units like in lines 13ff;  Column headers.
20131109	1	100	1	1
0	9	9.99	999.9	999.9
[s]	[1]	[K]	[ppb]	[ppb]
TimeCRef	IStatus	Temperature	Species1MR	Species1Err
64825	1	2.90	999.9	999.9
64835	2	2.93	413.5	4.7
64845	2	2.95	418.7	4.5
64855	2	2.96	414.4	4.7
64865	2	2.96	411.4	4.9
64875	3	9.99	999.9	999.9
64885	3	9.99	999.9	999.9
64895	2	2.95	408.2	5.3
64905	2	2.96	402.6	5.1
