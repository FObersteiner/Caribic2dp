﻿-------------------------------------------------------------
CARIBIC-NASA-Ames-File Conventions 'Info_Nasa_Ames_Format_03'
-------------------------------------------------------------

The description is intended to give advices and examples how to make proper NASA Ames files according to CARIBIC conventions.
It is the successor of 'Info_Nasa_Ames_Format_01.doc' from 2007-01-23.

Content of the zip-file:
  + "Description.odt" and "Description.pdf"
        Description of the conventions for creating CARIBIC-NASA-Ames-Files.
        The file "Description.odt" and its pdf-version "Description.pdf" have been created with OpenOffice version 3.4.1.
  + "ExampleFile1.txt", ..., "ExampleFile8.txt"
        Examples for CARIBIC-NASA-Ames files, which can be used as template or for inspiration.
  + "NoS_airports.txt"
	Contains list of airports
        The file is an example for a non-standard CARIBIC-NASA-Ames file including text columns
  + "NoS_StandardNames.txt"
	Contains list of CF standard names
        The file is an example for a non-standard CARIBIC-NASA-Ames file including text columns

________________________________________________________________________________________________________
created in 2014 by Hella Riede, Dieter Scharffe, Armin Rauthe-Schöch, David Walter
last modification of this file: 2022-12-06, F.Obersteiner 
