#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 08:45:59 2022

@author: Florian Obersteiner, f.obersteiner@kit.edu
"""
import os
import sys
import subprocess
import shutil
from pathlib import Path
from datetime import datetime

try:
    wd = __file__
    print(f"using {wd} as initial path")
except NameError:
    wd = sys.path[0]
    if ".pyenv" in wd:
        # interpreter path...
        wd = os.getcwd()
    print(f"failed to access __file__, using {wd} as path")

wd = Path(wd).parent

print(f"calling sphinx in {wd}...")

# make new markdown files
subprocess.run("make clean html", shell=True, check=True)

# clean existing directory
for filename in (wd / "api_ref_html").glob("*"):
    if filename.name == "README.md":
        continue
    try:
        if os.path.isfile(filename) or os.path.islink(filename):
            os.unlink(filename)
        elif os.path.isdir(filename):
            shutil.rmtree(filename)
    except Exception as e:
        print("Failed to delete %s. Reason: %s" % (filename, e))

# copy newly created docs to public directory
shutil.copytree(wd / "_build/html", wd / "api_ref_html", dirs_exist_ok=True)

print(f"\n{datetime.now().astimezone(None).isoformat(timespec='seconds')}")
