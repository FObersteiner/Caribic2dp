import sys
from pathlib import Path
from datetime import datetime
import seedir as sd

try:
    wd = __file__
    print(f"using {wd} as path")
except NameError:
    wd = sys.path[0]

    if ".pyenv" in wd:
        # interpreter path
        import os

        wd = os.getcwd()
    print(f"failed to access __file__, using {wd} as path")

wd = Path(wd).parents[1]
print(f"using {wd} to call seedir...")

print("")
sd.seedir(
    path=wd.as_posix(),
    style="emoji",  # "lines",
    first="folders",
    sort=True,
    itemlimit=20,
    depthlimit=10,
    exclude_files=[".gitignore", "README.md", "__init__.py"],
    exclude_folders=[
        "tests",
        "docs",
        "dist",
        ".git",
        ".vscode",
        ".old",
        ".pytest_cache",
        ".spyproject",
        "__pycache__",
    ],
    # regex=True,
)
print(f"\n{datetime.now().astimezone(None).isoformat(timespec='seconds')}")
