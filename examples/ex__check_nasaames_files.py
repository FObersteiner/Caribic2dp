# -*- coding: utf-8 -*-
"""
Validate CARIBIC NASA Ames format.

Check CARIBIC-specific parameters such as information on departure and arrival airport.

Check additional information such as CF units and standard-names.
"""
import sys
from pathlib import Path  # to handle file paths

from caribic2dp import carfileutils
from caribic2dp.check_caribic_na import validate

# must specify where to look for flight data
# assume there is only one non-Windows user
flights_dir = Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/Flights"
if sys.platform == "win32":
    flights_dir = Path("//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/")


# -----------------------------------------------------------------------------


prefix = "MA"
flight = 591

src = carfileutils.find_files(flight, prefix, flights_dir, resolution="10s")[0]
print(src)
# >>> ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/MA_20200304_591_CPT_MUC_10s_V01.txt

#####
# 1) only check if NASA Ames format is ok, no CARIBIC-specific checks:
result = validate(src, check_caribic2=False)

# result has a 'msg' field that contains check results
print(result.msg)
# >>> check MA_20200304_591_CPT_MUC_10s_V01.txt: OK (no CARIBIC-specific constraints checked)

# result also has a 'na1001' field, that contains the data if the format check was ok:
print(result.na1001)
# >>>
# NASA Ames 1001
# SRC: .../MA_20200304_591_CPT_MUC_10s_V01.txt
# ---
# ONAME: F. Obersteiner; H. Boenisch; T. Gehrlein; A. Zahn; florian.obersteiner@kit.edu
# ORG: Institute of Meteorology and Climate Research (IMK), Karlsruhe Institute of Technology (KIT), 76021 Karlsruhe, P.O. Box 3640, Germany
# SNAME: ARINC Data from Aircraft during IAGOS-CARIBIC (onboard Airbus A340-600 HE of Lufthansa)
# DATE: [2020, 3, 4], RDATE: [2020, 3, 6]


#####
# 2) enable CARIBIC-specific checks
# we must specify an airports configuration file first.

ap_file = Path().home() / "Code/Python/Caribic2_DataProc/config/CARIBIC2_Airports.csv"
assert ap_file.is_file(), "failed to load airports config"

result = validate(src, airports_file=ap_file, check_caribic2=True)

print(result.msg)
# >>> check MA_20200304_591_CPT_MUC_10s_V01.txt: OK


#####
# 3) enable CF checks
# Note: this requires the cfunits package to be set up correctly, including lib udunits2 available
#       on the system. Also, this might still fail when called via Spyder IDE's IPython console.
result = validate(src, airports_file=ap_file, check_caribic2=True, cf_checks=True)

print(result.msg)
# CF checks are very strickt...
# >>>
# check MA_20200304_591_CPT_MUC_10s_V01.txt:
#     nok CF standard name for variable 4:
#         'platform_status_flag' is not a CF standard name
#     nok CF standard name for variable 15:
#         'air_total_temperature' is not a CF standard name
#     nok CF standard name for variable 16:
#         'tendency_of_barometric_altitude' is not a CF standard name
#     nok CF standard name for variable 18:
#         'platform_attack_angle' is not a CF standard name
#     nok CF standard name for variable 19:
#         'air_total_pressure' is not a CF standard name
#     nok CF standard name for variable 20:
#         'platform_vertical_speed_wrt_air' is not a CF standard name
