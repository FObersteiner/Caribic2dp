# -*- coding: utf-8 -*-
"""
Convert a CARIBIC NASA Ames file to a CARIBIC netCDF4 file.
"""
from pathlib import Path
from sys import platform

from caribic2dp import carfileutils
from caribic2dp.convert_caribic_na_nc4 import na_to_nc4

# must specify where to look for flight data
# assume there is only one non-Windows user
flights_dir = Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/Flights"
if platform == "win32":
    flights_dir = Path("//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/")


# -----------------------------------------------------------------------------


prefix = "OM"  # Ozone data
flight = 591

src = carfileutils.find_files(flight, prefix, flights_dir, resolution="10s")[0]
print(src)
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/OM_20200304_591_CPT_MUC_10s_V02.txt

# to convert na to nc4, we must specify CARIBIC parameters configuration file first.
params_file = Path().home() / "Code/Python/Caribic2_DataProc/config/parms_units_2023-11-16.csv"
assert params_file.is_file(), "failed to load CARIBIC parameters config"

# convert to nc4, however don't write to disk but only return xarray.DataSet
ds = na_to_nc4(src, Path(), params_file=params_file, write_to_disk=False)


# global attributes:
for a in ds.attrs:
    print(f"{a}: {ds.attrs[a]}")

# Title: IAGOS-CARIBIC netCDF4 data file
# Creation_date: 2022-12-28T09:12:31.509504+00:00
# Mission: IAGOS-CARIBIC (CARIBIC-2), http://www.caribic-atmospheric.com/
# Data_description: Ozone measured with the dual-beam UV-photometer that is part of the FAIRO instrument during IAGOS-CARIBIC (onboard A340-600 D-AIHE of Lufthansa)
# Data_institute: Institute of Meteorology and Climate Research (IMK), Karlsruhe Institute of Technology (KIT), 76021 Karlsruhe, P.O. Box 3640, Germany
# Data_owners: F. Obersteiner; A. Zahn; f.obersteiner@kit.edu; andreas.zahn@kit.edu
# Format_date: 2022-12-27
# Format_version: 0.3
# History: Converted from NASA Ames format with na_to_nc4 from caribic2dp.convert_caribic_na_nc4 module. Might contain only a subset of the parameters from the original NASA Ames file. Version of caribic2dp package: 0.2.4, https://git.scc.kit.edu/FObersteiner/CaribicDP/-/tree/master/Caribic2_DataProc
# Conventions: CF-1.10


# variables:
for v in ds.data_vars:
    if v != "File_header":
        print(f"----- {v} -----")
        for a in ds[v].attrs:
            print(f"{a}: {ds[v].attrs[a]}")

# ----- Ozone -----
# unit: ppb
# standard_name: mole_fraction_of_ozone_in_air
# standard_unit: 1e-9
# scale: 1
# long_name: Ozone volume mixing ratio
# C_format: %.2f
# ----- H_rel_TP -----
# unit: km
# standard_name: altitude_wrt_tropopause
# standard_unit: 1e3 m
# scale: 1
# long_name: Ozone-based representative height above the tropopause
# C_format: %.2f
