# -*- coding: utf-8 -*-
"""
Find NASA Ames files of a certain type on the CARIBIC server.

File type means e.g. Master data with prefix MA etc.

Once you collected the file paths to all the files you need, you can copy them
to a local directory.
"""
import sys
import shutil  # to copy files from a to b
from datetime import datetime  # to specify date ranges
from pathlib import Path  # to handle file paths

from caribic2dp import carfileutils

# must specify where to look for flight data
# assume there is only one non-Windows user
flights_dir = Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/Flights"
if sys.platform == "win32":
    flights_dir = Path("//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/")


# -----------------------------------------------------------------------------
# 1) find only files of a certain type, for a certain flight

prefix = "MA"
flight = 591

paths = carfileutils.find_files(flight, prefix, flights_dir)
for p in paths:
    print(p)
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/MA_20200304_591_CPT_MUC_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/MA_20200304_591_CPT_MUC_V01.txt

# 1.1) also specify a resolution, e.g. extract 10 s binned data:
paths = carfileutils.find_files(flight, prefix, flights_dir, resolution="10s")
for p in paths:
    print(p)
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/MA_20200304_591_CPT_MUC_10s_V01.txt


# -----------------------------------------------------------------------------
# 2) find files of multiple types, with in an optional flight- and/or daterange

prefixes = ["MA", "OM"]  # Master and ozone data
flightrange = (500, 503)  # flights 500 to 503
res = "10s"  # only 10s binned data

paths = carfileutils.gather_data(prefixes, flights_dir, flightrange=flightrange, resolution=res)
for p in paths:
    print(p)
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight501_20150420/MA_20150420_501_MUC_HND_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight501_20150420/OM_20150420_501_MUC_HND_10s_V03.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight503_20150421/MA_20150421_503_MUC_MEX_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight503_20150421/OM_20150421_503_MUC_MEX_10s_V03.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight500_20150312/MA_20150312_500_HND_MUC_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight502_20150421/MA_20150421_502_HND_MUC_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight502_20150421/OM_20150421_502_HND_MUC_10s_V03.txt

# 2.1) do the same but for a date range, e.g. only March 2020:
daterange = (datetime(2020, 3, 1), datetime(2021, 3, 31))

paths = carfileutils.gather_data(prefixes, flights_dir, daterange=daterange, resolution=res)
for p in paths:
    print(p)
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight588_20200302/MA_20200302_588_MUC_LAX_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight588_20200302/OM_20200302_588_MUC_LAX_10s_V02.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/MA_20200304_591_CPT_MUC_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight591_20200304/OM_20200304_591_CPT_MUC_10s_V02.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight590_20200303/MA_20200303_590_MUC_CPT_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight590_20200303/OM_20200303_590_MUC_CPT_10s_V02.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight589_20200303/MA_20200303_589_LAX_MUC_10s_V01.txt
# ~/Carfs1/Caribic/extern/CARIBIC2data/Flights/Flight589_20200303/OM_20200303_589_LAX_MUC_10s_V02.txt


# -----------------------------------------------------------------------------
# 3) copy files to a local directory
dst = Path.home() / "Downloads"  # user / Downloads
for p in paths:
    done = shutil.copy(p, dst / p.name)
    print(f"copied {done.name}")
# copied MA_20200302_588_MUC_LAX_10s_V01.txt
# copied OM_20200302_588_MUC_LAX_10s_V02.txt
# copied MA_20200304_591_CPT_MUC_10s_V01.txt
# copied OM_20200304_591_CPT_MUC_10s_V02.txt
# copied MA_20200303_590_MUC_CPT_10s_V01.txt
# copied OM_20200303_590_MUC_CPT_10s_V02.txt
# copied MA_20200303_589_LAX_MUC_10s_V01.txt
# copied OM_20200303_589_LAX_MUC_10s_V02.txt
