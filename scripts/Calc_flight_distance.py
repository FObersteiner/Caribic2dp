# -*- coding: utf-8 -*-
from pathlib import Path

import numpy as np
from pyfuppes.geo import geodesic_dist
from pyfuppes.misc import print_progressbar
from pyfuppes.na1001 import FFI1001 as na_r

from caribic2dp.carfileutils import dissect_filename, gather_data

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

flightrange = [590, 591]
vmiss = 999999

logfile = Path(
    "/home/va6504/Data/Caribic_2/MA/flight_statistics/flstat_"
    + str(flightrange[0])
    + "-"
    + str(flightrange[1])
    + ".txt"
)

files = []
RESET_PARMS = False
dist = 0.0
flight_hours = 0.0

SEP, CRLF = "\t", "\n"
HEADER = (
    "fl_no"
    + SEP
    + "MA_File"
    + SEP
    + "fl_dist"
    + SEP
    + "fl_hrs"
    + SEP
    + "tot_fl_dist"
    + SEP
    + "tot_fl_hrs"
    + CRLF
)

with open(logfile, "w") as file_obj:
    file_obj.write(HEADER)

n, i = (flightrange[1] - flightrange[0]) + 1, 0
print_progressbar(i, n, prefix="processing...", suffix="done", length=50)

files = gather_data(["MA"], flights_dir, flightrange=flightrange, resolution="10s")

for file in files:
    # print(file.name)
    i += 1

    data = na_r(file)

    ix_lat, ix_lon = None, None

    for ix, string in enumerate(data.VNAME):
        if string.startswith("PosLat"):
            ix_lat = ix
        if string.startswith("PosLong"):
            ix_lon = ix
    assert (ix_lat is not None) and (ix_lon is not None)

    v_lat = np.array(data.V[ix_lat], dtype="float64")
    v_lat = v_lat[np.where(v_lat != vmiss)]
    v_lon = np.array(data.V[ix_lon], dtype="float64")
    v_lon = v_lon[np.where(v_lon != vmiss)]

    if RESET_PARMS:
        dist = 0.0  # travelled distance
        flight_hours = 0.0

    tmp_dist = geodesic_dist(v_lat, v_lon)
    dist += tmp_dist

    ### flight time
    fl_tmp = (float(data.X[-1]) - float(data.X[0])) / 3600.0
    flight_hours += fl_tmp

    with open(logfile, "a") as file_obj:
        if tmp_dist < 15000.0 and fl_tmp < 15.0:
            line = (
                f"{dissect_filename(file.name).flight_no}{SEP}{file.name}{SEP}"
                + f"{tmp_dist:.1f}\t{fl_tmp:.1f}\t{dist:.1f}\t{flight_hours:.1f}\n"
            )
            file_obj.write(line)
        else:
            file_obj.write(f"{dissect_filename(file.name).flight_no}{SEP}ERROR!{CRLF}")

    print_progressbar(i, n, prefix="processing...", suffix="done", length=50)
