# -*- coding: utf-8 -*-
from datetime import datetime, timezone
from pathlib import Path

import numpy as np
from pyfuppes.na1001 import FFI1001 as na1001

from caribic2dp.carfileutils import flight_no_to_flightdir as fldir

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths


# -------------------- HELPERS ------------------------------------------------
def write_header(dst, flight_no, missing=None):
    s = (
        "#------------------------------------------------------------\n"
        f"# FLIGHT {str(flight_no)}\n"
        f"# STATUS FROM {datetime.now(timezone.utc).replace(microsecond=0).isoformat()}\n"
    )

    if missing:
        s += "# FILES MISSING: " + ", ".join(sorted(missing)) + "\n"

    s += "#------------------------------------------------------------\n"

    with open(dst, "w") as f:
        f.write(s)


# -----------------------------------------------------------------------------


def write_data(src, dst, covdict):
    w = max(len(k) for k in covdict) + 1
    w = w if w >= 5 else 5

    s = (
        "#\n"
        "#-----------------------------\n"
        "#------------------------------------------------------------\n"
        f"{'FILE':<{w}}{src.name}\n"
        "#------------------------------------------------------------\n"
        "PARM" + " " * (w - 4) + "COVERAGE[0-1]\n"
    )

    with open(dst, "a") as f:
        f.write(s)
        for k, v in covdict.items():
            f.write(f"{k:<{w}}{v:.3f}\n")


# -----------------------------------------------------------------------------


# where to save results
dst = Path("../stats/data_coverage").resolve()

# columns to exclude:
excl_keys = ("TimeCRef", "year", "month", "day", "hour", "min", "sec")

# file prefixes to expect
pfx_expected = {
    "CO",
    "GHG",
    "HCF",
    "INT",
    "MA",
    "MS",
    "NO",
    "OM",
    "P2C",
    "PIC",
    "PTR",
    "SP2",
    "SU",
    "WA",
    "WB",
}

# flight range to check
flrange = (537, 591)

for fno in range(flrange[0], flrange[1] + 1):
    src = fldir(fno, flights_dir)
    files = list(src.glob("*.txt"))
    if files:
        print(f"\nprocessing flight {fno}...")
        # check for expected files...
        missing = pfx_expected - {f.name.split("_")[0] for f in files}
        print(f"missing files: {missing}")
        write_header(dst / f"F{str(fno)}_dataCoverage.log", fno, missing=missing)
    else:
        print(f"no files found for flight {fno}!")

    for file in files:
        print(f"    analysing {file.name}")
        na = na1001(file)
        d = na.to_dict_nparray()

        coverage = {}
        for k in d:
            if k not in excl_keys:
                n_fin = np.count_nonzero(np.isfinite(d[k]))
                coverage[k] = 0 if n_fin == 0 else n_fin / d[k].shape[0]

        if coverage:
            write_data(file, dst / f"F{str(fno)}_dataCoverage.log", coverage)
            # print("wrote", dst/f"F{str(fno)}_dataCoverage.log")
