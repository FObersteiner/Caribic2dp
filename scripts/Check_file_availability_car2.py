# -*- coding: utf-8 -*-
import os
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap

from caribic2dp.carfileutils import PREFIXES

# --- CONFIG ------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

dst = Path("../stats/file_availability").resolve()
fl_range = [537, 591]

### PLOTs
save_figs = True
xmod = 2  # plot: xticks where flight_no % xmod == 0

# -----------------------------------------------------------------------------

allFl = os.listdir(flights_dir)
selDirs = [
    flights_dir / [s for s in allFl if "Flight" + str("%3.3u" % ix) in s][0]
    for ix in range(fl_range[0], fl_range[1] + 1)
]

# os.stat(file).st_size

k, l = len(PREFIXES), len(selDirs)
m = np.zeros((k, l))
n_total, frac, fpf = 0, [], []

for col, path in enumerate(selDirs):
    txtfiles = [s.name for s in path.glob("*.txt")]
    n_total += len(txtfiles)
    frac.append(n_total)
    fpf.append(len(txtfiles))
    for row, pfx in enumerate(PREFIXES):
        if any(pfx == f[: len(pfx)] for f in txtfiles):
            m[row, col] = 1

frac = np.array(frac) / frac[-1]
fpf = np.array(fpf) / np.max(fpf)
flights = list(range(fl_range[0], fl_range[1] + 1))

fig, ax = plt.subplots()
p0 = ax.plot(np.array(flights), fpf, color="r", label="per flight")
p1 = ax.plot(np.array(flights), frac, color="b", label="cumulative")
ax.set_xlabel("flight no.")
ax.set_ylabel("data density")
plots = p0 + p1
lbls = [l.get_label() for l in plots]
plt.legend(plots, lbls, loc=0)
plt.tight_layout()
plt.show(block=False)


if save_figs:
    fig.savefig(
        dst / ("NAfiles_availability_1_" + str(fl_range[0]) + "-" + str(fl_range[1]) + ".png"),
        dpi=330,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )

x_pos, y_pos = np.arange(l), np.arange(k)


x_ticks = [str(flights[0] + i) if i % xmod == 0 else "" for i in range(0, len(flights))]

cmap = ListedColormap(["w", "g"])

fig, ax = plt.subplots(figsize=(16, 9))
plt.title(
    "Caribic 2 data availability, flights " + str(fl_range[0]) + "-" + str(fl_range[1]),
    weight="bold",
)

ax.imshow(m, cmap=cmap, aspect=(l / k) * (9 / 16), alpha=0.9)

ax.set_xlabel("flight no.", weight="bold")
ax.set_ylabel("file type", weight="bold")
plt.xticks(x_pos, x_ticks)
plt.yticks(y_pos, [pfx[:-1] for pfx in PREFIXES])
ts = "created " + str(datetime.utcnow())[0:19] + " UTC"
plt.text(0.815, 0.04, ts, fontsize=8, transform=plt.gcf().transFigure)
plt.tight_layout()
plt.show()

if save_figs:
    fig.savefig(
        dst / ("NAfiles_availability_2_" + str(fl_range[0]) + "-" + str(fl_range[1]) + ".png"),
        dpi=330,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )
