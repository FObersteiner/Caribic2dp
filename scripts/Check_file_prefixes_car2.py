# -*- coding: utf-8 -*-
from collections import defaultdict
from pathlib import Path

from pyfuppes.na1001 import FFI1001

from caribic2dp.carfileutils import get_all_flightdirs

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

fldirs = get_all_flightdirs(flights_dir)

# dict prefix:first-example
pfx = defaultdict(str)

for d in fldirs:
    files = list(d.glob("*.txt"))
    for f in files:
        parts = f.name.split("_")
        if len(parts) > 1 and parts[0] not in pfx:
            pfx[parts[0]] = f

for i in sorted(pfx.items()):
    print(f'"{i[0]}_", # ex: {i[1].name}')


# "AN_", # ex: AN_20060220_138_FRA_GRU_10s_V02.txt
# "Ac_", # ex: Ac_20060220_138_FRA_GRU_10s_V02.txt
# "C2_", # ex: C2_20051115_131_CAN_MNL_10s_V25.txt
# "C2I_", # ex: C2I_20070306_182_FRA_CAN_V11.txt
# "C4I_", # ex: C4I_20050519_110_FRA_CAN_10s_V26.txt
# "CM_", # ex: CM_20101020_313_FRA_CCS_10s_V02.txt
# "CN_", # ex: CN_20050519_110_FRA_CAN_10s_V08.txt
# "CO_", # ex: CO_20050519_110_FRA_CAN_10s_V03.txt
# "DD_", # ex: DD_20100420_294_FRA_FRA_V13.txt
# "DIS_", # ex: DIS_20070522_190_FRA_CAN_V01.txt
# "DN_", # ex: DN_20051005_127_GRU_SCL_10s_V03.txt
# "DU_", # ex: DU_20110118_325_FRA_JNB_10s_V02.txt
# "GHG_", # ex: GHG_20051004_126_FRA_GRU_V11.txt
# "HCF_", # ex: HCF_20180322_544_MUC_DEN_V05.txt
# "HG_", # ex: HG_20050519_110_FRA_CAN_10s_V08.txt
# "INT_", # ex: INT_20050519_110_FRA_CAN_V15.txt
# "MA_", # ex: MA_20050519_110_FRA_CAN_10s_V05.txt
# "ME_", # ex: ME_20140819_472_MUC_SFO_10s_V01.txt
# "MS_", # ex: MS_20050519_110_FRA_CAN_10s_V29.txt
# "NO_", # ex: NO_20050519_110_FRA_CAN_10s_V03.txt
# "OM_", # ex: OM_20050620_114_FRA_GRU_10s_V04.txt
# "P2C_", # ex: P2C_20180729_550_MUC_HND_10s_V01.txt
# "PIC_", # ex: PIC_20180729_550_MUC_HND_10s_V03.txt
# "PTR_", # ex: PTR_20171211_537_MUC_ICN_10s_V01.txt
# "SD_", # ex: SD_20100420_294_FRA_FRA_10s_V14.txt
# "SP2_", # ex: SP2_20140819_472_MUC_SFO_10s_V02.txt
# "SU_", # ex: SU_20050519_110_FRA_CAN_10s_V26.txt
# "WA_", # ex: WA_20050519_110_FRA_CAN_10s_V02.txt
# "WAS_", # ex: WAS_20050519_110_FRA_CAN_V11.txt
# "WB_", # ex: WB_20050519_110_FRA_CAN_V02.txt
# "WI_", # ex: WI_20100622_297_FRA_CCS_10s_V02.txt


# check variables in each file

var_2_pfx = {}

for prefix, path in pfx.items():
    na = FFI1001(path)
    names = na.VNAME
    for name_idx, n in enumerate(names):
        parts = n.split(";")
        for idx, part in enumerate(parts):
            if "#" in part:
                parts[idx] = part.split("#")[0]
            parts[idx] = parts[idx].strip()  # .lower()
        cleaned = "\t".join(parts)

        if var_2_pfx.get(cleaned):
            var_2_pfx[cleaned][0] += f", {prefix}"
        else:
            var_2_pfx[cleaned] = [f"{prefix}"]
            var_2_pfx[cleaned] += [
                parts[0],
                parts[1],
                parts[2],
                na.VSCAL[name_idx],
                na.VMISS[name_idx],
            ]

# sort by short_name
var_2_pfx = dict(sorted(var_2_pfx.items(), key=lambda x: x[1][1]))

dst = Path("/home/va6504/Code/CaribicDP/Config/parms_from_nafiles.csv")
with open(dst, "w") as fobj:
    fobj.write("file_prefix\tshort_name\tlong_name\tunit\tvscal\tvmiss\n")
    for entry in var_2_pfx.values():
        fobj.write("\t".join(entry) + "\n")
