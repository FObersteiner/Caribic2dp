# -*- coding: utf-8 -*-
import os
from datetime import datetime, timezone
from pathlib import Path

from pyfuppes.misc import find_youngest_file as find_youngest
from pyfuppes.misc import set_compare

# ------------------------------------------------------------------------------------------------------------
# GET STATUS
# -> get status of database on server

# SAVE STATUS
# -> save status to config file

# CHECK STATUS
# -> get status of database on server, load config file, compare
# ------------------------------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

CHECK_FILES_EXT = "*.txt"


# ------------------------------------------------------------------------------------------------------------
def get_status(src, flrange):
    flrange = tuple(range(flrange[0], flrange[1] + 1))
    folders = [f for f in src.glob("*") if int(f.name[6:9]) in flrange]
    is_on_server = {}
    for f in folders:
        is_on_server[int(f.name[6:9])] = {
            "path": f,
            "files": [file.name for file in f.glob(CHECK_FILES_EXT)],
            "mtime": [os.path.getmtime(file) for file in f.glob(CHECK_FILES_EXT)],
        }
        # make sure file names are sorted
        files = is_on_server[int(f.name[6:9])]["files"]
        mtimes = is_on_server[int(f.name[6:9])]["mtime"]
        ix_sort = sorted(range(len(files)), key=lambda k: files[k])
        if ix_sort != list(range(len(files))):
            is_on_server[int(f.name[6:9])]["files"] = sorted(files)
            is_on_server[int(f.name[6:9])]["mtime"] = [mtimes[i] for i in ix_sort]

    print(f"+++ Received status from {src}")
    return is_on_server


# ------------------------------------------------------------------------------------------------------------


# ------------------------------------------------------------------------------------------------------------
def save_status(is_on_server, dst, timestamp, sep=";"):
    log = dst / (timestamp.strftime("%Y%m%d_%H%M%SZ") + ".log")
    header = ["flight", "path", "file", "mtime"]
    with open(log, "w") as fobj:
        fobj.write(sep.join(header) + "\n")
        for flight in is_on_server:
            for i, f in enumerate(is_on_server[flight]["files"]):
                line = map(
                    str,
                    [
                        flight,
                        is_on_server[flight]["path"],
                        f,
                        is_on_server[flight]["mtime"][i],
                    ],
                )
                fobj.write(sep.join(line) + "\n")

    print(f"+++ Logged server status to {log.name}")


# ------------------------------------------------------------------------------------------------------------


# ------------------------------------------------------------------------------------------------------------
def load_status(src, flrange, sep=";"):
    logfile = find_youngest(src, "*.log")[0]
    with open(logfile, "r") as fobj:
        data = fobj.read().splitlines()[1:]

    types = (int, Path, str, float)
    is_in_log = {}
    for line in data:
        line = [types[i](x) for i, x in enumerate(line.split(sep))]
        if line[0] not in is_in_log:
            is_in_log[line[0]] = {
                "path": line[1],
                "files": [line[2]],
                "mtime": [line[3]],
            }
        else:
            is_in_log[line[0]]["files"].append(line[2])
            is_in_log[line[0]]["mtime"].append(line[3])

    unused = set(is_in_log.keys()) - set(range(flrange[0], flrange[1] + 1))
    if unused:
        for u in unused:
            del is_in_log[u]

    print(f"+++ Loaded status from {logfile.name}")
    return is_in_log


# ------------------------------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    ### GET STATUS on server
    src = flights_dir
    flrange = (110, 591)
    is_on_server = get_status(src, flrange)

    # check if there are multiple files of the same type with different version
    for k, v in is_on_server.items():
        result = {}
        for f in v["files"]:
            parts = f.split("_")
            t = parts[0] if "_10s_" not in f else parts[0] + "_10s"
            # special case: HG also exists as int file
            if t == "HG" and "_int_" in f:
                t = "HG_int"
            # result[t] = result.setdefault(t, 0) + 1
            if result.get(t):
                # if result already contains an entry for the key, that means we
                # have multiple files of the same type, maybe different version.
                # raise ValueError(f"flight {k}: duplicate files for type {t}")
                print(f"flight {k}: duplicate files for type {t}")
            else:
                result[t] = True

    ### LOAD STATUS from logfile
    src = Path("../../Stat/file_status/").resolve()
    is_in_log = load_status(src, flrange)

    ### CHECK STATUS from logfile against status on server
    changes = 0
    print("+++ comparing...")

    # compare flight number only
    only_log, only_server, common = set_compare(is_in_log.keys(), is_on_server.keys())

    # TODO
    # log changes in separate file with same date/time in filename
    dst = Path("../../Stat/file_status")
    timestamp = datetime.now(timezone.utc)

    if any((only_log, only_server)):
        changes = 1
        if only_log:
            for flight in only_log:
                print(
                    f"  ~ logged flight not found on server: {flight}"
                    " (deleted?)\n"
                    f'    path: {str(is_in_log[flight]["path"])}'
                )
        if only_server:
            for flight in only_server:
                print(
                    f"  ~ flight on server not found in log: {flight}"
                    " (new?)\n"
                    f'    path: {str(is_on_server[flight]["path"])}'
                )
    else:
        print("  ~ all flights from log found on server")

    for flight in common:
        d_log, d_server = is_in_log[flight], is_on_server[flight]
        if d_log != d_server:
            changes = 1
            print(f"  ~ flight {flight} changed,\n" f'path: {str(d_server["path"])}')
            # compare files
            f_log, f_server, f_shared = set_compare(d_log["files"], d_server["files"])
            if f_log:
                for f in f_log:
                    print(f"file {f}  -->  not found on server, removed?")
            if f_server:
                for f in f_server:
                    print(f"file {f}  -->  not found in log, new?")
            # compare mtime for shared files
            for f in f_shared:
                # calling sorted(f_shared) is sane here since filenames were sorted before
                i_server, i_log = d_server["files"].index(f), d_log["files"].index(f)
                t_server, t_log = d_server["mtime"][i_server], d_log["mtime"][i_log]
                if t_log > t_server:
                    print(f"file {f} outdated on server, t_log-t_server = {t_log-t_server}")
                if t_log < t_server:
                    print(f"file {f} outdated in log, t_log-t_server = {t_log-t_server}")
        else:
            print(f"  ~ flight {flight} unchanged ~")

    if changes:
        print("+++ changes found, creating new logfile +++")
        ### SAVE STATUS from server to new logfile if changes were found
        save_status(is_on_server, dst, timestamp)
    else:
        print("+++ no changes found. +++")
