# -*- coding: utf-8 -*-
import xarray as xr
from netCDF4 import Dataset

###############################################################################


# walk the group tree using a recursive generator.
# use if there are nested groups.
def walktree(top):
    values = top.groups.values()
    yield values
    for value in top.groups.values():
        for children in walktree(value):
            yield children


###############################################################################

file_nc = "/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/netcdf/MA_20200304_591_CPT_MUC_V01.nc"
# file_nc = "/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/netcdf/MS_20200304_591_CPT_MUC_10s_V10.nc"# file_nc = "/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/netcdf/MS_20200304_591_CPT_MUC_10s_V10.nc"

# file_nc = "/home/va6504/Data/Caribic_2/netcdf/tmp_out/MA_20200304_591_CPT_MUC_V01.nc"
# file_nc = "/home/va6504/Data/Caribic_2/netcdf/tmp_out/MS_20200304_591_CPT_MUC_10s_V11.nc"
# file_nc = "/home/va6504/Data/Caribic_2/na_2_nc/output/test.nc"


with Dataset(file_nc) as rootgrp:
    print(rootgrp)

    print("\n### data model, file format: ###")
    print(rootgrp.data_model, rootgrp.file_format)

    print("\n### global attributes: ###")
    gattrs = rootgrp.ncattrs()
    for key in gattrs:
        attr = getattr(rootgrp, key)
        print("[" + key + "] : " + str(attr) + ", type: " + str(type(attr)))

    #
    #
    # for children in walktree(rootgrp):
    #     for child in children:
    #         print(child)
    #

    dimensions = rootgrp.dimensions
    keys_dim = list(dimensions.keys())
    # for dimobj in rootgrp.dimensions.values():
    #     print(dimobj)
    # print(rootgrp.dimensions["time"])

    variables = rootgrp.variables
    keys_var = list(variables.keys())
    print(rootgrp.variables["time"])
    # print(rootgrp.variables["PCUTC"])

    print(f"\ndims: {keys_dim}\n\nvars: {keys_var}")


ds = xr.load_dataset(file_nc, decode_times=False)
# print(ds)

print(ds["time"].data)
print(ds["time"].data[0])
