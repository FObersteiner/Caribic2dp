# -*- coding: utf-8 -*-
import fnmatch
import os
import sys
from datetime import datetime as dt
from pathlib import Path

import matplotlib.dates as mdates
import matplotlib.ticker as mticker
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from pyfuppes.plottools import get_plot_range
from pyfuppes.v25 import collect_V25Logs, logs_cleanup

from caribic2dp.ma_tools import MA_data_chk_time, MA_get_flight_ix

# ------------------------------------------------------------------------------

#####
### CONFIG
#####

# startflightno = 570
# path = Path('/home/va6504/Data/Caribic_2/MA/F570-573/LH_570_MA/MASTDATA')
# path_dl_raw = '/home/va6504/Data/Caribic_2/Dataloggers_Container/F570-573/LH_570_Datalogger'

# startflightno = 566
# path = Path('/home/va6504/Data/Caribic_2/MA/F566-569/LH_566_MA/MASTDATA')
# path_dl_raw = '/home/va6504/Data/Caribic_2/Dataloggers_Container/F566-569/LH_566_Datalogger'

startflightno = 562
path = Path("/home/va6504/Data/Caribic_2/MA/F562-565/LH_562_MA/MASTDATA")
path_dl_raw = "/home/va6504/Data/Caribic_2/Dataloggers_Container/F562-565/LH_562_Datalogger"


folders = [path / f for f in os.listdir(path) if os.path.isdir(path / f)]
outpath = path_dl_raw + "/sorted/"
types = ["txt", "csv"]
delimiter = ","
ts_fmt = "%Y-%m-%d %H:%M:%S"
ari_ts_fmt = "%d.%m.%y %H:%M:%S.%f"
ix_colhdr_txt = 0
ix_colhdr_csv = 4

T_MIN = 0.0  # plot: minimum temperature

write_sortfiles = False
show_plots = True

#####
### CONFIG done. RUN Forrest RUN!
#####


for folder in folders:
    try:
        os.stat(folder)
    except FileNotFoundError:
        print("ERROR: folder not found.")
        sys.exit()


sel_files = []
for file in os.listdir(path_dl_raw):
    for ext in types:
        if fnmatch.fnmatch(file, "*." + ext):
            sel_files.append(path_dl_raw + "/" + file)


# load logger data to dictionary
loggerdata = {}
if sel_files:
    for file in sel_files:
        if file.find("txt") != -1:
            ix_colhdr = ix_colhdr_txt
            def_keys = []
        if file.find("csv") != -1:
            ix_colhdr = ix_colhdr_csv
            def_keys = ["Time", "Temperature"]

        try:
            with open(file, "r", encoding="cp1252") as file_obj:
                data = file_obj.readlines()
        except FileNotFoundError:
            print("file not found: " + file)
            continue

        if data:
            header = (data[ix_colhdr]).strip()
            header = header.rsplit(delimiter)[1:-1]  # remove index and serial number

            if def_keys:
                header = def_keys

            data = data[1 + ix_colhdr :]  # remove line with serial number

            tmp_dict = {}
            for element in header:
                tmp_dict[element] = []

            for line in data:
                if line.find("\n") > -1:
                    line = line.strip()  # remove surrounding whitespaces
                    line = line.rsplit(delimiter)[1 : len(header) + 1]
                    for i in range(len(header)):
                        (tmp_dict[header[i]]).append((line[i]).strip())

            key = os.path.basename(os.path.splitext(file)[0])
            loggerdata[key] = tmp_dict

            del tmp_dict, data

else:
    loggerdata = None


# analyse the flight sequence to parse loggerdata into flights
ARINC_Data = {}

logs_cleanup(folders, exts="ari")
ARINC_Data["ari"] = collect_V25Logs(folders, "ari", delimiter="\t", colhdr_ix=0)

ARINC_Data = MA_data_chk_time(ARINC_Data, {"ari": "%d.%m.%y %H:%M:%S.%f"})

flinfo = MA_get_flight_ix(ARINC_Data, "ari", startflightno)

flightseries = list(map(str, flinfo["flights"]))

t_dep = []
t_arr = []
for i in range(len(flinfo["ix"][0])):
    t = dt.strptime(ARINC_Data["ari"]["PCUTC"][flinfo["ix"][0][i]], ari_ts_fmt)
    t_dep.append(t)
    t = dt.strptime(ARINC_Data["ari"]["PCUTC"][flinfo["ix"][1][i]], ari_ts_fmt)
    t_arr.append(t)

merge_keys = list(loggerdata.keys())


# write one csv per logger and flight
if write_sortfiles:
    try:
        os.stat(outpath)
    except OSError:
        print("created directory: " + outpath)
        os.mkdir(outpath)

    for mkey in merge_keys:
        Time_instr = loggerdata[mkey]["Time"]
        Time_instr = [dt.strptime(element, ts_fmt) for element in Time_instr]
        for f in range(len(flightseries)):
            delta_dep = [abs(element - t_dep[f]) for element in Time_instr]
            ix_dep = delta_dep.index(min(delta_dep))
            delta_arr = [abs(element - t_arr[f]) for element in Time_instr]
            ix_arr = delta_arr.index(min(delta_arr))

            fname = outpath + "Flight_" + flightseries[f] + "_loggerdata_"
            fname += mkey + ".csv"

            keys = list(loggerdata[mkey].keys())
            n_keys = len(keys)

            deli_list = [delimiter] * n_keys
            deli_list[-1] = "\n"
            header = ""
            for i in range(0, n_keys):
                header += keys[i] + deli_list[i]

            try:
                os.stat(os.path.dirname(fname))
            except OSError:
                os.mkdir(os.path.dirname(fname))

            with open(fname, "w", encoding="UTF-8") as file_obj:
                file_obj.write(header)
                for i in range(ix_dep, ix_arr):
                    line = ""
                    for j in range(0, n_keys):
                        line += str(loggerdata[mkey][keys[j]][i]) + deli_list[j]
                    file_obj.write(line)


if show_plots:
    # get nested keys
    sub_keys = []
    for key in merge_keys:
        sub_keys.append(list(loggerdata[key].keys()))

    # convert variables to datetime + float
    for k in range(len(merge_keys)):
        for s in range(len(sub_keys[k])):
            if s == 0:
                x = loggerdata[merge_keys[k]][sub_keys[k][s]]
                loggerdata[merge_keys[k]][sub_keys[k][s]] = [
                    dt.strptime(x[i], ts_fmt) for i in range(len(x))
                ]
            else:
                v = loggerdata[merge_keys[k]][sub_keys[k][s]]
                loggerdata[merge_keys[k]][sub_keys[k][s]] = [float(tmp) for tmp in v]

    # some general plot settings
    days = mdates.DayLocator()
    hours = mdates.HourLocator()
    minutes = mdates.MinuteLocator()
    sec = mdates.SecondLocator()
    title = "CARIBIC container loggerdata, flight " + flightseries[0]
    title += " to " + flightseries[-1]
    xlabel = "t[UTC]"
    xcoords = t_dep + t_arr
    xcoords_txt = t_dep
    x_txt = ["Flight " + flight + " ->" for flight in flightseries]
    xlimits = [
        mdates.date2num(t_dep[0]) * 0.9999995,
        mdates.date2num(t_arr[-1]) * 1.0000005,
    ]
    nticks = 11

    x0 = []  # loggerdata['BPS_L1']['Time']
    v0 = []  # loggerdata['BPS_L1']['Voltage(Volts)']
    x1 = []  # loggerdata['BPS_L2']['Time']
    v1 = []  # loggerdata['BPS_L2']['Voltage(Volts)']
    x2 = []  # loggerdata['BPS_L3']['Time']
    v2 = []  # loggerdata['BPS_L3']['Voltage(Volts)']
    x3 = []  # loggerdata['humidity']['Time']
    v31 = []  # loggerdata['humidity']['Celsius(°C)']
    v32 = []  # loggerdata['humidity']['Humidity(%rh)']
    v33 = []  # loggerdata['humidity']['Dew Point(°C)']
    x4 = []  # loggerdata['T1_IN']['Time']
    v4 = []  # loggerdata['T1_IN']['Celsius(°C)']
    x5 = []  # loggerdata['T2_AE']['Time']
    v5 = []  # loggerdata['T2_AE']['Celsius(°C)']
    x6 = []  # loggerdata['T3_out']['Time']
    v6 = []  # loggerdata['T3_out']['Temperature']
    x7 = []  # loggerdata['T4_outside']['Time']
    v7 = []  # loggerdata['T4_outside']['Celsius(°C)']
    x8 = []  # loggerdata['T5_OPC']['Time']
    v8 = []  # loggerdata['T5_OPC']['Celsius(°C)']
    x9 = []  # loggerdata['T_BPS_Back']['Time']
    v9 = []  # loggerdata['T_BPS_Back']['Celsius(°C)']
    x10 = []  # loggerdata['T_BPS_Front']['Time']
    v10 = []  # loggerdata['T_BPS_Front']['Celsius(°C)']
    x11 = []  # loggerdata['T_spin_AE']['Time']
    v11 = []  # loggerdata['T_spin_AE']['Temperature']
    x12 = []  # loggerdata['T_spin_CCB']['Time']
    v12 = []  # loggerdata['T_spin_CCB']['Temperature']
    x13 = []  # loggerdata['T_TRU']['Time']
    v13 = []  # loggerdata['T_TRU']['Celsius(°C)']

    if "BPS_L1" in merge_keys:
        x0 = loggerdata["BPS_L1"]["Time"]
        v0 = loggerdata["BPS_L1"]["Voltage(Volts)"]

    if "BPS_L2" in merge_keys:
        x1 = loggerdata["BPS_L2"]["Time"]
        v1 = loggerdata["BPS_L2"]["Voltage(Volts)"]

    if "BPS_L3" in merge_keys:
        x2 = loggerdata["BPS_L3"]["Time"]
        v2 = loggerdata["BPS_L3"]["Voltage(Volts)"]

    if "humidity" in merge_keys:
        x3 = loggerdata["humidity"]["Time"]
        v31 = loggerdata["humidity"]["Celsius(°C)"]
        v32 = loggerdata["humidity"]["Humidity(%rh)"]
        v33 = loggerdata["humidity"]["Dew Point(°C)"]

    if "T1_IN" in merge_keys:
        x4 = loggerdata["T1_IN"]["Time"]
        v4 = loggerdata["T1_IN"]["Celsius(°C)"]

    if "T2_AE" in merge_keys:
        x5 = loggerdata["T2_AE"]["Time"]
        v5 = loggerdata["T2_AE"]["Celsius(°C)"]

    if "T3_out" in merge_keys:
        x6 = loggerdata["T3_out"]["Time"]
        v6 = loggerdata["T3_out"]["Temperature"]

    if "T4_outside" in merge_keys:
        x7 = loggerdata["T4_outside"]["Time"]
        v7 = loggerdata["T4_outside"]["Celsius(°C)"]

    if "T5_OPC" in merge_keys:
        x8 = loggerdata["T5_OPC"]["Time"]
        v8 = loggerdata["T5_OPC"]["Celsius(°C)"]

    if "T_BPS_Back" in merge_keys:
        x9 = loggerdata["T_BPS_Back"]["Time"]
        v9 = loggerdata["T_BPS_Back"]["Celsius(°C)"]

    if "T_BPS_Front" in merge_keys:
        x10 = loggerdata["T_BPS_Front"]["Time"]
        v10 = loggerdata["T_BPS_Front"]["Celsius(°C)"]

    if "T_spin_AE" in merge_keys:
        x11 = loggerdata["T_spin_AE"]["Time"]
        v11 = loggerdata["T_spin_AE"]["Temperature"]

    if "T_spin_CCB" in merge_keys:
        x12 = loggerdata["T_spin_CCB"]["Time"]
        v12 = loggerdata["T_spin_CCB"]["Temperature"]

    if "T_TRU" in merge_keys:
        x13 = loggerdata["T_TRU"]["Time"]
        v13 = loggerdata["T_TRU"]["Celsius(°C)"]

    ### Figure 1, parameters BPS L1-L3 and BPS temperatures
    fname = "Plot_BPS_L1-L3+Temp.png"
    fig, ax1 = plt.subplots(figsize=(16, 10))
    plt.title(title, weight="bold", fontsize=14)
    plt.xticks(rotation=45)
    plt.xlabel(xlabel)

    p0 = ax1.plot_date(x0, v0, "darkblue", label="BPS_L1")
    p1 = ax1.plot_date(x1, v1, "g", label="BPS_L2")
    p2 = ax1.plot_date(x2, v2, "b", label="BPS_L3")

    ax1.set_ylabel("[V]", color="k")
    ax1.tick_params("y", colors="k")

    ax1.set_xlim(xlimits)
    ax1.autoscale_view()

    # vertical lines indicating the flights + text
    for xc in xcoords:
        plt.axvline(x=xc, color="k", linewidth=2)
    for f in range(len(x_txt)):
        ax1.text(
            mdates.date2num(t_arr[f]),
            1,
            x_txt[f],
            weight="bold",
            horizontalalignment="right",
            verticalalignment="top",
            fontsize=12,
        )

    ax2 = ax1.twinx()
    p3 = ax2.plot_date(x9, v9, "r", label="T_BPS_Back")
    p4 = ax2.plot_date(x10, v10, "y", label="T_BPS_Front")
    p5 = ax2.plot_date(x13, v13, "lime", label="T_TRU")
    ax2.set_ylabel("[°C]", color="k")
    ax2.tick_params("y", colors="k")
    # ylim
    ylts = get_plot_range(v9 + v10 + v13, add_percent=10, v_min_lim=T_MIN)

    ax2.set_ylim(ylts)
    # tick and grid
    ax1.yaxis.set_major_locator(mticker.LinearLocator(nticks))
    ax2.yaxis.set_major_locator(mticker.LinearLocator(nticks))
    ax1.grid(color="k", linestyle="dashed")

    plots = p0 + p1 + p2 + p3 + p4 + p5
    lbls = [l.get_label() for l in plots]
    legend = ax1.legend(plots, lbls, loc=0, shadow=True)
    legend.get_frame().set_facecolor("#ffffff")
    legend.get_frame().set_alpha(1.0)

    plt.tight_layout()

    majorFormatter = mdates.DateFormatter("%d.%m.%Y")
    ax1.xaxis.set_major_formatter(majorFormatter)

    plt.show()

    fig.savefig(
        path_dl_raw + fname,
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        pad_inches=0.1,
    )

    ### Figure 2, humidity, more temperatures
    fname = "Plot_rH+Temp.png"
    fig, ax1 = plt.subplots(figsize=(16, 10))
    plt.title(title, weight="bold", fontsize=14)
    plt.xticks(rotation=45)
    plt.xlabel(xlabel)

    p0 = ax1.plot_date(x3, v31, "r", label="T_hum_sensor")
    p1 = ax1.plot_date(x3, v33, "darkblue", label="dew_point")

    ax1.set_ylabel("[°C]", color="k")
    ax1.tick_params("y", colors="k")

    ax1.set_xlim(xlimits)
    ax1.autoscale_view()

    # vertical lines indicating the flights + text
    for xc in xcoords:
        plt.axvline(x=xc, color="k", linewidth=2)
    for f in range(len(x_txt)):
        ax1.text(
            mdates.date2num(t_arr[f]),
            1,
            x_txt[f],
            weight="bold",
            horizontalalignment="right",
            verticalalignment="top",
            fontsize=12,
        )

    ax2 = ax1.twinx()
    p3 = ax2.plot_date(x3, v32, "g", label="humidity")

    ax2.set_ylabel("[%rH]", color="k")
    ax2.tick_params("y", colors="k")
    # ylim
    ylts = get_plot_range(v31 + v33)
    ax1.set_ylim(ylts)
    # tick and grid
    ax2.yaxis.set_major_locator(mticker.LinearLocator(nticks))
    ax1.yaxis.set_major_locator(mticker.LinearLocator(nticks))

    ax1.grid(color="k", linestyle="dashed")

    plots = p0 + p1 + p3
    lbls = [l.get_label() for l in plots]
    legend = ax1.legend(plots, lbls, loc=0, shadow=True)
    legend.get_frame().set_facecolor("#ffffff")
    legend.get_frame().set_alpha(1.0)

    plt.tight_layout()

    majorFormatter = mdates.DateFormatter("%d.%m.%Y")
    ax1.xaxis.set_major_formatter(majorFormatter)

    plt.show()

    fig.savefig(
        path_dl_raw + fname,
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )

    ### Figure 3, temperature madness 1
    fname = "Plot_Temperatures_01.png"
    fig, ax1 = plt.subplots(figsize=(16, 10))
    plt.title(title, weight="bold", fontsize=14)
    plt.xticks(rotation=45)
    plt.xlabel(xlabel)

    p0 = ax1.plot_date(x3, v31, "r", label="T_hum_sensor")
    p1 = ax1.plot_date(x4, v4, "g", label="T1_IN")
    p2 = ax1.plot_date(x6, v6, "b", label="T3_out")
    p3 = ax1.plot_date(x7, v7, "lime", label="T4_outside")
    plots = p0 + p1 + p2 + p3

    ax1.set_ylabel("[°C]", color="k")
    ax1.tick_params("y", colors="k")

    ax1.set_xlim(xlimits)
    ax1.autoscale_view()

    # vertical lines indicating the flights + text
    for xc in xcoords:
        plt.axvline(x=xc, color="k", linewidth=2)
    for f in range(len(x_txt)):
        ax1.text(
            mdates.date2num(t_arr[f]),
            1,
            x_txt[f],
            weight="bold",
            horizontalalignment="right",
            verticalalignment="top",
            fontsize=12,
        )

    # ylim
    ylts = get_plot_range(v31 + v4 + v6 + v7, v_min_lim=T_MIN)
    ax1.set_ylim(ylts)

    ax1.yaxis.set_major_locator(mticker.LinearLocator(nticks))
    ax1.grid(color="k", linestyle="dashed")

    lbls = [l.get_label() for l in plots]
    legend = ax1.legend(plots, lbls, loc=0, shadow=True)
    legend.get_frame().set_facecolor("#ffffff")
    legend.get_frame().set_alpha(1.0)

    plt.tight_layout()

    majorFormatter = mdates.DateFormatter("%d.%m.%Y")
    ax1.xaxis.set_major_formatter(majorFormatter)

    plt.show()

    fig.savefig(
        path_dl_raw + fname,
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )

    ### Figure 4, temperature madness 1
    fname = "Plot_Temperatures_02.png"
    fig, ax1 = plt.subplots(figsize=(16, 10))
    plt.title(title, weight="bold", fontsize=14)
    plt.xticks(rotation=45)
    plt.xlabel(xlabel)

    p0 = ax1.plot_date(x8, v8, "r", label="T5_OPC")
    p1 = ax1.plot_date(x5, v5, "g", label="T2_AE")
    p2 = ax1.plot_date(x11, v11, "b", label="T_spin_AE")
    p3 = ax1.plot_date(x12, v12, "lime", label="T_spin_CCB")
    plots = p0 + p1 + p2 + p3

    ax1.set_ylabel("[°C]", color="k")
    ax1.tick_params("y", colors="k")

    ax1.set_xlim(xlimits)
    ax1.autoscale_view()

    # vertical lines indicating the flights + text
    for xc in xcoords:
        plt.axvline(x=xc, color="k", linewidth=2)
    for f in range(len(x_txt)):
        ax1.text(
            mdates.date2num(t_arr[f]),
            1,
            x_txt[f],
            weight="bold",
            horizontalalignment="right",
            verticalalignment="top",
            fontsize=12,
        )

    # ylim
    ylts = get_plot_range(v8 + v5 + v11 + v12, v_min_lim=T_MIN)
    ax1.set_ylim(ylts)

    ax1.yaxis.set_major_locator(mticker.LinearLocator(nticks))
    ax1.grid(color="k", linestyle="dashed")

    lbls = [l.get_label() for l in plots]
    legend = ax1.legend(plots, lbls, loc=0, shadow=True)
    legend.get_frame().set_facecolor("#ffffff")
    legend.get_frame().set_alpha(1.0)

    plt.tight_layout()

    majorFormatter = mdates.DateFormatter("%d.%m.%Y")
    ax1.xaxis.set_major_formatter(majorFormatter)

    plt.show()

    fig.savefig(
        path_dl_raw + fname,
        dpi=300,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format=None,
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
    )


#### Figure 5, special 1
#    x0 = loggerdata['Cargo_li_oben']['Time']
#    v0 = loggerdata['Cargo_li_oben']['Celsius(°C)']
#    x1 = loggerdata['Cargo_li_unten']['Time']
#    v1 = loggerdata['Cargo_li_unten']['Celsius(°C)']
#    x2 = loggerdata['Cargo_re_oben']['Time']
#    v2 = loggerdata['Cargo_re_oben']['Celsius(°C)']
#    x3 = loggerdata['Cargo_re_unten']['Time']
#    v3 = loggerdata['Cargo_re_unten']['Celsius(°C)']
#    x4 = loggerdata['Containerdach_links']['Time']
#    v4 = loggerdata['Containerdach_links']['Celsius(°C)']
#    x5 = loggerdata['Containerdach_rechts']['Time']
#    v5 = loggerdata['Containerdach_rechts']['Celsius(°C)']
#    fname = "Plot_Temperatures_Cargo.png"
#    fig, ax1 = plt.subplots(figsize=(16, 10))
#    plt.title(title, weight="bold", fontsize=14)
#    plt.xticks(rotation=45)
#    plt.xlabel(xlabel)
#
#    p0 = ax1.plot_date(x0, v0, 'r', label='Cargo_li_oben')
#    p1 = ax1.plot_date(x1, v1, 'g', label='Cargo_li_unten')
#    p2 = ax1.plot_date(x2, v2, 'b', label='Cargo_re_oben')
#    p3 = ax1.plot_date(x3, v3, 'lime', label='Cargo_re_unten')
#    p4 = ax1.plot_date(x4, v4, 'magenta', label='Containerdach_links')
#    p5 = ax1.plot_date(x5, v5, 'k', label='Containerdach_rechts')
#    plots = p0 + p1 + p2 + p3 + p4 + p5
#
#    ax1.set_ylabel('[°C]', color='k')
#    ax1.tick_params('y', colors='k')
#
#    ax1.set_xlim(xlimits)
#    ax1.autoscale_view()
#
#    # vertical lines indicating the flights + text
#    for xc in xcoords:
#        plt.axvline(x=xc, color='k', linewidth=2)
#    for f in range(len(x_txt)):
#        ax1.text(mdates.date2num(t_arr[f]), 1, x_txt[f], weight="bold",
#                 horizontalalignment='right', verticalalignment='top', fontsize=12)
#
#    # ylim
#    ylts = get_plot_range(v0+v1+v2+v3+v4+v5, v_min_lim=T_MIN)
#    ax1.set_ylim(ylts)
#
#    ax1.yaxis.set_major_locator(mticker.LinearLocator(nticks))
#    ax1.grid(color='k', linestyle='dashed')
#
#    lbls = [l.get_label() for l in plots]
#    legend = ax1.legend(plots, lbls, loc=0, shadow=True)
#    legend.get_frame().set_facecolor('#ffffff')
#    legend.get_frame().set_alpha(1.)
#
#    plt.tight_layout()
#
#    majorFormatter = mdates.DateFormatter('%d.%m.%Y')
#    ax1.xaxis.set_major_formatter(majorFormatter)
#
#    plt.show()
#
#    fig.savefig(path_dl_raw+fname, dpi=300, facecolor='w', edgecolor='w',
#                orientation='portrait', format=None,
#                transparent=False, bbox_inches=None, pad_inches=0.1)
#
#
#### Figure 6, special 2
##    x0 = loggerdata['Containerdach_links']['Time']
##    v0 = loggerdata['Containerdach_links']['Celsius(°C)']
#    x1 = loggerdata['Rack6_obenlinks']['Time']
#    v1 = loggerdata['Rack6_obenlinks']['Celsius(°C)']
#    x2 = loggerdata['Rack2_obenrechts']['Time']
#    v2 = loggerdata['Rack2_obenrechts']['Celsius(°C)']
#    x3 = loggerdata['Rack5_oben']['Time']
#    v3 = loggerdata['Rack5_oben']['Celsius(°C)']
#    fname = "Plot_Temperatures_ContOben.png"
#    fig, ax1 = plt.subplots(figsize=(16, 10))
#    plt.title(title, weight="bold", fontsize=14)
#    plt.xticks(rotation=45)
#    plt.xlabel(xlabel)
#
##    p0 = ax1.plot_date(x0, v0, 'r', label='Containerdach_links')
#    p1 = ax1.plot_date(x1, v1, 'g', label='Rack6_obenlinks')
#    p2 = ax1.plot_date(x2, v2, 'b', label='Rack2_obenrechts')
#    p3 = ax1.plot_date(x3, v3, 'lime', label='Rack5_oben')
#    plots = p1 + p2 + p3 #p0 +
#
#    ax1.set_ylabel('[°C]', color='k')
#    ax1.tick_params('y', colors='k')
#
#    ax1.set_xlim(xlimits)
#    ax1.autoscale_view()
#
#    # vertical lines indicating the flights + text
#    for xc in xcoords:
#        plt.axvline(x=xc, color='k', linewidth=2)
#    for f in range(len(x_txt)):
#        ax1.text(mdates.date2num(t_arr[f]), 1, x_txt[f], weight="bold",
#                 horizontalalignment='right', verticalalignment='top', fontsize=12)
#
#    # ylim
#    ylts = get_plot_range(v0+v1+v2+v3, v_min_lim=T_MIN)
#    ax1.set_ylim(ylts)
#
#    ax1.yaxis.set_major_locator(mticker.LinearLocator(nticks))
#    ax1.grid(color='k', linestyle='dashed')
#
#    lbls = [l.get_label() for l in plots]
#    legend = ax1.legend(plots, lbls, loc=0, shadow=True)
#    legend.get_frame().set_facecolor('#ffffff')
#    legend.get_frame().set_alpha(1.)
#
#    plt.tight_layout()
#
#    majorFormatter = mdates.DateFormatter('%d.%m.%Y')
#    ax1.xaxis.set_major_formatter(majorFormatter)
#
#    plt.show()
#
#    fig.savefig(path_dl_raw+fname, dpi=300, facecolor='w', edgecolor='w',
#                orientation='portrait', format=None,
#                transparent=False, bbox_inches=None, pad_inches=0.1)


df_Tin = pd.DataFrame(
    {"T_in": loggerdata["T1_IN"]["Celsius(°C)"]}, index=loggerdata["T1_IN"]["Time"]
)
df_Tin = df_Tin.resample("5T").mean()

df_Tout = pd.DataFrame(
    {"T_out": loggerdata["T3_out"]["Temperature"]}, index=loggerdata["T3_out"]["Time"]
)
df_Tout.loc[df_Tout["T_out"] < -15, "T_out"] = np.nan
df_Tout = df_Tout.resample("5T").mean()

df = df_Tin.join(df_Tout)
df.index.name = "Datetime"

df["flight"] = -1

for i, t0t1 in enumerate(zip(t_dep, t_arr)):
    df.loc[(df.index >= t0t1[0]) & (df.index <= t0t1[1]), "flight"] = startflightno + i

dst = Path(outpath) / f"F{startflightno:03d}inOutT.csv"
df.to_csv(dst, float_format="%.1f")
