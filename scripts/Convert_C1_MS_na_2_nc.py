# -*- coding: utf-8 -*-
import warnings
from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr
from caribic2dp.check_caribic_na import Header_Defaults
from caribic2dp.convert_caribic_na_nc4 import (
    CARIBIC_IVAR,
    CARIBIC_NC4_CFCONVENTION,
    CARIBIC_NC4_FORMAT_DATE,
    CARIBIC_NC4_FORMAT_VERS,
)
from caribic2dp.convert_caribic_na_nc4 import LICENSE as lic
from caribic2dp.convert_caribic_na_nc4 import SW_SPEC as sw_spec
from caribic2dp.convert_caribic_na_nc4 import CARIBIC_DVAR_lat, CARIBIC_DVAR_lon, CARIBIC_IVAR_long
from pyfuppes.na1001 import FFI1001
from pyfuppes.numberstring import NumStr

CARIBIC_DVAR_alt = "Altitude"  # C1 specific

params_file = sorted(
    f
    for f in (Path.home() / Path("Code/Python/Caribic2_DataProc/config/")).glob("parms_units*.csv")
    if "WSM" not in f.name
)[-1]

specs = pd.read_csv(params_file, sep=";").set_index("VNAME")

src = Path.home() / Path("Data/Caribic_1/MS_na")
dst = Path.home() / Path(
    "Data/Caribic_2/MS/zenodo_publication/2024.10.28/IAGOS-CARIBIC_MS_files_collection_20241028/CARIBIC-1"
)


doi = "10.5281/zenodo.14000090"
contributors = None  # could be a string naming all the PIs

use_float32 = True
write_to_disk = True
_warnings_enabled = False

msfiles = sorted(src.glob("MS_*.txt"))
fnos = {int(p.name.split("_")[2]) for p in msfiles}
print(f"missing: {sorted(set(range(1, 98))-fnos)}")

for f in msfiles:
    print(f.name)
    out = dst / f.name.upper().replace(".TXT", ".nc", 1).replace("SMV", "s", 1)

    # we need a valid caribic NASA Ames format
    na = FFI1001(f)

    # update ORG, MNAME etc.
    na.ONAME = "T. Schuck; D. Scharffe; C. Brenninkmeijer; schuck@iau.uni-frankfurt.de"  # type: ignore
    na.ORG = "Max Planck Institute for Chemistry (MPI-C), Mainz, Germany"  # type: ignore
    na.SNAME = "All continuous measurements (10s averages) for IAGOS-CARIBIC (onboard a Boeing-767ER of LTU)"  # type: ignore
    na.MNAME = "IAGOS-CARIBIC (CARIBIC-1), http://www.caribic-atmospheric.com/"  # type: ignore
    na.XNAME = Header_Defaults["XNAME"]

    vnames = [name.split(";")[0] for name in na.VNAME]

    dvars = na.to_dict_nparray(vdtype=np.float32 if use_float32 else np.float64)

    dvars.pop(CARIBIC_IVAR)  # we just want dependent variables, so pop ivar

    # rename CARIBIC 1 vars to the names used in CARIBIC 2
    for old, new in (
        ("LocTime", "LocalTime"),
        ("CO_ppbV", "CO"),
        ("spec_hum__e_3_g_kg_", "spec_hum__g_kg_"),
    ):
        vnames[vnames.index(old)] = new
        dvars[new] = dvars.pop(old)

    # TODO : ?
    #    - 'PosLat' --> 'lat'
    #    - 'PosLong' --> 'lon'

    dvars["spec_hum__g_kg_"] *= 1e3  # was kg/kg, has to be g/kg

    # export all variables
    params_to_process = list(dvars.keys())
    # remove H2Ogas since this did not come from a good instrument, according to AZ (personal com. 2024)
    rmv = params_to_process.index("H2Ogas")
    assert rmv >= 0
    params_to_process.pop(rmv)

    coords = {
        "time": pd.Timestamp(*na.DATE)
        + pd.to_timedelta(
            np.array(na.X, float), unit="s"
        ),  # NOTE : xarray does not want to handle time zones, so do not set UTC explicitly
    }

    # verify specified parameters exist in data
    param_index = {}
    for p in params_to_process:
        if p not in dvars:
            raise KeyError(f"could not find parameter '{p}' in data")
            # print(f"could not find parameter '{p}' in data")
        try:
            param_index[p] = specs.index.get_loc(p)
        except KeyError:
            raise KeyError(f"could not find parameter '{p}' in parameters config")
            # print(f"could not find parameter '{p}' in parameters config")

    data = {k: (["time"], v) for k, v in dvars.items() if k in params_to_process}

    ds = xr.Dataset(data, coords=coords)

    # add NASA Ames file header
    ds.attrs["na_file_header"] = np.array(na._HEADER, dtype=str)

    # general (global) attributes
    ds.attrs["title"] = "IAGOS-CARIBIC netCDF4 data file"
    ds.attrs["date_created"] = (
        pd.Timestamp.now(tz="UTC").isoformat(timespec="seconds").replace("+00:00", "Z")
    )
    ds.attrs["summary"] = "merged IAGOS-CARIBIC data (CARIBIC-1), on a 10s grid"  # new in v0.5
    ds.attrs["keywords"] = (
        "greenhouse gases, aerosols, trace gases, aircraft observations, in-situ, monitoring, long-term"  # new in v0.5
    )
    ds.attrs["conventions"] = CARIBIC_NC4_CFCONVENTION
    ds.attrs["mission"] = na.MNAME
    ds.attrs["data_description"] = na.SNAME
    ds.attrs["data_institute"] = na.ORG
    ds.attrs["data_owners"] = (
        "A. Zahn; H. Boenisch; T. Gehrlein; F. Obersteiner; contact: andreas.zahn@kit.edu"
    )

    if contributors:
        ds.attrs["data_contributors"] = contributors

    ds.attrs["license"] = lic  # added in v0.2

    if doi:
        ds.attrs["doi"] = doi  # added in v0.2

    ds.attrs["format_date"] = CARIBIC_NC4_FORMAT_DATE
    ds.attrs["format_version"] = CARIBIC_NC4_FORMAT_VERS
    ds.attrs["history"] = (
        "Converted from NASA Ames format. "
        "Might contain only a subset of the parameters from the original NASA Ames file. "
    ) + sw_spec

    # to be able to recreate the input NASA Ames file, we need time format
    fmt = NumStr(na.X[0]).analyse_format()[0]
    ds.attrs["ivar_C_format"] = "%" + fmt

    ds["time"].attrs["name"] = CARIBIC_IVAR
    ds["time"].attrs["standard_name"] = "time"
    ds["time"].attrs["long_name"] = CARIBIC_IVAR_long

    # the index where to check parameter format (decimal places etc.)
    check_index = int(len(na.X) / 2)

    # attributes of variables
    for p in params_to_process:
        ds[p].attrs["standard_name"] = specs.iloc[param_index[p]].CF_StandardName
        ds[p].attrs["long_name"] = specs.iloc[param_index[p]].Long_Name
        ds[p].attrs["unit"] = specs.iloc[param_index[p]].Unit.strip("[]")
        ds[p].attrs["standard_unit"] = specs.iloc[param_index[p]].CF_Unit.strip("[]")
        if ds[p].attrs["standard_name"].endswith("standard_error"):
            ds[p].attrs["coverage_content_type"] = "qualityInformation"  # ISO 19115-1
        else:
            ds[p].attrs["coverage_content_type"] = "physicalMeasurement"  # ISO 19115-1

        # verify parameter format
        vname_idx = vnames.index(p)
        want = specs.iloc[param_index[p]].py_format.strip("{}").replace(":", "")
        have = NumStr(na.V[vname_idx][check_index]).analyse_format()[0]
        if want.strip("%") != have and _warnings_enabled:
            warnings.warn(
                f"Warning: format specifier mismatch for parameter '{p}', want '{want}'"
                f" but found '%{have}' (e.g. value is {na.V[vname_idx][check_index]})"
            )
        # set the format from params_file
        ds[p].attrs["C_format"] = "%" + want

    # mark time coordinate as T
    ds["time"].attrs["axis"] = "T"
    ds["time"].attrs["coverage_content_type"] = "coordinate"

    # try to add lat/lon/alt as additional coordinates # added in v0.3
    # for C1 data, this has to succeed; no try / except
    ds.coords[CARIBIC_DVAR_lat] = ds.data_vars[CARIBIC_DVAR_lat]
    ds[CARIBIC_DVAR_lat].attrs["axis"] = "Y"
    ds[CARIBIC_DVAR_lat].attrs["coverage_content_type"] = "coordinate"
    ds.coords[CARIBIC_DVAR_lon] = ds.data_vars[CARIBIC_DVAR_lon]
    ds[CARIBIC_DVAR_lon].attrs["axis"] = "X"
    ds[CARIBIC_DVAR_lon].attrs["coverage_content_type"] = "coordinate"
    ds.coords[CARIBIC_DVAR_alt] = ds.data_vars[CARIBIC_DVAR_alt]
    ds[CARIBIC_DVAR_alt].attrs["axis"] = "Z"
    ds[CARIBIC_DVAR_alt].attrs["coverage_content_type"] = "coordinate"

    # add bounding box (new in v0.5)
    ds.attrs["geospatial_bounds_crs"] = "EPSG:4326"
    ds.attrs["geospatial_bounds_vertical_crs"] = "EPSG:5829"
    ds.attrs["geospatial_lat_min"] = ds[CARIBIC_DVAR_lat].values.min()
    ds.attrs["geospatial_lat_max"] = ds[CARIBIC_DVAR_lat].values.max()
    ds.attrs["geospatial_lon_min"] = ds[CARIBIC_DVAR_lon].values.min()
    ds.attrs["geospatial_lon_max"] = ds[CARIBIC_DVAR_lon].values.max()
    ds.attrs["geospatial_lat_units"] = "degree_north"
    ds.attrs["geospatial_lon_units"] = "degree_east"
    ds.attrs["geospatial_vertical_min"] = ds[CARIBIC_DVAR_alt].values.min()
    ds.attrs["geospatial_vertical_max"] = ds[CARIBIC_DVAR_alt].values.max()
    ds.attrs["geospatial_vertical_positive"] = "up"
    ds.attrs["geospatial_vertical_units"] = "m"
    ds.attrs["time_coverage_start"] = coords["time"].min().isoformat().replace("+00:00", "Z")
    ds.attrs["time_coverage_end"] = coords["time"].max().isoformat().replace("+00:00", "Z")

    # except KeyError:
    #     if _warnings_enabled:
    #         warnings.warn("failed to add lat/lon/alt coordinates")

    if write_to_disk:
        n = ds.to_netcdf(out)
        print(f"    wrote: {out.as_posix()}")
        if n == 0 and _warnings_enabled:
            warnings.warn("no data written")
    else:
        print(f"    skipped: {out.name}, write disabled")
