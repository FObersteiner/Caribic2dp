# -*- coding: utf-8 -*-
"""
get info on flights and available data,
prints .txt and .nas file paths to the console for selected flight range
"""
import os
from pathlib import Path

import numpy as np

from caribic2dp.carfileutils import find_files, flight_no_to_flightdir

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
# CARIBIC 1
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic1data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC1data/Flights/"
ma_iata_offset = -21

# CARIBIC 2
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
ma_iata_offset = -15
# <<< ! user-specific paths

flight_range = [590, 591]
FILE_EXT = [".txt", ".nas"]


prefixes = []
for flight_no in range(flight_range[0], flight_range[1] + 1):
    sel_flight_dir = flight_no_to_flightdir(flight_no, flights_dir)

    if sel_flight_dir:
        print("+++")
        print("+++ Flight No.", flight_no, "+++")
        ma_file = find_files(flight_no, "MA", "", file_dir=sel_flight_dir, resolution="full")

        if ma_file:
            print(
                "from:",
                str(ma_file)[ma_iata_offset : ma_iata_offset + 3],
                "to:",
                str(ma_file)[ma_iata_offset + 4 : ma_iata_offset + 7],
            )
            print("data:")
        else:
            print("no master data found!")
            continue  # skip this iteration

        all_files = os.listdir(sel_flight_dir)
        for file in all_files:
            for ext in FILE_EXT:
                if file.endswith(ext):
                    print(file)
                    prefixes.append(file[: file.find("_")])
    else:
        print("+++ Flight", flight_no, "not available!")

prefixes = np.unique(prefixes)
