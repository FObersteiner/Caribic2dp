# -*- coding: utf-8 -*-
import os
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
from matplotlib.font_manager import FontProperties
from mpl_toolkits.basemap import Basemap
from pyfuppes.misc import print_progressbar
from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.txt2dict import txt_2_dict_simple

from caribic2dp.carfileutils import dissect_filename, find_files

# ------------------------------------------------------------------------------

wd = sys.path[0]
if ".pyenv" in wd:
    wd = os.getcwd()
wd = Path(wd)

# config ----------------------------------------------------------------------

caribic = 2
flights = list(range(588, 592))
flight_min_max = [min(flights), max(flights)]  # [591, 591]
PROJECTION = "merc"

# ! user-specific paths >>>
flights_dir = Path(f"/home/va6504/Carfs1/Caribic/extern/CARIBIC{caribic}data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
save_png = (
    Path("/home/va6504/Carfs1/Caribic/extern/CARIBIC_flightroutes")
    / f"CARIBIC_flightroutes_{flight_min_max[0]}_{flight_min_max[1]}_{PROJECTION}.png"
)
# <<< ! user-specific paths


# data source ---
ap_subset = None  # ["MAA", "BKK", "KUL", "CAN", "HKG"] # set to None to show all

ap_file = Path("../../Config/CARIBIC2_Airports.csv").resolve()
ap_dict = txt_2_dict_simple(ap_file)["data"]

file_prefix = "MA"
lat_lon_vnames = ["PosLat", "PosLong"]
depdest_tag, depdest_str = "FlightRoute", None
noval = 999

# plotting ---

fig_sz = (16, 9)

PLOT_TRACKS, PLOT_BLOBS, SAVE_GRAPH = 1, 0, 0

colcode_select, track_basecolor, track_lw = (
    "Continent",
    "midnightblue",
    1.0,
)  # 'Continent'
ap_marker, ap_marker_color, ap_marker_sz = "o", "lightgrey", 5
ap_text_color = "w"
show_ap_labels = True


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


# find all MA 10s files in Caribic 2 flights folder
print_progressbar(
    0,
    (flight_min_max[1] - flight_min_max[0] + 1),
    prefix="Searching MA files...",
    suffix="Complete",
    length=50,
)
files = []
for i in flights:  # range(flight_min_max[0], flight_min_max[1]+1):
    print_progressbar(
        (i + 1 - flight_min_max[0]),
        (flight_min_max[1] - flight_min_max[0] + 1),
        prefix="Searching MA files...",
        suffix="Complete",
        length=50,
    )
    file = find_files(
        i,
        file_prefix,
        flights_dir,
        caribic=caribic,  # show_error=True,
        resolution="10s",
    )[0]
    if file:
        files.append(file)


madata_dict = {"flight_no": [], "dep_dest": [], "lat": [], "lon": []}
allairports = set()


# load na files and extract lat/lon + dep/dest airport
print_progressbar(0, len(files), prefix="loading Data...", suffix="Complete", length=50)

for i, file in enumerate(files):
    print_progressbar(i + 1, len(files), prefix="loading Data...", suffix="Complete", length=50)
    na_data = na1001(file)

    depdest = file.name.split("_")[3:5]
    allairports.add(depdest[0])
    allairports.add(depdest[1])

    if depdest[0] not in ap_dict["IATA"]:
        print(f"airport {depdest_str[0]} not found in airports data.")
    if depdest[1] not in ap_dict["IATA"]:
        print(f"airport {depdest_str[1]} not found in airports data.")

    incl = 1
    if ap_subset:  # only airport subset should be used
        incl = [ap for ap in ap_subset if ap == depdest[0] or ap == depdest[1]]

    if incl:
        madata_dict["dep_dest"].append(depdest)

        ix_lat, ix_lon = -1, -1
        for i, _ in enumerate(na_data.VNAME):
            if na_data.VNAME[i][0 : len(lat_lon_vnames[0])] == lat_lon_vnames[0]:
                ix_lat = i
            if na_data.VNAME[i][0 : len(lat_lon_vnames[1])] == lat_lon_vnames[1]:
                ix_lon = i

        if ix_lat > -1 and ix_lon > -1:
            madata_dict["lat"].append(list(map(float, na_data.V[ix_lat])))
            madata_dict["lon"].append(list(map(float, na_data.V[ix_lon])))
            madata_dict["flight_no"].append(dissect_filename(file).flight_no)


ap_lat, ap_lon = np.array(ap_dict["PosLat"], dtype="float32"), np.array(
    ap_dict["PosLon"], dtype="float32"
)
aptxt_lat, aptxt_lon = np.array(ap_dict["Plttxt_lat"], dtype="float32"), np.array(
    ap_dict["Plttxt_lon"], dtype="float32"
)

print("\ncreating map...")


if PLOT_TRACKS:
    fig, axes = plt.subplots(figsize=fig_sz)
    plt.title(
        "CARIBIC flights " + str(flight_min_max[0]) + " to " + str(flight_min_max[1]) + "\n",
        color="k",
    )

    m = Basemap(
        lat_0=48,
        lon_0=11,
        ax=axes,
        projection=PROJECTION,
        resolution="c",
        llcrnrlat=-80,
        urcrnrlat=80,
        llcrnrlon=-180,
        urcrnrlon=180,
        lat_ts=20,
    )

    m.bluemarble()  # scale=0.5

    # m.drawlsmask(land_color='white', ocean_color='magenta', lakes=True)

    # m.drawcoastlines(color='w')
    # m.drawmapboundary(fill_color='aqua')
    # m.fillcontinents(color='w', lake_color='aqua')
    # m.drawparallels(np.arange(10, 90, 20), labels=[1, 1, 0, 1])
    # m.drawmeridians(np.arange(-180, 180, 30), labels=[1, 1, 0, 1])

    print("plotting flight tracks...")
    for i, fno in enumerate(madata_dict["flight_no"]):
        lat, lon = np.array(madata_dict["lat"][i]), np.array(madata_dict["lon"][i])
        # nan_ix = np.append(np.where(lat > noval), np.where(lon > noval))
        # nan_ix = np.unique(nan_ix)
        ix_vd = np.nonzero((lat < noval) & (lon < noval))
        # lat, lon = np.delete(lat, nan_ix), np.delete(lon, nan_ix)
        x, y = m(lon[ix_vd], lat[ix_vd])
        flcol = track_basecolor
        if colcode_select == "Continent":
            cont = [
                ap_dict["Continent"][ap_dict["IATA"].index(madata_dict["dep_dest"][i][0])],
                ap_dict["Continent"][ap_dict["IATA"].index(madata_dict["dep_dest"][i][1])],
            ]
            cont = cont[1] if cont[0] == "Europe" else cont[0]
            flcol = ap_dict["Pltflght_col"][ap_dict["Continent"].index(cont)]
        axes.plot(x, y, "-", color=flcol, linewidth=track_lw)

    print("plotting airports...")
    fp0 = FontProperties()
    fp = fp0.copy()  # create copy to not overwrite default
    fp.set_weight("bold")
    fp.set_size(10)
    for i, _ in enumerate(ap_dict["IATA"]):
        # IATA names only if not network plot
        if int(ap_dict["show_on_map"][i]) == 1 and not PLOT_BLOBS:
            x, y = m(ap_lon[i], ap_lat[i])
            axes.plot(
                x,
                y,
                marker=ap_marker,
                color=ap_marker_color,
                markersize=ap_marker_sz,
            )
            if show_ap_labels:
                x, y = m(aptxt_lon[i], aptxt_lat[i])
                axes.text(x, y, ap_dict["IATA"][i], fontproperties=fp, color=ap_text_color)

    plt.tight_layout()
    plt.show()  # block=False

    if SAVE_GRAPH and not PLOT_BLOBS:
        print(f"saving {save_png}...")
        fig.savefig(
            save_png,
            dpi=330,  # bbox_inches='tight',facecolor='w', edgecolor='w',
            orientation="landscape",
            # format="eps",
        )  # transparent=False,

#### plot network:
# for each airport: iata / n_visits / lat lon
if PLOT_BLOBS:
    netdata = {"dep_dest": [], "n_flights": [], "pos_dep": [], "pos_dest": []}

    for ix, flno in enumerate(madata_dict["flight_no"]):
        dep_dest = "_".join(madata_dict["dep_dest"][ix])
        if dep_dest not in netdata["dep_dest"]:
            netdata["dep_dest"].append(dep_dest)
            netdata["n_flights"].append(1)
            netdata["pos_dep"].append(
                (
                    ap_lat[ap_dict["IATA"].index(madata_dict["dep_dest"][ix][0])],
                    ap_lon[ap_dict["IATA"].index(madata_dict["dep_dest"][ix][0])],
                )
            )
            netdata["pos_dest"].append(
                (
                    ap_lat[ap_dict["IATA"].index(madata_dict["dep_dest"][ix][1])],
                    ap_lon[ap_dict["IATA"].index(madata_dict["dep_dest"][ix][1])],
                )
            )
        else:
            netdata["n_flights"][netdata["dep_dest"].index(dep_dest)] += 1

    df = pd.DataFrame(
        {
            "from": [s.split("_")[0] for s in netdata["dep_dest"]],
            "to": [s.split("_")[1] for s in netdata["dep_dest"]],
        }
    )

    G = nx.from_pandas_edgelist(df, "from", "to", create_using=nx.Graph())
    G.nodes()

    if not PLOT_TRACKS:
        fig, axes = plt.subplots(figsize=(16, 9))
        m = Basemap(lat_0=48, lon_0=11, ax=axes)  # projection=PROJECTION, resolution=None,
        m.bluemarble()

    mx, my = m(ap_lon, ap_lat)

    nodes = list(G.nodes)
    dep_ap = [ap.split("_")[0] for ap in netdata["dep_dest"]]
    node_pos = {}
    node_sz = []
    node_col = []
    for ix, node in enumerate(nodes):
        node_pos[node] = (
            mx[ap_dict["IATA"].index(node)],
            my[ap_dict["IATA"].index(node)],
        )
        if node in ["MUC", "FRA"]:
            node_sz.append(1500)
            node_col.append("g")
        else:
            node_sz.append(700 + 200 * netdata["n_flights"][dep_ap.index(node)])
            node_col.append("b")

    nx.draw_networkx_nodes(G, node_pos, node_size=node_sz, node_color=node_col, alpha=0.65)
    nx.draw_networkx_labels(G, node_pos, font_color="w", font_weight="bold")

    plt.show()  # block=True

    if SAVE_GRAPH:
        print(f"saving {save_png}...")
        fig.savefig(
            Path(str(save_png).replace("proj.png", "proj_blobs.png")),
            dpi=300,
            facecolor="w",
            edgecolor="w",
            orientation="portrait",
            papertype=None,
            format=None,
            transparent=False,
            bbox_inches=None,
            pad_inches=0.1,
        )

#    dep = [s.split('_')[0] for s in netdata['dep_dest']]
#    dest = [s.split('_')[1] for s in netdata['dep_dest']]
#    unq_ap = list(set(dep+dest))
#    unq_ap.pop(unq_ap.index('MUC'))
#    unq_ap_n_fl = [0] * len(unq_ap)
#    for ix_ap, ap in enumerate(unq_ap):
#        for ix_dd, dpdst in enumerate(netdata['dep_dest']):
#            if ap in dpdst:
#                unq_ap_n_fl[ix_ap] += netdata['n_flights'][ix_dd]


print("done.")
