# -*- coding: utf-8 -*-
import os
import sys
from datetime import datetime, timedelta  # , timezone
from pathlib import Path

import numpy as np
import pandas as pd
from pyfuppes.geo import geodesic_dist
from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.txt2dict import txt_2_dict_simple

from caribic2dp.carfileutils import find_files

# ------------------------------------------------------------------------------

wd = sys.path[0]
if ".pyenv" in wd:
    wd = os.getcwd()
wd = Path(wd)

flightrange = [590, 591]
caribic_no = 2 if flightrange[1] > 100 else 1

# ! user-specific paths >>>
fldir = Path(f"/home/va6504/Carfs1/Caribic/extern/CARIBIC{caribic_no}data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

ap_file = (wd / f"../../Config/CARIBIC{caribic_no}_Airports.csv").resolve()
out = (wd / f"../../Stat/Caribic{caribic_no}_Statistics.xlsx").resolve()
outg0 = (wd / f"../../Stat/Caribic{caribic_no}_Statistics_byYear.xlsx").resolve()
outg1 = (wd / f"../../Stat/Caribic{caribic_no}_Statistics_byDest.xlsx").resolve()

overwrite_existing = True
add_nonexisting = True  # add entries for non-existing flights

flightrange[1] += 1
flights = set(range(*flightrange))

df0 = pd.DataFrame()
if not overwrite_existing:
    df0 = pd.read_excel(out)
    flights -= set(df0["flight"])

ap_dict = txt_2_dict_simple(ap_file)["data"]

d = {
    "flight": [],
    "year": [],
    "dest": [],
    "dest_long": [],
    "startUTC": [],
    "endUTC": [],
    "onlineDurH": [],
    "onlineDistKM": [],
}


# for all flights in the flightrange...
for fl in flights:
    print(f"processing flight: {fl}")

    # find the MA file and load data
    mafile = find_files(
        fl,
        "MA",
        flights_dir=fldir,
        caribic=caribic_no,
        resolution="10s" if caribic_no == 1 else "full",
    )[0]
    if not mafile:
        print(f"no MA file found for flight {fl}!")
        if add_nonexisting:
            d["dest"].append(None)
            d["dest_long"].append(None)
            d["flight"].append(fl)
            d["year"].append(None)
            d["startUTC"].append(None)
            d["endUTC"].append(None)
            d["onlineDurH"].append(None)
            d["onlineDistKM"].append(None)
        continue

    dest = mafile.name.split("_")[4]
    if dest not in ap_dict["IATA"]:
        print(f"airport not found: {d['dest'][-1]}!")
        dest = "undefined"

    d["dest"].append(dest)
    d["dest_long"].append(ap_dict["Name"][ap_dict["IATA"].index(d["dest"][-1])])

    # load the data
    madata = na1001(mafile)
    fldate = datetime(*madata.DATE)  # , tzinfo=timezone.utc)

    d["flight"].append(fl)
    # add start and end of master data recording + duration in hours
    d["year"].append(fldate.year)
    d["startUTC"].append(fldate + timedelta(seconds=float(madata.X[0])))
    d["endUTC"].append(fldate + timedelta(seconds=float(madata.X[-1])))
    d["onlineDurH"].append((d["endUTC"][-1] - d["startUTC"][-1]).total_seconds() / 3600)

    # add travelled distance
    ix_lat, ix_lon = None, None
    for ix, string in enumerate(madata.VNAME):
        if "poslat" in string[0:8].lower():
            ix_lat = ix
        if "poslon" in string[0:8].lower():
            ix_lon = ix
    vmiss = float(madata.VMISS[0])

    v_lat = np.array(madata.V[ix_lat], dtype=float)
    v_lat = v_lat[~np.isclose(v_lat, vmiss)]
    v_lon = np.array(madata.V[ix_lon], dtype=float)
    v_lon = v_lon[~np.isclose(v_lon, vmiss)]
    d["onlineDistKM"].append(geodesic_dist(v_lat, v_lon))


df1 = pd.DataFrame(d)
df = pd.concat([df0, df1]).sort_values(by=["flight"])
df.to_excel(out, index=False)

dfg = df.groupby(["year"]).sum(numeric_only=True).drop(["flight"], axis=1)
dfg.to_excel(outg0, index=True)

dfg = df.groupby(["dest"]).sum(numeric_only=True).drop(["year", "flight"], axis=1)
dfg["count"] = df.groupby(["dest"]).size()
dfg.to_excel(outg1, index=True)
