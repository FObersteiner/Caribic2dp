# -*- coding: utf-8 -*-
import math
import os
from datetime import date, datetime, timezone
from pathlib import Path

from pyfuppes.timeconversion import dtstr_2_mdns, mdns_2_dtobj
from pyfuppes.txt2dict import txt_2_dict_simple as t2d
from timezonefinder import TimezoneFinder
from zoneinfo import ZoneInfo

from caribic2dp.ma_tools import MA_check_Arinc_essential_v2

# ------------------------------------------------------------------------------

###############################################################################
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F550-553/processed/Flightseries_550_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F554-557/processed/Flightseries_554_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F554-557/processed/Flightseries_554_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F562-565/processed/Flightseries_562_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F566-569/processed/Flightseries_566_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F570-573/processed/Flightseries_570_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F574-577/processed/Flightseries_574_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F578-583/processed/Flightseries_578_Sortfiles')
# dir_in = Path('D:/KIT/Dataprocessing_Caribic/MA_data/F584-587/processed/Flightseries_584_Sortfiles')
dir_in = Path("/home/va6504/Data/Caribic_2/MA/F588-591/processed/Flightseries_588_Sortfiles")


TS_FMT = "%d.%m.%y %H:%M:%S.%f"
VMISS = "999999"
SEP, CRLF = "\t", "\n"

arifiles = list(dir_in.glob("Flight_*_merged.ari"))
tf = TimezoneFinder()

for file in arifiles:
    print("processing: " + file.name)

    ari_dict = t2d(file, sep="\t")["data"]

    ari_dict = MA_check_Arinc_essential_v2({"ari": ari_dict}, "ari", ts_fmt=TS_FMT, errorlog=False)[
        "ari"
    ]

    keys = list(ari_dict.keys())
    n_el = len(ari_dict[keys[0]])
    PosLong = ari_dict["PosLong"]

    LocalTime, pstatic, Tpot, Altitude, Utc_Arinc = [], [], [], [], []

    day_zero = datetime(year=1899, month=12, day=30)
    day_zero = day_zero.replace(tzinfo=timezone.utc)

    fldate = datetime.strptime(ari_dict["PCUTC"][0], TS_FMT)
    fldate = fldate.replace(tzinfo=timezone.utc)
    fldate_str = datetime.strftime(fldate, "%Y%m%d")

    days_delta = float((fldate - day_zero).days)
    days_add, days_add_max = 0.0, 1.0

    tz_off = 0  # local solar time initial time zone shift
    # constants for local solar time
    DoY = date(fldate.year, fldate.month, fldate.day) - date(fldate.year, 1, 1)
    DoY = float(DoY.days)
    B = (360.0 / 365.0) * (DoY - 81.0)  # [d]
    EoT = 9.87 * math.sin(2.0 * B) - 7.53 * math.cos(B) - 1.5 * math.sin(B)  # equation of time

    for ix, entry in enumerate(ari_dict["PCUTC"]):
        tstr = ari_dict["UTC"][ix]
        try:
            mdnsec = dtstr_2_mdns(
                fldate_str + tstr,
                "%Y%m%d%H%M.%S",
                ymd=[fldate.year, fldate.month, fldate.day],
            )
        except ValueError:
            Utc_Arinc.append(VMISS)
        else:
            if ix > 0 and float(tstr) < float(ari_dict["UTC"][ix - 1]) and days_add < days_add_max:
                days_add += 1.0
            Utc_Arinc.append(days_delta + mdnsec / 86400.0 + days_add)

    for i in range(n_el):  # calculated parameters
        # static pressure
        if ari_dict["StdAltitu"][i] != VMISS:
            pstatic.append(
                math.exp(
                    (1 / 0.190284)
                    * math.log(1 - (float(ari_dict["StdAltitu"][i])) * 0.3048 * 0.0065 / 288.15)
                )
                * 1013.246
            )
        else:
            pstatic.append(VMISS)

        # potential temperature
        if ari_dict["StcAirTmp"][i] != VMISS and pstatic[-1] != VMISS:
            Tpot.append(
                "{0:.1f}".format(
                    (float(ari_dict["StcAirTmp"][i]) + 273.15)
                    * math.exp(0.2857 * math.log(1000.0 / pstatic[-1]))
                )
            )
        else:
            Tpot.append(VMISS)

        # pressure altitude
        if ari_dict["StdAltitu"][i] != VMISS:
            Altitude.append("{0:.1f}".format(float(ari_dict["StdAltitu"][i]) * 0.3048))
        else:
            Altitude.append(VMISS)

        # local solar time
        try:
            tz_str = tf.timezone_at(
                lng=float(ari_dict["PosLong"][i]), lat=float(ari_dict["PosLat"][i])
            )
        except KeyError:
            tz_str = None

        if tz_str:  # only update timezone shift if tzwhere doesn't return None
            zone = ZoneInfo(tz_str)
            if zone is not None:
                tz_off = fldate.replace(tzinfo=zone).utcoffset().total_seconds() / 3600

        LSTM = 15.0 * tz_off  # Local Standard Time Meridian
        t_corr = (4.0 * (float(PosLong[i]) - LSTM) + EoT) / 60.0 / 24.0  # [d]

        if Utc_Arinc[i] == VMISS:
            LocalTime.append(VMISS)
        else:
            LST_frac = (Utc_Arinc[i] + tz_off / 24.0 - days_delta) + t_corr
            if LST_frac > 1.0:
                LST_frac -= math.floor(LST_frac)
            LocalTime.append(LST_frac)

    for ix, entry in enumerate(LocalTime):
        if entry != VMISS:
            LocalTime[ix] = mdns_2_dtobj(entry * 86400, fldate, posix=False, str_fmt="%H:%M:%S")

    ari_dict["LocUTC"] = LocalTime
    ari_dict["pstatic"] = ["{0:.2f}".format(v) for v in pstatic]
    ari_dict["Tpot"] = Tpot
    ari_dict["Altitude"] = Altitude
    ari_dict["ToAiTmp"] = ari_dict.pop("ToAiSpeed")

    # delete :
    for key in ["DistToGo", "TimeToGo", "LeftPres", "RightPres", "PCUTC_datetime"]:
        ari_dict.pop(key)

    outfile = Path(str(file) + "xt")
    keys = list(ari_dict.keys())

    if keys.index("ToAiTmp") != 13:
        keys.insert(13, keys.pop(keys.index("ToAiTmp")))

    header = "".join([k + SEP for k in keys])
    with open(outfile, "w", encoding="UTF-8") as file_obj:
        file_obj.write(header[:-1] + CRLF)
        for i in range(n_el):
            line = "".join([ari_dict[k][i] + SEP for k in keys])
            file_obj.write(line[:-1] + CRLF)

    print("wrote: " + os.path.basename(outfile))
