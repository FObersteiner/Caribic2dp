# -*- coding: utf-8 -*-
import os
import sys
from datetime import datetime, timezone
from pathlib import Path

import numpy as np
from pyfuppes.avgbinmap import bin_t_10s, bin_y_of_t
from pyfuppes.geo import get_EoT, get_LSTdayFrac
from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.timeconversion import dtobj_2_mdns
from pyfuppes.v25 import collect_V25Logs, logs_cleanup
from timezonefinder import TimezoneFinder
from zoneinfo import ZoneInfo

from caribic2dp.find_airport import get_closest_AP as get_ap
from caribic2dp.ma_tools import (
    MA_check_Arinc_essential_v2,
    MA_check_Arinc_optional,
    MA_data_chk_time,
    MA_get_flight_ix,
    MA_get_pstat_palt_Tpot,
    MA_load_fileheader,
    MA_write_sortfiles,
)
from caribic2dp.ma_tools_diagnostics import diag_plots_indiv_flights, diag_plots_ov
from caribic2dp.parms_units import vname_2_ncom_tblock
from caribic2dp.check_caribic_na import Header_Defaults

###############################################################################
tf = TimezoneFinder()

t0 = datetime.now()  # timer t0
# config ######################################################################
print("+++")

#
#
path = Path.home() / Path("Data/Caribic_2/MA/F150-153/LH_150_MA/MASTDATA")
folders = [path / f for f in os.listdir(path) if os.path.isdir(path / f)]
outpath = path.parent.parent / "processed"
config = outpath / "filehdr_config" / "MA_file_cnf_150.txt"
#
#
PARM_CFG = Path.home() / Path("Code/Python/Caribic2_DataProc/config/parms_units_2023-11-16.csv")
assert PARM_CFG.exists(), f"could not load params config from\n{PARM_CFG}"

AP_FILE = Path.home() / Path("Code/Python/Caribic2_DataProc/config/CARIBIC2_Airports.csv")
assert AP_FILE.exists(), f"could not load airports from\n{AP_FILE}"

NA_CFG = Path.home() / Path("Code/Python/Caribic2_DataProc/config/NA_header_cfg.toml")
assert NA_CFG.exists(), f"could not load na config from\n{NA_CFG}"

flightseries = int(str(path)[-15:-12])

__version__ = "2024.04.19a"

verbose = True
clean_input = True
write_sortfiles = True
errorlog = True
check_optional = True

diagn_plots_overview = [True, False]  # ix0: call, ix1: show
diagn_plots_flightwise = [True, True]  # ix0: call, ix1: close


MA_header = MA_load_fileheader(config)
vnames_tmp = []
for element in MA_header["VNAME"]:
    vnames_tmp.append(element)

NCOM_terminal_block = vname_2_ncom_tblock(
    vnames_tmp,
    param_file=PARM_CFG,
    header_config_file=NA_CFG,
    out_type="MA",
    set_vmiss=MA_header["VMISS"],
)

vmiss = MA_header["VMISS"]  # type int
vers_tag = MA_header["vers_tag"]

exts = ["ari", "ilt", "snd"]
# ext_special = 'prc' # prc data is written with irregular number of columns!

key_ari = "ari"
ts_fmts = {
    "ari": "%d.%m.%y %H:%M:%S.%f",
    "ilt": "%d.%m.%y %H:%M:%S",
    "snd": "%d.%m.%y %H:%M:%S.%f",
    "prc": "%d.%m.%y %H:%M:%S.%f",
}

# note on flstates: TakeOff is considered == TakeOffRoll in MA_get_flight_ix()
flstates = [
    "nn",
    "PowerOn",
    "EngineStart",
    "TaxiOut",
    "TakeOff",
    "Climb",
    "Cruise",
    "Approach",
    "RollOut",
    "TaxiIn",
    "EngineStop",
]
DEL = "\t"
COLHDR_IX = 0
OUTPUT_MODES = ["1s", "10s"]

for folder in folders:
    try:
        os.stat(folder)
    except FileNotFoundError:
        print("\nERROR: folder not found.\n")
        sys.exit()

outpath_sort = outpath / ("Flightseries_" + str(flightseries) + "_Sortfiles/")
outpath_logs = outpath / ("Flightseries_" + str(flightseries) + "_Logfiles/")

try:
    os.stat(outpath)
except OSError:
    print(f"created directory: {outpath.as_posix()}")
    os.mkdir(outpath)


# beginning import/export #####################################################
if clean_input:
    logs_cleanup(folders, verbose=verbose)

print("+++ loading data... +++")
data = {}
for ext in exts:
    data[ext] = collect_V25Logs(folders, ext, delimiter=DEL, colhdr_ix=COLHDR_IX, verbose=verbose)

# if ext_special:
#     if clean_input:
#         V25Logs_cleanup(folders, ext_special, verbose=verbose)
#     data[ext_special] = Collect_irregular(folders, ext_special,
#                                           delimiter=DEL,
#                                           colhdr_ix=COLHDR_IX, verbose=verbose)

if write_sortfiles:
    print("+++ parsing flight series(0)... +++")
    flight_info = MA_get_flight_ix(
        data,
        key_ari,
        flightseries,
        states=flstates,
        save_diagn_pl=outpath / "diagnostics/ari_FlightState_v0.png",
        plt_keep=diagn_plots_overview[1],
    )
    print("+++ detected flights, pre-cleanup: " + str(flight_info["flights"]) + " +++")
    MA_write_sortfiles(data, flight_info, ts_fmts, outpath_sort, merge_keys=["ari"])
    print("+++ created appended logfiles (flight-wise) +++")

print("+++ checking time vectors... +++")
data = MA_data_chk_time(data, ts_fmts)

print("+++ performing consistency check: essential params... +++")
data = MA_check_Arinc_essential_v2(
    data, key_ari, errorlog=errorlog, logpath=outpath_logs, flightseries=[flightseries]
)

print("+++ parsing flight series... +++")
flight_info = MA_get_flight_ix(
    data,
    key_ari,
    flightseries,
    states=flstates,
    write_log=outpath_logs,
    save_diagn_pl=outpath / "diagnostics/ari_FlightState_v1.png",
    plt_keep=diagn_plots_overview[1],
)
print(f"+++ detected flights, post-cleanup: {flight_info['flights']} +++")

data[key_ari]["FlightPhas"] = flight_info["state_nbr"]

flightseries = list(map(str, flight_info["flights"]))

if diagn_plots_overview[0]:
    diag_plots_ov(data, flightseries, outpath / "diagnostics", show_plots=diagn_plots_overview[1])

MA_data = {}
keys_ari = list(data[key_ari].keys())
for i in range(len(flightseries)):
    MA_data[flightseries[i]] = data[key_ari].copy()
    for key in keys_ari:
        MA_data[flightseries[i]][key] = (MA_data[flightseries[i]][key])[
            flight_info["ix"][0][i] : flight_info["ix"][1][i]
        ]

del data

if check_optional:
    print("+++ performing consistency check: optional params... +++")
    MA_data = MA_check_Arinc_optional(
        MA_data,
        flightseries,
        vmiss=vmiss,  # leaves vmiss = int ... in MA_data lists
        errorlog=errorlog,
        logpath=outpath_logs,
    )

print("+++ preparing output... +++")
### BEGIN LOOP FLIGHTS ########################################################

tz_off = 0  # local solar time initial time zone shift
DAY_ZERO = datetime(year=1899, month=12, day=30, tzinfo=timezone.utc)

for flight in flightseries:
    # flight = flightseries[-2]
    na_1001 = na1001()

    fldate = datetime.strptime(MA_data[flight]["PCUTC"][0], ts_fmts[key_ari]).replace(
        tzinfo=timezone.utc
    )
    fldate_str = datetime.strftime(fldate, "%Y%m%d")
    na_1001.DATE = fldate.timetuple()[:3]

    days_delta = (fldate - DAY_ZERO).days
    days_add = 0
    days_add_max = (
        MA_data[flight]["PCUTC_datetime"][-1].date() - MA_data[flight]["PCUTC_datetime"][0].date()
    ).days

    EoT = get_EoT(fldate)  # equation of time

    t_axis, CRefTime, Utc_Arinc, t_ari_totsec, ToAirTmp = [], [], [], [], []
    LocalTime, pstatic, Tpot, Altitude = [], [], [], []

    for ix, dt_obj in enumerate(MA_data[flight]["PCUTC_datetime"]):
        # master time as seconds since midnight
        mdnsec = dtobj_2_mdns(dt_obj, na_1001.DATE)
        t_axis.append(mdnsec)
        # master time as days since day_zero
        CRefTime.append(days_delta + mdnsec / 86400)
        # ARINC time as days since day_zero
        tstr = MA_data[flight]["UTC"][ix]
        if len(tstr) < 7:  # format is not hhmm.ss; -> add missing zeros
            tstr = "0" * (7 - len(tstr)) + tstr
        hms = [int(tstr[:2]), int(tstr[2:4]), int(tstr[5:7])]
        if hms[2] > 59:  # let's hope this doesn't change the hour!
            print("    invalid A/C time:", hms)
            hms[1], hms[2] = hms[1] + 1, hms[2] - 60
        t_ari_totsec.append(hms[0] * 3600 + hms[1] * 60 + hms[2])
        # check for date change:
        if ix > 0 and all([t_ari_totsec[-1] < t_ari_totsec[-2], days_add < days_add_max]):
            days_add += 1
        Utc_Arinc.append(days_delta + t_ari_totsec[-1] / 86400.0 + days_add)

    for i in range(len(CRefTime)):  # calculated parameters
        tmp0 = float(MA_data[flight]["StdAltitu"][i])
        tmp1 = float(MA_data[flight]["StcAirTmp"][i])
        parms = MA_get_pstat_palt_Tpot(tmp0, tmp1, vmiss)
        pstatic.append(parms[0])
        Altitude.append(parms[1])
        Tpot.append(parms[2])

        try:
            tz_str = tf.timezone_at(
                lng=float(MA_data[flight]["PosLong"][i]),
                lat=float(MA_data[flight]["PosLat"][i]),
            )
        except KeyError:
            tz_str = None

        if tz_str:  # only update timezone shift if tzwhere doesn't return None
            zone = ZoneInfo(tz_str)
            if zone is not None:
                tz_off = fldate.replace(tzinfo=zone).utcoffset().total_seconds() / 3600

        LST_frac = get_LSTdayFrac(
            float(MA_data[flight]["PosLong"][i]), tz_off, EoT, days_delta, Utc_Arinc[i]
        )
        LocalTime.append(LST_frac)

    ap_dep = get_ap(
        MA_data[flight]["PosLat"][0], MA_data[flight]["PosLong"][0], ap_file=AP_FILE
    )  # ['IATA', 'Name'] # ix 0: IATA, ix 1: name
    ap_dest = get_ap(MA_data[flight]["PosLat"][-1], MA_data[flight]["PosLong"][-1], ap_file=AP_FILE)

    # fill na1001 class attributes; same for both 10s file and 1s file:
    na_1001.RDATE = datetime.now(tz=timezone.utc).timetuple()[:3]
    na_1001.ONAME = MA_header["ONAME"]
    na_1001.ORG = MA_header["ORG"]
    na_1001.SNAME = MA_header["SNAME"]
    na_1001.MNAME = Header_Defaults["MNAME"]
    na_1001.XNAME = Header_Defaults["XNAME"]
    na_1001.VNAME = MA_header["VNAME"]  # also sets na_1001.NV
    na_1001.VSCAL = [MA_header["VSCAL"]] * na_1001.NV
    na_1001.VMISS = [vmiss] * na_1001.NV

    ####### output modes: 1s and 10s ##############################################

    for mode in OUTPUT_MODES:
        if mode == "1s":
            print("--- 1s output: formatting... ---")

            na_1001.X = [f"{v:.1f}" for v in t_axis]  # also sets na_1001.DX

            mode_str = ""
            FileName = f"MA_{fldate_str}_{flight}_{ap_dep[0]}_{ap_dest[0]}{mode_str}{vers_tag}.txt"

            na_1001.SCOM = [
                MA_header["ffmt_info"],
                f"FlightNo: {flight}",
                f"FlightRoute: {ap_dep[0]} {ap_dest[0]} # IATA code of DepartureAirport and ArrivalAirport",
                f"DepartureAirport: {ap_dep[1]}",
                f"ArrivalAirport: {ap_dest[1]}",
                f"FileName: {FileName}",
            ] + MA_header["scom_term"]

            ncom = [
                "data given within 1s intervals",
                "interval is reported as midpoint UTC",
                "# ",
            ]

            for line in NCOM_terminal_block:
                if "#fldate#" in line:
                    line = line.replace("#fldate#", fldate_str)
                if "#version#" in line:
                    line = line.replace("#version#", f"({__version__})")
                ncom.append(line)

            na_1001.NCOM = ncom

            na_1001.V = [
                [f"{v:.7f}" for v in CRefTime],
                [f"{v:.7f}" for v in Utc_Arinc],
                MA_data[flight]["FlightPhas"],
                MA_data[flight]["PosLat"],
                MA_data[flight]["PosLong"],
                MA_data[flight]["TrueHead"],
                MA_data[flight]["WindSpeed"],
                MA_data[flight]["WindDirTr"],
                MA_data[flight]["PitchAng"],
                MA_data[flight]["RollAng"],
                MA_data[flight]["StdAltitu"],
                MA_data[flight]["BaroAltit"],
                MA_data[flight]["TrAiSpeed"],
                MA_data[flight]["ToAiSpeed"],  # actually temperature
                MA_data[flight]["AltitRate"],
                MA_data[flight]["StcAirTmp"],
                MA_data[flight]["AngOAttck"],
                MA_data[flight]["TotPres"],
                MA_data[flight]["VertSpeed"],
                MA_data[flight]["GndSpeed"],
                [f"{v:.8f}" if v != vmiss else str(vmiss) for v in LocalTime],
                [f"{v:.1f}" if v != vmiss else str(vmiss) for v in pstatic],
                [f"{v:.1f}" if v != vmiss else str(vmiss) for v in Tpot],
                [f"{v:.1f}" if v != vmiss else str(vmiss) for v in Altitude],
            ]

            ### write 1s nasa ames text file...
            io = na_1001.to_file(outpath / FileName, sep=" ", sep_data="\t", overwrite=True)
            if io:
                print("+++ successfully wrote file " + FileName + " +++")

            ### create diagnostic plots if desired...
            if diagn_plots_flightwise[0]:
                diag_plots_indiv_flights(
                    na_1001,
                    FileName,
                    outpath / "diagnostics/",
                    call_plots=True,
                    save_close_dpi_pdf=[True, diagn_plots_flightwise[1], 300, True],
                    call_stat=True,
                )

        ########### END if mode == '10s' ##############################################

        if mode == "10s":  # average and format
            print("--- 10s output: binning data... ---")

            bin_info = bin_t_10s(np.array(t_axis))

            na_1001.X = ["%d" % v for v in bin_info["t_binned"]]

            CRefTime = bin_y_of_t(np.array(CRefTime, dtype="float64"), bin_info, vmiss=vmiss)
            Utc_Arinc = bin_y_of_t(np.array(Utc_Arinc, dtype="float64"), bin_info, vmiss=vmiss)
            FlightPhas = bin_y_of_t(
                np.array(MA_data[flight]["FlightPhas"], dtype="int16"),
                bin_info,
                vmiss=vmiss,
            )
            PosLat = bin_y_of_t(
                np.array(MA_data[flight]["PosLat"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            PosLong = bin_y_of_t(
                np.array(MA_data[flight]["PosLong"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            TrueHead = bin_y_of_t(
                np.array(MA_data[flight]["TrueHead"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            WindSpeed = bin_y_of_t(
                np.array(MA_data[flight]["WindSpeed"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            WindDirTr = bin_y_of_t(
                np.array(MA_data[flight]["WindDirTr"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            PitchAng = bin_y_of_t(
                np.array(MA_data[flight]["PitchAng"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            RollAng = bin_y_of_t(
                np.array(MA_data[flight]["RollAng"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            StdAltitu = bin_y_of_t(
                np.array(MA_data[flight]["StdAltitu"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            BaroAltit = bin_y_of_t(
                np.array(MA_data[flight]["BaroAltit"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            TrAiSpeed = bin_y_of_t(
                np.array(MA_data[flight]["TrAiSpeed"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            ToAirTmp = bin_y_of_t(
                np.array(MA_data[flight]["ToAiSpeed"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            AltitRate = bin_y_of_t(
                np.array(MA_data[flight]["AltitRate"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            StcAirTmp = bin_y_of_t(
                np.array(MA_data[flight]["StcAirTmp"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            AngOAttck = bin_y_of_t(
                np.array(MA_data[flight]["AngOAttck"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_angle",
            )
            TotPres = bin_y_of_t(
                np.array(MA_data[flight]["TotPres"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            VertSpeed = bin_y_of_t(
                np.array(MA_data[flight]["VertSpeed"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            GndSpeed = bin_y_of_t(
                np.array(MA_data[flight]["GndSpeed"], dtype="float64"),
                bin_info,
                vmiss=vmiss,
            )
            LocalTime = bin_y_of_t(
                np.array(LocalTime, dtype="float64"),
                bin_info,
                vmiss=vmiss,
                return_type="mean_day_frac",
            )
            pstatic = bin_y_of_t(np.array(pstatic, dtype="float64"), bin_info, vmiss=vmiss)
            Tpot = bin_y_of_t(np.array(Tpot, dtype="float64"), bin_info, vmiss=vmiss)
            Altitude = bin_y_of_t(np.array(Altitude, dtype="float64"), bin_info, vmiss=vmiss)

            mode_str = f"_{mode}"
            FileName = f"MA_{fldate_str}_{flight}_{ap_dep[0]}_{ap_dest[0]}{mode_str}{vers_tag}.txt"

            na_1001.SCOM = [
                MA_header["ffmt_info"],
                f"FlightNo: {flight}",
                f"FlightRoute: {ap_dep[0]} {ap_dest[0]} # IATA code of DepartureAirport and ArrivalAirport",
                f"DepartureAirport: {ap_dep[1]}",
                f"ArrivalAirport: {ap_dest[1]}",
                f"FileName: {FileName}",
            ] + MA_header["scom_term"]

            ncom = [
                "data averaged over " + mode + " intervals",
                "time-stamp is in UTC and denotes the middle of the 10s interval (lower boundary included, upper boundary excluded)",
                "# ",
            ]

            for line in NCOM_terminal_block:  # MA_header['NCOM']:
                if "#fldate#" in line:
                    line = line.replace("#fldate#", fldate_str)
                if "#version#" in line:
                    line = line.replace("#version#", f"({__version__})")
                ncom.append(line)

            na_1001.NCOM = ncom

            na_1001.V = [
                ["%.7f" % v for v in CRefTime],
                ["%.7f" % v for v in Utc_Arinc],
                ["%d" % v for v in FlightPhas],
                ["%.3f" % v for v in PosLat],
                ["%.3f" % v for v in PosLong],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in TrueHead],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in WindSpeed],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in WindDirTr],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in PitchAng],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in RollAng],
                ["%.1f" % v if not np.isnan(v) else str(vmiss) for v in StdAltitu],
                ["%.1f" % v if not np.isnan(v) else str(vmiss) for v in BaroAltit],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in TrAiSpeed],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in ToAirTmp],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in AltitRate],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in StcAirTmp],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in AngOAttck],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in TotPres],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in VertSpeed],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in GndSpeed],
                ["%.8f" % v if not np.isnan(v) else str(vmiss) for v in LocalTime],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in pstatic],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in Tpot],
                ["%.2f" % v if not np.isnan(v) else str(vmiss) for v in Altitude],
            ]

            ### write 10s nasa ames text file...
            io = na_1001.to_file(outpath / FileName, sep=" ", sep_data="\t", overwrite=True)
            if io:
                print("+++ successfully wrote file " + FileName + " +++")

########### END if mode == '10s' ##############################################

####### END LOOP MODES (1s, 10s) ##############################################

### END LOOP FLIGHTS ##########################################################

print(f"+++ MA file processing finished. +++\n+++ location: {str(outpath)} +++")
print(f"--- elapsed time: {datetime.now()-t0}")
print("+++")
