# -*- coding: utf-8 -*-
"""
What?!
    - load SU file for a flight (or iterate over x flights)
    - load cnf file for the MS file header
    - load respective model data
    - modify SU file header to contain info about model data
    - combine SU file content and model data
    - write Nasa Ames file "MS"
"""
import os
from datetime import datetime, timezone
from pathlib import Path

import numpy as np
import yaml
from pyfuppes.avgbinmap import map_dependent
from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.txt2dict import txt_2_dict_simple as t2d

from caribic2dp.carfileutils import (
    dissect_filename,
    find_files,
    flight_no_to_modeldir,
    increment_version,
)
from caribic2dp.ms_tools import load_moddata
from caribic2dp.parms_units import vname_2_ncom_tblock

# from caribic2dp import __version__ as caribic2dp_version


# ------------------------------------------------------------------------------
# begin config section
# ------------------------------------------------------------------------------

VERBOSE = True
VERS_DEF = False  # set version of SU file - default is False.
VERS_INC = 1  # increment version found on server by... - default is 1.
flights_from_to = (294, 591)
changelog_add = ""  # "Header update, data unchanged."

# ------------------------------------------------------------------------------
verboseprint = print if VERBOSE else lambda *a, **k: None
# ------------------------------------------------------------------------------

# ! user-specific paths >>>
# INPUT
fldir_server = Path().home() / "Carfs1/Caribic/extern/Caribic2data/Flights"
dir_mdata = Path().home() / "Carfs1/Caribic/extern/Caribic2data/data_model"
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# OUTPUT
folder_out = Path().home() / "Data/Caribic_2/MS/output"
# <<< ! user-specific paths

# header defaults
src_cnf_header = Path("../config/NA_header_cfg.toml").resolve()
assert src_cnf_header.exists()

# CARIBIC parameters and units general specification
src_cnf_params = Path("../config/parms_units_2023-11-16.csv").resolve()
assert src_cnf_params.exists()

# MS-specific config
src_ms_hdr_cnf = Path("../config/MS_file_cnf_v2023-06-29.yml").resolve()
assert src_ms_hdr_cnf.exists()


# ------------------------------------------------------------------------------
# config done
# ------------------------------------------------------------------------------


cnf_params = t2d(src_cnf_params, sep=";")["data"]

with open(src_ms_hdr_cnf) as f:
    ms_hdr_cnf = yaml.load(f, Loader=yaml.FullLoader)

### search su file and model data
modelfolders, modeldata, su_files_exist, ms_files_exist = [], [], [], []

verboseprint("checking model data...")
for i in range(min(flights_from_to), max(flights_from_to) + 1):
    # prerequisite #1 = SU file
    su_file = find_files(i, "SU", fldir_server, resolution="10s")
    if not su_file:
        verboseprint(f"--- no SU file found for flight {i} ---")
        continue

    # prerequisite #2 = model data
    try:
        mod_dir = flight_no_to_modeldir(i, dir_mdata)
        tmp = load_moddata(mod_dir)
    except FileNotFoundError:
        tmp = None
    if tmp is None:
        verboseprint(f"--- no model data found for flight {i} ---")
        continue

    su_files_exist.append(su_file[0])
    modelfolders.append(mod_dir)
    modeldata.append(tmp)

    # MS file might or might not exist
    ms_file = find_files(i, "MS", fldir_server, resolution="10s")
    if ms_file:
        ms_files_exist.append(ms_file[0])
    else:
        ms_files_exist.append(None)


ms_filevers_output = [
    VERS_DEF if (VERS_DEF or file is None) else dissect_filename(file).version + VERS_INC
    for file in ms_files_exist
]


if not su_files_exist or not modelfolders:
    raise ValueError("SU file or model data not found.")


for f_ix, su_file in enumerate(su_files_exist):
    verboseprint(f">>> processing {su_file.name}...")

    dir_moddata = modelfolders[f_ix]
    if [f for f in os.listdir(dir_moddata) if not f.startswith(".")] == []:
        verboseprint(f" -> skipped: no model data found for {su_file.name}")
        continue

    ### load stuff
    na_out = na1001(su_file)
    moddata = modeldata[f_ix]
    if not moddata:
        verboseprint(
            f" -> skipped: no model data found for {su_file.name}\n"
            f"    no data in {str(dir_moddata)}"
        )
        continue

    ### preparations
    FileName = os.path.basename(su_file).replace("SU", "MS", 1)
    FileName = increment_version(FileName, specify_versnum=ms_filevers_output[f_ix])

    ms_file = folder_out / Path(FileName)

    vnames_to_add = [v.rsplit(";")[0].strip() for v in ms_hdr_cnf["VNAME_ADD"].splitlines()]

    out_spec = moddata["source_info"]["output"]

    model_datatypes = list(moddata.keys())
    model_datatypes.pop(model_datatypes.index("source_info"))
    # eqlat = 0
    # if "trackMLpd_eqlat" in keys0:
    #     keys0.pop(keys0.index("trackMLpd"))
    #     eqlat = 1

    vmiss = float(na_out.VMISS[0])

    vars_new = {}
    for datatype in model_datatypes:
        vars_new[datatype] = {}
        vars_new[datatype]["UTC[s]"] = moddata[datatype][out_spec]["UTC[s]"]
        for name in vnames_to_add:
            if name in moddata[datatype][out_spec]:
                vars_new[datatype][name] = moddata[datatype][out_spec][name]

    ### extend SU file header
    na_out.RDATE = datetime.now(timezone.utc).timetuple()[:3]
    na_out.VNAME += ms_hdr_cnf["VNAME_ADD"].splitlines()

    na_out.VSCAL = [na_out.VSCAL[0]] * na_out.NV
    na_out.VMISS = [na_out.VMISS[0]] * na_out.NV

    # scom: add info about used model data (file names)
    exception_line = "0"
    for ix, line in enumerate(na_out.SCOM):
        if "FileName:" in line:
            na_out.SCOM[ix] = "FileName: " + ms_file.name
        if line.startswith("Exception") and line != "ExceptionToConsider: no":
            exception_line = str(na_out.NV + 14 + ix)
            verboseprint(f"exception specified in line {exception_line}")
        if "ChangeLog: " in line:
            na_out.SCOM[ix] = "ChangeLog: 0"  # default changelog
            # update changelog
            if changelog_add != "" and ms_files_exist[f_ix]:
                old = na1001(ms_files_exist[f_ix]).SCOM[ix]
                new = f"V{ms_filevers_output[f_ix]:02d} - {changelog_add}"
                if old == "ChangeLog: 0":
                    new = old.replace("ChangeLog: 0", f"ChangeLog: {new}")
                else:
                    new = f"{old} / {new}"
                na_out.SCOM[ix] = new

    mod_src_files = moddata["source_info"]["input"]
    # if eqlat == 1:
    #     for ix, src in enumerate(mod_src_files):
    #         if fnm(src, "*trackMLpd_*hr.dat"):
    #             mod_src_files.pop(ix)

    mod_src = ""
    for src in mod_src_files:
        if src:
            mod_src += os.path.basename(src) + "; "
    mod_src = mod_src[:-2]
    na_out.SCOM.append(mod_src)

    # ncom: get indices of block separators / '#'
    ix_blocksep = []
    offset = 0
    for ix, line in enumerate(na_out.NCOM):
        if "#" in line:
            ix_blocksep.append(ix)
        # TODO : this SHOULD be obsolete:
        # if "introduced in October 2018" in line:
        #     # na_out.NCOM[ix] = line[:-10] + '(v' + str(MS_proc_vers) + ').'
        #     na_out.NCOM[ix] = (
        #         "this file was generated with "
        #         f"caribic2dp v{caribic2dp_version}, "
        #         "https://git.scc.kit.edu/FObersteiner/CaribicDP"
        #     )
        #     offset += 1

    # replace terminal block, after the last '#' / blocksep
    fldate = na_out.NCOM[ix_blocksep[-1] + 1 :][10][0:8]

    NCOM_terminal_block = vname_2_ncom_tblock(
        list(na_out.VNAME),
        src_cnf_params,
        src_cnf_header,
        out_type="MS",
        exception_line=exception_line,
    )[2:]

    NCOM_terminal_block[10] = NCOM_terminal_block[10].replace("#fldate#", fldate)
    na_out.NCOM[ix_blocksep[-1] + 1 :] = NCOM_terminal_block

    # add comment on model data
    model_info = ["#", "comments for " + mod_src] + ms_hdr_cnf["NCOM_model_info"].splitlines()

    ncom_pt1 = na_out.NCOM[: ix_blocksep[-1 - offset]]
    ncom_pt2 = na_out.NCOM[ix_blocksep[-1 - offset] :]
    ncom_new = ncom_pt1 + model_info + ncom_pt2
    na_out.NCOM = ncom_new

    ### extend su file content
    x = np.array(na_out.X, dtype="float64")
    for datatype in model_datatypes:
        x_new = vars_new[datatype]["UTC[s]"]

        parms = list(vars_new[datatype].keys())
        parms.pop(parms.index("UTC[s]"))

        for parm in parms:
            ### new in v >= 2020-02-14:
            v_tmp = vars_new[datatype][parm]
            vars_new[parm] = map_dependent(x, x_new, v_tmp, vmiss=vmiss)

    v = na_out.V
    for ix, name in enumerate(vnames_to_add):
        v_new = [str(int(vmiss))] * len(x)
        v_fmt = cnf_params["py_format"][cnf_params["VNAME"].index(name)]
        if name in vars_new:
            if ":d" in v_fmt:
                v_new = [
                    str(int(v_n)) if v_n == vmiss else v_fmt.format(int(v_n))
                    for v_n in vars_new[name]
                ]
            else:
                v_new = [
                    str(int(v_n)) if v_n == vmiss else v_fmt.format(v_n) for v_n in vars_new[name]
                ]
        else:
            verboseprint("  - parameter not found in model data: ", name)
        v.append(v_new)

    na_out.V = v

    ### output
    io = na_out.to_file(ms_file, sep=" ", sep_data="\t", overwrite=True)
    if io:
        print(f"successfully wrote {FileName}")

print("all done.")
