# -*- coding: utf-8 -*-
import os
from datetime import datetime, timezone
from fnmatch import fnmatch as fnm
from pathlib import Path

import numpy as np
import yaml
from pyfuppes.avgbinmap import map_dependent
from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.txt2dict import txt_2_dict_simple as t2d

from caribic2dp import __version__ as caribic2dp_version
from caribic2dp.carfileutils import (
    dissect_filename,
    flight_no_to_flightdir,
    increment_version,
)
from caribic2dp.check_caribic_na import Header_Defaults
from caribic2dp.parms_units import vname_2_ncom_tblock
from caribic2dp.su_tools import calc_SZA

try:
    wd = Path(__file__).parent
except NameError:
    wd = Path.cwd()  # TODO : how to use the path of the script ?
assert wd.is_dir(), "faild to obtain working directory"


# ------------------------------------------------------------------------------
# begin config section
# ------------------------------------------------------------------------------

VERBOSE = True
VERS_DEF = False  # set version of SU file - default is False.
VERS_INC = 1  # increment version found on server by... - default is 1.
flights_from_to = (294, 591)
changelog_add = "updated ozone data."  # set to "" to skip
software_spec = (
    f"this file was generated with caribic2dp v{caribic2dp_version}, "
    "https://gitlab.kit.edu/FObersteiner/Caribic2dp"
)

# ------------------------------------------------------------------------------
verboseprint = print if VERBOSE else lambda *a, **k: None
# ------------------------------------------------------------------------------

# ! user-specific paths >>>
fldir_server = Path().home() / "Carfs1/Caribic/extern/Caribic2data/Flights"
folder_out = Path().home() / "Data/Caribic_2/SU/output"
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

# header defaults
src_cnf_header = wd / Path("../config/NA_header_cfg.toml").resolve()
assert src_cnf_header.exists(), f"could not load {src_cnf_header}"

# CARIBIC parameters and units general specification
src_cnf_params = wd / Path("../config/parms_units_2023-11-16.csv").resolve()
assert src_cnf_params.exists(), f"could not load {src_cnf_params}"

# SU-specific config
src_cnf_nafile = wd / Path("../config/SU_file_cnf_2023-09-20.yml").resolve()
assert src_cnf_nafile.exists(), f"could not load {src_cnf_nafile}"

### prefix / suffix / search patterns definition
prefix = [
    "Ac",  # vname change from Ac to ACE (as in PTR file)
    "AN",  # vname change from AN to ACN (as in PTR file)
    "C2",  # CO2, old
    "CM",  # CH4, old
    "CN",
    "CO",
    "HG",
    "MA",
    "NO",
    "OM",
    "P2C",  # IAGOS core package 2 c
    "PIC",  # Picarro instrument, since Mod.C (operat. since 2018)
    "PTR",  # PTRMS, since Mod.C (operat. since 2018). combines AN, Ac
    "SD",
    "SP2",  # new 2023-09-20
    "SDA",
    "SU",  # check if SU exists
    "WA",
]

suffix = [".txt", ".na", ".ames"]

# ------------------------------------------------------------------------------
# config done
# ------------------------------------------------------------------------------

cnf_params = t2d(src_cnf_params, sep=";")["data"]

with open(src_cnf_nafile) as f:
    cnf_nafile = yaml.load(f, Loader=yaml.FullLoader)

p_patterns = []
for p in prefix:
    p_patterns.append(p + "_*_10s_*")
s_patterns = []
for s in suffix:
    s_patterns.append("*_10s_*" + s)

flightfolders = []
for i in range(min(flights_from_to), max(flights_from_to) + 1):
    flightfolders.append(flight_no_to_flightdir(i, fldir_server))

### search for data to merge into SU file
for folder_in in flightfolders:
    # get files and select newest version
    MA_found = [False, -1, -1]
    SU_found = [False, -1, -1]
    files_flight = os.listdir(folder_in)
    files_select = [[], [], []]  # index, prefix, version

    for ix, file in enumerate(files_flight):
        for p in p_patterns:
            if fnm(file, p):
                for s in s_patterns:
                    if fnm(file, s):
                        if fnm(file, "MA_*_10s_*"):
                            MA_found = [
                                True,
                                ix,
                                dissect_filename(folder_in / files_flight[ix]).version,
                            ]
                        elif fnm(file, "SU_*_10s_*"):
                            SU_found = [
                                True,
                                ix,
                                dissect_filename(folder_in / files_flight[ix]).version,
                            ]
                        else:
                            files_select[0].append(ix)
                            files_select[1].append(p[0 : p.find("_")])
                            files_select[2].append(
                                dissect_filename(folder_in / files_flight[ix]).version
                            )

    if not MA_found[0]:
        print(f"--- MA file not found in {folder_in.name} - no SU file created ---")
        continue

    file_vers = 1  # default version is 1
    ### auto-determine SU version suffix if not defined by VERS_DEF
    if not VERS_DEF and not SU_found[0]:
        file_vers = 1
    elif not VERS_DEF and SU_found[0]:
        file_vers = SU_found[2] + VERS_INC
    else:
        files_vers = VERS_DEF

    ### load na file content
    data = {}
    data["MA"] = na1001(
        folder_in / files_flight[MA_found[1]],
        sep=" ",
        # sep_com=";", # obsolete
        sep_data="\t",
        auto_nncoml=True,
    )
    verboseprint("+++ loaded " + str(files_flight[MA_found[1]]) + " +++")
    for ix, prfx in enumerate(files_select[1]):
        data[prfx] = na1001(
            folder_in / files_flight[files_select[0][ix]],
            sep=" ",
            # sep_com=";", # obsolete
            sep_data="\t",
            auto_nncoml=True,
        )
        verboseprint("+++ loaded " + str(files_flight[files_select[0][ix]]) + " +++")

    ### begin summary output
    na_out = na1001()
    na_out.DATE = data["MA"].DATE

    na_out.RDATE = datetime.now(timezone.utc).timetuple()[:3]
    na_out.ONAME = cnf_nafile["ONAME"]
    na_out.ORG = cnf_nafile["ORG"]
    na_out.SNAME = cnf_nafile["SNAME"]
    na_out.MNAME = Header_Defaults["MNAME"]
    na_out.XNAME = Header_Defaults["XNAME"]
    na_out.VNAME = cnf_nafile["VNAME"].splitlines()
    na_out.VSCAL = [cnf_nafile["VSCAL"]] * na_out.NV
    na_out.VMISS = [cnf_nafile["VMISS"]] * na_out.NV

    FileName = files_flight[MA_found[1]].replace("MA", "SU", 1)
    FileName = increment_version(FileName, specify_versnum=file_vers)

    na_out.SCOM = list(data["MA"].SCOM)
    exception_line = "0"
    for ix, line in enumerate(na_out.SCOM):
        na_out.SCOM[ix] = na_out.SCOM[ix].strip()
        if "FileName:" in line:
            na_out.SCOM[ix] = "FileName: " + FileName
        if line.startswith("Exception") and line != "ExceptionToConsider: no":
            exception_line = str(na_out.NV + 14 + ix)
            verboseprint(f"exception specified in line {exception_line}")
        if "ChangeLog: " in line:
            # use existing changelog of SU file exists, default otherwise
            if SU_found[0]:
                changelog = na1001(folder_in / files_flight[SU_found[1]]).SCOM[ix]
            else:
                changelog = "ChangeLog: 0"
            # update changelog if changelog_add defined
            if changelog_add != "":
                new = f"V{file_vers:02d} - {changelog_add}"
                if changelog == "ChangeLog: 0":
                    changelog = changelog.replace("ChangeLog: 0", f"ChangeLog: {new}")
                else:
                    changelog = f"{changelog} / {new}"
            na_out.SCOM[ix] = changelog
            na_out.SCOM = na_out.SCOM[: ix + 1]
            break

    na_out.SCOM.append("# ")
    na_out.SCOM.append("Names of used input files:")
    na_out.SCOM.append(files_flight[MA_found[1]])
    for ix, prfx in enumerate(files_select[1]):
        na_out.SCOM.append(files_flight[files_select[0][ix]])

    na_out.NCOM = []
    na_out.NCOM.append(data["MA"].NCOM[0].strip())
    na_out.NCOM.append(data["MA"].NCOM[1].strip())

    # variable "solar zenith angle" is added here; to MA data first
    data["MA"].VNAME.append("SZA; solar zenith angle")
    data["MA"].VMISS.append(data["MA"].VMISS[-1])
    data["MA"].V.append(calc_SZA(data, use_pysolar=False))

    ftype_var = [[], [], []]  # need that later to pull variables...
    for filetype in list(data.keys()):
        # file type, e.g. "MA"
        ftype_var[0] += [filetype] * len(data[filetype].VNAME)

        # all vnames from that file type
        if filetype == "Ac":  # old Ac is now ACE
            ftype_var[1] += [
                "ACE; Acetone volume mixing ratio; [pptV] # quantified by signal integration on protonated ACE (m/z = 59)"
            ]
        elif filetype == "AN":  # old AN is now ACN
            ftype_var[1] += [
                "ACN; Acetonitrile volume mixing ratio; [pptV] # quantified by signal integration on protonated ACN (m/z = 42)"
            ]
        else:  # default
            ftype_var[1] += data[filetype].VNAME

        # variable index for given file type
        ftype_var[2] += list(range(len(data[filetype].VNAME)))

        na_out.NCOM.append("# ")

        if filetype == "MA":
            na_out.NCOM.append("comments from " + files_flight[MA_found[1]])
        else:
            ix = files_select[0][files_select[1].index(filetype)]
            na_out.NCOM.append("comments from " + files_flight[ix])

        tmp_scom = [entry.strip() for entry in data[filetype].SCOM]
        for ix, line in enumerate(tmp_scom):
            if "FileFormatVersionInfo" in line:
                na_out.NCOM.append("#" + line.split(sep="#")[-1])
            if "ExceptionToConsider" in line:
                na_out.NCOM.append(filetype + "_" + line)
            if "ChangeLog" in line:
                na_out.NCOM.append(filetype + "_" + line)
                while line[-1] == "\\" or line[-1] == "/":  # changelog sep
                    ix += 1
                    if ix > len(tmp_scom):
                        break
                    line = tmp_scom[ix]
                    na_out.NCOM.append(line)

        tmp_ncom = [entry.strip() for entry in data[filetype].NCOM]
        for line in tmp_ncom:
            if line.startswith("The following two lines") or line.startswith("#"):
                break
            else:
                na_out.NCOM.append(line)

    na_out.NCOM.append("# ")

    fldate = datetime(*na_out.DATE).strftime("%Y%m%d")

    # weired construct to make function parameter "vnames" truly
    # local to the function... -> could avoid this by using a
    # private class method.
    NCOM_terminal_block = vname_2_ncom_tblock(
        list(na_out.VNAME),
        src_cnf_params,
        src_cnf_header,
        out_type="SU",
        exception_line=exception_line,
    )

    for line in NCOM_terminal_block:
        line = line.strip()
        if "#software_spec#" in line:
            line = software_spec
        if "#fldate#" in line:
            line = line.replace("#fldate#", fldate)
        na_out.NCOM.append(line)

    na_out.X = data["MA"].X

    # now the funny stuff... find variables in loaded files -
    # if not found -> fill column with VMISS
    # if found -> add to V; with VMISS if not in MA x-range
    na_out.V = []
    var_formats = []
    x = np.array(na_out.X, dtype=int)  # MA time, xref

    # configured variables
    cfgnames = [s.split(";")[0] for s in ftype_var[1]]

    for vname_idx, name in enumerate(na_out.VNAME):
        v = np.full(len(x), na_out.VMISS[0], dtype=float)
        sel_vname = name[: name.find(";")]
        try:
            cfg_ix = cfgnames.index(sel_vname)
            add = True
        except ValueError:
            verboseprint(f"{sel_vname} not found in provided input files")
            add = False

        if add:
            v_ix = ftype_var[2][cfg_ix]
            v_orig = ftype_var[0][cfg_ix]
            v_tmp = np.array(data[v_orig].V[v_ix], dtype=float)
            v_miss = np.float64(data[v_orig].VMISS[v_ix].strip())
            v_tmp[np.where(v_tmp == v_miss)] = np.float64(na_out.VMISS[0])
            x_tmp = np.array(data[v_orig].X, dtype=float).astype(int)

            # check time vector
            dt = x_tmp[1:] - x_tmp[:-1]
            if min(dt) < 10.0 or max(dt) > 10.0:
                verboseprint(f"gaps in x vector, variable: {sel_vname}, from: {v_orig}")
                verboseprint(f"min & max delta: {min(dt)}, {max(dt)}")

            # select data that is in time range of MA data
            v = map_dependent(x, x_tmp, v_tmp, vmiss=np.float64(na_out.VMISS[vname_idx]))
            if (v >= float(na_out.VMISS[0])).all():
                verboseprint(f"warning: data for {v_orig} parameter {sel_vname} is empty !")

        else:
            v = np.full(len(x), na_out.VMISS[0])

        na_out.V.append(v)

        # get format code
        cnf_params["VNAME"] = np.array(cnf_params["VNAME"], dtype=str)
        cnf_params["py_format"] = np.array(cnf_params["py_format"], dtype=str)
        ix = np.where(cnf_params["VNAME"] == sel_vname)[0]

        if ix.shape[0] == 1:  # have exactly one match?
            var_formats.append(cnf_params["py_format"][ix][0])
        else:
            var_formats.append("{:3.f}")

    # format the output...
    for ix in range(na_out.NV):
        fmt = var_formats[ix]
        if ":d" in fmt:
            na_out.V[ix] = [
                var_formats[ix].format(int(v)) if v < na_out.VMISS[ix] else str(int(v))
                for v in na_out.V[ix]
            ]
        else:
            # ensure nan is vmiss
            na_out.V[ix] = np.where(np.isfinite(na_out.V[ix]), na_out.V[ix], na_out.VMISS[ix])
            # format all vmiss equally (integer)
            na_out.V[ix] = [
                var_formats[ix].format(v) if v < na_out.VMISS[ix] else str(int(v))
                for v in na_out.V[ix]
            ]

    io = na_out.to_file(
        folder_out / Path(FileName),
        sep=" ",
        # sep_com=";", # obsolete
        sep_data="\t",
        overwrite=True,
    )
    if io:
        print(f"+++ successfully wrote file {FileName} +++")
    else:
        print(f"+++ skipped file {FileName} +++")


print("all done.")
