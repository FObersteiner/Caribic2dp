# -*- coding: utf-8 -*-
"""Make whole air sampler data merge files in netCDF v4 format."""
import logging
import sys
from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr
from pyfuppes.na1001 import FFI1001

from caribic2dp.carfileutils import (
    dissect_flightdirname,
    find_files,
    get_all_flightdirs,
)
from caribic2dp.convert_caribic_na_nc4 import (
    CARIBIC_NC4_CFCONVENTION,
    CARIBIC_NC4_FORMAT_DATE,
    CARIBIC_NC4_FORMAT_VERS,
    CARIBIC_IVAR_long,
)
from caribic2dp.convert_caribic_na_nc4 import CARIBIC_IVAR as CARIBIC2_IVAR
from caribic2dp.convert_caribic_na_nc4 import LICENSE as lic
from caribic2dp.convert_caribic_na_nc4 import SW_SPEC as sw_spec

# -------------------------------------------------------------------------------------
# CONFIG

WRITE = 1  # set to falsy to check for existing files only

# file version
version = 2  # TODO : determine version dynamically based on what is already on the server

caribics_to_process = (2,)

# data owners are the KIT people / IAGOS-CARIBIC coordination
WSM_DATA_OWNERS = "A. Zahn; H. Boenisch; T. Gehrlein; F. Obersteiner; contact: andreas.zahn@kit.edu"

# INT and GHG is mostly Tanja's work, GHG data re-analysis by Florian, Claus did the bulk  of the measurements
# For WAS, consider adding
# A. Rauthe-Schoech, T. Umezawa, T. J. Schuck, C. Koeppel, A. K. Baker, U. Thorenz, E. Karu, C. A. M. Brenninkmeijer
WSM_CONTRIBUTORS = "T. Schuck; F. Obersteiner; C. Koeppel; contact: schuck@iau.uni-frankfurt.de"

WSM_DOI = "10.5281/zenodo.12755525"

DST = Path().home() / "Data/Caribic_2/WSM/zenodo_publication/2024.07.17/tmp"
if not DST.exists():
    DST.mkdir(parents=True)

logging.basicConfig(
    filename=DST / f"out_{pd.Timestamp('now', tz='UTC').strftime('%Y-%m-%dT%H%M%SZ')}.log",  # type: ignore
    encoding="utf-8",
    level=logging.DEBUG,
    format="%(asctime)s -- %(levelname)s -- %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S%z",
)

logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))  # log to stdout as well

CARIBIC1_IVAR = "Elapsed_time_in_UTC_seconds_from_0_hours_on_the_day_given_in_line_7"

DVAR_lat = "lat"  # these vars are different in continuous data
DVAR_lon = "lon"
DVAR_p = "p"

# use latest config file:
PARAMS_FILE = sorted(
    (Path().home() / "Code/Python/Caribic2_DataProc/config/").glob("parms_units*WSM*.csv")
)[-1]

FLIGHTDIRS = {  # where to look for data
    "CARIBIC-1": Path().home() / "Carfs1/Caribic/extern/CARIBIC1data/Flights",
    "CARIBIC-2": Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/Flights",
}

EXTS = {"CARIBIC-1": "nas", "CARIBIC-2": "txt"}

# define which parameters should NOT go into the netCDF file
drop = {"INT": {CARIBIC1_IVAR, CARIBIC2_IVAR, "year", "month", "day", "hour", "min", "sec"}}
drop["GHG"] = drop["INT"].union({"lat", "lon", "p", "type"})
drop["HCF"] = drop["INT"].union({"lat", "lon", "p", "type"})
drop["WAS"] = drop["INT"].union(  # remove GHG data from WAS:
    {
        "lon",
        "lat",
        "p",
        "type",
        "ch4",
        "d_ch4",
        "co2",
        "d_co2",
        "n2o",
        "d_n2o",
        "sf6",
        "d_sf6",
    }
)

# -------------------------------------------------------------------------------------
# HELPERS


def update_dvars(dvars: dict, data: dict, key: str, drop: dict, caribic: int):
    """
    A helper to update the dict of dependent variables in-place.
    Inserts 'key' in lower case before the variable name.
    """
    if data[key] is not None:
        for k, v in data[key].to_dict_nparray(vname_delimiter=":" if caribic == 1 else ";").items():
            if k not in drop[key]:
                if key == "GHG":  # hack for lower-case vNames in GHG
                    parts = k.split("_")
                    parts[-1] = parts[-1].upper()
                    k = "_".join(parts)
                if key == "WAS" and "_MPS" in k:  #  hack for data with unknown origin "_MPS"
                    continue
                dvars[f"{key.lower()}_{k}"] = v


# -------------------------------------------------------------------------------------
# SETUP

specs = pd.read_csv(PARAMS_FILE, sep=";").set_index("VNAME")
directories = {
    "CARIBIC-1": get_all_flightdirs(FLIGHTDIRS["CARIBIC-1"]),
    "CARIBIC-2": get_all_flightdirs(FLIGHTDIRS["CARIBIC-2"]),
}
files = {"CARIBIC-1": {}, "CARIBIC-2": {}}
missing = {"CARIBIC-1": set(), "CARIBIC-2": set()}

# -------------------------------------------------------------------------------------
#
# --- load all the data for CARIBIC-1 and CARIBIC-2 ---
# files dict will contain
# caribic-no --> flight-no --> file-prefix ---> path-to-file
#
logging.info("==> create WSM files")
for caribic in caribics_to_process:
    logging.info(f"--> CARIBIC-{caribic}")
    for d in directories[f"CARIBIC-{caribic}"]:
        fno = dissect_flightdirname(d).flight_no
        # INT files are required since the contain position and time info
        files_int = find_files(
            fno,
            "INT",
            flights_dir=Path(),
            file_dir=d,
            caribic=caribic,
            ext=EXTS[f"CARIBIC-{caribic}"],
        )

        if not files_int:
            missing[f"CARIBIC-{caribic}"].add(fno)
            logging.warning(f"CARIBIC-{caribic} : no INT found for flight {fno}")
            continue

        files[f"CARIBIC-{caribic}"][fno] = {
            "INT": sorted(files_int)[-1],
        }

        for t in "GHG", "HCF":  # , "WAS":
            files_t = find_files(
                fno,
                t,
                flights_dir=Path(),
                file_dir=d,
                caribic=caribic,
                ext=EXTS[f"CARIBIC-{caribic}"],
            )
            if files_t:
                files[f"CARIBIC-{caribic}"][fno][t] = sorted(files_t)[-1]

        if not WRITE:
            continue

        # --- write netCDF -----------------------------------------------------

        flight = files[f"CARIBIC-{caribic}"][fno]
        out = DST / (flight["INT"].stem[:-3].replace("INT", "WSM", 1) + f"V{version:02d}.nc")

        data = {
            "INT": FFI1001(flight["INT"]),
            "GHG": FFI1001(flight["GHG"]) if "GHG" in flight else None,
            "HCF": FFI1001(flight["HCF"]) if "HCF" in flight else None,
            "WAS": FFI1001(flight["WAS"]) if "WAS" in flight else None,
        }

        if (data["GHG"] is None) and (data["HCF"] is None) and (data["WAS"] is None):
            logging.warning(f"neither GHG, WAS or HCF data for flight {fno}, skipping")
            continue

        # safety checks
        if data["GHG"] is not None:
            assert data["INT"].X == data["GHG"].X, "ghg: independent variable (time) must match"
        if data["HCF"] is not None:
            assert data["INT"].X == data["HCF"].X, "hcf: independent variable (time) must match"
        # if data["WAS"] is not None:# TODO : add WAS ?
        #     assert data["INT"].X == data["WAS"].X, "was: independent variable (time) must match"

        # collect the data; drop keys if specified
        dvars = {
            k: v
            for k, v in data["INT"]
            .to_dict_nparray(vname_delimiter=":" if caribic == 1 else ";")
            .items()
            if k not in drop["INT"]
        }
        update_dvars(dvars, data, "GHG", drop, caribic)
        update_dvars(dvars, data, "HCF", drop, caribic)
        # update_dvars(dvars, data, "WAS", drop, caribic)# TODO : add WAS ?

        # verify that all loaded variables are found in specifications
        param_index = {}
        for p in dvars:
            try:
                param_index[p] = specs.index.get_loc(p)
            except KeyError:
                raise KeyError(
                    f"(flight {fno}) could not find parameter '{p}' in parameters config"
                )

        # --- make xarray DataSet for netCDF output ---

        coords = {
            "time": pd.Timestamp(*data["INT"].DATE, tz="UTC")
            + pd.to_timedelta(np.array(data["INT"].X, float), unit="s"),
        }

        # prepare data to put into xarray.DataSet.
        # dvars should now only contain variables that should go into the netCDF.
        data_nc = {k: (["time"], v) for k, v in dvars.items()}

        ds = xr.Dataset(data_nc, coords=coords)

        # put the complete NASA Ameas file header of all the input files into a nc global attr
        ds.attrs["na_file_header_INT"] = np.array(data["INT"]._HEADER, dtype=str)
        for key in "GHG", "HCF":  # TODO : add WAS ?
            if data[key] is not None:
                ds.attrs[f"file_header_{key}"] = np.array(data[key]._HEADER, dtype=str)

        # general (global) attributes
        ds.attrs["title"] = "IAGOS-CARIBIC netCDF4 whole air sampler merge file"
        ds.attrs["date_created"] = (
            pd.Timestamp.now(tz="UTC").isoformat(timespec="seconds").replace("+00:00", "Z")
        )
        ds.attrs["summary"] = "IAGOS-CARIBIC whole air sample data"
        ds.attrs["keywords"] = (
            "greenhouse gases, trace gases, aircraft observations, in-situ, monitoring, long-term, whole air sampling"  # new in v0.5
        )

        ds.attrs["conventions"] = CARIBIC_NC4_CFCONVENTION
        ds.attrs["mission"] = (
            f"IAGOS-CARIBIC (CARIBIC-{caribic}), <http://www.caribic-atmospheric.com/>"
        )
        ds.attrs["data_description"] = "Whole air sampler data for IAGOS-CARIBIC"
        ds.attrs["data_institute"] = data["INT"].ORG

        if WSM_DATA_OWNERS:
            ds.attrs["data_owners"] = WSM_DATA_OWNERS

        if WSM_CONTRIBUTORS:
            ds.attrs["data_contributors"] = WSM_CONTRIBUTORS

        ds.attrs["license"] = lic

        if WSM_DOI:
            ds.attrs["doi"] = WSM_DOI

        ds.attrs["format_date"] = CARIBIC_NC4_FORMAT_DATE
        ds.attrs["format_version"] = CARIBIC_NC4_FORMAT_VERS
        ds.attrs["history"] = (
            "Converted from NASA Ames format to netCDF v4. "
            "Might contain only a subset of the parameters from the original NASA Ames files. "
            "V02: updated ozone data for flights 294 to 591. "
        ) + sw_spec

        ds["time"].attrs["name"] = CARIBIC2_IVAR
        ds["time"].attrs["standard_name"] = "time"
        ds["time"].attrs["long_name"] = CARIBIC_IVAR_long

        # add variable descriptions from specifications csv
        for p in dvars:
            ds[p].attrs["standard_name"] = specs.iloc[param_index[p]].CF_StandardName
            ds[p].attrs["long_name"] = specs.iloc[param_index[p]].Long_Name
            ds[p].attrs["unit"] = specs.iloc[param_index[p]].Unit.strip("[]")
            ds[p].attrs["standard_unit"] = specs.iloc[param_index[p]].CF_Unit.strip("[]")
            if ds[p].attrs["standard_name"].endswith("standard_error"):
                ds[p].attrs["coverage_content_type"] = "qualityInformation"  # ISO 19115-1
            else:
                ds[p].attrs["coverage_content_type"] = "physicalMeasurement"  # ISO 19115-1

        # set coordinates for nc
        ds["time"].attrs["axis"] = "T"
        ds["time"].attrs["coverage_content_type"] = "coordinate"
        ds.coords[DVAR_lat] = ds.data_vars[DVAR_lat]
        ds[DVAR_lat].attrs["axis"] = "Y"
        ds[DVAR_lat].attrs["coverage_content_type"] = "coordinate"
        ds.coords[DVAR_lon] = ds.data_vars[DVAR_lon]
        ds[DVAR_lon].attrs["axis"] = "X"
        ds[DVAR_lon].attrs["coverage_content_type"] = "coordinate"
        ds.coords[DVAR_p] = ds.data_vars[DVAR_p]
        ds[DVAR_p].attrs["axis"] = "Z"
        ds[DVAR_p].attrs["coverage_content_type"] = "coordinate"

        # add bounding box
        ds.attrs["geospatial_bounds_crs"] = "EPSG:4326"
        ds.attrs["geospatial_bounds_vertical_crs"] = "EPSG:5829"
        ds.attrs["geospatial_lat_min"] = ds[DVAR_lat].values.min()
        ds.attrs["geospatial_lat_max"] = ds[DVAR_lat].values.max()
        ds.attrs["geospatial_lon_min"] = ds[DVAR_lon].values.min()
        ds.attrs["geospatial_lon_max"] = ds[DVAR_lon].values.max()
        ds.attrs["geospatial_lat_units"] = "degree_north"
        ds.attrs["geospatial_lon_units"] = "degree_east"
        ds.attrs["geospatial_vertical_min"] = ds[DVAR_p].values.max()
        ds.attrs["geospatial_vertical_max"] = ds[DVAR_p].values.min()
        ds.attrs["geospatial_vertical_positive"] = "down"  # Note : pressure drops with alt
        ds.attrs["geospatial_vertical_units"] = "hPa"
        ds.attrs["time_coverage_start"] = coords["time"].min().isoformat().replace("+00:00", "Z")
        ds.attrs["time_coverage_end"] = coords["time"].max().isoformat().replace("+00:00", "Z")

        # output filename: WSM_[yyyymmdd]_[fno]_[dep]_[dst]_V[vv].nc
        logging.info(f"make {out}")
        ds.to_netcdf(out)

logging.info("done.")
