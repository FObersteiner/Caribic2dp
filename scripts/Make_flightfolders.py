# -*- coding: utf-8 -*-
"""
WHAT?!
    - create directories for CARIBIC flight data on server.
    - needs flightlog files as input (output of MA_2_nasaames)

HOW?
    - set "basedir" to where the folders should be created
    - set "flightlog" to path+filename of the FlightLog of the respective
      flight sequence.
    - run.
"""
import os
from datetime import datetime, timezone
from pathlib import Path

from pyfuppes.txt2dict import txt_2_dict_simple as txt2dict

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
###
basedir = Path("/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights/")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
###
flightlog = Path(
    "/home/va6504/Data/Caribic_2/MA/F588-591/processed/Flightseries_588_Logfiles/Flightseries_588_FlightsLog.txt"
)
###
# <<< ! user-specific paths


colhdr_ix = 2
fllog = txt2dict(flightlog, sep="\t", colhdr_ix=colhdr_ix)["data"]

flights = fllog["Flight_No"]
t_to = [
    datetime.strptime(x, "%d.%m.%y %H:%M:%S.%f").replace(tzinfo=timezone.utc)
    for x in fllog["t_start[UTC]"]
]
t_lnd = [
    datetime.strptime(x, "%d.%m.%y %H:%M:%S.%f").replace(tzinfo=timezone.utc)
    for x in fllog["t_end[UTC]"]
]

folders = []
for ix, flight in enumerate(flights):
    folders.append(basedir / ("Flight" + flight + "_" + t_to[ix].strftime("%Y%m%d")))

for folder in folders:
    if os.path.exists(folder):
        print("already exists: " + str(folder))
    else:
        os.mkdir(folder)
        print("created: " + str(folder))
