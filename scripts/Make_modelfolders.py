# -*- coding: utf-8 -*-
"""
WHAT?!
    - create directories for KNMI model on server.

HOW?
    - set "flightrange" to [min_flight, max_flight]
    - set "moddir" to where the "model data folders" should be created.
    - run.
"""
import os
from pathlib import Path

from caribic2dp.carfileutils import flight_no_to_flightdir

# ------------------------------------------------------------------------------

flightrange = [584, 591]
###
moddir = Path(r"/home/va6504/Carfs1/Caribic/extern/Caribic2data/data_model")
###

for f in range(flightrange[0], flightrange[1] + 1):
    fldir = str(
        flight_no_to_flightdir(f, "/home/va6504/Carfs1/Caribic/extern/Caribic2data/Flights")
    )
    modnewdir = moddir / (fldir[-6:] + "_" + str(f))

    if os.path.exists(modnewdir):
        print("already exists: " + str(modnewdir))
    else:
        os.mkdir(modnewdir)
        print("created: " + str(modnewdir))
