# -*- coding: utf-8 -*-
from pathlib import Path

from pyfuppes.na1001 import FFI1001 as nacls

from caribic2dp.carfileutils import dissect_filename, flight_no_to_flightdir

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

flights = (541, 544)

for fno in tuple(range(flights[0], flights[1] + 1)):
    print(f"checking files for flight {fno}:")
    files = list(
        flight_no_to_flightdir(fno, flights_dir).glob("*.txt")
    )  # assume extension is always .txt
    v_int, v_ms, v_su = -1, -1, -1
    for f in files:
        # get file version and content:
        v, na1001, chlog = dissect_filename(f).version, nacls(f), None
        # extract the changelog, accept multi-line changelog at end of _SCOM:
        for i, line in enumerate(na1001.SCOM):
            if "ChangeLog:" in line:
                chlog = na1001.SCOM[i:]
                break
        if not chlog:
            print(f"--> file {f.name},\n" "(!) completely missing changelog!")
            continue

        if f.name.startswith("INT") and v > 1:
            v_int = v
        elif f.name.startswith("MS") and v > 1:
            v_ms = v
        elif f.name.startswith("SU") and v > 1:
            v_su = v
            src_versions = []
            for line in na1001.NCOM:
                if line.startswith("comments from "):
                    src_versions.append(dissect_filename(line[14:]).version)
            if sum(src_versions) - len(src_versions) != v:
                print(
                    f"--> SU version is {v}, however the sum of source file "
                    f"increments is {sum(src_versions) - len(src_versions)}"
                )
        else:
            if v > 1:
                # version is greater 1,
                # check if changelog contains info on at least all version greater 1

                chlog = " ".join(chlog) if len(chlog) > 1 else chlog[0]
                lookfor = [f"V{i:02d}" for i in range(2, v + 1)]
                notfound = [s not in chlog for s in lookfor]
                if any(notfound):
                    print(f"--> file {f.name},")
                    for idx, boolean in enumerate(notfound):
                        if notfound:
                            print(f"    missing info on changes in version {lookfor[idx]}")

    if v_ms > v_su:
        print(
            f"--> version of MS file is {v_ms}, "
            f"however it should be {v_su} according to SU file version"
        )

    if v_int > v_ms:
        print(
            f"--> version of INT file is {v_int}, "
            f"however it should be {v_ms} according to MS file version"
        )
