# -*- coding: utf-8 -*-
from datetime import datetime, timezone
from pathlib import Path

import numpy as np
from pyfuppes.avgbinmap import bin_t_10s, bin_y_of_t
from pyfuppes.na1001 import FFI1001 as na1001

from caribic2dp.carfileutils import dissect_filename, find_files, increment_version
from caribic2dp.o3_file_tools import calc_o3tp_relhght, load_tp_hght_data

# ------------------------------------------------------------------------------

FLIGHTSDIR = Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/Flights"
###############################################################################
# nasa ames general config
sep_com, sep_data, sep_misc = ";", "\t", " "
inc_filevers = False

calc_hreltp = True
src = Path().home() / "Data/Caribic_2/O3/Flights/NASA_AMES_files/"
dst = src
fl_range = [537, 591]

# load Hohenpeissenberg O3 TP height data
hpdat_path = (
    Path().home()
    / "Code/Python/Caribic2_DataProc/config/O3_climatology_Hohenpeissenberg_for_H_rel_TP.csv"
)

tpdata = load_tp_hght_data(hpdat_path)
lat_lim = 35.0  # limit latitude to x°N - essential
alt_llim = False  # 5000 # minimum altitude in m - optional, can be set to False

###############################################################################
################  CARIBIC F537+ 10s ###########################################

na_files = [f for f in src.glob("OM_*.txt") if "_10s_" not in f.name]


# na_files = [na_files[1]]
for file in na_files:
    fl_no = dissect_filename(file).flight_no
    if fl_no < fl_range[0] or fl_no > fl_range[1]:
        continue  # skip loop if flight no out of selected flight range

    print(f"processing {file.name}...")
    file_out = dst / (file.name[:-7] + "10s" + file.name[-8:])
    na_1001 = na1001(file)

    # update header

    if inc_filevers:
        file_out = increment_version(file_out)
    FileName = file_out.name  # type: ignore

    na_1001.RDATE = datetime.now(timezone.utc).timetuple()[:3]

    na_1001.VMISS += na_1001.VMISS
    na_1001.VSCAL += na_1001.VSCAL
    na_1001.VNAME.append(
        "H_rel_TP; Ozone-based representative height above the tropopause; "
        + "[km] # derived from ozone sonde data collected at Hohenpeissenberg "
        + "[Sprung and Zahn, JGR, 2010]"
    )

    for ix, line in enumerate(na_1001.SCOM):
        if line.startswith("FileName"):
            na_1001.SCOM[ix] = "FileName: " + FileName
            break

    na_1001.SCOM[-1] = "ChangeLog: V02 revised binning to 10s."

    ncom = na_1001.NCOM
    for ix, line in enumerate(ncom):
        if line.startswith("The following two lines contain"):
            ncom[ix + 1] += "\t" + "altitude_wrt_tropopause"
            ncom[ix + 2] += "\t" + "[1e3 m]"
            ncom[ix + 4] += "\t" + na_1001.VSCAL[-1]
            ncom[ix + 5] += "\t" + na_1001.VMISS[-1]
            ncom[ix + 6] += "\t" + "[km]"
            ncom[ix + 7] += "\t" + "H_rel_TP"
            break

    ncom_add = [
        "Ozone data binned to 10s intervals (arithmetic mean), 'VMISS' excluded.",
        (
            "time-stamp is in UTC seconds since midnight and denotes the middle "
            + "of the 10s interval (lower boundary included, upper boundary excluded)."
        ),
        "# ",
        (
            "H_rel_TP is derived for the binned ozone values and reported if "
            + "ozone >= 60 ppb AND latitude >= 35 deg N AND resulting H_rel_TP >= -1.5 km."
        ),
        (
            "Monthly average ozone/H_rel_TP data from Hohenpeissenberg is interpolated linearly "
            + "to match the measured ozone value as in [Sprung and Zahn, JGR, 2010]."
        ),
        "# ",
    ]
    na_1001.NCOM = ncom_add + ncom

    # bin x to 10s
    bin_info = bin_t_10s(np.array(na_1001.X, dtype=np.float), drop_empty=False)

    # calculate bin means for dependent variables
    data = [[v if v != na_1001.VMISS[0] else None for v in l] for l in na_1001.V]
    binned_data = []
    for i, vlist in enumerate(data):
        ### OLD
        # binned_data.append((bin_yvar(list(map(float, vlist)), bin_info,
        #                              no_val=vmiss, calc_stat=True, exclude_noval=True,
        #                              output='string', outfmt="%.2f"))['bin_mean'])
        y = bin_y_of_t(np.array(vlist, dtype=np.float), bin_info)
        binned_data.append([f"{v:.2f}" if not np.isnan(v) else na_1001.VMISS[i] for v in y])

    na_1001.V = binned_data
    na_1001.X = [f"{int(v)}" for v in bin_info["t_binned"]]

    # calculate height relative to tropopause
    # first, fill vector with vmiss
    hreltp = [na_1001.VMISS[0] for _ in binned_data[0]]
    na_1001.V.append(hreltp)
    if calc_hreltp:
        VMISS = float(na_1001.VMISS[-1])
        # find the appropriate 10s MA file to get latitude
        ma_file = find_files(fl_no, "MA", FLIGHTSDIR, resolution="10s", raise_filenotfound=True)[0]
        if ma_file:  # MA file found, continue
            print("adding h rel. tp...")
            ma_data = na1001(ma_file).to_dict_nparray()
            ma_x = ma_data["TimeCRef"].astype(np.int)
            ma_lat = ma_data["PosLat"]
            ma_alt = ma_data["Altitude"]

            om_data = na_1001.to_dict_nparray()
            om_x = om_data["TimeCRef"].astype(np.int)
            om_o3 = om_data["Ozone"]

            for ix, v in enumerate(om_o3):
                w_x = np.where(ma_x == om_x[ix])
                if w_x[0].shape[0] > 0:  # corresponding time found...
                    if alt_llim:
                        lmt_ok = ((ma_alt[w_x] > alt_llim) & (ma_lat[w_x] >= lat_lim)).all()
                    else:
                        lmt_ok = (ma_lat[w_x] >= lat_lim).all()
                    if lmt_ok:
                        h = calc_o3tp_relhght(
                            tpdata,
                            v,
                            na_1001.DATE[1],
                            sel_day_of_month=na_1001.DATE[2],
                            vmiss=VMISS,
                        )
                        if not np.isclose(h, VMISS):
                            na_1001.V[-1][ix] = f"{h:.2f}"

    # updated file output

    io = na_1001.to_file(file_out, sep=sep_misc, sep_com=sep_com, sep_data=sep_data, overwrite=True)
    if io:
        print("+++ wrote " + FileName + " +++")
