# -*- coding: utf-8 -*-
"""
What?
    - from a CARIBIC flight log file, generate a default FAIROeval config .csv!
"""
import contextlib
import os
from datetime import datetime, timezone
from fnmatch import fnmatch as fnm

from pyfuppes.txt2dict import txt_2_dict_simple as txt2dict

# ------------------------------------------------------------------------------

# flight log -> O3 eval config files
fname = "/home/va6504/Data/Caribic_2/MA/F497-500/processed/Flightseries_497_Logfiles/Flightseries_497_FlightsLog.txt"
datpath = "/home/va6504/Data/Caribic_2/O3/Flights/LH_497_O3/data"

# output = 'D:/KIT/Dataprocessing_Caribic/O3_data/Flights/cnf_tmp/FAIROeval_config_CARIBIC_'
config_rev = "rev. 2020-10-21"
config_nlhead = 5
crlf = "\n"
colhdr_ix = 2

# read flightlog
fllog = txt2dict(fname, sep="\t", colhdr_ix=colhdr_ix)["data"]
flights = list(map(int, fllog["Flight_No"]))
t_to = [
    datetime.strptime(x, "%d.%m.%y %H:%M:%S.%f").replace(tzinfo=timezone.utc)
    for x in fllog["t_start[UTC]"]
]
t_lnd = [
    datetime.strptime(x, "%d.%m.%y %H:%M:%S.%f").replace(tzinfo=timezone.utc)
    for x in fllog["t_end[UTC]"]
]

for ix, flight in enumerate(flights):
    # get flight name (exp_name)
    exp_name = "F" + str(flight)

    # from t_start create exp_date_UTC
    exp_date_UTC = (
        "[" + str(t_to[ix].year) + ", " + str(t_to[ix].month) + ", " + str(t_to[ix].day) + "]"
    )

    # from t_start and t_end create exp_t_range, preselect_t_range, path_data
    # exp_t_range is assumed to be takeoff and landing
    exp_t_range = (
        "['"
        + t_to[ix].strftime("%Y-%m-%dT%H:%M:%S")
        + "', "
        + "'"
        + t_lnd[ix].strftime("%Y-%m-%dT%H:%M:%S")
        + "']"
    )

    # add 3 minutes to exp_t_range to get data range to select:
    sel_t0 = datetime.utcfromtimestamp(t_to[ix].timestamp() - 180.0)
    sel_t1 = datetime.utcfromtimestamp(t_lnd[ix].timestamp() + 180.0)
    preselect_t_range = (
        "['"
        + sel_t0.strftime("%Y-%m-%dT%H:%M:%S")
        + "', "
        + "'"
        + sel_t1.strftime("%Y-%m-%dT%H:%M:%S")
        + "']"
    )

    path_data = []
    delta_days = t_lnd[ix].day - t_to[ix].day
    if delta_days > 0:  # flight over more than one day
        for i in range(delta_days + 1):
            path_data.append(
                datpath
                + "/"
                + str(t_to[ix].year)[2:]
                + "_"
                + "{:02d}".format(t_to[ix].month)
                + "_"
                + "{:02d}".format(t_to[ix].day + i)
            )
    else:
        path_data.append(
            datpath
            + "/"
            + str(t_to[ix].year)[2:]
            + "_"
            + "{:02d}".format(t_to[ix].month)
            + "_"
            + "{:02d}".format(t_to[ix].day)
        )

    path_out = os.path.dirname(datpath) + "/py_eval_" + exp_name + "/"

    # defaults for the other settings...
    cnf = [
        "# FAIRO Eval Settings for CARIBIC, " + exp_name + crlf,
        "# " + config_rev + crlf,
        "# KEY; Parameter; comment" + crlf,
        "# ***" + crlf,
        "platform: 'CARIBIC'" + crlf,
        "instrument: 'FAIRO3'" + crlf,
        "exp_name: '" + exp_name + "'" + crlf,
        "exp_date_UTC: " + exp_date_UTC + crlf,
        "exp_t_range: " + exp_t_range + crlf,
        "preselect_t_range: " + preselect_t_range + crlf,
        "path_data: " + str(path_data) + crlf,
        "path_output: '" + path_out + "'" + crlf,
        "LED_2_Hg_rat: 0.9088 # from calib experiment 19-10-08" + crlf,
        "O3_thresh: [5, 2500]" + crlf,
        "OMC_thresh_dI1I2: 20000" + crlf,
        "OMC_thresh_dI: 2000" + crlf,
        "OMC_thresh_follow: 1" + crlf,
        "OMC_unc: 0.02" + crlf,
        "OSC_eval: True" + crlf,
        "OSC_Ijump_filter: False" + crlf,
        "OSC_I_maxjump: 10000.0" + crlf,
        "OSC_I_range: [500, 5000000]" + crlf,
        "OSC_p_range: [50, 800]" + crlf,
        "OSC_pjump_filter: False # [True, 1, 2]" + crlf,
        "OSC_dp_range: [3, 17]" + crlf,
        "OSC_avg: 110.0" + crlf,
        "OSC_S_range: [0.5, 50.0]" + crlf,
        "OSC_S_ipsect: [0]" + crlf,
        "OSC_cut_edges: 750" + crlf,
        "OSC_smooth_freq: 5.0" + crlf,
        "OSC_unc: 0.005" + crlf,
        "apply_t_corr: True" + crlf,
        "use_t_ref: True" + crlf,
        "t_ref_max_delta: 999" + crlf,
    ]

    for ix, line in enumerate(cnf):
        if fnm(line.strip(), "*NPARMS*"):
            cnf[ix] = cnf[ix].replace("NPARMS", str(len(cnf) - config_nlhead))

    cnffile = path_out + "FAIROeval_config_" + exp_name + ".csv"
    with contextlib.suppress(FileExistsError):
        os.mkdir(os.path.dirname(cnffile))

    # save settings file
    with open(
        path_out + "FAIROeval_config_" + exp_name + ".yml", "w", encoding="UTF-8"
    ) as file_obj:
        for line in cnf:
            file_obj.write(line)

    print(f"'{path_out}FAIROeval_config_{exp_name}.yml',")

# done!
