# -*- coding: utf-8 -*-
import json
import re
from datetime import datetime, timezone
from pathlib import Path

from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.txt2dict import txt_2_dict_simple

from caribic2dp.carfileutils import find_files

# for the old settings:
from caribic2dp.o3_file_tools import FAIROeval_load_settings as load_old_settings
from caribic2dp.o3_file_tools import load_settings as load_pyFairoproc_settings

###############################################################################
### nasa ames general config
# delimiters:
sep_com, sep_data, sep_misc = ";", "\t", " "
# missing value:
vmiss = 9999
# version tag of file name:
filevers = "_V01"
# process what? last n entries of CarO3_files_cnf lists:
process = slice(0, None, 1)  # 39#slice(-4,)
# json with file paths to eval setting files and results:
CarO3_files_cnf = Path("/home/va6504/Data/Caribic_2/O3/Flights/CarO3_files.json")
# where to save na files:
outpath = Path("/home/va6504/Data/Caribic_2/O3/Flights/NASA_AMES_files/")
###############################################################################

with open(CarO3_files_cnf, "r") as fobj:
    CarO3_files = json.load(fobj)

setfiles = CarO3_files["setfiles"][process]
OMC_files = CarO3_files["OMC_files"][process]
OSC_files = CarO3_files["OSC_files"][process]


for ix, setting in enumerate(setfiles):
    print(f"processing {Path(OMC_files[ix]).name}...")
    if setting.endswith(".csv"):
        set_dict = load_old_settings(setting)
    else:
        set_dict = load_pyFairoproc_settings(setting)

    fldate = datetime(*set_dict["exp_date_UTC"], tzinfo=timezone.utc)
    fldate_str = fldate.strftime("%Y%m%d")

    ma_file = find_files(int(Path(setting).name[-7:-4]), "MA", binned_10s=False)

    if not ma_file:
        raise ValueError("--- failed: " + set_dict["exp_name"] + ", no MA data found. ---")

    ma_data = na1001(ma_file)
    FileName = ma_file.name.replace("MA_", "TEMP_")[:-8] + filevers + ".txt"

    for detector in ["OMC"]:  # , 'OSC']:
        if detector == "OMC":
            dat_omc = txt_2_dict_simple(OMC_files[ix], sep="\t", colhdr_ix=3, ignore_colhdr=True)
            try:
                fairoeval_vers = re.findall(r"(?<=version ).*(?=\))", dat_omc["file_hdr"][1])[0]
            except IndexError:
                fairoeval_vers = "unknown"

            x = dat_omc["data"]["col_001"]
            v = [[y if float(y) > 0.0 else str(vmiss) for y in dat_omc["data"]["col_002"]]]
            FileName = FileName.replace("TEMP", "OM")
            sname = (
                "Ozone measured with the "
                + "dual-beam UV-photometer that is part of the FAIRO instrument "
                + "during IAGOS-CARIBIC (onboard A340-600 D-AIHE of Lufthansa)"
            )

        #            if detector == 'OSC':
        #                dat_osc = txt_2_dict_simple(OSC_files[ix], sep='\t', colhdr_ix=3,
        #                                            ignore_colhdr=True)['data']
        #                x = dat_osc['col_001']
        #                v = [[y if float(y)>0. else str(vmiss) for y in dat_omc['col_002']]]
        #                FileName = FileName.replace('TEMP', 'OSC')
        #                sname = ("Ozone measured with the " +
        #                         "chemiluminescence detector that is part of the FAIRO instrument " +
        #                         "during IAGOS-CARIBIC (onboard D-AIHE A340-600 of Lufthansa)")

        mname = "IAGOS-CARIBIC (CARIBIC-2), http://www.caribic-atmospheric.com/"

        vname = ["Ozone; Ozone volume mixing ratio; [ppbV]"]
        scom = ma_data.SCOM[0:5]
        scom += ["FileName: " + FileName, "ExceptionToConsider: no", "ChangeLog: 0"]
        ncom = [
            f"FAIRO eval software version: {fairoeval_vers}",
            "Info on adjustments of the independent variable:",
            "Measurement time-stamp is synchronized to CARIBIC master PC time.",
            "Info on measurement uncertainty:",
            "Uncertainty is estimated to be at least 1 ppb OR 2.0% for the UV photometer and 2.5% combined for the chemiluminescence detector (use whatever value is larger).",
            "#",
            "The following two lines contain standard names and units according to the 'NetCDF Climate and Forecast (CF) Metadata Convention', see http://cfconventions.org/. Each entry (name or unit) corresponds to a certain column; the entry before the first tab belongs to the independent column.",
            "CF_StandardNames: time	mole_fraction_of_ozone_in_air",
            "CF_Units: [s]\t[1e-9]",
            "The following four lines contain: Start date like in line 7 (formatted as YYYYMMDD), followed by the multiplicators like in line 11; Line number containing the KeyLabel 'ExceptionToConsider' (0 if no such exception is mentioned), followed by the NaN-values like in line 12; Units like in lines 13ff; Column headers.",
            fldate_str + "\t1",
            "0\t9999",
            "[s]\t[ppbV]",
            "TimeCRef\tOzone",
        ]

        na_1001 = na1001()

        na_1001.ONAME = "F. Obersteiner; A. Zahn; f.obersteiner@kit.edu; andreas.zahn@kit.edu"
        na_1001.ORG = "Institute of Meteorology and Climate Research (IMK), Karlsruhe Institute of Technology (KIT), 76021 Karlsruhe, P.O. Box 3640, Germany"
        na_1001.SNAME = sname
        na_1001.MNAME = mname

        fldate_str = datetime.strftime(fldate, "%Y%m%d")
        na_1001.DATE = fldate.timetuple()[:3]

        na_1001.RDATE = datetime.now(timezone.utc).timetuple()[:3]

        na_1001.XNAME = [
            "TimeCRef",
            " CARIBIC_reference_time_since_0_hours_UTC_on_first_date_in_line_7",
            " [s]",
        ]

        na_1001.VNAME = vname

        na_1001.VSCAL = [1] * na_1001.NV
        na_1001.VMISS = [vmiss] * na_1001.NV

        na_1001.SCOM = scom

        na_1001.NCOM = ncom

        na_1001.X = x
        na_1001.V = v

        io = na_1001.to_file(
            outpath / FileName,
            sep=sep_misc,
            sep_com=sep_com,
            sep_data=sep_data,
            overwrite=True,
        )
        if io:
            print("+++ wrote " + FileName + " +++\n")


print("done.")
