# -*- coding: utf-8 -*-
import os
import shutil
from pathlib import Path

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
src = Path("/home/va6504/Data/Caribic_2/OverviewPlots")
dst = Path("/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/Flights_OverviewPlots")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths

ext = "*.pdf"
MIRROR = True  # set to True to remove files from server that are not in local dir

src_files = sorted(f.name for f in src.glob(ext))
dst_files = sorted(f.name for f in dst.glob(ext))

for f in src_files:
    if f in dst_files:
        ix_dst = [i for i, v in enumerate(dst_files) if v == f][0]
        dt = os.path.getmtime(dst / dst_files[ix_dst]) - os.path.getmtime(src / f)
        if dt < 0:
            print(f"{f} - file on server is younger => updating...")
            shutil.copy(src / f, dst / dst_files[ix_dst])
        else:
            print(f"{f} - file on server up-to-date => skipping...")
    else:
        print(f"not found on server: {f} => adding.")
        shutil.copy(src / f, dst / f)


if MIRROR:
    src_files = [f.name for f in src.glob("*")]
    dst_files = [f.name for f in dst.glob("*")]
    for f in dst_files:
        if f not in src_files:
            print(f"{f} - not found in local directory => deleting...")
            os.remove(dst / f)

print("done.")
