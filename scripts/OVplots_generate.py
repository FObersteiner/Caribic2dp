# -*- coding: utf-8 -*-
from datetime import datetime, timezone
from pathlib import Path

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import yaml
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.font_manager import FontProperties
from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, VPacker
from mpl_toolkits.basemap import Basemap
from pyfuppes.na1001 import FFI1001 as na1001
from pyfuppes.plottools import get_plot_range
from pyfuppes.txt2dict import txt_2_dict_simple as t2d
from scipy import stats
from scipy.interpolate import interp1d

from caribic2dp.carfileutils import dissect_filename, find_files
from caribic2dp.find_airport import get_closest_AP as get_ap
from caribic2dp.su_tools import calc_SZA

plt.rcParams["figure.figsize"] = (16, 10)

# ------------------------------------------------------------------------------

# ! user-specific paths >>>
flights_dir = Path("/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/Flights")
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"

# save pdf to?
diagn_save_dir = Path("/home/va6504/Data/Caribic_2/OverviewPlots")
# <<< ! user-specific paths


###############################################################################
"""
TODO
    - weitere Variablen?
    - Dynamik vs. Chemie Plots?
"""
# select a flight
flights = [570, 591]  # [single_flight] or [first_flight, last_flight]
yrange_cfg = Path("../config/OVplots_yranges.yml").resolve()


# airport data
ap_file = Path("../config/CARIBIC2_Airports.csv").resolve()

plot_categories = [1, 1, 1]  # flighttrack, timeseries, correlations

# plot to pdf? close plots after creation?
plt2pdf, pltcls = 1, 1


# default font sizes
FSIZE_HEADING = 16
FSIZE_AXIS = 13

# default yranges
def_y = {
    "PIC_CO2": [390, 420],  # ppm
    "PIC_CH4": [1600, 2000],  # ppb
    "ACE": [0, 3e3],  # ppt
    "ACN": [0, 1e3],  # ppt
    "Tpot": [280, 385],  # K
    "pstatic": [1e2, 1e3],  # hPa
    "H2O_gas": [3e0, 1e4],  # ppm
    "H2O_cld": [1e-1, 1e3],  # ppm
    "O3": [0, 1000],  # ppb
    "CO": [0, 250],  # ppb
    "NOy": [0, 10],  # ppb
    "BC_conc": [0, 150],  # 1/cm^3
    "alt": [-3, 18],  # m
    "SZA": [-85, 90],  # degr
    "pv": [-5, 15],  # pvu
}

with open(yrange_cfg) as f:
    set_y = yaml.load(f, Loader=yaml.FullLoader)


# default color map
cm = plt.cm.get_cmap("rainbow")

######################################################## loop: selected flights
for fno in range(flights[0], flights[-1] + 1, 1):
    print("processing flight", fno)

    # check if specific yranges are configured
    if fno in set_y:
        # use default if key is not specified in set_y
        yranges = {k: set_y[fno].get(k, v) for k, v in def_y.items()}
    else:
        yranges = def_y

    ##################################################################### load data
    # search the flight folder for data ->
    files = {
        "CO": find_files(fno, "CO", flights_dir, resolution="full"),
        "INT": find_files(fno, "INT", flights_dir, resolution="full"),
        "MA": find_files(fno, "MA", flights_dir, resolution="full"),
        "MS": find_files(fno, "MS", flights_dir, resolution="10s"),
        "NO": find_files(fno, "NO", flights_dir, resolution="full"),
        "O3": find_files(fno, "OM", flights_dir, resolution="full"),
        "O3_10s": find_files(fno, "OM", flights_dir, resolution="10s"),
        "PIC": find_files(fno, "PIC", flights_dir, resolution="full"),
        "PTR": find_files(fno, "PTR", flights_dir, resolution="10s"),
        "SP2": find_files(fno, "SP2", flights_dir, resolution="10s"),
        "WA": find_files(fno, "WA", flights_dir, resolution="full"),
    }
    files = {k: v[0] if v else None for k, v in files.items()}

    # try to find 10s WA files as a second option
    if not files["WA"]:
        files["WA"] = find_files(fno, "WA", flights_dir, resolution="10s")
        files["WA"] = files["WA"][0] if files["WA"] else None

    # double-check if file is correct ->
    if dissect_filename(files["MA"]).flight_no != fno:
        raise ValueError("Wrong flight number in MA file!")

    # load all available data ->
    data = {}
    for k, v in files.items():
        if v:
            data[k] = na1001(v)

    # metadata from master file ->
    flDate = data["MA"].DATE
    flRoute = data["MA"].SCOM[2].split("#")[0].strip()  # always at ix=2 ?
    flRoute = flRoute.split(" ")[-2:]
    title = (
        "F"
        + str(fno)
        + " on "
        + str(flDate).replace(", ", "-").replace("[", "").replace("]", "")
        + " from "
        + " to ".join(flRoute)
    )

    # convert data to numpy nd array type: data representation ->
    XDTYPE, VDTYPE = "float32", "float64"

    # define what to extract ->
    data_extr_def = {
        "MA": ["PosLat", "PosLong", "Altitude", "Tpot", "WindSpeed", "pstatic"],
        "CO": ["CO"],
        "INT": ["lat", "lon"],
        "MS": ["SZA", "pv__pvu_", "eq_latitude_deg_n_"],
        "NO": ["NO", "NOy"],
        "O3": ["Ozone"],
        "O3_10s": ["H_rel_TP"],
        "PIC": ["CO2", "CH4", "H2O"],
        "PTR": ["ACE", "ACN"],
        "SP2": ["BC_Conc", "BC_Mass_Conc"],
        "WA": ["H2Ogas", "H2Ocloud"],
    }

    # check if everything is defined correctly ->
    if not all(dkey in files for dkey in data_extr_def):
        raise ValueError("extraction keys must be part of loaded data!")

    data_extr = {}
    for key in data:
        data_extr[key] = data[key].to_dict_nparray(xdtype=XDTYPE, vdtype=VDTYPE)

    data_extr["MA"]["SZA"] = np.array(calc_SZA(data), dtype=VDTYPE)

    if plt2pdf:
        pp = PdfPages(
            diagn_save_dir / ("_".join(files["MA"].name.split("_")[1:5]) + "_ovplots.pdf")
        )
        pdf_info = pp.infodict()
        pdf_info["CreationDate"] = datetime.now(timezone.utc)

    ############################################################## now the plots...

    # default xrange:
    def_xrange = [
        data_extr["MA"]["TimeCRef"][0] - 900,
        data_extr["MA"]["TimeCRef"][-1] + 900,
    ]

    ################################################################## Flight track
    # airport stuff
    if plot_categories[0]:
        ap_dict = t2d(ap_file)["data"]
        ap = np.array(ap_dict["IATA"], dtype="str")
        ap_lat, ap_lon = (
            np.array(ap_dict["PosLat"], dtype="float32"),
            np.array(ap_dict["PosLon"], dtype="float32"),
        )
        aptxt_lat, aptxt_lon = (
            np.array(ap_dict["Plttxt_lat"], dtype="float32"),
            np.array(ap_dict["Plttxt_lon"], dtype="float32"),
        )
        apDep_info = get_ap(data_extr["MA"]["PosLat"][0], data_extr["MA"]["PosLong"][0], ap_file)
        w_Dep = np.where(ap == apDep_info[0])
        apDest_info = get_ap(data_extr["MA"]["PosLat"][-1], data_extr["MA"]["PosLong"][-1], ap_file)
        w_Dest = np.where(ap == apDest_info[0])
        apDepPlttxtlatlon = [aptxt_lat[w_Dep][0], aptxt_lon[w_Dep][0]]
        apDestPlttxtlatlon = [aptxt_lat[w_Dest][0], aptxt_lon[w_Dest][0]]

        pr = get_plot_range(data_extr["MA"]["PosLong"], add_percent=15) + get_plot_range(
            data_extr["MA"]["PosLat"], add_percent=15
        )
        if pr[1] - pr[0] < 100:
            pr[:2] = [
                np.mean([data_extr["MA"]["PosLong"][0], data_extr["MA"]["PosLong"][-1]]) - 60,
                np.mean([data_extr["MA"]["PosLong"][0], data_extr["MA"]["PosLong"][-1]]) + 60,
            ]
        if pr[3] - pr[2] < 50:
            pr[2:] = [
                np.mean([data_extr["MA"]["PosLat"][0], data_extr["MA"]["PosLat"][-1]]) - 30,
                np.mean([data_extr["MA"]["PosLat"][0], data_extr["MA"]["PosLat"][-1]]) + 30,
            ]

        fig0 = plt.figure(figsize=(16, 10))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95)
        plt.title(title, weight="bold", fontsize=FSIZE_HEADING)
        m = Basemap(
            llcrnrlon=pr[0],
            llcrnrlat=pr[2],
            urcrnrlon=pr[1],
            urcrnrlat=pr[3],
            resolution="l",
            projection="merc",
        )
        m.drawcoastlines()
        m.fillcontinents()
        m.drawparallels(np.arange(-90, 90, (pr[3] - pr[2]) // 5), labels=[1, 1, 0, 1])
        m.drawmeridians(np.arange(-180, 180, (pr[1] - pr[0]) // 5), labels=[1, 1, 0, 1])
        # plot flight trajectory
        x, y = m(data_extr["MA"]["PosLong"], data_extr["MA"]["PosLat"])
        plt.plot(x, y, color="b", linewidth=2.0, label="flight track")  # , '-'
        # plot airports
        x, y = m(ap_lon[w_Dep], ap_lat[w_Dep])
        plt.plot(x, y, color="g", ls="", marker="*", markersize=20, label="departure AP")
        x, y = m(apDepPlttxtlatlon[1], apDepPlttxtlatlon[0])
        fp0 = FontProperties()
        fp = fp0.copy()  # create copy to not overwrite default
        fp.set_weight("bold")
        fp.set_size(18)
        plt.text(x, y, apDep_info[0], fontproperties=fp, color="k")
        x, y = m(ap_lon[w_Dest], ap_lat[w_Dest])
        plt.plot(x, y, color="r", ls="", marker="*", markersize=20, label="arrival AP")
        x, y = m(apDestPlttxtlatlon[1], apDestPlttxtlatlon[0])
        plt.text(x, y, apDest_info[0], fontproperties=fp, color="k")

        # check for WAS filling(sampling) times
        if "INT" in data_extr:
            x, y = m(data_extr["INT"]["lon"], data_extr["INT"]["lat"])
            plt.plot(
                x,
                y,
                color="magenta",
                ls="",
                marker="o",
                markersize=8,
                label="WAS filling",
            )

        plt.legend(loc=0)

        if plt2pdf:
            pp.savefig(fig0)
        if pltcls:
            plt.close()

        # cleanup
        del (
            ap,
            apDepPlttxtlatlon,
            apDestPlttxtlatlon,
            ap_lat,
            ap_lon,
            aptxt_lat,
            aptxt_lon,
            pr,
            w_Dep,
            w_Dest,
            x,
            y,
        )

    ##############
    #            #
    # TIMESERIES #
    #            #
    ################################################################ timeseries (1)
    ################################### Tpot, pstatic / O3, CO / H2O gas, H2O cloud
    if plot_categories[1]:
        fig_ts1, axs = plt.subplots(3, figsize=(16, 10), sharex=True, gridspec_kw={"hspace": 0.1})

        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.94)

        axs[0].set_title(title + " (timeseries 1)", fontsize=FSIZE_HEADING, weight="bold")
        axs[2].set_xlabel(
            f'UTC[s] from {"-".join(list(map(str, flDate)))} 00:00h',
            color="k",
            fontsize=FSIZE_AXIS,
        )
        axs[2].ticklabel_format(style="sci", axis="x")
        axs[0].set_xlim(def_xrange)

        # Tpot, alt, PV
        axs[0].set_ylim(yranges["Tpot"])
        axs[0].set_ylabel("Tpot [K]", color="r", fontsize=FSIZE_AXIS)
        axs[0].tick_params(colors="r", axis="y")
        axs[0].yaxis.set_major_locator(mticker.LinearLocator(8))
        axs[0].plot(data_extr["MA"]["TimeCRef"], data_extr["MA"]["Tpot"], "r")
        if "INT" in data_extr:
            f_ip_Tpot = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Tpot"],
                kind="linear",
                bounds_error=False,
            )
            p_was = axs[0].plot(
                data_extr["INT"]["TimeCRef"],
                f_ip_Tpot(data_extr["INT"]["TimeCRef"]),
                color="b",
                ls="",
                marker="o",
                markersize=8,
                label="WAS filling",
            )
            axs[0].legend(p_was, ["WAS Filling"], loc=0)
        axs_01 = axs[0].twinx()
        axs_01.yaxis.set_major_locator(mticker.LinearLocator(8))
        axs_01.set_ylim(yranges["alt"])
        axs_01.tick_params(colors="k", which="both", axis="y")
        axs_01.set_yticklabels(["", "0", "3", "6", "9", "12", "15", ""])
        ybox1 = TextArea(
            "altitude [km],",
            textprops={
                "color": "k",
                "size": FSIZE_AXIS,
                "rotation": 90,
                "ha": "left",
                "va": "bottom",
            },
        )
        ybox3 = TextArea(
            "PV [PVU]",
            textprops={
                "color": "dodgerblue",
                "size": FSIZE_AXIS,
                "rotation": 90,
                "ha": "left",
                "va": "bottom",
            },
        )
        ybox = VPacker(children=[ybox3, ybox1], align="bottom", pad=0, sep=5)
        anchored_ybox = AnchoredOffsetbox(
            loc=8,
            child=ybox,
            pad=0.0,
            frameon=False,
            bbox_to_anchor=(1.035, 0.14),
            bbox_transform=axs[0].transAxes,
            borderpad=0.0,
        )
        axs[0].add_artist(anchored_ybox)
        axs_01.plot(data_extr["MA"]["TimeCRef"], data_extr["MA"]["Altitude"] / 1000, "k")
        if "MS" in data_extr:
            axs_01.plot(data_extr["MS"]["TimeCRef"], data_extr["MS"]["pv__pvu_"], "dodgerblue")

        axs[0].grid()

        # O3, CO
        axs[1].set_ylim(yranges["O3"])
        axs[1].yaxis.set_major_locator(mticker.LinearLocator(6))
        axs[1].set_ylabel("O$_3$[ppb]", color="g", fontsize=FSIZE_AXIS)
        axs[1].tick_params(colors="g", axis="y")
        if "O3" in data_extr:
            axs[1].plot(data_extr["O3"]["TimeCRef"], data_extr["O3"]["Ozone"], "g")
        axs_11 = axs[1].twinx()
        axs_11.set_ylim(yranges["CO"])
        axs_11.yaxis.set_major_locator(mticker.LinearLocator(6))
        axs_11.set_ylabel("CO[ppb]", color="k", fontsize=FSIZE_AXIS)
        axs_11.tick_params(colors="k", axis="y")
        if "CO" in data_extr:
            axs_11.plot(data_extr["CO"]["TimeCRef"], data_extr["CO"]["CO"], "k")
        axs[1].grid()

        # gasphase and cloud H2O
        axs[2].set_ylim(yranges["H2O_gas"])
        axs[2].set_yscale("log")
        axs[2].set_ylabel("H$_2$O_gas[ppm]", color="b", fontsize=FSIZE_AXIS)
        axs[2].tick_params(colors="b", axis="y")
        axs_21 = axs[2].twinx()
        axs_21.set_ylim(yranges["H2O_cld"])
        axs_21.set_yscale("log")
        axs_21.set_ylabel("H$_2$O_cloud[ppm]", color="aqua", fontsize=FSIZE_AXIS)
        axs_21.tick_params(colors="aqua", axis="y")
        if "WA" in data_extr:
            axs[2].plot(data_extr["WA"]["TimeCRef"], data_extr["WA"]["H2Ogas"], "b")
            axs_21.plot(
                data_extr["WA"]["TimeCRef"][np.where(data_extr["WA"]["H2Ocloud"] > 0.0)],
                data_extr["WA"]["H2Ocloud"][np.where(data_extr["WA"]["H2Ocloud"] > 0.0)],
                marker="x",
                color="aqua",
                ls="",
            )
        if "PIC" in data_extr:
            pich2o = axs[2].plot(
                data_extr["PIC"]["TimeCRef"], data_extr["PIC"]["H2O"], "lime", alpha=0.4
            )
            axs[2].legend(pich2o, ["tracegas H$_2$O (PIC)"], loc=0)

        axs[2].grid()

        if plt2pdf:
            pp.savefig(fig_ts1)
        if pltcls:
            plt.close()

        ################################################################ timeseries (2)
        ###############################################  ACE, CO / O3, NOy / ACN, SP2BC
        fig_ts2, axs = plt.subplots(3, figsize=(16, 10), sharex=True, gridspec_kw={"hspace": 0.1})

        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95)

        axs[0].set_title(title + " (timeseries 2)", fontsize=FSIZE_HEADING, weight="bold")
        axs[2].set_xlabel(
            f'UTC[s] from {"-".join(list(map(str, flDate)))} 00:00h',
            color="k",
            fontsize=FSIZE_AXIS,
        )
        axs[0].set_xlim(def_xrange)

        # ACE, CO
        axs[0].set_ylim(yranges["ACE"])
        axs[0].yaxis.set_major_locator(mticker.LinearLocator(6))
        axs[0].set_ylabel("Acetone[ppt]", color="r", fontsize=FSIZE_AXIS)
        axs[0].tick_params(colors="r", axis="y")
        if "PTR" in data_extr:
            axs[0].plot(
                data_extr["PTR"]["TimeCRef"][np.isfinite(data_extr["PTR"]["ACE"])],
                data_extr["PTR"]["ACE"][np.isfinite(data_extr["PTR"]["ACE"])],
                marker="x",
                color="r",
                ls="",
            )
        axs_01 = axs[0].twinx()
        axs_01.set_ylim(yranges["CO"])
        axs_01.yaxis.set_major_locator(mticker.LinearLocator(6))
        axs_01.set_ylabel("CO[ppb]", color="k", fontsize=FSIZE_AXIS)
        axs_01.tick_params(colors="k", axis="y")
        if "CO" in data_extr:
            axs_01.plot(data_extr["CO"]["TimeCRef"], data_extr["CO"]["CO"], "k")
        axs[0].grid()

        # O3, NOy
        axs[1].set_ylim(yranges["O3"])
        axs[1].set_ylabel("O$_3$[ppb]", color="g", fontsize=FSIZE_AXIS)
        axs[1].tick_params(colors="g", axis="y")
        if "O3" in data_extr:
            axs[1].plot(data_extr["O3"]["TimeCRef"], data_extr["O3"]["Ozone"], "g")
        axs_11 = axs[1].twinx()
        axs_11.set_ylim(yranges["NOy"])
        axs_11.set_ylabel("NO$_y$[ppb]", color="firebrick", fontsize=FSIZE_AXIS)
        axs_11.tick_params(colors="firebrick", axis="y")
        if "NO" in data_extr:
            axs_11.plot(data_extr["NO"]["TimeCRef"], data_extr["NO"]["NOy"], color="firebrick")
        axs[1].grid()

        # ACN, SP2BC
        axs[2].set_ylim(yranges["ACN"])
        axs[2].yaxis.set_major_locator(mticker.LinearLocator(6))
        axs[2].set_ylabel("Acetonitrile[ppt]", color="darkorchid", fontsize=FSIZE_AXIS)
        axs[2].tick_params(colors="darkorchid", axis="y")
        if "PTR" in data_extr:
            axs[2].plot(
                data_extr["PTR"]["TimeCRef"][np.isfinite(data_extr["PTR"]["ACN"])],
                data_extr["PTR"]["ACN"][np.isfinite(data_extr["PTR"]["ACN"])],
                marker="x",
                color="darkorchid",
                ls="",
            )
        axs_21 = axs[2].twinx()
        axs_21.set_ylim(yranges["BC_conc"])
        axs_21.yaxis.set_major_locator(mticker.LinearLocator(6))
        axs_21.set_ylabel("Black Carbon conc. [1/cm^3]", color="darkslategrey", fontsize=FSIZE_AXIS)
        axs_21.tick_params(colors="darkslategrey", axis="y")
        if "SP2" in data_extr:
            axs_21.plot(
                data_extr["SP2"]["TimeCRef"],
                data_extr["SP2"]["BC_Conc"],
                "darkslategrey",
            )
        axs[2].grid()

        if plt2pdf:
            pp.savefig(fig_ts2)
        if pltcls:
            plt.close()

        ################################################################ timeseries (3)
        ################################################ alt, SZA / Tpot, PV / CO2, CH4
        fig_ts3, axs = plt.subplots(3, figsize=(16, 10), sharex=True, gridspec_kw={"hspace": 0.1})

        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95)

        axs[0].set_title(title + " (timeseries 3)", fontsize=FSIZE_HEADING, weight="bold")
        axs[2].set_xlabel(
            f'UTC[s] from {"-".join(list(map(str, flDate)))} 00:00h',
            color="k",
            fontsize=FSIZE_AXIS,
        )
        axs[0].set_xlim(def_xrange)

        # alt, SZA
        axs[0].set_ylim(yranges["alt"])
        axs[0].set_ylabel("altitude[km]", color="g", fontsize=FSIZE_AXIS)
        axs[0].tick_params(colors="g")
        axs[0].yaxis.set_major_locator(mticker.LinearLocator(8))
        axs[0].set_yticklabels(["", "0", "3", "6", "9", "12", "15", ""])
        axs[0].plot(data_extr["MA"]["TimeCRef"], data_extr["MA"]["Altitude"] / 1000, "g")
        if "O3_10s" in data_extr:
            f_ip = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Altitude"] / 1000,
                kind="linear",
                bounds_error=False,
            )
            hreltp = axs[0].plot(
                data_extr["O3_10s"]["TimeCRef"],
                f_ip(data_extr["O3_10s"]["TimeCRef"]) - data_extr["O3_10s"]["H_rel_TP"],
                "lime",
            )  # , alpha=0.4)
            axs[0].legend(hreltp, ["O$_3$-based TP height[m]"], loc=0)
        axs_01 = axs[0].twinx()
        axs_01.set_ylim(yranges["SZA"])
        axs_01.tick_params(colors="k", which="both")
        axs_01.yaxis.set_major_locator(mticker.LinearLocator(8))
        axs_01.set_ylabel("SZA, eqLat [degr]", color="k", fontsize=FSIZE_AXIS)
        sza_colors = [np.array([0, 0, 0, 1]), np.array([0.953, 0.51, 0.063, 1])]
        c = np.array([sza_colors[0] if v > 90 else sza_colors[1] for v in data_extr["MA"]["SZA"]])
        axs_01.scatter(
            data_extr["MA"]["TimeCRef"],
            (data_extr["MA"]["SZA"] - 90) * -1,
            color=c,
            marker="o",
            s=0.5,
        )
        if "MS" in data_extr:
            axs_01.plot(data_extr["MS"]["TimeCRef"], data_extr["MS"]["eq_latitude_deg_n_"], "k")
        axs[0].grid()

        # TODO: find something new here
        # axs[1].set_ylim(yranges['Tpot'])
        # axs[1].set_ylabel('Tpot[K]', color='r', fontsize=FSIZE_AXIS)
        # axs[1].tick_params(colors='r', axis='y')
        # axs[1].yaxis.set_major_locator(mticker.LinearLocator(6))
        # axs[1].plot(data_extr['MA']['TimeCRef'], data_extr['MA']['Tpot'], 'r')
        # if 'INT' in data_extr.keys():
        #     p_was = axs[1].plot(data_extr['INT']['TimeCRef'], f_ip_Tpot(data_extr['INT']['TimeCRef']),
        #                         color='k', ls='', marker='x', markersize=10, label='WAS filling')
        #     axs[1].legend(p_was, ['WAS Filling'], loc=0)
        # axs_11 = axs[1].twinx()
        # axs_11.set_ylim(yranges['pv'])
        # axs_11.set_ylabel('ECMWF_PV[PVU]', color='dodgerblue', fontsize=FSIZE_AXIS)
        # axs_11.tick_params(colors='dodgerblue')
        # axs_11.yaxis.set_major_locator(mticker.LinearLocator(6))
        # if 'MS' in data_extr.keys():
        #     axs_11.plot(data_extr['MS']['TimeCRef'], data_extr['MS']['pv__pvu_'], 'dodgerblue')
        # axs[1].grid()

        # CO2, CH4
        axs[2].set_ylim(yranges["PIC_CO2"])
        axs[2].set_ylabel("CO$_2$[ppm]", color="k", fontsize=FSIZE_AXIS)
        axs[2].tick_params(colors="k")
        axs[2].yaxis.set_major_locator(mticker.LinearLocator(5))
        if "PIC" in data_extr:
            axs[2].plot(data_extr["PIC"]["TimeCRef"], data_extr["PIC"]["CO2"], "k")
        axs_21 = axs[2].twinx()
        axs_21.yaxis.set_major_locator(mticker.LinearLocator(5))
        axs_21.set_ylim(yranges["PIC_CH4"])
        axs_21.tick_params(colors="magenta", which="both")
        axs_21.set_ylabel("CH$_4$[ppb]", color="magenta", fontsize=FSIZE_AXIS)
        if "PIC" in data_extr:
            axs_21.plot(data_extr["PIC"]["TimeCRef"], data_extr["PIC"]["CH4"], "magenta")
        axs[2].grid()

        if plt2pdf:
            pp.savefig(fig_ts3)
        if pltcls:
            plt.close()

    ##################################### altitude, Tpot, SZA or windspeed if no MS
    #    l1, l2, l3 = 'alt[m]', 'Tpot[K]', 'wndspd[m/s]'
    #
    #    fig1 = plt.figure(figsize=(16, 10))
    #    host = host_subplot(111, axes_class=AA.Axes)
    #    plt.tight_layout()
    #    plt.subplots_adjust(left=0.12, right=0.92)
    #    par1 = host.twinx()
    #    par2 = host.twinx()
    #    offset = -60
    #    new_fixed_axis = par2.get_grid_helper().new_fixed_axis
    #    par2.axis["right"] = new_fixed_axis(loc="left",
    #                                        axes=par2,
    #                                        offset=(offset, 0))
    #    par1.axis["right"].toggle(all=True)
    #    par2.axis["right"].toggle(all=True)
    #    host.set_xlim(def_xrange)
    #    par2.set_ylim(yranges['Tpot'])
    #
    #    p1 = host.plot(data_extr['MA']['TimeCRef'], data_extr['MA']['Altitude'], color='k', label=l1, zorder=1)
    #    p2 = par2.plot(data_extr['MA']['TimeCRef'], data_extr['MA']['Tpot'], label=l2, color='r', zorder=3)
    #    if 'MS' in data.keys():
    #        c_k, c_o = np.array([0,0,0,1]), np.array([0.953,0.51,0.063,1])
    #        c = np.array([c_k if v>90 else c_o for v in data_extr['MS']['SZA']])
    #        p3 = par1.scatter(data_extr['MS']['TimeCRef'][np.isfinite(data_extr['MS']['SZA'])],
    #                          data_extr['MS']['SZA'][np.isfinite(data_extr['MS']['SZA'])],
    #                          color=c, marker='o', s=0.5, label='SZA[degr]', zorder=2)
    #        par1.set_ylabel('SZA[degr]', color='darkorange')
    #    else:
    #        p3 = par1.plot(data_extr['MA']['TimeCRef'],
    #                       data_extr['MA']['WindSpeed']*(1852/3600), color='b', label=l3, zorder=2)
    #        par1.set_ylabel(l3, color='b')
    #
    #    host.set_xlabel('UTC[s]', color='k')
    #    host.set_ylabel(l1, color='k')
    #    par2.set_ylabel(l2, color='r')
    #    host.legend()
    #    plt.show(block=False)
    #
    #    if plt2pdf:
    #        pp.savefig(fig1)

    ############################################ Picarro CO2, CH4, Tpot color coded
    #    if all([k in data.keys() for k in ['PIC']]):
    #        f_ip = interp1d(data_extr['MA']['TimeCRef'], data_extr['MA']['Tpot'],
    #                        kind='linear', bounds_error=False)
    #        z = f_ip(data_extr['PIC']['TimeCRef'])
    #        xRange = get_plot_range(data_extr['PIC']['TimeCRef'], add_percent=1)
    #
    #        fig4, ax1 = plt.subplots(figsize=(16, 10))
    #        plt.title(title)
    #        ax2 = ax1.twinx()
    #        p0 = ax1.scatter(data_extr['PIC']['TimeCRef'], data_extr['PIC']['CH4'],
    #                         c=z, cmap=cm,
    #                         vmin=yranges['Tpot'][0], vmax=yranges['Tpot'][1],
    #                         label='Picarro CH4', s=2)
    #        plt.colorbar(p0).set_label('Tpot[K]', rotation=90)
    #        p1 = ax2.plot(data_extr['PIC']['TimeCRef'], data_extr['PIC']['CO2'],
    #                      'k', label='Picarro CO2')
    #        ax1.set_xlabel('UTC[s]', color='k')
    #        ax1.set_ylabel('CH4[ppb]', color='g')
    #        ax1.set_xlim(def_xrange)
    #        ax1.set_ylim(yranges['PIC_CH4'])
    #        ax2.set_ylabel('CO2[ppm]', color='k')
    #        ax2.set_ylim(yranges['PIC_CO2'])
    #        plt.legend(loc=0)
    #        plt.tight_layout()
    #        plt.show(block=False)
    #
    #        if plt2pdf:
    #            pp.savefig(fig4)

    ################################################# CO, Acetone, Acetonitrile, O3
    #    if all([k in data.keys() for k in ['O3', 'CO', 'PTR']]):
    #        l1, l2, l3 = 'CO[ppb]', 'Acetone[ppt]', 'Tpot[K]'
    #        f_ip = interp1d(data_extr['O3']['TimeCRef'], data_extr['O3']['Ozone'],
    #                        fill_value='extrapolate',
    #                        kind='linear', bounds_error=False)
    #        z = f_ip(data_extr['PTR']['TimeCRef'])
    #        xRange = get_plot_range(np.append(data_extr['CO']['TimeCRef'], data_extr['PTR']['TimeCRef']), add_percent=1)
    #        y1Range = get_plot_range(data_extr['CO']['CO'], add_percent=3)
    #        y2Range = get_plot_range(data_extr['PTR']['ACE'], add_percent=3)
    #        y3Range = get_plot_range(data_extr['MA']['Tpot'], add_percent=3)
    #
    #        fig5 = plt.figure(figsize=(16, 10))
    #        host = host_subplot(111, axes_class=AA.Axes)
    #        plt.tight_layout()
    #        plt.subplots_adjust(left=0.12, right=1.05)
    #        par1 = host.twinx()
    #        par2 = host.twinx()
    #        offset = -60
    #        new_fixed_axis = par2.get_grid_helper().new_fixed_axis
    #        par2.axis["right"] = new_fixed_axis(loc="left",
    #                                            axes=par2,
    #                                            offset=(offset, 0))
    #
    #        par1.axis["right"].toggle(all=True)
    #        par2.axis["right"].toggle(all=True)
    #        host.set_xlim(def_xrange)
    #        host.set_ylim(y1Range)
    #        par1.set_ylim(yranges['ACE'])
    #        host.set_xlabel('UTC[s]', color='k')
    #        host.set_ylabel(l1, color='k')
    #        par1.set_ylabel(l2, color='k')
    #        par2.set_ylabel(l3, color='r')
    #        p1 = host.plot(data_extr['CO']['TimeCRef'], data_extr['CO']['CO'],
    #                       color='k', label=l1, alpha=0.75, zorder=1)
    #        p2 = par1.scatter(data_extr['PTR']['TimeCRef'], data_extr['PTR']['ACE'],
    #                          c=z, cmap=cm, label=l2, zorder=3)
    #        p3 = par2.plot(data_extr['MA']['TimeCRef'], data_extr['MA']['Tpot'],
    #                       label=l3, color='r', zorder=2)
    #        plt.colorbar(p2).set_label('O3[ppb]', rotation=90)
    #        par1.set_ylim(y2Range)
    #        par2.set_ylim(yranges['Tpot'])
    #        host.legend(loc=0)
    #        host.axis["left"].label.set_color('k')
    #        par1.axis["right"].label.set_color('k')
    #        par2.axis["right"].label.set_color('r')
    #        plt.show(block=False)
    #
    #        if plt2pdf:
    #            pp.savefig(fig5)

    ################################################## Acetone, Acetonitrile, Ozone
    #    f_ip = interp1d(data_extr['O3']['TimeCRef'], data_extr['O3']['Ozone'],
    #                    kind='linear', bounds_error=False)
    #    z = f_ip(data_extr['PTR']['TimeCRef'])
    #
    #    xRange = get_plot_range(data_extr['PTR']['TimeCRef'], add_percent=1)
    #    y1Range = get_plot_range(data_extr['PTR']['ACE'], add_percent=3)
    #    y2Range = get_plot_range(data_extr['PTR']['ACN'], add_percent=3)
    #
    #    fig6, ax1 = plt.subplots(figsize=(16, 10))
    #    plt.title(title)
    #    ax2 = ax1.twinx()
    #    p0 = ax1.scatter(data_extr['PTR']['TimeCRef'], data_extr['PTR']['ACE'],
    #                     c=z, cmap=cm, s=25, zorder=1)
    #    p00 = ax1.plot(data_extr['PTR']['TimeCRef'][np.isfinite(data_extr['PTR']['ACE'])],
    #                   data_extr['PTR']['ACE'][np.isfinite(data_extr['PTR']['ACE'])], 'grey',
    #                   alpha=0.3, label='Acetone[ppt]', zorder=2)
    #    plt.colorbar(p0).set_label('O3[ppb]', rotation=90)
    #    #    p1 = ax2.scatter(data_extr['PTR']['TimeCRef'], data_extr['PTR'][ACN'], c='k', s=12)
    #    p10 = ax2.plot(data_extr['PTR']['TimeCRef'][np.isfinite(data_extr['PTR']['ACN'])],
    #                   data_extr['PTR']['ACN'][np.isfinite(data_extr['PTR']['ACN'])], 'g',
    #                   alpha=0.75, label='Acetonitrile[ppt]', zorder=3)
    #    ax1.set_xlabel('UTC[s]', color='k')
    #    ax1.set_ylabel('Acetone[ppt]', color='k')
    #    ax1.set_xlim(def_xrange)
    #    ax1.set_ylim(yranges['ACE'])
    #    ax2.set_ylabel('Acetonitrile[ppt]', color='g')
    #    ax2.set_ylim(yranges['ACN'])
    #    lbls = [l.get_label() for l in p00+p10]
    #    plt.legend(p00+p10, lbls, loc=0)
    #    plt.tight_layout()
    #    plt.show(block=False)
    #
    #    if plt2pdf:
    #        pp.savefig(fig6)

    ########################################### Acetonitrile vs. SP2 BC, timeseries
    #    y0Range = get_plot_range(data_extr['PTR']['ACN'])
    #    y1Range = get_plot_range(data_extr['SP2']['BC_Conc'])
    #
    #    fig9, ax1 = plt.subplots(figsize=(16, 10))
    #    plt.title(title)
    #    ax2 = ax1.twinx()
    #    ax1.set_xlim(def_xrange)
    #    ax1.set_xlabel('UTC [s]', color='k')
    #
    #    ax2.set_ylabel('Acetonitrile [ppt]', color='r')
    #    ax2.set_ylim(yranges['ACN'])
    #    ax2.tick_params('y', colors='r')
    #
    #    ax1.set_ylabel('black carbon conc. [1/cm^3]', color='k')
    #    ax1.tick_params('y', colors='k')
    #    ax1.set_ylim(y1Range)
    #    p0 = ax2.plot(data_extr['PTR']['TimeCRef'], data_extr['PTR']['ACN'], color='r', label='ACN', marker='o')
    #    p1 = ax1.plot(data_extr['SP2']['TimeCRef'], data_extr['SP2']['BC_Conc'], color='k', label='BC')
    #    lbls = [l.get_label() for l in p0+p1]
    #    plt.legend(p0+p1, lbls, loc=0)
    #    plt.tight_layout()
    #    ax2.grid(b=True, which='major', color='r', alpha=0.6)
    #    ax1.grid(b=True, which='major', linestyle='--', color='k', alpha=0.6)
    #    plt.show(block=False)
    #
    #    if plt2pdf:
    #        pp.savefig(fig9)

    ################
    #              #
    # CORRELATIONS #
    #              #
    ############################################################# O3-CO correlation
    if plot_categories[0]:
        if all(k in data_extr for k in ["O3", "CO"]):
            f_ip = interp1d(
                data_extr["O3"]["TimeCRef"],
                data_extr["O3"]["Ozone"],
                kind="linear",
                bounds_error=False,
            )
            vO3_ip = f_ip(data_extr["CO"]["TimeCRef"])
            f_ip = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Tpot"],
                kind="linear",
                bounds_error=False,
            )
            z = f_ip(data_extr["CO"]["TimeCRef"])

            yRange = get_plot_range(vO3_ip, add_percent=1)
            xRange = get_plot_range(data_extr["CO"]["CO"], add_percent=3)

            fig_corrO3CO, ax1 = plt.subplots(figsize=(16, 10))
            plt.subplots_adjust(top=0.95, bottom=0.075, left=0.06, right=0.9)

            plt.title(title + " (O$_3$-CO)", fontsize=FSIZE_HEADING, weight="bold")
            p0 = ax1.scatter(
                data_extr["CO"]["CO"],
                vO3_ip,
                c=z,
                cmap=cm,
                vmin=yranges["Tpot"][0],
                vmax=yranges["Tpot"][1],
                label="O$_3$ vs. CO",
            )

            cbaxes = fig_corrO3CO.add_axes([0.92, 0.12, 0.03, 0.8])
            cb = plt.colorbar(p0, cax=cbaxes)
            cb.set_label("Tpot[K]", rotation=90, fontsize=FSIZE_AXIS)

            ax1.set_ylabel("O$_3$[ppb]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_xlabel("CO[ppb]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_xlim(yranges["CO"])
            ax1.set_ylim(yranges["O3"])
            ax1.legend(loc=0)
            ax1.grid()
            plt.show(block=False)

            if plt2pdf:
                pp.savefig(fig_corrO3CO)
            if pltcls:
                plt.close()

        ######################################################## Acetone-CO correlation
        if all(k in data_extr for k in ["PTR", "CO"]):
            f_ip = interp1d(
                data_extr["CO"]["TimeCRef"],
                data_extr["CO"]["CO"],
                kind="linear",
                bounds_error=False,
            )
            vCO_ip = f_ip(data_extr["PTR"]["TimeCRef"])
            f_ip = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Tpot"],
                kind="linear",
                bounds_error=False,
            )
            z = f_ip(data_extr["PTR"]["TimeCRef"])

            v0_finite = np.arange(0, len(vCO_ip), 1)[np.isfinite(vCO_ip)]
            v1_finite = np.arange(0, len(data_extr["PTR"]["ACE"]), 1)[
                np.isfinite(data_extr["PTR"]["ACE"])
            ]
            finite_mask = np.intersect1d(v0_finite, v1_finite)
            slope, intercept, r_value, p_value, std_err = stats.linregress(
                vCO_ip[finite_mask], data_extr["PTR"]["ACE"][finite_mask]
            )

            fig_corrACECO, ax1 = plt.subplots(figsize=(16, 10))
            plt.subplots_adjust(top=0.95, bottom=0.075, left=0.06, right=0.9)

            plt.title(title + " (ACE-CO)", fontsize=FSIZE_HEADING, weight="bold")

            p0 = ax1.scatter(
                vCO_ip,
                data_extr["PTR"]["ACE"],
                c=z,
                cmap=cm,
                vmin=yranges["Tpot"][0],
                vmax=yranges["Tpot"][1],
                label="Acetone vs. CO",
            )

            cbaxes = fig_corrACECO.add_axes([0.92, 0.12, 0.03, 0.8])
            cb = plt.colorbar(p0, cax=cbaxes)
            cb.set_label("Tpot[K]", rotation=90, fontsize=FSIZE_AXIS)

            p1 = ax1.plot(
                vCO_ip,
                vCO_ip * slope + intercept,
                color="k",
                linewidth=2,
                label="CO *%6.2f %+6.2f" % (slope, intercept) + ", r^2 = %6.5f" % (r_value**2),
            )

            ax1.set_xlabel("CO[ppb]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_ylabel("Acetone[ppt]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_xlim(yranges["CO"])
            ax1.set_ylim(yranges["ACE"])
            ax1.legend(loc=0)
            ax1.grid()
            plt.show(block=False)

            if plt2pdf:
                pp.savefig(fig_corrACECO)
            if pltcls:
                plt.close()

        ########################################################## O3 - H2O correlation
        if all(k in data_extr for k in ["O3", "WA"]):
            f_ip = interp1d(
                data_extr["WA"]["TimeCRef"],
                data_extr["WA"]["H2Ogas"],
                kind="linear",
                bounds_error=False,
            )
            vH2O_ip = f_ip(data_extr["O3"]["TimeCRef"])
            f_ip = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Tpot"],
                kind="linear",
                bounds_error=False,
            )
            z = f_ip(data_extr["O3"]["TimeCRef"])

            fig_corrO3H2O, ax1 = plt.subplots(figsize=(16, 10))
            plt.subplots_adjust(top=0.95, bottom=0.075, left=0.06, right=0.9)

            plt.title(title + " (O$_3$-H$_2$O)", fontsize=FSIZE_HEADING, weight="bold")

            p0 = ax1.scatter(
                vH2O_ip,
                data_extr["O3"]["Ozone"],
                c=z,
                cmap=cm,
                vmin=yranges["Tpot"][0],
                vmax=yranges["Tpot"][1],
                label="O$_3$ vs. H$_2$O",
            )

            cbaxes = fig_corrO3H2O.add_axes([0.92, 0.12, 0.03, 0.8])
            cb = plt.colorbar(p0, cax=cbaxes)
            cb.set_label("Tpot[K]", rotation=90, fontsize=FSIZE_AXIS)

            ax1.set_xlabel("gasphase H$_2$O[ppm]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_ylabel("O$_3$[ppb]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_xscale("log")
            ax1.set_xlim(yranges["H2O_gas"])
            ax1.set_ylim(yranges["O3"])
            ax1.legend(loc=0)

            ax1.grid(axis="y", which="major")
            ax1.grid(axis="x", which="both")
            plt.show(block=False)

            if plt2pdf:
                pp.savefig(fig_corrO3H2O)
            if pltcls:
                plt.close()

        ######################################################### CO2 - CH4 correlation
        if all(k in data_extr for k in ["PIC"]):
            f_ip = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Tpot"],
                kind="linear",
                bounds_error=False,
            )
            z = f_ip(data_extr["PIC"]["TimeCRef"])

            fig_corrCO2CH4, ax1 = plt.subplots(figsize=(16, 10))
            plt.subplots_adjust(top=0.95, bottom=0.075, left=0.06, right=0.9)

            plt.title(title + " (CH$_4$-CO$_2$)", fontsize=FSIZE_HEADING, weight="bold")

            p0 = ax1.scatter(
                data_extr["PIC"]["CO2"],
                data_extr["PIC"]["CH4"],
                c=z,
                cmap=cm,
                vmin=yranges["Tpot"][0],
                vmax=yranges["Tpot"][1],
                label="CO$_2$ vs. CH$_4$",
            )

            cbaxes = fig_corrCO2CH4.add_axes([0.92, 0.12, 0.03, 0.8])
            cb = plt.colorbar(p0, cax=cbaxes)
            cb.set_label("Tpot[K]", rotation=90, fontsize=FSIZE_AXIS)

            ax1.set_xlabel("CO$_2$[ppm]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_ylabel("CH$_4$[ppb]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_xlim(yranges["PIC_CO2"])
            ax1.set_ylim(yranges["PIC_CH4"])
            ax1.legend(loc=0)
            ax1.grid(axis="y", which="major")
            ax1.grid(axis="x", which="both")
            plt.show(block=False)

            if plt2pdf:
                pp.savefig(fig_corrCO2CH4)
            if pltcls:
                plt.close()

        ######################################################### CH4 - CO correlation
        if all(k in data_extr for k in ["PIC", "CO"]):
            f_ip = interp1d(
                data_extr["CO"]["TimeCRef"],
                data_extr["CO"]["CO"],
                kind="linear",
                bounds_error=False,
            )
            vCO_ip = f_ip(data_extr["PIC"]["TimeCRef"])
            f_ip = interp1d(
                data_extr["MA"]["TimeCRef"],
                data_extr["MA"]["Tpot"],
                kind="linear",
                bounds_error=False,
            )
            z = f_ip(data_extr["PIC"]["TimeCRef"])

            fig_corrACECO, ax1 = plt.subplots(figsize=(16, 10))
            plt.subplots_adjust(top=0.95, bottom=0.075, left=0.06, right=0.9)

            plt.title(title + " (CH$_4$-CO)", fontsize=FSIZE_HEADING, weight="bold")

            p0 = ax1.scatter(
                vCO_ip,
                data_extr["PIC"]["CH4"],
                c=z,
                cmap=cm,
                vmin=yranges["Tpot"][0],
                vmax=yranges["Tpot"][1],
                label="CH$_4$ vs. CO",
            )

            cbaxes = fig_corrACECO.add_axes([0.92, 0.12, 0.03, 0.8])
            cb = plt.colorbar(p0, cax=cbaxes)
            cb.set_label("Tpot[K]", rotation=90, fontsize=FSIZE_AXIS)

            ax1.set_xlabel("CO[ppb]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_ylabel("CH$_4$[ppt]", color="k", fontsize=FSIZE_AXIS)
            ax1.set_xlim(yranges["CO"])
            ax1.set_ylim(yranges["PIC_CH4"])
            ax1.legend(loc=0)
            ax1.grid()
            plt.show(block=False)

            if plt2pdf:
                pp.savefig(fig_corrACECO)
            if pltcls:
                plt.close()

    ###############################################################################
    if plt2pdf and any(plot_categories):
        pdf_info["Title"] = "CARIBIC overview plot compilation"
        pdf_info["Author"] = "F.Obersteiner / Flights_DM_plots.py"
        pdf_info["Subject"] = title
        pdf_info["ModDate"] = datetime.now(timezone.utc)
        pp.close()
