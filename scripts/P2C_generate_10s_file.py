# -*- coding: utf-8 -*-
from pathlib import Path

import numpy as np
from pyfuppes.avgbinmap import bin_t_10s, bin_y_of_t
from pyfuppes.na1001 import FFI1001 as na1001

# ------------------------------------------------------------------------------

src = Path("/home/va6504/Data/Caribic_2/CORE_P2c/updated")
dst = src  # Path('/home/va6504/Data/Caribic_2CORE_P2c/test_10s')

files = list(src.glob("P2C*.txt"))

for file in files:
    if "_10s_" in file.name:
        print("skipped loading 10s file", file)
        continue

    FileName = file.name[:-7] + "10s" + file.name[-8:]

    # load original file
    na_1001 = na1001(file)

    # replace file name
    for ix, line in enumerate(na_1001.SCOM):
        if line.startswith("FileName"):
            na_1001.SCOM[ix] = "FileName: " + FileName

    ncom_add = [
        "data binned to 10s intervals (arithmetic mean), 'VMISS' excluded.",
        (
            "time-stamp is in UTC seconds since midnight and denotes the middle "
            "of the 10s interval (lower boundary included, upper boundary excluded)."
        ),
        "# ",
    ]

    na_1001.NCOM = ncom_add + na_1001.NCOM

    # calculate bins
    bin_info = bin_t_10s(np.array(na_1001.X, dtype=float), drop_empty=False)

    # update time vector
    na_1001.X = ["%d" % v for v in bin_info["t_binned"]]

    # calculate bin means for dependent variables
    binned_data = []
    vmiss_all = list(map(float, na_1001.VMISS))
    for i, arr in enumerate(na_1001.V):
        vmiss = vmiss_all[i]
        binned_data.append(
            bin_y_of_t(
                np.array(arr, dtype=float),
                bin_info,
                vmiss=vmiss,
                return_type="arit_mean",
            )
        )

    formatted_V = []
    for i, parm in enumerate(binned_data):
        formatted_V.append([f"{int(vmiss_all[i]):d}" if np.isnan(v) else f"{v:.2f}" for v in parm])

    na_1001.V = formatted_V

    # write the 10s file:
    io = na_1001.to_file(dst / FileName, sep=" ", sep_com=";", sep_data="\t", overwrite=True)
    if io:
        print("+++ wrote file " + FileName + " +++")
