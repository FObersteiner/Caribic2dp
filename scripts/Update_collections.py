# -*- coding: utf-8 -*-
import zipfile
from datetime import datetime
from pathlib import Path

# ------------------------------------------------------------------------------


def create_collection(file_prefix, src, dst, caribic=2):
    """
    create a zip file with a certain data type (file_prefix),
    containing data from all flights.
    """
    pattern = "---------------------"
    if caribic == 2:
        pattern = f"{file_prefix}_*_*_*_*_V*.txt"
    if caribic == 1:
        pattern = f"{file_prefix}_*_*_*_*_*.txt"

    flightdirs = list(src.glob("Flight*_*/"))
    files = []
    for d in flightdirs:
        f = list(d.glob(pattern))
        # if we have more than one file, check if they have equal string len,
        # i.e. same file (not 10s) but different version. Use higher version if so.
        if len(f) > 1 and len(set(map(len, str(f)))) == 1:
            files.append(sorted(f)[-1])
        else:
            files += f

    zipf_out = dst / f"{file_prefix}_files_collection_{datetime.utcnow().strftime('%Y%m%d')}.zip"

    print(f"preparing zipfile {zipf_out.name}...")

    with zipfile.ZipFile(zipf_out, "w") as zf:
        for f in files:
            print(f"adding {f.name}...")
            zf.write(f, f.name, compress_type=zipfile.ZIP_DEFLATED)

    print(f"done with {zipf_out.name}.")


# ------------------------------------------------------------------------------

if __name__ == "__main__":
    # ! user-specific paths >>>

    # basepath = Path("//IMK-ASF-CARFS/Caribic/extern/")
    basepath = Path().home() / "Carfs1/Caribic/extern/"

    caribic = {
        1: {
            "src": basepath / "CARIBIC1data/Flights",
            "dst": basepath / "CARIBIC1data/Archives/collections/nasa_ames",
        },
        2: {
            "src": basepath / "CARIBIC2data/Flights",
            "dst": basepath / "CARIBIC2data/Archives/collections/nasa_ames",
        },
    }

    # <<< ! user-specific paths

    for file_prefix in ("SU", "MS"):
        for c in 1, 2:
            print(f"--- CARIBIC-{c} {file_prefix} ---")
            create_collection(file_prefix, caribic[c]["src"], caribic[c]["dst"], caribic=c)
