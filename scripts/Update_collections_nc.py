# -*- coding: utf-8 -*-
import zipfile
from datetime import datetime, UTC
from pathlib import Path

# ------------------------------------------------------------------------------


def create_collection(file_prefix, src, dst, caribic=2):
    """
    create a zip file with a certain data type (file_prefix), containing data from all flights.
    """
    pattern = "---------------------"
    if caribic == 2:
        pattern = f"{file_prefix}_*_*_*_*_V*.nc"
    if caribic == 1:
        pattern = f"{file_prefix}_*_*_*_*_*.nc"

    files = sorted(src.glob(pattern))
    # print(files)
    if not files:
        raise ValueError(f"no files matching pattern {pattern} in {src}")

    zipf_out = dst / f"{file_prefix}_files_collection_{datetime.now(UTC).strftime('%Y%m%d')}.zip"

    print(f"preparing zipfile {zipf_out.name}...")

    with zipfile.ZipFile(zipf_out, "w") as zf:
        for f in files:
            print(f"adding {f.name}...")
            zf.write(f, f.name, compress_type=zipfile.ZIP_DEFLATED)

    print(f"done with {zipf_out.name}.")


# ------------------------------------------------------------------------------

if __name__ == "__main__":
    # ! user-specific paths >>>

    # basepath = Path("//IMK-ASF-CARFS/Caribic/extern/")
    basepath = Path().home() / "Carfs1/Caribic/extern/"

    caribic = {
        1: {
            "src": basepath / "CARIBIC1data/netcdf",
            "dst": basepath / "CARIBIC1data/Archives/collections/netcdf",
        },
        2: {
            "src": basepath / "CARIBIC2data/netcdf",
            "dst": basepath / "CARIBIC2data/Archives/collections/netcdf",
        },
    }

    # <<< ! user-specific paths

    for file_prefix in ("MS",):  # "WSM"):
        for c in (1,):  # 2:
            print(f"--- CARIBIC-{c} {file_prefix} ---")
            create_collection(file_prefix, caribic[c]["src"], caribic[c]["dst"], caribic=c)
