# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 17:25:41 2018

@author: Florian Obersteiner, f.obersteiner@kit.edu

Info:
    more functions to handle CARIBIC data, i.e. CARIBIC-specific file IO
    difference to "general_functions": the functions here MODIFY data, i.e.
    copy, move etc.
"""
import os
import shutil
from fnmatch import fnmatch as fnm
from pathlib import Path

from caribic2dp import carfileutils


###############################################################################

OVERWRITE_IF_V_EQUAL = False  # default is False. Use with caution !

# ! user-specific paths >>>
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
flights_dir = Path().home() / "Carfs1/Caribic/extern/Caribic2data/Flights"
archive_dir = Path().home() / "Carfs1/Caribic/extern/Caribic2data/Archives/nasaames"

fl_range = (294, 591)


#    C2 (LiCor CO2) files
# newdata_dir = Path().home() / "Data/Caribic_2/misc/2022-06-24_C2-Data-to-WMOX2019/converted"

#    C4I files - data that goes to the IAGOS server
# newdata_dir = Path().home() / "Data/Caribic_2/IAGOS_DB/output"

#    CO files
# newdata_dir = Path().home() / "Data/Caribic_2/CO/headerupdate/header_updated"

#    GHG files
# newdata_dir = Path().home() / "Data/Caribic_2/INT_GHG_HCF/GHG/Reprocessing/output"

#    H2O files (WA WB)
# newdata_dir = Path().home() / "Data/Caribic_2/WA_WB/updated"

#    HCF files
# newdata_dir = Path().home() / "Data/Caribic_2/INT_GHG_HCF/HCF/to_server"

#    INT files
newdata_dir = Path().home() / "Data/Caribic_2/INT_GHG_HCF/INT/output"

#    MA files
# newdata_dir = Path().home() / "Data/Caribic_2/MA/F588-591/processed"

#    MS files
# newdata_dir = Path().home() / "Data/Caribic_2/MS/output"

#    NO files
# newdata_dir = Path().home() / "Data/Caribic_2/NOy/to_server"

#    OM files
# newdata_dir = Path().home() / "Data/Caribic_2/O3/pCuv_NL_reproc/reprocess/output"

#    P2C files
# newdata_dir = Path().home() / "Data/Caribic_2/CORE_P2c/updated"

#    PIC files
# newdata_dir = Path().home() / "Data/Caribic_2/PIC/to_server"

#    PTR files
# newdata_dir = Path().home() / "Data/Caribic_2/PTR/to_server"

#    SD files
# newdata_dir = Path().home() / "Data/Caribic_2/SD_size_distribution/updated"

#    SP2 files
# newdata_dir = Path().home() / "Data/Caribic_2/SP2/to_server"

#    SU files
# newdata_dir = Path().home() / "Data/Caribic_2/SU/output"

#    WAS files
# newdata_dir = Path().home() / "Data/Caribic_2/WAS/to_server"

#    WA/WB
# newdata_dir = Path().home() / "Data/Caribic_2/WA_WB/updated"


### prefix / suffix / search patterns definition
prefix = [
    "AC",
    "AN",
    "C2",
    "C4I",
    "CM",
    "CN",
    "CO",
    "GHG",
    "HG",
    "HCF",
    "INT",
    "MA",
    "MS",
    "NO",
    "OM",
    "P2C",
    "PIC",
    "PTR",
    "SD",
    "SDA",
    "SP2",
    "SU",
    "WA",
    "WAS",
    "WB",
]

suffix = [".txt", ".na", ".ames"]
patterns = [p + "_*" + s for s in suffix for p in prefix]

allfiles = sorted(os.listdir(newdata_dir))
if not allfiles:
    raise FileNotFoundError(f"no files found in {newdata_dir}")

files_select = {
    "full_path": [],
    "prefix": [],
    "is_10s": [],
    "version": [],
    "flight_no": [],
}

# find all appropriate files in the directory that contains new data
for ix, file in enumerate(allfiles):
    for p in patterns:
        if fnm(file, p):
            fl_no = carfileutils.dissect_filename(file).flight_no
            if fl_no < fl_range[0] or fl_no > fl_range[1]:
                continue  # skip loop if flight no out of selected flight range

            files_select["full_path"].append(newdata_dir / file)
            files_select["prefix"].append(p[0 : p.find("_")])
            files_select["is_10s"].append("10s" in str(file))
            files_select["version"].append(
                carfileutils.dissect_filename(newdata_dir / allfiles[ix]).version
            )
            files_select["flight_no"].append(carfileutils.dissect_filename(file).flight_no)

# for each selected file, find the respective directory on the Caribic server
for ix, file in enumerate(files_select["full_path"]):
    fl_dir = carfileutils.flight_no_to_flightdir(files_select["flight_no"][ix], flights_dir)
    allfiles_flight = os.listdir(fl_dir)

    # check if file already exists on server
    if os.path.basename(file) in allfiles_flight and not OVERWRITE_IF_V_EQUAL:
        print("(!) aborted: " + os.path.basename(file) + " - file already exists on server.")
        continue  # skip to next file

    # check if preceeding version exists on the server
    existing = carfileutils.find_files(
        files_select["flight_no"][ix],
        files_select["prefix"][ix],
        flights_dir,
        file_dir=fl_dir,
        resolution="10s" if files_select["is_10s"][ix] else "highest",
        raise_filenotfound=False,
    )

    # file does not exist; check if version of file to upload is 1
    if not existing:
        if files_select["version"][ix] != 1 and not OVERWRITE_IF_V_EQUAL:
            print(
                "(!) aborted: "
                + os.path.basename(file)
                + " - file version is not 1 and file does not exist on server."
            )
            continue  # skip to next file
        shutil.copy(files_select["full_path"][ix], fl_dir / os.path.basename(file))
        print("copied: " + os.path.basename(file) + " to " + str(fl_dir))

    # preceeding version does exist, check if version of file to upload is 1+v_preceeding
    else:
        assert len(existing) == 1, "must not have more than one file"
        existing = existing[0]
        if files_select["version"][ix] - 1 != carfileutils.dissect_filename(existing.name).version:
            if not OVERWRITE_IF_V_EQUAL:
                print(
                    f" (!) aborted: {os.path.basename(file)}"
                    f" - file version of upload ({files_select['version'][ix]}) must be greater (+1) than "
                    f"the version of the file on the server ({carfileutils.dissect_filename(existing.name).version})."
                )
                continue  # skip to next file
            else:  # OVERWRITE
                subfolder = files_select["prefix"][ix]
                shutil.move(existing, archive_dir / subfolder / existing.name)
                print(f"moved {existing.name} to archive directory.")
                shutil.copy(files_select["full_path"][ix], fl_dir / os.path.basename(file))
                print("forced write: " + os.path.basename(file) + " to " + str(fl_dir))
        else:
            subfolder = files_select["prefix"][ix]
            shutil.move(existing, archive_dir / subfolder / existing.name)
            print(f"moved {existing.name} to archive directory.")
            shutil.copy(files_select["full_path"][ix], fl_dir / file.name)
            print(f"copied: {file.name} to {str(fl_dir)}")
