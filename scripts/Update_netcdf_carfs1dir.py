# -*- coding: utf-8 -*-
import os
import shutil
from pathlib import Path

from pyfuppes.misc import find_youngest_file

# ------------------------------------------------------------------------------

# config
# ! user-specific paths >>>
DIR_LOC = Path().home() / "Data/Caribic_2/netcdf/"
DIR_FS = Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/netcdf/"
DIR_FS_ARC = Path().home() / "Carfs1/Caribic/extern/CARIBIC2data/Archives/netcdf/"
# "//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/"
# <<< ! user-specific paths
PAT = "*.nc"


# -----------------------------------------------------------------------------
# 1)
# check local directory. if there are multiple versions of one file,
# keep only the newest.
FILES_LOC = list(DIR_LOC.glob(PAT))
FILES_NOVERS = [f.name[:-5] for f in FILES_LOC]
UNQ = sorted(set(FILES_NOVERS))
print("checking local dir...")
if len(UNQ) != len(FILES_NOVERS):
    for f_unq in UNQ:
        count = FILES_NOVERS.count(f_unq)
        if count > 1:
            print(
                "found %i occurances of %s*.nc" % (count, FILES_NOVERS[FILES_NOVERS.index(f_unq)])
            )
            newest = find_youngest_file(DIR_LOC, f_unq + "*", n=1)
            allfiles = list(DIR_LOC.glob(f_unq + "*"))
            for f in allfiles:
                if f not in newest:
                    print(f"deleting {f.name}...")
                    os.remove(f)


# -----------------------------------------------------------------------------
# 2)
# update netcdf dir on server.
# using local directory content as reference,
#   - check if file exists in fs dir
#       - if YES:
#           if timestamps are equal:
#               no action
#           if local file is newer:
#               move file from fs dir to fs archive dir
#               copy local file to fs
#           if local file ts is older or equal:
#               no action
#       - else:
#           - simply copy file
FILES_LOC = list(DIR_LOC.glob(PAT))  # update if something was deleted (duplicates)
FILES_FS = list(DIR_FS.glob(PAT))
FILES_NOVERS = [f.name[:-5] for f in FILES_FS]
FILES_LOC_NAMES = [f.name for f in FILES_LOC]
FILES_FS_NAMES = [f.name for f in FILES_FS]
n = len(FILES_LOC)
for ix, f_loc in enumerate(FILES_LOC):
    pre = str("(%i/%i) " % (ix + 1, n))
    if f_loc.name[:-5] in FILES_NOVERS:  # ignore the file version here
        try:
            sel_file = FILES_FS[FILES_FS_NAMES.index(f_loc.name)]
        except ValueError:  # not in list
            dt = -1
            sel_file = f_loc
        else:
            dt = os.path.getmtime(sel_file) - os.path.getmtime(f_loc)

        if dt >= 0.0:
            print(pre + "local file older or equal, skipping:", sel_file.name)
        else:
            print(pre + "local file newer, updating:", f_loc.name)
            shutil.copy(f_loc, DIR_FS / f_loc.name)
    else:
        print(f"{pre} copying {f_loc.name} since not yet on server")
        shutil.copy(f_loc, DIR_FS / f_loc.name)


# -----------------------------------------------------------------------------
# 3)
# check netcdf dir on server. if there are multiple versions of one file,
# keep only the newest and move the other to the archive dir.
FILES_FS = list(DIR_FS.glob(PAT))
FILES_FS_ARC = list(DIR_FS_ARC.glob(PAT))
FILES_NOVERS = [f.name[:-5] for f in FILES_FS]
UNQ = sorted(set(FILES_NOVERS))
print("checking server dir...")
if len(UNQ) != len(FILES_NOVERS):
    for f_unq in UNQ:
        count = FILES_NOVERS.count(f_unq)
        if count > 1:
            print(
                "found %i occurances of %s*.nc" % (count, FILES_NOVERS[FILES_NOVERS.index(f_unq)])
            )
            indices = [i for i, s in enumerate(FILES_NOVERS) if s == f_unq]
            newest = find_youngest_file(DIR_FS, f_unq + "*", n=1)
            allfiles = list(DIR_FS.glob(f_unq + "*"))
            for f in allfiles:
                if f not in newest:
                    print(f"moving {f.name} to archive...")
                    shutil.move(f, DIR_FS_ARC / f.name)

print("all done.")
