# data coverage

the logfiles list all files with data, for all CARIBIC-2 flights. For each parameter in each file, the relative coverage over the flight's duration is calculated.
