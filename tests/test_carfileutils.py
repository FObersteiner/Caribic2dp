# -*- coding: utf-8 -*-
"""Tests for CARIBIC file server tools."""
import datetime
import pathlib
import sys
import unittest

import pytest

from caribic2dp import carfileutils

try:
    # try to use the file path of the current file as working directory.
    # this *should* work if the whole .py file is run.
    wd = pathlib.Path(__file__).parent
except NameError:
    # __file__ is not defined if we run individual lines through an ipython
    # console for instance.
    # then use working directory that is currently ***set***.
    wd = pathlib.Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "faild to obtain working directory"

# assume there is only one non-Windows user
FLIGHTSDIR = pathlib.Path("/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/Flights")
MODELDATADIR = pathlib.Path("/home/va6504/Carfs1/Caribic/extern/CARIBIC2data/data_model")
if sys.platform == "win32":
    FLIGHTSDIR = pathlib.Path("//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/Flights/")
    MODELDATADIR = pathlib.Path("//IMK-ASF-CARFS/Caribic/extern/CARIBIC2data/data_model")


class CarfileutilsTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_dissect_flightdirname(self):
        # test invalid strings
        for invalid in (
            "",
            "0",
            "asdf",
            "a_b",
            "a_bbbbbbbbbbbbbb_b",
            "Flight11_200050520",  # flight no too short
            "Flight11111_200520",  # flight no too long
            "Flight111_202211223",  # date too long
        ):
            with self.assertRaises(AssertionError):
                carfileutils.dissect_flightdirname(invalid)
        for invalid in (
            "Flightxxx_20221122",  # invalid flight number
            "Flight111_20221199",  # invalid date
        ):
            with self.assertRaises(ValueError):
                carfileutils.dissect_flightdirname(invalid)
        # test valid strings
        for have, want in zip(
            ("Flight111_20221111", "Flight1111_20221111"),
            (
                (111, datetime.datetime(2022, 11, 11, tzinfo=datetime.timezone.utc)),
                (1111, datetime.datetime(2022, 11, 11, tzinfo=datetime.timezone.utc)),
            ),
        ):
            self.assertEqual(want, carfileutils.dissect_flightdirname(have))

    def test_dissect_filename(self):
        # invalid inputs
        ## assertions
        for invalid in (
            "this string has no filename extension",
            "too_few_parts_d_e.txt",
            "too_many_parts_d_e_f_g_h.txt",
            "AN_20060220_138_FRA_GRU_10s_V02.csv",  # csv is not allowed
            "OMG_20060220_138_FRA_GRU_10s_V02.txt",  # OMG is not a valid prefix
        ):
            with self.assertRaises(AssertionError):
                carfileutils.dissect_filename(invalid)
        ## value errors (conversion not possible)
        for invalid in (
            "AN_x0060220_138_FRA_GRU_10s_V02.txt",
            "AN_20060220_1x8_FRA_GRU_10s_V02.txt",
            # "AN_20060220_138_FRA_GRU_10s_V02.txt",
            # "AN_20060220_138_FRA_GRU_10s_V02.txt",
        ):
            with self.assertRaises(ValueError):
                carfileutils.dissect_filename(invalid)

        # valid inputs
        for have, want in zip(
            (
                "AN_20060220_138_FRA_GRU_10s_V02.txt",
                "HCF_20180322_544_MUC_DEN_V05.txt",
                "INTtpc_20180322_544_MUC_DEN_V42.txt",
            ),
            (
                (
                    "AN",
                    datetime.datetime(2006, 2, 20, tzinfo=datetime.timezone.utc),
                    138,
                    "FRA",
                    "GRU",
                    "10s",
                    2,
                ),
                (
                    "HCF",
                    datetime.datetime(2018, 3, 22, tzinfo=datetime.timezone.utc),
                    544,
                    "MUC",
                    "DEN",
                    "full",
                    5,
                ),
                (
                    "INTtpc",
                    datetime.datetime(2018, 3, 22, tzinfo=datetime.timezone.utc),
                    544,
                    "MUC",
                    "DEN",
                    "full",
                    42,
                ),
            ),
        ):
            self.assertEqual(want, carfileutils.dissect_filename(have))

    @pytest.mark.need_carfs
    def test_getallflightdirs(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        dirs = carfileutils.get_all_flightdirs(FLIGHTSDIR)
        # flight directories start with prefix "Flight"
        self.assertTrue(dirs[0].name.startswith("Flight"))
        dir_data = [carfileutils.dissect_flightdirname(d) for d in dirs]
        # CARIBIC-2 flight numbers start with 110
        self.assertEqual(110, dir_data[0].flight_no)
        # flight numbers must be increasing strictly monotonic
        self.assertEqual(len(dirs), (dir_data[-1].flight_no - dir_data[0].flight_no) + 1)
        for i, v in enumerate(dir_data[:-1]):
            self.assertEqual(v.flight_no, dir_data[i + 1].flight_no - 1)

    def test_increment_version(self):
        # valid inputs
        for have, want in {
            "AN_20060220_138_FRA_GRU_10s_V02.txt": "AN_20060220_138_FRA_GRU_10s_V03.txt",
            "HCF_20180322_544_MUC_DEN_V05.txt": "HCF_20180322_544_MUC_DEN_V06.txt",
            # Path input is OK, too - but return type is still string.
            pathlib.Path(
                "/some/directory/AN_20060220_138_FRA_GRU_10s_V02.txt"
            ): "AN_20060220_138_FRA_GRU_10s_V03.txt",
            pathlib.Path(
                "/another/dir/HCF_20180322_544_MUC_DEN_V05.txt"
            ): "HCF_20180322_544_MUC_DEN_V06.txt",
        }.items():
            self.assertEqual(want, carfileutils.increment_version(have))

        have, want = (
            "AN_20060220_138_FRA_GRU_10s_V02.txt",
            "AN_20060220_138_FRA_GRU_10s_V04.txt",
        )
        self.assertEqual(want, carfileutils.increment_version(have, increment_by=2))
        have, want = (
            "AN_20060220_138_FRA_GRU_10s_V02.txt",
            "AN_20060220_138_FRA_GRU_10s_V99.txt",
        )
        self.assertEqual(want, carfileutils.increment_version(have, specify_versnum=99))
        # invalid
        with self.assertRaises(ValueError):
            carfileutils.increment_version("AN_20060220_138_FRA_GRU_10s_V99.txt")
            carfileutils.increment_version("AN_20060220_138_FRA_GRU_10s_V02.txt", increment_by=-3)
            carfileutils.increment_version("AN_20060220_138_FRA_GRU_10s_V02.txt", increment_by=100)
            carfileutils.increment_version(
                "AN_20060220_138_FRA_GRU_10s_V02.txt", specify_versnum=122
            )

    @pytest.mark.need_carfs
    def test_fno_to_fdir(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        # valid
        d = carfileutils.flight_no_to_flightdir(591, FLIGHTSDIR)
        self.assertTrue(d.name.startswith("Flight591"))
        # invalid
        with self.assertRaises(FileNotFoundError):
            carfileutils.flight_no_to_flightdir(0, FLIGHTSDIR)
            carfileutils.flight_no_to_flightdir(-3, FLIGHTSDIR)
            carfileutils.flight_no_to_flightdir(99999, FLIGHTSDIR)

    @pytest.mark.need_carfs
    def test_fno_to_moddir(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        # valid
        d = carfileutils.flight_no_to_modeldir(591, MODELDATADIR)
        self.assertTrue(d.name.endswith("_591"))
        # invalid
        with self.assertRaises(FileNotFoundError):
            carfileutils.flight_no_to_modeldir(0, MODELDATADIR)
            carfileutils.flight_no_to_modeldir(-3, MODELDATADIR)
            carfileutils.flight_no_to_modeldir(99999, MODELDATADIR)

    @pytest.mark.need_carfs
    def test_find_files(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        # valid
        self.assertEqual(
            "AN_20060220_138_FRA_GRU_10s_V02.txt",
            carfileutils.find_files(138, "AN", "", wd / "testdata", resolution="10s")[0].name,
        )
        self.assertEqual(
            "AN_20060220_138_FRA_GRU_V02.txt",
            carfileutils.find_files(138, "AN", "", wd / "testdata", resolution="highest")[0].name,
        )
        self.assertEqual(
            "MA_20020316_93.txt",
            carfileutils.find_files(93, "MA", "", wd / "testdata", caribic=1, resolution="highest")[
                0
            ].name,
        )
        self.assertEqual(
            "MA_20020316_093_VRA_DUS_10sMV_v02.txt",
            carfileutils.find_files(93, "MA", "", wd / "testdata", caribic=1, resolution="10s")[
                0
            ].name,
        )
        self.assertListEqual(
            sorted(["MA_20020316_93.txt", "MA_20020316_093_VRA_DUS_10sMV_v02.txt"]),
            sorted(
                f.name
                for f in carfileutils.find_files(
                    93, "MA", "", wd / "testdata", resolution="all", caribic=1
                )
            ),
        )
        # no data
        # errors are suppressed by default and None must be returned if no data found:
        self.assertListEqual([], carfileutils.find_files(0, "MA", FLIGHTSDIR))
        self.assertListEqual([], carfileutils.find_files(591, "xyz", FLIGHTSDIR))
        # errors must be raised if suppression is deactivated:
        with self.assertRaises(FileNotFoundError):
            carfileutils.find_files(0, "MA", FLIGHTSDIR, raise_filenotfound=True)
            carfileutils.find_files(591, "xyz", FLIGHTSDIR, raise_filenotfound=True)

    @pytest.mark.need_carfs
    def test_find_fno_range(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        # invalid
        with self.assertRaises(ValueError):
            # lower limit must be <= upper limit
            carfileutils.find_flights_range(130, 129, FLIGHTSDIR)
        # valid
        # one flight must give one result
        self.assertEqual(1, len(carfileutils.find_flights_range(591, 591, FLIGHTSDIR)))
        # n flights must give n results
        self.assertEqual(3, len(carfileutils.find_flights_range(589, 591, FLIGHTSDIR)))
        # all available flights
        self.assertEqual(
            len(carfileutils.get_all_flightdirs(FLIGHTSDIR)),
            len(carfileutils.find_flights_range(0, 9999, FLIGHTSDIR)),
        )
        self.assertListEqual([], carfileutils.find_flights_range(9999, 10000, FLIGHTSDIR))

    @pytest.mark.need_carfs
    def test_find_fno_daterange(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        # invalid
        with self.assertRaises(ValueError):
            # lower limit must be <= upper limit
            carfileutils.find_flights_daterange(
                datetime.datetime(2022, 11, 21),
                datetime.datetime(2021, 11, 21),
                FLIGHTSDIR,
            )
        # valid
        self.assertEqual(
            len(carfileutils.get_all_flightdirs(FLIGHTSDIR)),
            len(
                carfileutils.find_flights_daterange(
                    datetime.datetime(1900, 1, 1), datetime.datetime.now(), FLIGHTSDIR
                )
            ),
        )
        # no flights in 2021
        self.assertListEqual(
            [],
            carfileutils.find_flights_daterange(
                datetime.datetime(2021, 1, 1),
                datetime.datetime(2021, 12, 31),
                FLIGHTSDIR,
            ),
        )

    @pytest.mark.need_carfs
    @pytest.mark.slow
    def test_gather_data(self):
        assert FLIGHTSDIR.is_dir(), f"failed to get flight data directory (have: '{FLIGHTSDIR}')"
        assert MODELDATADIR.is_dir(), f"failed to get flight data directory (have: '{MODELDATADIR}'"
        # not providing any range gives all flights
        self.assertEqual(
            len(carfileutils.gather_data(["SU"], FLIGHTSDIR)),
            len(carfileutils.gather_data(["SU"], FLIGHTSDIR, flightrange=(0, 9999))),
        )

        # each flight has two MA files, 1s and 10s
        self.assertEqual(
            2,
            len(
                carfileutils.gather_data(
                    ["MA"], FLIGHTSDIR, flightrange=(591, 591), resolution="all"
                )
            ),
        )
        # two MA files, one SU file, one MS file
        self.assertEqual(
            (591 - 536) * 4,
            len(
                carfileutils.gather_data(
                    ["MA", "MS", "SU"],
                    FLIGHTSDIR,
                    flightrange=(537, 591),
                    resolution="all",
                )
            ),
        )
        # there is no flight 0
        self.assertEqual(0, len(carfileutils.gather_data(["MA"], FLIGHTSDIR, flightrange=(0, 0))))
        # no flights in 2021
        self.assertEqual(
            0,
            len(
                carfileutils.gather_data(
                    ["MA"],
                    FLIGHTSDIR,
                    daterange=(
                        datetime.datetime(2021, 1, 1),
                        datetime.datetime(2021, 12, 31),
                    ),
                )
            ),
        )


if __name__ == "__main__":
    unittest.main()
