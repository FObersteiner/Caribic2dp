# -*- coding: utf-8 -*-
"""Tests for CARIBIC NASA Ames checker."""
import pathlib
import unittest

import pytest

from caribic2dp import check_caribic_na

try:
    # try to use the file path of the current file as working directory.
    # this *should* work if the whole .py file is run.
    wd = pathlib.Path(__file__).parent
except NameError:
    # __file__ is not defined if we run individual lines through an ipython
    # console for instance.
    # then use working directory that is currently ***set***.
    wd = pathlib.Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "faild to obtain working directory"

ap_file = wd / ("../config/CARIBIC2_Airports.csv")


class CheckerTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_hdrdef(self):
        """header defaults"""
        defaults = check_caribic_na.Header_Defaults
        for k in "MNAME", "XNAME", "NCOM_CFinfo", "NCOM_final_words":
            self.assertIn(k, defaults.keys())

    def test_validate(self):
        """Validate NASA Ames FFI 1001."""
        # cases
        # (filepath, expect_fail?)
        cases = sorted(
            (f, "invalid" in f.name) for f in (wd / "testdata" / "validate_na").glob("*.txt")
        )
        self.assertNotEqual([], cases)
        for path, expect_fail in cases:
            # print(f"check {path.name}, expect fail: {expect_fail}")
            result = check_caribic_na.validate(path, check_caribic2=False)
            if expect_fail:
                self.assertFalse(result.msg.startswith(f"check {path.name}: OK"))
            else:
                self.assertIsNotNone(result.na1001)

    @pytest.mark.need_cfunits
    def test_validate_caribic(self):
        """Validate CARIBIC constraints."""
        # cases
        # (filepath, expect_fail?)
        cases = (
            (f, "invalid" in f.name)
            for f in (wd / "testdata" / "validate_caribic_na").glob("*.txt")
        )
        cases = sorted(cases, key=lambda x: x[0].lstat().st_mtime)
        self.assertNotEqual([], cases)
        for path, expect_fail in cases:
            # print(f"check {path.name}, expect error: {expect_fail}")
            result = check_caribic_na.validate(
                path, check_caribic2=True, airports_file=ap_file, cf_checks=True
            )
            # print(result.msg, end="\n\n")
            if expect_fail:
                self.assertFalse(result.msg.startswith(f"check {path.name}: OK"))
            else:
                self.assertIsNotNone(result.na1001)
                # print(result.msg, end="\n\n")
                self.assertTrue(result.msg.startswith(f"check {path.name}: OK"))


if __name__ == "__main__":
    unittest.main()
