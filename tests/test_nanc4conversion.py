# -*- coding: utf-8 -*-
"""Tests for CARIBIC NASA Ames / nc4 conversion."""
import pathlib
import unittest
from datetime import date

import numpy as np
import pandas as pd
import xarray as xr
from pyfuppes.na1001 import FFI1001

from caribic2dp import convert_caribic_na_nc4

try:
    # try to use the file path of the current file as working directory.
    # this *should* work if the whole .py file is run.
    wd = pathlib.Path(__file__).parent
except NameError:
    # __file__ is not defined if we run individual lines through an ipython
    # console for instance.
    # then use working directory that is currently ***set***.
    wd = pathlib.Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "faild to obtain working directory"

params_file = sorted(
    f
    for f in pathlib.Path(wd / "../config/").resolve().glob("parms_units*.csv")
    if "WSM" not in f.name
)[-1]


class ConvertersTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_convert_na_nc4_valid(self):
        # exclude invalid files
        srcs = [
            f
            for f in (wd / "testdata" / "na_nc_convert" / "na_input").glob("*.txt")
            if "invalid" not in f.name
        ]

        # all valid na files must convert without error
        for f in srcs:
            dst = wd / "testdata" / "na_nc_convert" / "nc_output" / f.name.replace(".txt", ".nc")
            ds = convert_caribic_na_nc4.na_to_nc4(
                f,
                dst,
                params_file,
                _warnings_enabled=False,
            )
            self.assertTrue(dst.is_file())

            # compare netCDF variables to those specified in header_lines
            # vs. specified output variables
            na = FFI1001(f)

            xname = na.XNAME.split(";")[0]
            self.assertTrue(ds["time"].attrs["name"] == xname)

            vnames = [n.split(";")[0] for n in na.VNAME]
            nc_vars = dict(ds.variables)
            # nc_vars.pop("na_file_header")
            # nc_vars.pop("reference_time") # removed in format version 0.3
            nc_vars.pop("time")

            self.assertEqual(len(nc_vars), len(vnames))  # -1 because one is file header
            # # format version 0.3 adds 3 coordinates that appear as additional variables:
            # self.assertTrue(4 > len(nc_vars) - len(vnames) >= 0)

            # compare date
            self.assertEqual(pd.to_datetime(ds["time"].data[0]).date(), date(*na.DATE))

            # compare keys
            self.assertEqual(
                set(nc_vars.keys()),
                set(vnames),
                msg="parameters in VNAMES must be found in nc variables",
            )

            # compare data
            na_data = na.to_dict_nparray()
            na_data.pop(convert_caribic_na_nc4.CARIBIC_IVAR)
            for p in na_data:
                na_var_fin = na_data[p][np.isfinite(na_data[p])]
                nc_var_fin = ds[p].data[np.isfinite(ds[p].data)]
                self.assertTrue(na_var_fin.shape == nc_var_fin.shape)
                m = np.isclose(na_var_fin, nc_var_fin)
                self.assertTrue(m.all(), msg=f"invalid comparison for parameter {p}")

            dst.unlink()

        cases = sorted(
            f
            for f in (wd / "testdata" / "na_nc_convert" / "reference").glob("*.nc")
            if "invalid" not in f.name
        )

        # check converted nc; required global attributes
        required = [
            "title",
            "date_created",
            "summary",
            "keywords",
            "conventions",
            "mission",
            "data_description",
            "data_institute",
            "data_owners",
            "license",
            "format_date",
            "format_version",
            "history",
        ]
        for path in cases:
            ds = xr.open_dataset(path)
            for attr in required:
                # key exists
                self.assertTrue(attr.lower() in ds.attrs, f"{attr.lower()} not found in ds.attrs")
                # key has content
                if isinstance(ds.attrs[attr], str):
                    self.assertTrue(len(ds.attrs[attr]) > 0)

    def test_convert_na_nc4_invalid(self):
        # only invalid files
        srcs_ivd = [
            f
            for f in (wd / "testdata" / "na_nc_convert" / "na_input").glob("*.txt")
            if "invalid" in f.name
        ]
        # exclude invalid files
        srcs_vd = [
            f
            for f in (wd / "testdata" / "na_nc_convert" / "na_input").glob("*.txt")
            if "invalid" not in f.name
        ]

        # all invalid na files must fail
        with self.assertRaises(Exception):
            # invalid input:
            for f in srcs_ivd:
                convert_caribic_na_nc4.na_to_nc4(
                    f,
                    wd / "testdata" / "na_nc_convert" / "nc_output" / f.name.replace(".txt", ".nc"),
                    params_file,
                    _warnings_enabled=False,
                )
            # valid input but invalid parameters
            for f in srcs_vd:
                convert_caribic_na_nc4.na_to_nc4(
                    f,
                    pathlib.Path(),
                    params_file,
                    params="a list of non-existing parameters",
                    _warnings_enabled=False,
                )  # invalid output params

    def test_convert_nc4_na(self):
        # round-trip: Nasa ames converted to netcdf, converted to nasa ames must
        # give the same nasa ames

        # 1) make nc
        src = wd / "testdata" / "na_nc_convert" / "na_input" / "MA_20190210_562_MUC_LAX_10s_V02.txt"
        dst_nc = (
            wd / "testdata" / "na_nc_convert" / "nc_output" / "MA_20190210_562_MUC_LAX_10s_V02.nc"
        )
        _ = convert_caribic_na_nc4.na_to_nc4(
            src,
            dst_nc,
            params_file,
            # all params:
            # params="CRefTime UTC_ARINC FlightPhase PosLat PosLong TrueHead WindSpeed WindDirTr PitchAng RollAng StdAltitu BaroAltit TrAirSpeed ToAirTmp AltitRate StcAirTmp AngOAttck TotPres VertSpeed GndSpeed LocalTime pstatic Tpot Altitude",
            _warnings_enabled=False,
        )
        self.assertTrue(dst_nc.is_file())

        # 2) nc to na
        dst_na = (
            wd / "testdata" / "na_nc_convert" / "na_output" / "MA_20190210_562_MUC_LAX_10s_V02.txt"
        )
        na_out = convert_caribic_na_nc4.nc4_to_na(dst_nc, dst_na)
        na_in = FFI1001(src)

        self.assertEqual(na_in.XNAME, na_out.XNAME)
        self.assertListEqual(na_in.VNAME, na_out.VNAME)

        self.assertEqual(len(na_in.X), len(na_out.X))

        ok = na_out.to_file(dst_na, overwrite=1)
        self.assertGreater(ok, 0)
        self.assertTrue(dst_na.is_file())

        with open(src, "r", encoding="utf-8") as a, open(dst_na, "r", encoding="utf-8") as b:
            want = a.read().splitlines()
            have = b.read().splitlines()

        self.assertEqual(len(have), len(want))

        # check header equal
        # count = 0
        for idx, (line_want, line_have) in enumerate(zip(want, have)):
            # ignore trailing spaces
            line_want, line_have = line_want.strip(" "), line_have.strip(" ")
            self.assertEqual(line_have, line_want)
            # if line_have != line_want:
            #     count += 1
            #     print(f"\nline {idx+1} -> want:\n{line_want}\nhave:\n{line_have}")
            #     if count > 10:
            #         print("more potential mismatches skipped")
            #         break
            if idx >= na_out.NLHEAD - 1:
                break

        dst_nc.unlink()
        dst_na.unlink()


if __name__ == "__main__":
    unittest.main()
