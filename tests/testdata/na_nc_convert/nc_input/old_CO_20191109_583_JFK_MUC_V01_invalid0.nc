CDF  U�   
      time       header_lines   (   line_length   ;      
   conventions       CF-1.6     title         IAGOS-CARIBIC netCDF data file     format_date       
2019-02-22     format_version        >L��   history       Sconverted from NASA Ames format at KIT ("convert_AMES2netCDF" by A. Rauthe-Schoech)    creation_date         22.11.2022 09:07:49    data_owners       PA. Zahn; H. Boenisch; T. Gehrlein; F. Obersteiner; contact: andreas.zahn@kit.edu   data_institute        �Institute for Meteorology and Climate Research (IMK), Karlsruhe Institute of Technology (KIT), 76021 Karlsruhe, P.O. Box 3640, Germany     data_description      zCO data measured with Aerolaser instrument during CARIBIC flights onboard the aircraft Airbus A340-600 D-AIHE of Lufthansa     mission       >IAGOS-CARIBIC (CARIBIC-2), http://www.caribic-atmospheric.com/           Header                    	long_name         @Header lines from NASA Ames file CO_20191109_583_JFK_MUC_V01.txt     18  x   time                units         !seconds since 2019-11-08 23:00:00      calendar      standard   standard_name         time   standard_units        s      scale         ?�     	long_name         >Elapsed_time_in_UTC_seconds_from_0_hours_on_the_day_09.11.2019          7�   CO                  
_FillValue        It#�   missing_value         It#�   units         ppbV   standard_name         'mole_fraction_of_carbon_monoxide_in_air    standard_units        1e-9   scale         ?�     	long_name         (CO; Carbon monoxide mixing ratio; [ppbV]   C_format      %.2f            7�40 1001                                                                                                                                                                                                                                                                                                                    Dieter Scharfe (d.scharffe@mpic.de); Torsten Gehrlein (torsten.gehrlein@kit.edu)                                                                                                                                                                                                                                           Max-Planck-Institut fuer Chemie, Chemie der Atmosphaere, Hahn-Meitner-Weg 1, 55128 Mainz, Germany (www.mpic.de); Karlsruher Institute of Technologies, Hermann-von-Helmholtz-Platz 1, 76344 Eggenstein-Leopoldshafen, Germany (www.kit.edu)                                                                                CO data measured with Aerolaser instrument during CARIBIC flights onboard the aircraft Airbus A340-600 D-AIHE of Lufthansa                                                                                                                                                                                                 IAGOS-CARIBIC (CARIBIC-2), http://www.caribic-atmospheric.com/                                                                                                                                                                                                                                                             1 1                                                                                                                                                                                                                                                                                                                        2019 11 09 2020 02 18                                                                                                                                                                                                                                                                                                      0                                                                                                                                                                                                                                                                                                                          TimeCRef; CARIBIC_reference_time_since_0_hours_UTC_on_first_date_in_line_7; [s]                                                                                                                                                                                                                                            1                                                                                                                                                                                                                                                                                                                          1                                                                                                                                                                                                                                                                                                                          999999                                                                                                                                                                                                                                                                                                                     CO; Carbon monoxide mixing ratio; [ppbV]                                                                                                                                                                                                                                                                                   8                                                                                                                                                                                                                                                                                                                          FileFormatVersionInfo: CARIBIC_NAmes_v02 standard # This file was created according to the conventions for CARIBIC NASA Ames data files, given by 'Info_Nasa_Ames_Format_02.zip'.                                                                                                                                          FlightNo: 583                                                                                                                                                                                                                                                                                                              FlightRoute: JFK MUC                                                                                                                                                                                                                                                                                                       DepartureAirport: New York # USA, 10.1 masl, 1012 mbar                                                                                                                                                                                                                                                                     ArrivalAirport: Munich # Germany, 453.2 masl, 960 mbar                                                                                                                                                                                                                                                                     FileName: CO_20191109_583_JFK_MUC_V01.txt                                                                                                                                                                                                                                                                                  ExceptionToConsider: no                                                                                                                                                                                                                                                                                                    ChangeLog: 0                                                                                                                                                                                                                                                                                                               17                                                                                                                                                                                                                                                                                                                         Date-Time start: 2019-11-09 23:54:56.670                                                                                                                                                                                                                                                                                   Date-Time end: 2019-11-10 06:23:56.670                                                                                                                                                                                                                                                                                     CO time resolution = 1Hz                                                                                                                                                                                                                                                                                                   Calibration cycles = every 15 min                                                                                                                                                                                                                                                                                          Measurement time-stamp is synchronized to CARIBIC master PC time.                                                                                                                                                                                                                                                          time offset = -5.670 s                                                                                                                                                                                                                                                                                                     time drift = 11.000 s                                                                                                                                                                                                                                                                                                      Measurement time-stamp is corrected for sample travel time from inlet to detector as t90 = 5.0 s                                                                                                                                                                                                                           #                                                                                                                                                                                                                                                                                                                          The following two lines contain standard names and units according to the 'NetCDF Climate and Forecast (CF) Metadata Convention', see http://cfconventions.org/. Each entry (name or unit) corresponds to a certain column; the entry before the first tab belongs to the independent column.                              CF_StandardNames: time mole_fraction_of_carbon_monoxide_in_air                                                                                                                                                                                                                                                             CF_Units: [s] [1e-9]                                                                                                                                                                                                                                                                                                       The following four lines contain: Start date like in line 7 (formatted as YYYYMMDD), followed by the multiplicators like in line 11; Line number containing the KeyLabel 'ExceptionToConsider' (0 if no such exception is mentioned), followed by the NaN-values like in line 12; Units like in lines 13ff; Column headers.20191109 1                                                                                                                                                                                                                                                                                                                 0 999999                                                                                                                                                                                                                                                                                                                   [s] [ppbV]                                                                                                                                                                                                                                                                                                                 TimeCRef CO                                                                                                                                                                                                                                                                                                                G� B�W
G��B�� G� B��G��B�G� B���G��B�G� B�=qG��B�=qG� B��G��B�8RG�  B�G� �B��G�! B���G�!�B���G�" B�
=G�"�B�p�G�# B�33G�#�B�#�G�$ B�� G�$�B��HG�% B�ffG�%�B�\)G�& B��G�&�B�u�G�' B���G�'�B�8RG�( B��qG�(�B�B�G�) B�ǮG�)�B�{G�* B�
=G�*�B�G�+ B���G�+�B�ǮG�, B�B�G�,�B�k�G�- B�aHG�-�B�u�G�. B���G�.�B�
=G�/ B��G�/�B�
=G�0 B��\G�0�B���G�1 B��G�1�B�k�G�2 B�  G�2�B�ǮG�3 B�z�G�3�B�W
G�4 B�(�G�4�B���G�5 B�G�5�B~�
G�6 B�8RG�6�B~
=G�7 B|
=G�7�B��3G�8 B�(�G�8�Bx=qG�9 BxG�9�B}33G�: B|�\G�:�B{ffG�; B�p�G�;�B�
=G�< B|z�G�<�By=qG�= B��fG�=�B}
=G�> By=qG�>�B~�
G�? By�G�?�B}\)G�@ B~�
G�@�B�{G�A B�aHG�A�B~
=G�B Bw�
G�B�ByQ�G�C BqG�C�B{=qG�D B�\G�D�B��G�E B{�HG�E�B~��G�F B�{G�F�B��G�G B�8RG�G�B��qG�H B���G�H�B��G�I B�ǮG�I�B�G�J B�G�J�B��HG�K B�(�G�K�B���G�L B��3G�L�B��{G�M B��G�M�B�  G�N B�{G�N�B�B�G�O B��)G�O�B�.G�P B���G�P�B��HG�Q B�\)G�Q�B�G�R B�\)G�R�B�z�G�S B�\G�S�B�G�T B�G�T�B��G�U B��RG�U�B��G�V B��HG�V�B���G�W B�8RG�W�B��
G�X B���G�X�B��G�Y B��G�Y�B��G�Z B�G�Z�B�
=G�[ B�L�G�[�B��RG�\ B���G�\�B��3G�] B�ǮG�]�B�k�G�^ B��qG�^�B�L�G�_ B���G�_�B���G�` B�ǮG�`�B�=qG�a B��qG�a�B��G�b B�8RG�b�B���G�c B�z�G�c�B���G�d B���G�d�B���G�e B��3G�e�B��G�f B�\)G�f�B�\G�g B��G�g�B��qG�h B�#�G�h�B�L�G�i B��=G�i�Bz�G�j B}ffG�j�B��G�k B��RG�k�B��
G�l B��)G�l�B��G�m B~=qG�m�B�\)G�n B�G�n�B~  G�o B{�
G�o�B|�
G�p B��)G�p�B�
=G�q B�G�q�B��
G�r B�B�G�r�B=qG�s B~G�s�B��G�t B��\G�t�Bw  G�u Bx  G�u�By�HG�v Bz��G�v�B|��G�w B{�G�w�Bj��G�x Bq=qG�x�B{��G�y B�\)G�y�Byz�G�z Bs��G�z�By��G�{ B{G�{�By  G�| Bkz�G�|�BlG�G�} Bk��G�}�Bj��G�~ BmG�~�Bi�
G� Bop�G��Bs33G�� Bl�G���Be�G�� Bgp�G���Bf�
G�� B`G�G���Bc�\G�� Bh\)G���Bl��G�� Bg�
G���Ba=qG�� Ba��G���B_�G�� Bc�\G���Bg�G�� Bc{G���B]G�G�� B^p�G���BeG�G�� B]�G���BY  G�� Ba�
G���Bf
=G�� B_G�G���B\z�G�� B\ffG���B^�RG�� Bd�\G���B_��G�� B_G�G���B`�G�� BaG���B^��G�� B^�G���Bd=qG�� Bd  G���BdffG�� B`�G���BaG�G�� Bc{G���B^z�G�� B\��G���B](�G�� BZ\)G���B^��G�� Bb=qG���B]Q�G�� B\��G���B^��G�� B_G�G���Bb  G�� Bc=qG���B`�G�� Bd��G���BaG�� B_��G���B[  G�� BW�\G���B]{G�� B]Q�G���BY33G�� BY�G���B\Q�G�� B[�G���BZ\)G�� Ba�G���Bg�\G�� B]�G���BW��G�� B[33G���B](�G�� BY�HG���BZp�G�� BZ\)G���BX33G�� BZ\)G���B^(�G�� B\�G���BQ�G�� BN(�G���BQ�G�� BUffG���BX�\G�� BW�\G���BT��G�� BQ��G���BQz�G�� BQ��G���BN�
G�� BQ��G���BS��G�� BSG�G���BY�HG�� BX�HG���BSp�G�� BS�G���BW{G�� BY
=G���BUG�� BSp�G���BRp�G�� BOz�G���BT��G�� BT�
G���BJ��G�� BQQ�G���BQ��G�� BG{G���BM33G�� BTG���BS��G�� BW��G���BV{G�� BN�G���BJffG�� BM�
G���BT�
G�� BT�G���BT�G�� BIz�G���B>�G�� BE��G���BGG�� BJ��G���BQ��G�� BVG���BY��G�� BUG���BK�\G�� BK\)G���BH�\G�� BI=qG���BH�G�� BF�G�BJ=qG�� BK
=G�ÀBL��G�� BO��G�ĀBR��G�� BT��G�ŀBT\)G�� BRQ�G�ƀBL
=G�� BGG�ǀBI=qG�� BN
=G�ȀBL��G�� BK
=G�ɀBN\)G�� BU�G�ʀBQ(�G�� BD��G�ˀBH=qG�� BNG�G�̀BLG�G�� BJffG�̀BAffG�� BGG�΀BN�G�� BSQ�G�πBS(�G�� BNG�ЀBN33G�� BOG�рBXp�G�� B]�G�ҀBZffG�� BXG�G�ӀBYffG�� BY�G�ԀBM�HG�� BO�G�ՀBU��G�� BN�G�րBN�G�� BSQ�G�׀BR�
G�� BN�RG�؀BG��G�� BC  G�ـBF��G�� BMffG�ڀBWp�G�� B[{G�ۀBW�G�� BUQ�G�܀BN
=G�� BK�RG�݀BM
=G�� BI�G�ހBO�G�� BO�G�߀BS=qG�� BO�G���BM(�G�� BT(�G��BWp�G�� BW\)G��BS��G�� BL=qG��BJG�� BR�G��BU(�G�� BS=qG��BT�\G�� BR��G��BR�
G�� BR  G��BP�
G�� BTQ�G��BX��G�� BWG�G��BV��G�� BW�\G��BR\)G�� BN�\G��BJ  G�� BQ\)G��BUffG�� BR\)G��BS�
G�� BT(�G��BS  G�� BM�\G��BG��G�� BG��G���Bh�G�� B_�RG��BT=qG��BU  G�� BP�RG��BH�\G�� BL=qG��BS{G�� BO�RG��BN�RG�� BO�HG���BR�G�� BVQ�G���BS  G�� BUz�G���BWQ�G�� BSG���BU{G�� BS�G���BQ�G�� BQ�HG���BN��G�� BQ�G���BT�G�� BR
=G���BV  G�� BU�G���BS
=G�� BV��G���BS�G�� BU�G���BV(�G�  BV�RG� �B](�G� BMG��BN�G� BU(�G��BY��G� BS33G��BJ��G� BP�HG��BX�RG� BS�G��BQ�HG� BV  G��BPz�G� BO�HG��BUG� BX��G��BVQ�G�	 BMp�G�	�BQ�HG�
 BY��G�
�BY��G� BT�G��BP�\G� BO=qG��BKp�G� BO�\G��BYQ�G� BYffG��BM�
G� BP�\G��BT�G� BJ��G��BP�RG� B]�G��B`ffG� BN�
G��BR33G� BT�G��BS�G� BU33G��BU\)G� BO  G��BN{G� BP{G��BS�\G� BX�RG��BU�G� BP�G��BK��G� BO=qG��BU
=G� BVp�G��BT33G� BP�G��BM33G� BM33G��BW�
G� BXQ�G��BM�G� BM�G��BW�G� BWG��BK�G�  BC�G� �BS�\G�! BZ  G�!�BN�G�" BK�\G�"�BL�\G�# BP=qG�#�BWG�$ BR=qG�$�BM�G�% BM��G�%�BO�G�& BP{G�&�BO�G�' BQ{G�'�BOp�G�( BT�HG�(�BV\)G�) BO�G�)�BP�G�* BN��G�*�BL��G�+ BQ��G�+�BW�G�, B\��G�,�BV�G�- BR�\G�-�BR�RG�. BD�RG�.�BJ=qG�/ BX�G�/�BQ=qG�0 BT�RG�0�BW�G�1 BQp�G�1�BP\)G�2 BP33G�2�BT=qG�3 B\��G�3�BW��G�4 BMffG�4�BP�G�5 BQ��G�5�BV�RG�6 B`��G�6�B\{G�7 BSffG�7�BSz�G�8 BNG�G�8�BK�G�9 BPG�9�BS�G�: BS(�G�:�BX�G�; BX33G�;�BU��G�< BY33G�<�BY
=G�= BW�HG�=�BW\)G�> BNffG�>�BM(�G�? BSz�G�?�BM  G�@ BO��G�@�BV�HG�A BQp�G�A�BKG�B BRG�G�B�BT{G�C BRp�G�C�B\z�G�D B_��G�D�BV�RG�E BQG�G�E�B^{G�F BZ�
G�F�BT�G�G BS(�G�G�BOffG�H BK(�G�H�BIp�G�I BL�
G�I�BR�G�J BVz�G�J�BSG�G�K BL�G�K�BJ�
G�L BN��G�L�BS�G�M BOQ�G�M�BN  G�N BPz�G�N�BT=qG�O BY�HG�O�BS�G�P BSG�P�B[G�G�Q BP��G�Q�BR�
G�R BY�G�R�BU�
G�S BQp�G�S�BM�G�T BQ��G�T�BU(�G�U BO��G�U�BKG�V BK
=G�V�BJ
=G�W BY�\G�W�BY
=G�X B[G�X�BX�\G�Y BMQ�G�Y�BM�
G�Z BN��G�Z�BN��G�[ BX=qG�[�BS�G�\ BI��G�\�BM�G�] BI�\G�]�BC33G�^ BJ
=G�^�BJ\)G�_ BH=qG�_�BM�G�` BM�G�`�BH�\G�a BJ��G�a�BS�G�b BQz�G�b�BS�G�c BSG�G�c�BM�
G�d BW�\G�d�B[33G�e BV�G�e�BO�G�f BFffG�f�BGQ�G�g BP{G�g�BV��G�h BI�HG�h�BL33G�i BF�G�i�BE��G�j BEG�j�BA�RG�k BF��G�k�BPQ�G�l BS�G�l�BK33G�m BJ�HG�m�BJ�\G�n BIffG�n�BF�G�o BC
=G�o�BH{G�p BN�
G�p�BN�
G�q BJ
=G�q�BM
=G�r BO�G�r�BPQ�G�s BK��G�s�BF�G�t BKffG�t�BF�G�u BFG�u�BG{G�v BF�G�v�BM��G�w BI(�G�w�BJ=qG�x BHffG�x�B>  G�y BX{G�y�BX{G�z BK
=G�z�BP�
G�{ BP{G�{�BO�G�| BQ  G�|�BN��G�} BL�HG�}�BJ�\G�~ BH=qG�~�BK=qG� BP��G��BM33G�� BL�RG���BG�G�� BC��G���BI�G�� BM�RG���BO33G�� BIG���BI�G�� BM�G���BL�G�� BQ�G���BSQ�G�� BO�G���BTQ�G�� BT��G���BW�G�� B[  G���BRz�G�� BKffG���BJ�G�� BL�HG���BMffG�� BK{G���BX�G�� B[=qG���BT��G�� BS=qG���BQ\)G�� BT��G���BQ33G�� BTQ�G���BZ{G�� BY
=G���B`�HG�� BfQ�G���B^�\G�� B^�HG���B\�RG�� BV��G���B]  G�� B[��G���BS�G�� BZ��G���BXG�G�� BR�G���BYG�G�� B[�G���B[=qG�� BW�G���B^�\G�� B_ffG���BV��G�� BXG�G���BZp�G�� B]��G���B`��G�� BZG���Bb�G�� Bb�
G���BWz�G�� BS(�G���BR�G�� BU{G���BXG�G�� BT�G���BW��G�� BZ�
G���BWz�G�� BT=qG���BS�G�� BX��G���BW
=G�� BT  G���BT(�G�� BV  G���BU  G�� B]{G���B\{G�� BK�G���BR\)G�� BY��G���BT{G�� BLffG���BHp�G�� BM�G���BSG�� BU�G���BR�G�� BVQ�G���B](�G�� B\33G���BP
=G�� BYz�G���B`�HG�� B^(�G���BV  G�� BS�G���BT�
G�� BWQ�G���Ba=qG�� B`{G���B`ffG�� B`��G���BWQ�G�� BW��G���B`��G�� B_ffG���BZ��G�� B[��G���B[�G�� B_�
G���B]��G�� BY��G���B]p�G�� BaffG���Bd�G�� Ba=qG���BU�G�� BT��G���BZz�G�� BZ�HG���Baz�G�� BY��G���B[�G�� Bg\)G���BVG�G�� B]�G���Bf33G�� BiQ�G���Bg  G�� BgG�G���Bm�G�� Bi(�G�Bd33G�� BiffG�ÀBl��G�� Bh  G�ĀBc�\G�� BfG�ŀBb�HG�� Bb{G�ƀBm��G�� Bp�G�ǀBn�G�� Bi�G�ȀBc=qG�� BaffG�ɀBgp�G�� BiG�ʀBn��G�� Bo�G�ˀBnp�G�� Bj�
G�̀Bb�RG�� Bd�HG�̀Bg�G�� Bi��G�΀Bd�\G�� B]��G�πBZ�HG�� B]
=G�ЀBZz�G�� BW
=G�рB\�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�  It#�G� �It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G�	 It#�G�	�It#�G�
 It#�G�
�It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��BH(�G� B@(�G��B==qG� BG(�G��BGz�G� BE��G��BB�HG� BC�G��BA��G� BF��G��BR�HG� BKG��BE�
G� BG(�G��B@33G� B?33G��BG�\G� BLp�G��BJ33G� BJG��BIp�G� BD��G��BDQ�G� BH�G��BE��G�  BG��G� �BG33G�! BBp�G�!�BA\)G�" BE��G�"�BGp�G�# BF��G�#�BI�G�$ BC{G�$�BEQ�G�% BD��G�%�B<�G�& BLQ�G�&�BH�G�' BDz�G�'�BD�\G�( BA�G�(�BDQ�G�) BC(�G�)�B>33G�* BF
=G�*�BD�\G�+ BE��G�+�BG
=G�, B@Q�G�,�BD\)G�- BI��G�-�BMz�G�. BJz�G�.�BDG�G�/ B@G�/�B:z�G�0 B;
=G�0�B@�RG�1 B>�G�1�BC��G�2 BI(�G�2�BE\)G�3 B?  G�3�B@��G�4 BH�G�4�BF{G�5 BI=qG�5�BH��G�6 BC��G�6�BJffG�7 BN��G�7�B?�\G�8 BA�RG�8�BEp�G�9 BF{G�9�BH��G�: BH�\G�:�BD�G�; BG�RG�;�B@��G�< B=ffG�<�BEG�= BC�G�=�B@z�G�> BC�G�>�BA��G�? B>=qG�?�BI��G�@ BF�\G�@�BAG�G�A BG
=G�A�BGffG�B BG
=G�B�BE  G�C BF��G�C�BC�G�D BF(�G�D�BL{G�E BF=qG�E�BB��G�F BE�RG�F�BB�G�G B>33G�G�BA{G�H BC{G�H�BHG�I BG\)G�I�BNQ�G�J BG�G�J�B7�
G�K B=�G�K�BI\)G�L BD�RG�L�B>G�G�M BB�G�M�BB{G�N BA�G�N�BH33G�O BH�G�O�B=��G�P BCffG�P�B<��G�Q B:��G�Q�BI  G�R BB�HG�R�B6�
G�S B;\)G�S�BFp�G�T BH  G�T�BF�G�U BA{G�U�BB�RG�V BG\)G�V�BD�G�W BBffG�W�BG\)G�X BI��G�X�BA�HG�Y B@�\G�Y�BG{G�Z B>�G�Z�B?(�G�[ BD�
G�[�BB33G�\ B6(�G�\�B4�G�] BBp�G�]�B?�HG�^ B?  G�^�BF  G�_ BB�G�_�B@33G�` BG(�G�`�BL�
G�a BH��G�a�BAp�G�b B?�HG�b�B?Q�G�c BA�HG�c�BG  G�d BIG�G�d�BGz�G�e BE=qG�e�BEz�G�f BC(�G�f�BB�
G�g BA
=G�g�B>�\G�h B=�RG�h�B@�G�i BC(�G�i�B@�G�j B?�HG�j�B=�\G�k BK�G�k�BF��G�l BJG�l�BN�RG�m BG�G�m�BCz�G�n BCffG�n�BH
=G�o BHp�G�o�BE��G�p BH��G�p�BEQ�G�q BG��G�q�BKffG�r BC�\G�r�BC��G�s BH�G�s�BIp�G�t BB(�G�t�BA(�G�u BGp�G�u�BIG�v BK�HG�v�BGG�G�w B@z�G�w�BF�G�x BHp�G�x�BA��G�y BH�G�y�BK�RG�z BHp�G�z�B<�G�{ B7{G�{�B<�G�| BV��G�|�BJ=qG�} BE�G�}�BDp�G�~ B=(�G�~�BG{G� BG=qG��B?z�G�� BC��G���BE��G�� BGG���BI=qG�� BN\)G���BH�\G�� BAG�G���BG�\G�� BH�HG���BE  G�� BB
=G���BD�G�� BDp�G���BI��G�� BK33G���BJ�G�� BHz�G���BE�\G�� BD�G���BB
=G�� BGQ�G���BBG�G�� B>�G���BB�G�� B>��G���B:Q�G�� BNQ�G���BNQ�G�� BAG�G���BC{G�� BAp�G���BC{G�� B@�G���B=�\G�� B@��G���BCffG�� BFQ�G���B?�G�� B?�G���BB�
G�� BG
=G���BQ�G�� BNQ�G���BE
=G�� B?�G���BB{G�� B@=qG���B=p�G�� B@{G���BF�G�� BGG���B?�G�� BA=qG���BM��G�� BN��G���BA�\G�� B>G�G���B>��G�� B@G���BK  G�� BP�G���BN�RG�� BBQ�G���BF�
G�� BG(�G���BCz�G�� BE33G���B?=qG�� B=G���BIQ�G�� BN��G���BK�\G�� BK��G���BEp�G�� B<G���BC33G�� BD33G���B<��G�� B={G���B?�\G�� B>ffG���B?=qG�� BB�G���BE�G�� BEQ�G���B@�HG�� BA�HG���BFG�� BF�
G���BD�G�� BG(�G���BPffG�� BQ{G���BJ�G�� BF=qG���BC��G�� BOz�G���BN{G�� BK�G���BL{G�� BNG���BQ�\G�� BJG�G���BE��G�� BI��G���BI��G�� BJG�G���BJ�
G�� BHG�G���BGG�G�� BL�G���BI
=G�� BD��G���BM{G�� BO=qG���BC�\G�� BE�\G���BP
=G�� BJ�G���BN=qG�� BQ�G���BHG�� BB��G���BH33G�� BO
=G���BJ��G�� BJ��G���BK  G�� BD�G���BD�G�� BI=qG���BG33G�� BH{G�BE�G�� BB�RG�ÀBJ=qG�� BLffG�ĀBQ�G�� BL�RG�ŀB>(�G�� BA��G�ƀBF�G�� BE�G�ǀBH(�G�� BG�G�ȀB@(�G�� BCG�G�ɀBE
=G�� B@z�G�ʀBF�
G�� BGffG�ˀBB��G�� BF��G�̀BGG�� BE�G�̀BE{G�� BBp�G�΀BDG�� BHffG�πBE  G�� B@��G�ЀBC\)G�� BJ\)G�рBK\)G�� BH�HG�ҀB8��G�� BAG�G�ӀBC��G�� B9=qG�ԀB7G�G�� BC  G�ՀBP��G�� BE(�G�րB;z�G�� B>��G�׀B>p�G�� BD{G�؀BE=qG�� B@��G�ـBCQ�G�� BG�RG�ڀBI
=G�� BI�G�ۀBJ�G�� BD�HG�܀BD{G�� BG�HG�݀BG�G�� BEffG�ހBB{G�� BE�\G�߀BD��G�� B@�
G���BC  G�� BD��G��BJ�
G�� BLQ�G��B=p�G�� B3�G��BF
=G�� BE�\G��BI�G�� BLz�G��BO��G�� BN��G��BH�G�� BF\)G��BB�RG�� B?ffG��B9��G�� B=G�G��BG\)G�� BE��G��BC33G�� BKQ�G��BNG�G�� BJ�HG��BG(�G�� BG(�G��BB33G�� BE
=G��BIz�G�� BE�G��BD�
G�� B@�\G���BA33G�� BG{G��BM�G�� BJ�RG��BK\)G�� BK�HG��BG(�G�� BHQ�G��BIffG�� BB\)G���BFQ�G�� BHffG���BI�G�� BG��G���BC�G�� BCG�G���BDG�� BB��G���BI�G�� BM{G���BD�G�� BC(�G���BF(�G�� BH
=G���BG��G�� BG��G���BM�G�� BM�G���BK�G�� BHG�G���BB�G�  BD�G� �BAG�G� BE��G��BH��G� BB(�G��BH�HG� BJ33G��BBz�G� BA�
G��BEQ�G� BD(�G��B?�G� B:33G��BAG� BH�G��BIG�G� BJ(�G��BF��G�	 B<�G�	�B;33G�
 B?�G�
�BB��G� BI\)G��BI�
G� BG�G��BM�\G� BK�G��B@=qG� BC�G��B@(�G� BA��G��BF�G� BE�
G��BB��G� BH{G��BMG�G� B>�
G��BA��G� BFp�G��BB�RG� BI{G��BF  G� B=G��BCG�G� BK�RG��BK�RG� BD33G��BD33G� BKffG��BJQ�G� BI�\G��BH��G� BC
=G��BA�G� BE{G��BJ�\G� BK�RG��BIffG� BH{G��BG{G� BHz�G��BJ33G� BD{G��BA�G�  BG{G� �BA��G�! B@p�G�!�BN�G�" BN(�G�"�BG�\G�# BL�
G�#�BL\)G�$ BCQ�G�$�BD�\G�% BJ33G�%�BH33G�& BE(�G�&�BAp�G�' BD�HG�'�BG�G�( BK33G�(�BR�G�) BR�HG�)�BQG�G�* BNQ�G�*�BR��G�+ BQ��G�+�BJ�
G�, BLz�G�,�BK�G�- BH�G�-�BI\)G�. B?�G�.�B?��G�/ BO33G�/�BPp�G�0 BO��G�0�BL�\G�1 BE
=G�1�BF��G�2 BC33G�2�BIQ�G�3 BUG�3�BJ{G�4 B?�\G�4�BB
=G�5 BJz�G�5�BO��G�6 BM�RG�6�BJ�RG�7 BE�G�7�BI�
G�8 BLQ�G�8�BL�RG�9 BN�RG�9�BK��G�: BM�RG�:�BM�RG�; BL�RG�;�BF�G�< B>(�G�<�BC�G�= BL�HG�=�BM��G�> BI��G�>�BI{G�? BH{G�?�BFz�G�@ BH  G�@�BM\)G�A BUz�G�A�BRffG�B BN��G�B�BO=qG�C BQffG�C�BP=qG�D BM33G�D�BK��G�E BJ��G�E�BEz�G�F BF��G�F�BN�G�G BR=qG�G�BKG�G�H BI��G�H�BQffG�I BKG�I�BJ�G�J BKG�G�J�BIp�G�K BK�G�K�B>��G�L BDQ�G�L�BI33G�M BI�G�M�BI�G�N BS33G�N�BR�\G�O BI\)G�O�BN=qG�P BJG�G�P�BC��G�Q BJ��G�Q�BO=qG�R BLQ�G�R�BE�RG�S BGG�S�BO
=G�T BKG�T�BM�RG�U BP�RG�U�BQ�G�V BJffG�V�BC33G�W BC��G�W�BE��G�X BN�HG�X�BO�\G�Y BN�\G�Y�BK��G�Z BH�G�Z�BQ�G�[ BUffG�[�BWQ�G�\ B[p�G�\�BL=qG�] BN�G�]�BP�
G�^ BVQ�G�^�BLffG�_ BD��G�_�BN��G�` BIQ�G�`�BG{G�a BT��G�a�BSQ�G�b BCp�G�b�BF�G�c BP��G�c�BL�G�d BN�
G�d�BI��G�e BHQ�G�e�BL
=G�f BK\)G�f�BNG�G�g BM�G�g�BQ(�G�h BS��G�h�BV
=G�i BS��G�i�BQz�G�j BN�G�j�BL�G�k BL�G�k�BLp�G�l BL�G�l�BS(�G�m B[��G�m�BIp�G�n BQQ�G�n�BU�G�o BN  G�o�BI��G�p BH\)G�p�BB��G�q BNz�G�q�B\z�G�r BR�RG�r�BS�G�s BO=qG�s�BI�G�t BQz�G�t�BQ��G�u BNQ�G�u�BM��G�v BNz�G�v�BOffG�w BUG�G�w�B[�\G�x B[��G�x�BY{G�y BT�G�y�BUp�G�z BU�G�z�BW�\G�{ BS��G�{�BW�
G�| B^�G�|�Ba  G�} B[33G�}�BQ��G�~ BZ��G�~�B`�G� BM(�G��BO33G�� BP�G���BP��G�� BS\)G���BS��G�� BT  G���BUffG�� BO�G���BN��G�� BO��G���BRG�G�� BXffG���BQp�G�� BP��G���BTffG�� BU�HG���BX�RG�� BS�G���BRG�� BW�RG���BV�\G�� BT�\G���BS�\G�� BP��G���BP�G�� BUQ�G���BQ=qG�� BP=qG���BR{G�� BJz�G���BM��G�� BR{G���BPz�G�� BM�G���BQz�G�� BU33G���BR�\G�� BJ\)G���BG�HG�� BI��G���BQffG�� BV�G���BM�G�� BP�RG���BS�\G�� BH��G���BLp�G�� BS33G���BPz�G�� BS��G���BR�\G�� BL=qG���BM�G�� BS
=G���BP�\G�� BN{G���BI�G�� BJ�G���BN=qG�� BJG�G���BJG�G�� BT33G���BXffG�� BQ
=G���BSG�G�� BTp�G���BRp�G�� BF��G���BF��G�� BTp�G���BY��G�� BZz�G���BR
=G�� BO�HG���BS�G�� BT  G���BT�G�� BG\)G���BFG�� BP
=G���BK=qG�� BL�\G���BS  G�� BL=qG���BC��G�� BR33G���BU  G�� BP33G���BG��G�� BF��G���BL�RG�� BM33G���BTffG�� BTQ�G���BT�RG�� BO�G���BL\)G�� BQ{G���BSQ�G�� BV
=G���BO\)G�� BJ
=G���BK�G�� BX\)G���BS(�G�� BRz�G���BO�G�� BK�G���BRQ�G�� BR(�G���BOQ�G�� BRQ�G���BO�G�� BK�G���BM�G�� BH�RG���BI��G�� BO�G���BL�G�� BPz�G���BR(�G�� BTz�G���BWG�� BL�G���BH�RG�� BM�
G���BSz�G�� BR��G���BN��G�� BN{G���BMz�G�� BN  G���BN(�G�� BQQ�G�BO��G�� BP��G�ÀBV�G�� BK{G�ĀBO��G�� BQ��G�ŀBU�G�� BS��G�ƀBL�G�� BR��G�ǀBSG�G�� BM  G�ȀBPz�G�� BWG�ɀBSp�G�� BSp�G�ʀBT33G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�  It#�G� �It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G�	 It#�G�	�It#�G�
 It#�G�
�It#�G� It#�G��It#�G� It#�G��BN�G� BQQ�G��BRz�G� BS
=G��BN=qG� BL�
G��BQ�G� BH�HG��BG�\G� BO�G��BS=qG� BQ�G��BN�G� BR=qG��BTz�G� BT{G��BT�
G� BVz�G��BX��G� B]��G��BW��G� BQ�G��BR�
G� BY��G��BV(�G� BQ�HG��BX  G� B^G�G��BU�G� BS�G��B[Q�G� BX�G��BT
=G� BY{G��BV
=G� BR�HG��BX  G� BZ(�G��BZ�G�  BS�\G� �BW��G�! B]ffG�!�BW�G�" BV\)G�"�BR(�G�# BQ=qG�#�BV�\G�$ BTz�G�$�BU�HG�% B^(�G�%�B[�G�& BUQ�G�&�BY�\G�' BY\)G�'�BT  G�( BTffG�(�BV{G�) BW�\G�)�BX�\G�* BX(�G�*�BX��G�+ BT��G�+�BT��G�, BM�RG�,�BPG�G�- BW(�G�-�B[=qG�. BV�G�.�BY�\G�/ BX=qG�/�BP
=G�0 BL(�G�0�BS��G�1 B[=qG�1�BZz�G�2 B[�\G�2�BXp�G�3 B[�G�3�B[G�4 BW��G�4�BVp�G�5 BYp�G�5�B_ffG�6 B]Q�G�6�B\G�7 BU�G�7�BW33G�8 BWG�G�8�BM\)G�9 BT(�G�9�BU�\G�: BV=qG�:�BR�
G�; BOG�G�;�BSz�G�< BSffG�<�BR�G�= BO\)G�=�BP�G�> BZQ�G�>�BY�\G�? BZ��G�?�B[�\G�@ BW  G�@�BU  G�A BR�
G�A�BW  G�B BQ�HG�B�BLffG�C BR33G�C�BU�G�D BY(�G�D�BS33G�E BN�HG�E�BXG�G�F B]  G�F�BY{G�G BM=qG�G�BQ��G�H B](�G�H�BV
=G�I BQ��G�I�BX�
G�J BPffG�J�BLG�K BY�G�K�BZ�
G�L BVQ�G�L�BW
=G�M BS�RG�M�BP{G�N BQ=qG�N�BP  G�O BN�G�O�BQ  G�P BY�\G�P�BZG�G�Q BP�G�Q�BPG�G�R BQ�G�R�BQ��G�S BQ��G�S�BM�HG�T BU�G�T�B\
=G�U BZ�\G�U�BZ�RG�V BY�G�V�BW�
G�W BV�G�W�BR�G�X BO�\G�X�BS��G�Y BZ��G�Y�BW�G�Z BS\)G�Z�BUp�G�[ BU�HG�[�BS��G�\ BRQ�G�\�BN�G�] BW�G�]�BZp�G�^ BS��G�^�BWp�G�_ BY33G�_�BX
=G�` BXG�G�`�BW��G�a BV��G�a�BS(�G�b BWz�G�b�BS(�G�c BQ��G�c�BZp�G�d BUz�G�d�BW  G�e B]33G�e�B`�G�f BR�
G�f�BJz�G�g BP�HG�g�BT�G�h BXQ�G�h�Bb�G�i Bf��G�i�B]Q�G�j BS�
G�j�BQ�HG�k BUG�k�BV33G�l BW33G�l�BR
=G�m BL  G�m�BQ��G�n BR�\G�n�BOffG�o BW\)G�o�BU��G�p BPffG�p�BR��G�q BNffG�q�BOffG�r BKG�r�BE�HG�s BPz�G�s�BZ�G�t BU�HG�t�BK��G�u BMG�G�u�BT��G�v BW=qG�v�BV(�G�w BT=qG�w�BU=qG�x BS=qG�x�BN��G�y BL��G�y�BPG�z BT=qG�z�BQp�G�{ BU�G�{�BX  G�| BY{G�|�BZ=qG�} BY�RG�}�BV�G�~ BRG�G�~�BV(�G� Bc��G��Bm��G�� BdG���Ba��G�� Bg{G���Ba�\G�� BYG���BVG�� B_=qG���BXp�G�� BI�G���BQ��G�� B]��G���BZG�� BZ��G���BXG�G�� BS  G���BX�G�� BZ��G���B[��G�� B[�G���BW��G�� BV��G���B[��G�� B^�G���B\��G�� Bb�G���Bbp�G�� BV��G���BW  G�� BZz�G���BZ�RG�� B\G�G���BS��G�� BQ
=G���BXQ�G�� B^\)G���BY�
G�� BR33G���BQG�G�� BO��G���BV\)G�� BV\)G���BT
=G�� BO=qG���BN  G�� BV\)G���BV�HG�� BV
=G���BRffG�� BQffG���BU�HG�� B](�G���B[G�G�� BT�HG���BS  G�� BPffG���BU�RG�� BX��G���BZ
=G�� B\
=G���B[
=G�� BS�G���BRG�� BPG���BOG�G�� BQ{G���BMp�G�� BP�G���BV{G�� BY�RG���BY��G�� BZ�RG���B\33G�� BV�G���BP��G�� BM�\G���BN�G�� BL��G���BJ(�G�� BK�
G���BPQ�G�� BV�G���BR��G�� BT��G���BZ  G�� BZ(�G���BZ��G�� BTG�G���BS��G�� BM�
G���BQ{G�� BSz�G���BS(�G�� B[�
G���BW\)G�� BP�
G���BL33G�� BJ�G���BMG�� BK�G���BOG�� BYG�G���B]G�G�� B^�G���B\�G�� B_�G���Bf�RG�� BY�\G���B\��G�� B[�RG���BY��G�� BZz�G���BZ  G�� BZ��G���BV�G�� BJffG���BGG�� BLQ�G���BN=qG�� BW\)G���B]��G�� B_��G���B`��G�� B]��G���BV
=G�� BPffG���BIG�G�� BL=qG���BT�\G�� BT
=G���BT��G�� B]Q�G���B^��G�� BQ��G�BRffG�� B_�G�ÀBa(�G�� BZ\)G�ĀB]G�G�� B^�G�ŀBZ\)G�� BX
=G�ƀBY�G�� B_33G�ǀB^\)G�� BV(�G�ȀBOG�G�� BP�G�ɀBW�\G�� B]��G�ʀBZ�RG�� BV{G�ˀBW�G�� B]��G�̀B]
=G�� BY{G�̀BW�G�� B\�HG�΀Bb�G�� B\Q�G�πBS�HG�� BN{G�ЀBN��G�� BYG�рB[ffG�� BWG�G�ҀBW�G�� BU�RG�ӀBS
=G�� BNffG�ԀBU�G�� BZ
=G�ՀBT��G�� BY�G�րBZG�� BV�G�׀BR�G�� BUffG�؀BSffG�� BUQ�G�ـBY��G�� BSG�ڀBP�G�� BV  G�ۀBT{G�� BM��G�܀BK�HG�� BRG�G�݀BZ��G�� BT{G�ހBP�HG�� BT  G�߀BQ��G�� BSG���BY�
G�� B[{G��B[{G�� Bd�G��B\  G�� BM  G��BP33G�� BS�HG��BV�G�� BR�\G��BM{G�� BS�RG��BQ=qG�� BQffG��BV��G�� BV�RG��BV�HG�� BU
=G��BXp�G�� BT��G��BP�G�� BH�HG��BH��G�� BMp�G��BN�G�� BPG��BK��G�� BJp�G��BIz�G�� BP33G��BZ33G�� BQ�G���BO��G�� BW{G��BR�G�� BS��G��BQG�G�� BM��G��BR33G�� BOp�G��BLQ�G�� BO�RG���BU�G�� BU�G���BO�RG�� BM�\G���BNQ�G�� BN  G���BVG�� BQz�G���BG�G�� BK  G���BL=qG�� BH33G���BQ=qG�� BU�\G���BK�G�� BM(�G���BM  G�� BS  G���B\G�� BT(�G���BN�G�  BKG�G� �BL�
G� BT��G��BRz�G� BY
=G��BW  G� BL�HG��BQ�G� BNG��BJ
=G� BV�
G��BT�G� BM\)G��BO\)G� BI��G��BL�HG� BQ�G��BGffG�	 BE�G�	�BNz�G�
 BH=qG�
�BDG� BJ�
G��BP��G� BO=qG��BHG� BDp�G��BMz�G� BR�RG��BKG� BH�G��BL{G� BS�G��BQ{G� BDQ�G��BG��G� BMG��BG��G� BNG�G��BN  G� BI33G��BI��G� BJ33G��BRG� BP��G��BLG�G��BK33G� BL�HG��BRG� BLG�G��BH(�G� BLz�G��BGz�G� BE�
G��BHQ�G� BHQ�G��BOp�G� BTp�G��BDp�G� BHz�G��BJ�G� BG�G��BG  G� BIQ�G��BJ(�G�  BJ�G� �BL��G�! BMQ�G�!�BN��G�" BN�
G�"�BFp�G�# BA��G�#�BBz�G�$ BE33G�$�BL(�G�% BI\)G�%�BDffG�& BJ�G�&�BOG�' BO�G�'�BJ�G�( BJp�G�(�BN33G�) BJ33G�)�BI
=G�* BD�RG�*�BC{G�+ BG{G�+�BAp�G�, BC��G�,�BGffG�- BD��G�-�BIz�G�. BFG�.�B@��G�/ B<ffG�/�BB�G�0 BF�G�0�BD�G�1 BBG�G�1�B>��G�2 BC�G�2�BB��G�3 B?��G�3�B=��G�4 BD\)G�4�BI=qG�5 BE�G�5�BH�G�6 BHG�G�6�BNG�7 BM=qG�7�BD��G�8 BDz�G�8�BG
=G�9 BI33G�9�BIG�G�: BH�HG�:�BE��G�; BD��G�;�BG��G�< BJ��G�<�BM�
G�= BPp�G�=�BLG�G�> BJG�G�>�BC{G�? BB  G�?�BI��G�@ BPG�@�BE�G�A BA  G�A�BE(�G�B BB�
G�B�BIz�G�C BP�G�C�BP�G�D BU33G�D�BQ
=G�E BG�G�E�BD\)G�F BD33G�F�BH(�G�G BNffG�G�BM��G�H BF\)G�H�BD��G�I BF�G�I�BFG�G�J BN(�G�J�BL  G�K BK  G�K�BNG�L BK�
G�L�BJ33G�M BG�RG�M�BIG�N BI�G�N�BF�HG�O BH
=G�O�BR=qG�P BZG�G�P�BO{G�Q BIG�G�Q�BK
=G�R BR  G�R�BQG�G�S BP��G�S�BO
=G�T BL\)G�T�BP33G�U BVz�G�U�BS�G�V BL�RG�V�BM�\G�W BP�RG�W�BR��G�X BNffG�X�BK(�G�Y BR�G�Y�BPz�G�Z BJ�\G�Z�BQ\)G�[ BT33G�[�BL��G�\ BH�G�\�BFG�G�] BH�G�]�BM{G�^ BM�G�^�BM  G�_ BS�\G�_�BSffG�` BC�G�`�BG�G�a BR��G�a�B[��G�b BV��G�b�BP�G�c BHp�G�c�BD��G�d BJG�G�d�BI��G�e BF��G�e�BA�G�f BC(�G�f�BF  G�g BH��G�g�BJ�G�h BGQ�G�h�BK�\G�i BN�G�i�BJz�G�j BI��G�j�BN��G�k BJQ�G�k�BI(�G�l BN��G�l�BS��G�m BU�G�m�BQp�G�n BVG�G�n�BK��G�o BE��G�o�BK(�G�p BG�G�p�BG�G�q BF�
G�q�BI�
G�r BM  G�r�BLffG�s BM�G�s�BL�
G�t BK33G�t�BI�G�u BAffG�u�BE�HG�v BP�
G�v�BJ�G�w BEffG�w�BD=qG�x BI�G�x�BJ�HG�y BKp�G�y�BPp�G�z BP�
G�z�BV�RG�{ BQ�G�{�BA  G�| BF�RG�|�BN\)G�} BH{G�}�BE�G�~ BCp�G�~�BE(�G� BK�\G��BK=qG�� BH  G���BD�G�� BI�G���BNz�G�� BHffG���BL=qG�� BJ�
G���BBp�G�� BKG���BQ��G�� BN�\G���BQ�RG�� BU�HG���BM��G�� BN�G���BP(�G�� BSQ�G���BS(�G�� BT=qG���BQ=qG�� BMG���BRG�� BMG�G���BJ��G�� BN�G���BQG�� BQp�G���BM��G�� BM��G���BP�G�� BTffG���BP�G�� BP�
G���BQ�G�� BJz�G���BQ�G�� BX{G���BV=qG�� BXffG���BR�G�� BNz�G���BP�G�� BP�RG���BOz�G�� BR33G���BLQ�G�� BI�G���BN�\G�� BL��G���BN  G�� BSG�G���BLQ�G�� BDG�G���BN�
G�� BM(�G���BH�G�� BOQ�G���BM�
G�� BO�G���BUQ�G�� BN  G���BH�RG�� BK�G���BN\)G�� BH�HG���BH�\G�� BM�G���BNG�� BQ�G���BUz�G�� B^p�G���Ba��G�� B`��G���BVQ�G�� BTG�G���B[�HG�� B[ffG���BU�
G�� BV�G���BO��G�� BL��G���BQ�HG�� BV�G���BW�G�� BP33G���BM{G�� BH=qG���BH=qG�� BN{G���BN(�G�� BK(�G���BR
=G�� BU�RG���BF�G�� BE��G���BU�HG�� BY�G���BS{G�� BM\)G���BJG�G�� BN=qG���BW��G�� BZ�G���BUffG�� BU��G���BS=qG�� BTQ�G���B]�G�� B]��G���BS{G�� BL�G���B\�RG�� BZ�\G���BVG�� BS�G���BU�G�� B[�HG���BUG�G�� BW=qG���BX�G�� BU��G���BSG�� BRG�G���BYG�� BS�HG���BS��G�� BT�G���BNQ�G�� BN{G���BN��G�� BR��G���BV��G�� BT�G�BR�\G�� BX��G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�  It#�G� �It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� It#�G��It#�G� Ba��G��Bi�G� Bk��G��Bo=qG� Bi�G��Bg��G� Bkp�G��Bg��G�	 Bf��G�	�Bh�RG�
 BfffG�
�Bg��G� Bj��G��BgffG� Bb\)G��Ba�G� Bf�
G��Bk=qG� Bm(�G��Bj=qG� Bi  G��Bg�G� BjG��Bm�G� Bl  G��Bk�G� Bi�G��Bl�
G� Bm33G��Bn�G� Bm\)G��Bg��G� Bl�G��Bp�G� Bk��G��Brp�G� Bv  G��Bf�\G� Be��G��Bj�G� Bt\)G��B{  G� Bq�HG��Bkz�G� BfG��Bdp�G� Bc�G��B]=qG� B](�G��Bd��G� BiG��Bl�G� Bk33G��Bo\)G�  Bl��G� �Blp�G�! BnG�G�!�BgffG�" Ba  G�"�B_G�# Bh(�G�#�Bq�HG�$ Biz�G�$�Bf=qG�% BpffG�%�Bn�HG�& Bq��G�&�Br(�G�' Bo��G�'�Bw��G�( B�
=G�(�B|�G�) Bs�
G�)�Bv��G�* Bm  G�*�Bj�G�+ Bq�
G�+�Bm\)G�, Bh��G�,�Bhz�G�- BrG�-�ByQ�G�. Bp�HG�.�Be�G�/ B`�G�/�B`�G�0 Bb\)G�0�B}p�G�1 Bt�RG�1�Bn=qG�2 Bl=qG�2�Bo=qG�3 BmffG�3�Bg��G�4 Bk�G�4�Bj33G�5 Bh�HG�5�Be��G�6 Bd��G�6�Bi�G�7 BsG�7�Bi��G�8 B\�G�8�Bc��G�9 Bf(�G�9�B\�G�: B^33G�:�Bm��G�; Blz�G�;�BfG�< Bj��G�<�Bg�G�= B`�G�=�Bi�
G�> Bk  G�>�Bgp�G�? Bm=qG�?�Bn=qG�@ BiG�@�Bc�G�A B`z�G�A�Bf�RG�B Bh�G�B�Bm�G�C Bm�G�C�Bg=qG�D Bf{G�D�Bk
=G�E Bm33G�E�Bm
=G�F Bl��G�F�Bj��G�G BlQ�G�G�Ba�G�H Bc��G�H�BoffG�I BpffG�I�Bl�G�J Bg�G�J�Bm{G�K Bo=qG�K�Bj\)G�L Ba��G�L�Bb�G�M Bg�G�M�Bd��G�N Bb��G�N�Bd(�G�O Bf��G�O�Bi��G�P BeQ�G�P�Bf�\G�Q Bo33G�Q�Bo��G�R BoG�G�R�Bq\)G�S Br\)G�S�Bo��G�T Bk�
G�T�Bg�
G�U Bg(�G�U�Ba�HG�V Be
=G�V�Bk�G�W Bb�HG�W�Bh
=G�X Bm�G�X�Bm�G�Y Bn�
G�Y�Bk�G�Z Be�\G�Z�BcffG�[ Bj�\G�[�Bp�HG�\ Bu�G�\�Br��G�] Bj=qG�]�BiffG�^ Bk��G�^�Bf{G�_ Bh�\G�_�Bh��G�` B`G�`�BfG�a Bm{G�a�Bk�G�b Bl(�G�b�Bjp�G�c Bj�G�c�Br�G�d BtG�d�Bt�
G�e Bp{G�e�Bi��G�f Bh33G�f�Bg��G�g Bf�HG�g�BkG�G�h Bl33G�h�BiQ�G�i Bi(�G�i�Ba�
G�j Bb�G�j�Bj�
G�k Bk  G�k�Be�G�l Be�G�l�BlQ�G�m Bo�
G�m�Bn  G�n Bf�G�n�Bj  G�o BoG�o�Bp  G�p Bx�RG�p�Bw�
G�q Bo33G�q�Bo33G�r Bp��G�r�BnG�G�s Bop�G�s�Bo33G�t Bp�RG�t�Bu�G�u Bu�G�u�Bo�HG�v Bt�HG�v�Btz�G�w Bj�\G�w�BkG�x BoffG�x�BeG�G�y Bc��G�y�Br�G�z Bl��G�z�B^�\G�{ Bg�G�{�Bp  G�| Bp{G�|�Bl��G�} Bh��G�}�Bl�G�~ Bh��G�~�Bgz�G� Bo
=G��BlG�G�� BfQ�G���Bd�G�� Bf�
G���Bh(�G�� Bb�G���Bc33G�� Bk��G���Bj�G�� Bj�
G���Bh\)G�� Bc�G���Bk�
G�� Bt(�G���BuQ�G�� Bp=qG���Bp(�G�� Bi33G���Bn
=G�� Bt�G���Bj33G�� Bl\)G���Br33G�� BrG���Bm�HG�� Bj�\G���Bj�RG�� Bh  G���Bn�HG�� BlffG���BhffG�� Bs��G���Bt�RG�� Bg��G���Be�G�� Bf��G���Be��G�� BqG���Bw{G�� BoG���Bh��G�� Bip�G���Bjp�G�� Bj��G���Bm��G�� Bp33G���Bsp�G�� Bp��G���Bm�HG�� Bg  G���Bm�RG�� Bn��G���Bf=qG�� Bd�G���Bi  G�� Bm�G���Bs  G�� BmQ�G���Bi�G�� Bs  G���Bl{G�� Bh�G���Bs�
G�� Bx  G���Bt  G�� Bm�HG���Bip�G�� Bh��G���Bj�G�� Br�G���Bp33G�� Bf�G���Br�\G�� Bz\)G���Bq
=G�� Bk(�G���Bn�\G�� Bg{G���B]�RG�� BlffG���Bv(�G�� Bj��G���Bc�HG�� Bd�G���Bq(�G�� Bx{G���Bu{G�� Bx{G���Bt�
G�� Bm��G���Bl�G�� Bl\)G���Bo��G�� Bt��G���Bw�G�� Bu�G���Bup�G�� Bup�G���BkQ�G�� Bh�
G���BnffG�� Bn  G���Bp�G�� Bn�
G���Bk�
G�� Bq�G���Br  G�� Bn  G���Bs(�G�� Bu  G���Bm33G�� Bi�HG���Bs{G�� Bxp�G���Bp�G�� Bk
=G���Bm�G�� Br�RG���Bx��G�� Bt��G���Bq�\G�� BsG�G���BpffG�� Bt
=G���B|\)G�� Bu�RG���Bm�G�� BpffG���Bz�\G�� B�Q�G���B{��G�� Bo(�G���Bkp�G�� Br{G�Bw�G�� By��G�ÀBs�G�� Bn�RG�ĀBvG�� B|p�G�ŀB}G�� B|G�ƀBu�G�� Bq��G�ǀB~\)G�� B|�G�ȀBy��G�� B|��G�ɀBuz�G�� Br=qG�ʀBl�G�� Bg�
G�ˀBs��G�� Bvz�G�̀Bs�G�� Bg�G�̀Bg\)G�� Bo
=G�΀BtG�� BsG�G�πBw�G�� Bz\)G�ЀBo�G�� BlffG�рB{\)G�� Bz33G�ҀBo
=G�� Btz�G�ӀBv
=G�� Bs��G�ԀBv��G�� Bzz�G�ՀBwz�G�� Bt{G�րBvz�G�� Buz�G�׀Bk�G�� Bo��G�؀BvQ�G�� Bu�G�ـBx{G�� B}{G�ڀB�� G�� B��G�ۀB{G�� BxG�G�܀Bu�G�� B{G�G�݀B�L�G�� Bw��G�ހBq��G�� By��G�߀B{��G�� Bz
=G���Bv��G�� Byz�G��B~��G�� Bx��G��Bu  G�� Bx�G��Bz  G�� BvG��By(�G�� Bxz�G��B{ffG�� B{�
G��Bu\)G�� By(�G��B�G�� B{G��B}�G�� B~{G��B|�
G�� B�.G��B��
G�� Bzp�G��B�8RG�� B���G��B|
=G�� Bp�G��B�#�G�� Bs  G��B=qG�� B�ffG��B}�HG�� By�\G���Bz�G�� B��)G��B�=qG�� B|��G��Bv�
G�� B}=qG��B��HG�� B�u�G��B�=qG�� B�.G���B�aHG�� B�B�G���B�L�G�� B{��G���B�G�� B��G���B��G�� B�ffG���B���G�� Bz��G���BzffG�� B��\G���B��G�� B�=qG���B�z�G�� B�  G���B��3G�� B�  G���B�=qG�� B��HG���Bwp�G�  Bu33G� �B{�
G� B���G��Bw�\G� Bu��G��B|\)G� B�.G��B��fG� B}�\G��B{�HG� B|
=G��B��G� B��G��B�ǮG� B�G�G��B}z�G� Bz�G��B~G�	 Bw��G�	�B��G�
 B�
=G�
�B�
=G� B��G��B��{G� B�(�G��B�u�G� B�B�G��B�\G� B|\)G��B��RG� B���G��Bz�G� B��G��B���G� B���G��B�#�G� B�#�G��B�=qG� B��)G��B��qG� Bu(�G��B{�G� B�
=G��B��
G� B���G��B�=qG� B�� G��By\)G� B~G��B��
G� B��G��B�#�G� B��G��B���G� B�L�G��B��G� B��G��B��G� B���G��B�G� B�p�G��B�33G� B��G��B�(�G�  B��G� �B��G�! B��G�!�B�G�" B��G�"�B�.G�# B�(�G�#�B��G�$ By��G�$�B�z�G�% B�#�G�%�B�\G�& B�RG�&�B��RG�' B���G�'�B��fG�( B���G�(�B�G�G�) B�ffG�)�B��G�* B�ffG�*�B�33G�+ B�ffG�+�B�ffG�, B���G�,�B�{G�- B�k�G�-�B��{G�. B��HG�.�B���G�/ B�G�/�B�G�0 B���G�0�B�.G�1 B�z�G�1�B�� G�2 B��
G�2�B�8RG�3 B�.G�3�B�\G�4 B��)G�4�B��)G�5 B�G�5�B�G�6 B�Q�G�6�B���G�7 B��)G�7�B�G�G�8 B��G�8�B��G�9 B�aHG�9�B�(�G�: B���G�:�B��G�; B�\G�;�B��G�< B��=G�<�B��HG�= B�B�G�=�B�#�G�> B�Q�G�>�B��G�? B�G�?�B�\)G�@ B��3G�@�B�=qG�A B��fG�A�B�z�G�B B�\G�B�B�=qG�C B�G�G�C�B�{G�D B�u�G�D�B��HG�E B�  G�E�B�(�G�F B�k�G�F�B��
G�G B�=qG�G�B���G�H B�k�G�H�B��
G�I B�G�I�B�p�G�J B�B�G�J�B�p�G�K B�B�G�K�B��G�L B��G�L�B�G�G�M B�\)G�M�B�G�G�N B�Q�G�N�B��G�O B��)G�O�B��3G�P B�G�G�P�B���G�Q B�u�G�Q�B�aHG�R B�G�G�R�B��G�S B�� G�S�B��HG�T B���G�T�B��=G�U B���G�U�B�8RG�V B�u�G�V�B�{G�W B�G�W�B��RG�X B���G�X�B��fG�Y B���G�Y�B�G�Z B���G�Z�B�#�G�[ B���G�[�B�z�G�\ B�G�G�\�B�{G�] B��)G�]�B��{G�^ B��qG�^�B�u�G�_ B���G�_�B��
G�` B��{G�`�B���G�a B�.G�a�B��G�b B�#�G�b�B�� G�c B��RG�c�B��G�d B�.G�d�B��G�e B�Q�G�e�B��\G�f B�#�G�f�B�ffG�g B�G�g�B��3G�h B��fG�h�B���G�i B�{G�i�B��=G�j B�  G�j�B�\)G�k B�{G�k�B���G�l B�aHG�l�B��RG�m B�8RG�m�B�G�n B���G�n�B�W
G�o B�aHG�o�B��G�p B�L�G�p�B��fG�q B��RG�q�B��\G�r B�ǮG�r�B��\G�s B��G�s�B���G�t B�=qG�t�B�(�G�u B�{G�u�B�33G�v B�k�G�v�B���G�w B�
=G�w�B��G�x B���G�x�B�{G�y B�  G�y�B�G�z B�� G�z�B�\G�{ B�G�{�B��G�| B��G�|�B�G�} B�G�G�}�B�(�G�~ B���G�~�B�p�G� B���G��B��)G�� B���G���B�
=G�� B�Q�G���B��3G�� B��RG���B��=G�� B�aHG���B�8RG�� B�u�G���B�
=G�� B���G���B�aHG�� B�p�G���B�G�� B��
G���B���G�� B��qG���B��G�� B�p�G���B��fG�� B�z�G���B���G�� B��G���B��qG�� B�=qG���B���G�� B�u�G���B���G�� B��G���B��RG�� B��
G���B��G�� B�L�G���B�z�G�� B��
G���B�p�G�� B��G���B�.G�� B��\G���B��RG�� B��\G���B�p�G�� B�ffG���B�z�G�� B��{G���B�k�G�� B���G���B���G�� B�L�G���B��{G�� B��HG���B�.G�� B�G���B�L�G�� B�#�G���B���G�� B�8RG���B�8RG�� B���G���B�z�G�� B�ffG���B�Q�G�� B�ǮG���B��G�� B�
=G���B���G�� B��)G���B��G�� B�Q�G���B�{G�� B�8RG���B�{G�� B�W
G���B�(�G�� B��G���B��RG�� B�aHG���B�.G�� B�G���B�W
G�� B�L�G���B�G�� B��\G���B�G�G�� B��)G���B�ǮG�� B�ǮG���B��G�� B���G���B���G�� B�
=G���B�ǮG�� B��G���B�G�G�� B�u�G���B�u�G�� B�
=G���B���G�� B��=G���B��HG�� B��G���B��RG�� B�p�G���B�\)G�� B�#�G���B���G�� B��qG���B��fG�� B��\G���B�=qG�� B��G���B�  G�� B���G���B���G�� B�� G���B���G�� B�B�G���B�  G�� B��=G���B�  G�� B�.G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� B�W
G���B�� G�� B�=qG���B�k�G�  B��
G� �B�  G� B�k�G��B��HG� B���G��B�� G� B�� G��B��
G� B���G��B��G� B��G��B���G� B��G��B�{G� B��{G��B��
G� B��G��B��G�	 B�� G�	�B�k�G�
 B�L�G�
�B��G� B��G��B�B�G� B��G��B�B�G� B�  G��B�� G� B��\G��B��G� B�� G��B��G� B���G��B�G� B�W
G��B�k�G� B�B�G��B��G� B��
G��B�G� B�G��B�8RG� B��G��B��G� B��HG��B�p�G� B��G��B�ffG� B��
G��B��\G� B�.G��B�G� B��G��B���G� B�G��B�ffG� B���G��B�G� B�G��B�.G� B�\G��B��G� B��G��B��G�  B�.G� �B��)G�! B��RG�!�B�G�" B�G�G�"�B��fG�# B���G�#�B�ǮG�$ B��G�$�B��G�% B���G�%�B�ǮG�& B�Q�G�&�B��)G�' B�G�G�'�B�=qG�( B���G�(�B��)G�) B�z�G�)�B�\)G�* B��qG�*�B��qG�+ B��)G�+�B��G�, B�\G�,�B�z�G�- B�G�-�B��G�. B��G�.�B�G�/ B���G�/�B�p�G�0 B��G�0�B��)G�1 B�33G�1�B��G�2 B�33G�2�B��3G�3 B�ǮG�3�B�33G�4 B�
=G�4�B��)G�5 B��)G�5�B�
=G�6 B��)G�6�B�G�G�7 B�G�G�7�B���G�8 B���G�8�B�33G�9 B�
=G�9�B�k�G�: B��G�:�B��=G�; B�\)G�;�B�ǮG�< B�
=G�<�B�(�G�= B��=G�=�B���G�> B�=qG�>�B�33G�? B�L�G�?�B��G�@ B��G�@�B��G�A B���G�A�B�33G�B B��3G�B�B��{G�C B�G�G�C�B�33G�D B�L�G�D�B�� G�E B�
=G�E�B�(�G�F B���G�F�B��HG�G B��G�G�B�aHG�H B�aHG�H�B�u�G�I B�
=G�I�B�aHG�J B�aHG�J�B��=G�K B���G�K�B�\G�L B���G�L�B�  G�M B���G�M�B�#�G�N B��RG�N�B���G�O B�L�G�O�B���G�P B�aHG�P�B�8RG�Q B�z�G�Q�B���G�R B��RG�R�B�u�G�S B���G�S�B�\G�T B�u�G�T�B���G�U B���G�U�B��\G�V B�.G�V�B��\G�W B�W
G�W�B�L�G�X B�G�X�B��fG�Y B��G�Y�B�#�G�Z B�G�Z�B���G�[ B���G�[�B�z�G�\ B�\)G�\�B��fG�] B�z�G�]�B�p�G�^ B�G�^�B�\G�_ B�Q�G�_�B�z�G�` B�=qG�`�B�ffG�a B�ffG�a�B�Q�G�b B��fG�b�B�#�G�c B���G�c�B�Q�G�d B�G�d�B��qG�e B��fG�e�B�(�G�f B�=qG�f�B�\)G�g B��fG�g�B��fG�h B�(�G�h�B���G�i B�=qG�i�B��)G�j B�u�G�j�B��G�k B�{G�k�B���G�l B��)G�l�B���G�m B�(�G�m�B��=G�n B�� G�n�B���G�o B�Q�G�o�B�� G�p B�� G�p�B�Q�G�q B��
G�q�B�� G�r B�  G�r�B���G�s B�  G�s�B��{G�t B���G�t�B���G�u B�33G�u�B���G�v B�(�G�v�B�
=G�w B�(�G�w�B��G�x B�aHG�x�B��{G�y B��HG�y�B�  G�z B�  G�z�B�aHG�{ B�W
G�{�B�B�G�| B�k�G�|�B��RG�} B�G�}�B��G�~ B��=G�~�B�{G� B�� G��B�L�G�� B�{G���B�{G�� B�aHG���B��G�� B�.G���B�(�G�� B��RG���B�k�G�� B��G���B�.G�� B�B�G���B��G�� B�G���B�k�G�� B�8RG���B��G�� B�W
G���B��G�� B�G���B��G�� B��G���B���G�� B�u�G���B�L�G�� B���G���B�\)G�� B�ffG���B��G�� B��)G���B��G�� B��RG���B���G�� B��G���B��G�� B�#�G���B��G�� B��G���B���G�� B�p�G���B��)G�� B�
=G���B��3G�� B��G���B�p�G�� B���G���B�ǮG�� B�p�G���B��\G�� B��G���B�\)G�� B��)G���B�Q�G�� B�
=G���B��)G�� B�
=G���B�=qG�� B�(�G���B��fG�� B��G���B��G�� B�(�G���B���G�� B��3G���B��3G�� B��3G���B��3G�� B�
=G���B��G�� B�33G���B��fG�� B�
=G���B��=G�� B��HG���B��HG�� B���G���B�L�G�� B�aHG���B��
G�� B��RG���B��G�� B��G���B���G�� B�k�G���B��=G�� B�aHG���B��\G�� B�L�G���B�
=G�� B��G���B�L�G�� B���G���B�aHG�� B��G���B��G�� B���G���B�B�G�� B��G���B��HG�� B�#�G���B�8RG�� B��3G���B�(�G�� B�
=G���B�8RG�� B�L�G���B��HG�� B���G���B���G�� B��G���B�.G�� B�aHG���B��\G�� B���G���B�Q�G�� B���G���B��RG�� B��\G���B�8RG�� B�z�G���B�ffG�� B���G���B��\G�� B���G���B�
=G�� B���G���B���G�� B��
G���B�p�G�� B�W
G���B�.G�� B���G���B�ffG�� B��
G�B���G�� B���G�ÀB�p�G�� B�z�G�ĀB��qG�� B���G�ŀB�ffG�� B��G�ƀB��{G�� B�\G�ǀB��G�� B�\)G�ȀB���G�� B��\G�ɀB��G�� B�ffG�ʀB�\G�� B��)G�ˀB�k�G�� B���G�̀B�{G�� B���G�̀B�Q�G�� B�ffG�΀B�W
G�� B�(�G�πB��fG�� B�z�G�ЀB���G�� B��RG�рB�k�G�� B�{G�ҀB�.G�� B��{G�ӀB�{G�� B�G�ԀB�(�G�� B�z�G�ՀB�ffG�� B��{G�րB�
=G�� B�
=G�׀B�  G�� B��{G�؀B���G�� B��G�ـB��G�� B�W
G�ڀB�{G�� B�L�G�ۀB��
G�� B�W
G�܀B��RG�� B��
G�݀B��G�� B�  G�ހB��G�� B�p�G�߀B���G�� B�.G���B�G�� B��G��B�\)G�� B��\G��B�u�G�� B���G��B���G�� B��RG��B��fG�� B�B�G��B��)G�� B��G��B��G�� B��
G��B���G�� B�.G��B��G�� B�33G��B��G�� B�B�G��B��G�� B�G��B��G�� B�33G��B��G�� B�G��B�B�G�� B��G��B��G�� B�p�G��B�33G�� B�33G���B��G�� B�z�G��B�p�G�� B�G�G��B���G�� B�.G��B��3G�� B�G�G��B�z�G�� B�\)G���B��G�� B���G���B�ǮG�� B�� G���B�G�G�� B�ffG���B��=G�� B���G���B��G�� B�p�G���B��G�� B��)G���B�G�G�� B���G���B�
=G�� B��G���B���G�� B��qG���B�G�G�� B���G���B��3G�  B�33G� �B��)G� B��{G��B���G� B��HG��B�u�G� B��G��B��fG� B��qG��B�=qG� B���G��B��G� B��G��B�
=G� B�B�G��B�u�G� B��3G��B��HG�	 B�\G�	�B��3G�
 B���G�
�B���G� B�{G��B��HG� B�aHG��B���G� B�aHG��B�33G� B��G��B��)G� B��HG��B�aHG� B�L�G��B�
=G� B��qG��B�33G� B�33G��B�
=G� B�aHG��B��G� B���G��B���G� B���G��B�G� B�  G��B�33G� B�k�G��B���G� B���G��B�L�G� B�8RG��B�u�G� B�ffG��B�.G� B�� G��B�8RG� B�8RG��B�8RG� B��HG��B�\G� B���G��B���G� B�aHG��B��qG�  B���G� �B��3G�! B���G�!�B�ffG�" B�ffG�"�B�k�G�# B���G�#�B���G�$ B�k�G�$�B���G�% B�#�G�%�B�z�G�& B��HG�&�B��RG�' B�L�G�'�B���G�( B�aHG�(�B��G�) B�G�)�B�B�G�* B�G�*�B���G�+ B��fG�+�B��fG�, B�\G�,�B�#�G�- B�8RG�-�B��RG�. B�#�G�.�B��\G�/ B���G�/�B�L�G�0 B���G�0�B�(�G�1 B��G�1�B�ǮG�2 B�
=G�2�B�#�G�3 B�8RG�3�B�z�G�4 B��
G�4�B�G�5 B��\G�5�B��fG�6 B�#�G�6�B���G�7 B�L�G�7�B��G�8 B�\)G�8�B�p�G�9 B��G�9�B�� G�: B�.G�:�B���G�; B��RG�;�B�=qG�;�B�#�G�< B�#�G�<�B�� G�= B�  G�=�B�(�G�> B�8RG�>�B�z�G�? B�Q�G�?�B��{G�@ B��fG�@�B�G�G�A B�=qG�A�B�{G�B B�(�G�B�B�� G�C B�k�G�C�B��=G�D B���G�D�B�k�G�E B�=qG�E�B��G�F B���G�F�B�u�G�G B�� G�G�B���G�H B���G�H�B���G�I B��G�I�B���G�J B��G�J�B��fG�K B��G�K�B���G�L B��RG�L�B���G�M B���G�M�B�G�G�N B���G�N�B�G�O B�Q�G�O�B��G�P B���G�P�B�=qG�Q B��3G�Q�B��{G�R B��qG�R�B�aHG�S B�(�G�S�B�p�G�T B�Q�G�T�B�Q�G�U B�� G�U�B�(�G�V B��G�V�B�.G�W B��G�W�B��{G�X B�(�G�X�B�  G�Y B�k�G�Y�B�B�G�Z B�.G�Z�B��HG�[ B���G�[�B�  G�\ B�=qG�\�B��G�] B�� G�]�B�� G�^ B���G�^�B�(�G�_ B�ǮG�_�B�33G�` B�ǮG�`�B�Q�G�a B�� G�a�B���G�b B���G�b�B��{G�c B�B�G�c�B�G�d B�k�G�d�B��HG�e B�ǮG�e�B�G�f B��G�f�B��G�g B�B�G�g�B��
G�h B��{G�h�B�k�G�i B�  G�i�B��G�j B��)G�j�B��=G�k B��G�k�B�� G�l B��G�l�B��G�m B��HG�m�B���G�n B��3G�n�B�ffG�o B�u�G�o�B��{G�p B��)G�p�B��G�q B�G�q�B�\)G�r B��)G�r�B��G�s B�  G�s�B��)G�t B��G�t�B�ǮG�u B�G�G�u�B�ǮG�v B��3G�v�B�z�G�w B�
=G�w�B��HG�x B�ffG�x�B�
=G�y B��)G�y�B�p�G�z B�33G�z�B��qG�{ B�
=G�{�B��
G�| B�p�G�|�B��G�} B�{G�}�B�\)G�~ B�33G�~�B��G� B�z�G��B�u�G�� B�G�G���B�G�G�� B���G���B�  G�� B�#�G���B��G�� B�  G���B�.G�� B�{G���B��RG�� B���G���B��\G�� B�z�G���B��qG�� B��)G���B�B�G�� B��{G���B�8RG�� B��=G���B���G�� B�8RG���B��RG�� B���G���B�8RG�� B�B�G���B��
G�� B�8RG���B��=G�� B��G���B��G�� B�#�G���B��G�� B�z�G���B��HG�� B��RG���B�8RG�� B��G���B�\)G�� B�\G���B�G�G�� B��G���B�\)G�� B��3G���B��G�� B��)G���B�aHG�� B�z�G���B�B�G�� B�8RG���B�#�G�� B��HG���B���G�� B�\G���B��3G�� B��
G���B��\G�� B��G���B�L�G�� B��HG���B�(�G�� B�ǮG���B�p�G�� B�\)G���B�ǮG�� B�
=G���B�z�G�� B��
G���B���G�� B�L�G���B�G�G�� B���G���B�z�G�� B�
=G���B���G�� B�k�G���B�� G�� B�W
G���B��qG�� B�\)G���B�Q�G�� B�{G���B�ffG�� B�k�G���B�aHG�� B��G���B�L�G�� B��G���B���G�� B�B�G���B��{G�� B��qG���B�#�G�� B�  G���B�{G�� B�
=G���B�8RG�� B�ǮG���B��HG�� B�  G���B�aHG�� B���G���B�z�G�� B�L�G���B�W
G�� B�  G���B�(�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G���It#�G�� It#�G���B�33G�� B�8RG���B��RG�� B��RG���B���G�� B�ffG���B�#�G�� B�#�G���B�\G�� B�ffG���B�ffG�� B�Q�G���B��qG�� B��RG���B�aHG�� B���G���B�8RG�� B��fG���B�ffG�  B�\G� �B��=G� B���G��B�  G� B��qG��B���G� B���G��B���G� B���G��B���G� B���G��B���G� B�ffG��B�p�G� B��\G��B��fG� B�G��B�W
G�	 B���G�	�B���G�
 B���G�
�B�aHG� B�z�G��B��\G� B���G��B�  G� B�  G��B�G�G� B��G��B���G� B�W
G��B���G� B��{G��B���G� B�  G��B�=qG� B��)G��B�ǮG� B�W
G��B�ǮG� B���G��B��\G� B��)G��B�ffG� B�u�G��B�
=G� B��3G��B��G� B�  G��B�{G� B�Q�G��B��G� B�\G��B�
=G� B�B�G��B�\G� B�L�G��B��\G� B��\G��B��RG� B�u�G��B�8RG� B��3G��B��qG�  B���G� �B�� G�! B�L�G�!�B�G�G�" B���G�"�B�G�# B��)G�#�B�ǮG�$ B��G�$�B��\G�% B��fG�%�B�z�G�& B��\G�&�B���G�' B���G�'�B�G�( B���G�(�B��qG�) B��G�)�B�8RG�* B�ǮG�*�B��G�+ B���G�+�B�G�G�, B��fG�,�B�ffG�- B�ffG�-�B�\)G�. B�� G�.�B��G�/ B�=qG�/�B�ffG�0 B��=G�0�B�(�G�1 B�ffG�1�B��3G�2 B�#�G�2�B�Q�G�3 B��qG�3�B���G�4 B���G�4�B��RG�5 B�Q�G�5�B��qG�6 B��G�6�B�\G�7 B��HG�7�B��qG�8 B��\G�8�B��\G�9 B�.G�9�B�#�G�: B�� G�:�B��3G�; B��qG�;�B�{G�< B�(�G�<�B�z�G�= B�p�G�=�B�z�G�> B���G�>�B���G�? B��\G�?�B���G�@ B���G�@�B�33G�A B�  G�A�B��qG�B B�z�G�B�B���G�C B�Q�G�C�B�(�G�D B�p�G�D�B�B�G�E B���G�E�B�(�G�F B��=G�F�B��qG�G B��3G�G�B��G�H B��\G�H�B�L�G�I B���G�I�B�z�G�J B��3G�J�B��=G�K B�\G�K�B�
=G�L B�33G�L�B�.G�M B�u�G�M�B���G�N B�L�G�N�B�\G�O B�8RG�O�B�8RG�P B�\G�P�B�(�G�Q B��G�Q�B�{G�R B�z�G�R�B��fG�S B���G�S�B�8RG�T B�z�G�T�B��qG�U B���G�U�B���G�V B��qG�V�B��HG�W B�W
G�W�B���G�X B�
=G�X�B�u�G�Y B�#�G�Y�B���G�Z B��G�Z�B�Q�G�[ B��{G�[�B�L�G�\ B�L�G�\�B�(�G�] B��
G�]�B��\G�^ B�L�G�^�B�Q�G�_ B�G�G�_�B��G�` B�� G�`�B�aHG�a B�\G�a�B���G�b B���G�b�B��HG�c B��G�c�B�Q�G�d B�.G�d�B�\)G�e B�k�G�e�B�{G�f B�
=G�f�B��fG�g B�� G�g�B��HG�h B��G�h�B��=G�i B�{G�i�B�p�G�j B��fG�j�B��fG�k B���G�k�B�G�G�l B�u�G�l�B�G�m B��G�m�B�G�G�n B�Q�G�n�B��
G�o B��G�o�B��G�p B�(�G�p�B��G�q B��RG�q�B�B�G�r B��fG�r�B�{G�s B�{G�s�B���G�t B�ffG�t�B�Q�G�u B�G�G�u�B�
=G�v B��fG�v�B�  G�w B�=qG�w�B�.G�x B��{G�x�B��G�y B���G�y�B��fG�z B��\G�z�B���G�{ B��)G�{�B�  G�| B��G�|�B�G�} B��{G�}�B�=qG�~ B�
=G�~�B�=qG� B�33G��B�=qG�� B�G�G���B��3G�� B�� G���B�{G�� B�G���B�aHG�� B���G���B�B�G�� B��3G���B��=G�� B���G���B�.G�� B��qG���B�W
G�� B��qG���B��qG�� B���G���B�8RG�� B��G���B��
G�� B���G���B�G�� B�W
G���B�G�� B�{G���B�G�� B�k�G���B��{G�� B�L�G���B�33G�� B�\G���B�{G�� B��G���B�8RG�� B�u�G���B�  G�� B�u�G���B���G�� B��G���B��G�� B���G���B��fG�� B���G���B��G�� B��
G���B��G�� B�� G���B�=qG�� B�{G���B��
G�� B�Q�G���B�B�G�� B�8RG���B���G�� B��{G���B��G�� B��{G���B�{G�� B�
=G���B�  G�� B�k�G���B�{G�� B�Q�G���B��
G�� B���G���B���G�� B�{G���B��G�� B�ffG���B��)G�� B�ffG���B��\G�� B��G���B�� G�� B�{G���B�k�G�� B��G���B��qG�� B��fG���B�B�G�� B�ffG���B�G�� B���G���B���G�� B�Q�G���B���G�� B�\)G���B��G�� B��fG���B�  G�� B�ffG���B�8RG�� B���G���B�Q�G�� B��qG���B�z�G�� B�#�G���B�k�G�� B��
G���B���G�� B��HG���B�=qG�� B�z�G���B��HG�� B���G���B�L�G�� B���G���B�=qG�� B�� G���B�  G�� B���G���B�#�G�� B�ffG���B���G�� B�ffG���B�#�G�� B��fG���B��fG�� B�\G���B�G�G�� B���G���B���G�� B�� G���B��{G�� B�k�G���B��qG�� B��G���B�
=G�� B��\G���B�ffG�� B�  G���B���G�� B�G�B��qG�� B��qG�ÀB�G�� B��G�ĀB��
G�� B��
G�ŀB��fG�� B��G�ƀB���G�� B��qG�ǀB�ffG�� B��G�ȀB�z�G�� B��\G�ɀB�ffG�� B��RG�ʀB���G�� B���G�ˀB�=qG�� B�\G�̀B�B�G�� B�=qG�̀B��G�� B��\G�΀B��\G�� B�\G�πB��\G�� B�ffG�ЀB��HG�� B�z�G�рB���G�� B��RG�ҀB���G�� B�Q�G�ӀB�z�G�� B��G�ԀB�Q�G�� B�{G�ՀB�  G�� B�  G�րB���G�� B�=qG�׀B�=qG�� B�Q�G�؀B�(�G�� B��qG�ـB���G�� B�(�G�ڀB�G�G�� B�#�G�ۀB�aHG�� B���G�܀B��3G�� B��G�݀B�{G�� B��\G�ހB�33G�� B��G�߀B�(�G�� B��\G���B���G�� B�k�G��B�  G�� B�  G��B�(�G�� B�Q�G��B�z�G�� B�k�G��B�  G�� B�G��B�Q�G�� B���G��B�  G�� B�B�G��B�G�G�� B��
G��B���G�� B��G��B���G�� B�{G��B�Q�G�� B�W
G��B�u�G�� B��\G��B��qG�� B�{G��B��=G�� B�Q�G��B��qG�� B�k�G��B��G�� B�ffG���B�z�G�� B�=qG��B�\G�� B�G�G��B�\G�� B���G��B�33G�� B�\G��B��fG�� B�(�G���B�� G�� B�k�G���B�(�G�� B�ffG���B���G�� B�.G���B��
G�� B��{G���B��HG�� B��G���B�{G�� B��G���B��{G�� B�u�G���B���G�� B�{G���B�� G�� B���G���B�=qG�� B��G���B�W
G�  B��{G� �B�aHG� B�B�G��B�k�G� B��\G��B��\G� B�  G��B��HG� B�{G��B��\G� B�33G��B��3G� B�  G��B�k�G� B�L�G��B�k�G� B���G��B�ffG�	 B���G�	�B��G�
 B��G�
�B�
=G� B��=G��B��G� B��G��B��\G� B�\)G��B�z�G� B��\G��B���G� B���G��B�p�G� B�W
G��B�aHG� B�� G��B�B�G� B�k�G��B���G� B���G��B�W
G� B��{G��B��G� B�B�G��B�Q�G� B�{G��B��
G� B�(�G��B���G� B�#�G��B�ffG� B��\G��B��=G� B�=qG��B��G� B���G��B��)G� B��G��B���G� B���G��B���G� B�� G��B��RG� B��qG��B��=G�  B�L�G� �B�Q�G�! B�.G�!�B���G�" B�ffG�"�B�=qG�# B���G�#�B�\G�$ B��RG�$�B��G�% B�p�G�%�B�W
G�& B�33G�&�B�(�G�' B��G�'�B��G�( B�(�G�(�B��qG�) B�(�G�)�B��\G�* B�\G�*�B���G�+ B��G�+�B�k�G�, B���G�,�B���G�- B���G�-�B�#�G�. B��fG�.�B��qG�/ B��G�/�B�\G�0 B�Q�G�0�B���G�1 B�z�G�1�B�ǮG�2 B��qG�2�B��\G�3 B�{G�3�B��=G�4 B�L�G�4�B�\G�5 B���G�5�B��\G�6 B�z�G�6�B��{G�7 B�B�G�7�B�u�G�8 B�B�G�8�B�(�G�9 B��{G�9�B�  G�: B�8RG�:�B��qG�; B��{G�;�B�=qG�< B��qG�<�B���G�= B�z�G�=�B��G�> B�  G�>�B�z�G�? B���G�?�B���G�@ B�#�G�@�B��
G�A B�8RG�A�B�(�G�B B�=qG�B�B��
G�C B�z�G�C�B�z�G�D B��qG�D�B�k�G�E B�(�G�E�B���G�F B�u�G�F�B���G�G B�ffG�G�B�W
G�H B�ffG�H�B��3G�I B�{G�I�B�u�G�J B�(�G�J�B�(�G�K B���G�K�B��qG�L B�G�L�B�=qG�M B�z�G�M�B�Q�G�N B�p�G�N�B��G�O B�ffG�O�B�\)G�P B�\G�P�B�z�G�Q B�(�G�Q�B�{G�R B��G�R�B�W
G�S B��G�S�B�ffG�T B��qG�T�B�ffG�U B�ffG�U�B��HG�V B�
=G�V�B�B�G�W B��G�W�B��{G�X B��G�X�B���G�Y B�{G�Y�B��G�Z B�Q�G�Z�B���G�[ B�{G�[�B�k�G�\ B�� G�\�B�  G�] B�Q�G�]�B���G�^ B��{G�^�B�{G�_ B��=G�_�B���G�` B���G�`�B���G�a B�aHG�a�B���G�b B�k�G�b�B�W
G�c B��G�c�B��G�d B�\G�d�B�ffG�e B��G�e�B��G�f B��RG�f�B�(�G�g B�W
G�g�B���G�h B��G�h�B��G�i B���G�i�B�G�j B��3G�j�B��G�k B��G�k�B�u�G�l B���G�l�B��=G�m B�L�G�m�B�G�n B�  G�n�B�k�G�o B�  G�o�B��
G�p B��
G�p�B�ffG�q B��G�q�B��3G�r B��G�r�B�  G�s B�{G�s�B�.G�t B��qG�t�B�  G�u B��G�u�B�\)G�v B�L�G�v�B��qG�w B�ǮG�w�B�  G�x B�B�G�x�B�.G�y B�z�G�y�B�k�G�z B���G�z�B��G�{ B���G�{�B��G�| B���G�|�B��)G�} B��G�}�B�(�G�~ B�
=G�~�B���G� B��RG��B��qG�� B�ǮG���B��qG�� B��\G���B�{G�� B��G���B��G�� B�G�G���B�G�� B�� G���B�\G�� B�z�G���B��{G�� B���G���B�ffG�� B�#�G���B�#�G�� B��RG���B�(�G�� B��G���B���G�� B���G���B��)G�� B��{G���B�B�G�� B���G���B�=qG�� B���G���B�\G�� B���G���B���G�� B���G���B���G�� B�� G���B�W
G�� B���G���B�p�G�� B���G���B���G�� B��G���B��3G�� B���G���B��
G�� B�u�G���B�.G�� B��G���B��qG�� B�(�G���B�W
G�� B��RG���B�G�� B��qG���B�  G�� B�.G���B�ǮG�� B�{G���B��{G�� B�.G���B���G�� B�ǮG���B��fG�� B�{G���B��
G�� B�aHG���B��G�� B��3G���B�G�� B�Q�G���B��)G�� B�\G���B�
=G�� B��G���B�u�G�� B�W
G���B�u�G�� B�aHG���B���G�� B�p�G���B�G�� B��G���B��HG�� B�{G���B�ǮG�� B��G���B�33G�� B�(�G���B��G�� B�(�G���B�Q�G�� B��G���B��{G�� B��qG���B��G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��B��G�� B��G���B�� G�� B�33G��B�B�G�� B��G��B���G�� B�u�G��B�  G�� B�ffG��B���G�� B��3G���B�#�G�� B���G���B�\G�� B��G���B�8RG�� B�p�G���B�B�G�� B�u�G���B��G�� B�G���B��G�� B�aHG���B�8RG�� B��G���B���G�� B��G���B�8RG�� B�z�G���B�G�G�� B���G���B�8RG�  B��G� �B�p�G� B�L�G��B�� G� B��\G��B�G� B��G��B�(�G� B��G��B�G� B�p�G��B�G�G� B��qG��B�ǮG� B���G��B���G� B�\)G��B�G�	 B�.G�	�B�G�
 B��G�
�B�\)G� B�33G��B�  G� B�G��B���G� B�\)G��B��3G� B�ǮG��B�33G� B��=G��B��3G� B���G��B��G� B�aHG��B���G� B��
G��B�p�G� B�u�G��B�u�G� B�u�G��B��RG� B�G��B���G� B�L�G��B�L�G� B�p�G��B�.G� B�(�G��B�#�G� B��fG��B��3G� B��G��B��G� B�\)G��B��fG� B�G�G��B��G� B��)G��B�(�G� B��{G��B��3G� B��)G��B��G�  B��)G� �B��G�! B��3G�!�B�G�G�" B��)G�"�B�W
G�# B�z�G�#�B��=G�$ B���G�$�B��3G�% B���G�%�B�8RG�& B��HG�&�B�#�G�' B��HG�'�B�G�G�( B���G�(�B��G�) B�u�G�)�B�G�G�* B��3G�*�B���G�+ B�u�G�+�B��G�, B�8RG�,�B��{G�- B�.G�-�B���G�. B���G�.�B�  G�/ B�=qG�/�B�#�G�0 B�z�G�0�B�\G�1 B��G�1�B��\G�2 B���G�2�B���G�3 B�ffG�3�B��G�4 B���G�4�B�ffG�5 B���G�5�B��HG�6 B�Q�G�6�B�8RG�7 B�L�G�7�B��G�8 B�aHG�8�B��{G�9 B��3G�9�B�=qG�: B�u�G�:�B�=qG�; B���G�;�B��3G�< B���G�<�B���G�= B�#�G�=�B��3G�> B�p�G�>�B��qG�? B�B�G�?�B�{G�@ B�k�G�@�B��G�A B�p�G�A�B��G�B B�k�G�B�B�(�G�C B��=G�C�B�33G�D B�8RG�D�B��{G�E B�G�G�E�B�G�F B���G�F�B���G�G B���G�G�B�aHG�H B�� G�H�B�
=G�I B�
=G�I�B�
=G�J B�B�G�J�B��G�K B�z�G�K�B�G�L B�u�G�L�B�ǮG�M B�.G�M�B��{G�N B��G�N�B��3G�O B�(�G�O�B�(�G�P B�  G�P�B���G�Q B���G�Q�B��HG�R B��G�R�B��G�S B��G�S�B�u�G�T B��=G�T�B��
G�U B�.G�U�B�k�G�V B���G�V�B��fG�W B��qG�W�B��G�X B�aHG�X�B�\)G�Y B��G�Y�B�B�G�Z B�B�G�Z�B���G�[ B�p�G�[�B���G�\ B�u�G�\�B�Q�G�] B�u�G�]�B�B�G�^ B�33G�^�B�  G�_ B��G�_�B�.G�` B�p�G�`�B���G�a B��HG�a�B��3G�b B���G�b�B���G�c B��qG�c�B���G�d B�(�G�d�B�33G�e B�z�G�e�B��3G�f B���G�f�B��qG�g B�  G�g�B�� G�h B��fG�h�B��G�i B��3G�i�B��G�j B���G�j�B��fG�k B��G�k�B�  G�l B�k�G�l�B���G�m B�\)G�m�B��{G�n B��HG�n�B�{G�o B�G�o�B��{G�p B�W
G�p�B��G�q B�L�G�q�B���G�r B�L�G�r�B�G�s B�.G�s�B�� G�t B�� G�t�B��G�u B��G�u�B�ffG�v B�\)G�v�B��{G�w B�G�w�B��G�x B��)G�x�B�ǮG�y B��G�y�B��
G�z B��\G�z�B��G�{ B��G�{�B�=qG�| B��)G�|�B��)G�} B�k�G�}�B���G�~ B��{G�~�B�u�G� B���G��B��{G�� B�33G���B��
G�� B��G���B���G�� B���G���B���G�� B��HG���B���G�� B�
=G���B�aHG�� B�G�G���B���G�� B��G���B���G�� B��HG���B�k�G�� B���G���B�L�G�� B���G���B��fG�� B�8RG���B��G�� B��qG���B���G�� B�u�G���B�u�G�� B�8RG���B�aHG�� B��fG���B��)G�� B��=G���B�z�G�� B���G���B��=G�� B�� G���B��RG�� B�G���B��G�� B��\G���B��G�� B�u�G���B�W
G�� B�(�G���B�z�G�� B�33G���B�\)G�� B��\G���B���G�� B�(�G���B�p�G�� B�#�G���B��\G�� B�ǮG���B�  G�� B�\)G���B�z�G�� B��G���B�
=G�� B�G���B��G�� B�G���B��HG�� B���G���B�
=G�� B�#�G���B��fG�� B���G���B�#�G�� B�Q�G���B��G�� B�=qG���B�#�G�� B�#�G���B��qG�� B�W
G���B���G�� B�G���B��{G�� B�� G���B���G�� B��G���B�
=G�� B�33G���B�� G�� B��qG���B��G�� B�
=G���B�  G�� B�k�G���B��G�� B�\G���B��3G�� B���G���B��RG�� B�� G���B���G�� B�.G���B�G�G�� B�.G���B�ǮG�� B��=G���B�G�� B���G���B�
=G�� B�=qG���B�=qG�� B�(�G���B�  G�� B��{G���B��qG�� B��fG���B�k�G�� B�  G���B�G�� B��=G���B�u�G�� B��{G���B�W
G�� B��qG���B�G�G�� B���G���B�\G�� B��RG���B�ffG�� B���G���B��G�� B��\G���B���G�� B��HG���B���G�� B�.G���B��G�� B�Q�G�B�Q�G�� B���G�ÀB��{G�� B�W
G�ĀB�=qG�� B��G�ŀB��{G�� B�
=G�ƀB��qG�� B��RG�ǀB���G�� B�k�G�ȀB�u�G�� B�z�G�ɀB�Q�G�� B��G�ʀB��G�� B��3G�ˀB�=qG�� B�#�G�̀B���G�� B�Q�G�̀B�  G�� B�Q�G�΀B�8RG�� B�  G�πB��G�� B���G�ЀB��fG�� B�z�G�рB�\G�� B�=qG�ҀB��\G�� B�ffG�ӀB���G�� B�(�G�ԀB��qG�� B��3G�ՀB��{G�� B��qG�րB��{G�� B��
G�׀B�\G�� B�.G�؀B�W
G�� B�L�G�ـB�33G�� B��G�ڀB���G�� B�33G�ۀB���G�� B���G�܀B�aHG�� B�G�݀B��G�� B�8RG�ހB�8RG�� B�#�G�߀B�G�� B�ffG���B���G�� B��3G��B��fG�� B���G��B�=qG�� B�ffG��B�8RG�� B�Q�G��B�33G�� B��G��B�=qG�� B�{G��B��qG�� B��=G��B�� G�� B���G��B��qG�� B�W
G��B��=G�� B��qG��B�L�G�� B�k�G��B�k�G�� B��3G��B�=qG�� B�W
G��B���G�� B�=qG��B�� G�� B�  G��B���G�� B���G���B���G�� B���G��B�aHG�� B�k�G��B�u�G�� B�� G��B��G�� B��G��B�B�G�� B��G���B���G�� B��
G���B�k�G�� B�u�G���B�G�� B�k�G���B��G�� B�k�G���B�  G�� B�B�G���B���G�� B�W
G���B���G�� B�B�G���B�B�G�� B�#�G���B��{G�� B��{G���B�B�G�� B�G�G���B�p�G�  B��\G� �B�B�G� B�B�G��B���G� B��
G��B�z�G� B�p�G��B��G� B�B�G��B�\)G� B���G��B�.G� B�W
G��B�\G� B�ǮG��B���G� B��G��B��)G�	 B�.G�	�B�8RG�
 B��
G�
�B��qG� B���G��B�G� B���G��B��G� B�G��B�G� B���G��B��G� B��
G��B�.G� B��G��B�\G� B���G��B��)G� B��G��B��)G� B��fG��B�
=G� B�G��B�G� B��3G��B��G� B�=qG��B�
=G� B��qG��B��fG� B�G��B�L�G� B���G��B���G� B�8RG��B�\)G� B�ǮG��B��G� B�\)G��B��G� B��G��B�ǮG� B�ffG��B�Q�G� B��G��B��G�  B�\G� �B���G�! B��G�!�B�G�" B��
G�"�B���G�# B�\)G�#�B��RG�$ B�.G�$�B�ffG�% B���G�%�B�p�G�& B��G�&�B�(�G�' B�G�'�B�p�G�( B��
G�(�B�G�) B��\G�)�B��)G�* B�.G�*�B��3G�+ B��G�+�B�W
G�, B��G�,�B�z�G�- B���G�-�B�B�G�. B��=G�.�B�k�G�/ B���G�/�B��qG�0 B��fG�0�B�k�G�1 B�u�G�1�B�33G�2 B�k�G�2�B���G�3 B�
=G�3�B��3G�4 B��G�4�B��HG�5 B�33G�5�B�\)G�6 B��)G�6�B�Q�G�7 B�ǮG�7�B�.G�8 B�#�G�8�B���G�9 B�L�G�9�B�  G�: B�u�G�:�B�u�G�; B��HG�;�B�  G�< B���G�<�B�\G�= B��fG�=�B�aHG�> B���G�>�B��
G�? B�u�G�?�B�L�G�@ B�z�G�@�B��fG�A B�L�G�A�B�.G�B B��HG�B�B��G�C B��3G�C�B��RG�D B�G�D�B���G�E B�#�G�E�B�\G�F B�#�G�F�B���G�G B�G�G�B��G�H B��HG�H�B��RG�I B�z�G�I�B�ffG�J B��qG�J�B���G�K B�L�G�K�B���G�L B�z�G�L�B�\G�M B���G�M�B�B�G�N B�z�G�N�B���G�O B�G�G�O�B�z�G�P B��)G�P�B�k�G�Q B��
G�Q�B��G�R B�  G�R�B�
=G�S B��qG�S�B�G�T B��G�T�B�p�G�U B��{G�U�B���G�V B�ffG�V�B�G�G�W B�G�G�W�B�z�G�X B��qG�X�B��fG�Y B�B�G�Y�B�G�Z B��qG�Z�B�z�G�[ B�\G�[�B���G�\ B��G�\�B���G�] B�(�G�]�B�Q�G�^ B�{G�^�B�{G�_ B�33G�_�B�k�G�` B�  G�`�B�G�G�`�B��qG�a B�ǮG�a�B��qG�b B���G�b�B�ǮG�c B���G�c�B�(�G�d B���G�d�B��)G�e B���G�e�B�ǮG�f B�Q�G�f�B���G�g B���G�g�B�z�G�h B��)G�h�B�k�G�i B�Q�G�i�B�(�G�j B�� G�j�B�Q�G�k B�ffG�k�B��qG�l B�u�G�l�B�{G�m B���G�m�B�� G�n B�  G�n�B�{G�o B��qG�o�B���G�p B�k�G�p�B�B�G�q B�.G�q�B�(�G�r B�k�G�r�B�W
G�s B�u�G�s�B��G�t B�
=G�t�B�� G�u B��{G�u�B�\)G�v B���G�v�B�B�G�w B��{G�w�B�33G�x B�8RG�x�B�.G�y B���G�y�B��G�z B�� G�z�B�Q�G�{ B���G�{�B�L�G�| B�p�G�|�B�k�G�} B���G�}�B�33G�~ B���G�~�B�z�G� B�{G��B��\G�� B��\G���B�z�G�� B��G���B��
G�� B��qG���B��G�� B�z�G���B�Q�G�� B��HG���B�ǮG�� B�
=G���B���G�� B��G���B�  G�� B���G���B���G�� B�Q�G���B���G�� B��)G���B���G�� B��G���B���G�� B��{G���B�G�� B�(�G���B��G�� B�=qG���B��G�� B�.G���B�.G�� B��HG���B�aHG�� B��G���B��G�� B��qG���B��{G�� B�\G���B�\G�� B���G���B��\G�� B��fG���B�8RG�� B�G���B���G�� B�\)G���B�{G�� B�ffG���B�\G�� B��)G���B�{G�� B�� G���B�W
G�� B�aHG���B�Q�G�� B�p�G���B�k�G�� B���G���B�#�G�� B�G���B�z�G�� B�
=G���B�  G�� B�.G���B�W
G�� B�� G���B�G�� B��RG���B�=qG�� B�.G���B�\)G�� B���G���B��G�� B���G���B�(�G�� B�=qG���B�#�G�� B��G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� It#�G��It#�G�� B�=qG��B��3G�� B��\G��B�{G�� B���G��B��\G�� B�
=G��Bv��G�� B�#�G��B�#�G�� B�=qG��B��{G�� B��fG��B�u�G�� B���G��B�Q�G�� B|�G���B��G�� B�(�G��B�\G�� B{��G��B��qG�� B��G��B�  G�� B�W
G��B�G�� B��G���B���G�� B�W
G���B�L�G�� Bx��G���BuffG�� B~��G���B�G�� Bxz�G���Br��G�� B}G���B�G�G�� B|33G���B}�G�� B�W
G���B�k�G�� B��G���B���G�� B���G���B~�\G�� Bx�RG���Bs
=G�  B�� G� �B�� G� B���G��B���G� B�\G��B{�
G� Bv�\G��ByffG� Bz{G��B���G� B�� G��B}��G� Br�RG��Bx�
G� B�u�G��B�{G� B���G��B�=qG�	 Bq=qG�	�Bt33G�
 ByG�G�
�Bzz�G� B�{G��B�� G� B}z�G��B  G� B=qG��B�p�G� B{�
G��B{  G� B|(�G��BrG�G� Bw�\G��By(�G� Bsz�G��Bs�RG� BiG�G��Bq��G� By�G��B{\)G� B�=qG��B�\)G� B�
=G��B~(�G� Bu
=G��Bs\)G� Bq�G��Bp�G� Bt33G��Br�HG� BvffG��B|�G� By�\G��B~p�G� B|{G��Bq��G� BvG��B�=qG� B�{G��B�k�G� B�G��B��G� By�G��B�(�G�  B�p�G� �B�  G�! B�� G�!�B{��G�" B|Q�G�"�B��G�# B���G�#�B��=G�$ Bxz�G�$�B~(�G�% B���G�%�B|�
G�& B~G�&�B~��G�' B�\)G�'�B�(�G�( BxQ�G�(�B���G�) B�33G�)�Bz�G�* Bw�G�*�Bs�
G�+ B~�RG�+�B=qG�, B{�\G�,�Bs�
G�- Bp{G�-�Byz�G�. B|=qG�.�B}=qG�/ B�
=G�/�B�ffG�0 B�G�0�B���G�1 B�k�G�1�B�33G�2 Bx(�G�2�BvffG�3 Bx{G�3�Bp
=G�4 Blp�G�4�Bo�G�5 Bn
=G�5�BpffG�6 Bw�
G�6�BxG�7 Bw��G�7�BuG�8 BsG�8�Bn=qG�9 BiG�G�9�Bi�\G�: Bdp�G�:�BoG�; Bx��G�;�BrG�G�< Btp�G�<�Bp�G�= Bn{G�=�Bsz�G�> BqG�G�>�Bg�\G�? Bg��G�?�Bo��G�@ Bu{G�@�B~�RG�A Bz33G�A�BlG�G�B Bh{G�B�Bd�
G�C BkG�G�C�Bv  G�D Bp��G�D�BlffG�E BnffG�E�Bh��G�F Bm(�G�F�Be�G�G B\��G�G�Bl{G�H Bpp�G�H�Bl=qG�I Bf�G�I�Bh�G�J Bp�G�J�Bk�
G�K Bh�\G�K�Bg��G�L Bk�
G�L�Bm�HG�M Bi{G�M�Bg�G�N Bq=qG�N�BuG�O Bo(�G�O�Bm�\G�P Bi
=G�P�Bb  G�Q Bm�\G�Q�Bs�G�R Bn{G�R�BoG�S Bn�G�S�Bd�HG�T Bi�HG�T�Bm�G�U Bn�G�U�BnG�V Be��G�V�Bj(�G�W Bl�G�W�Bl  G�X Bg�\G�X�Bg�
G�Y BjG�Y�BkG�G�Z Blp�G�Z�Bn�G�[ Bn��G�[�Br��G�\ Bw�RG�\�Bv\)G�] BpQ�G�]�Bo(�G�^ ByffG�^�By{G�_ Bl  G�_�Bj��G�` Bj��G�`�Bl(�G�a Bi��G�a�Bm  G�b Bo\)G�b�Bh�G�c Bl�G�c�Bm\)G�d Bl�G�d�Bn��G�e Bs�G�e�BvG�G�f Bu  G�f�Bwp�G�g Bn�\G�g�Bd{G�h Bg�G�h�Bj�HG�i Bi�RG�i�Bk33G�j Bp�
G�j�BnffG�k Bm=qG�k�Bk�G�l Bf�G�l�BgG�G�m Bi�\G�m�Bk=qG�n Bg
=G�n�B^{G�o B\{G�o�B\  G�p Bh=qG�p�BqQ�G�q BjG�q�Be(�G�r Bhp�G�r�Bm��G�s Bl�G�s�Bk��G�t Bq  G�t�Bpz�G�u Bq�
G�u�Bp�G�v Bq�G�v�Bt\)G�w Bn�G�w�Bj(�G�x Bj(�G�x�Bl=qG�y B{33G�y�B{�G�z BwffG�z�Bq33G�{ Bk�G�{�Bq33G�| Bq�HG�|�Bi�G�} Bn�HG�}�Br�
G�~ Bz�G�~�BtG� Bk
=G��Bq=qG�� BpG���Bk�RG�� Bn�G���Boz�G�� BoffG���Bn{G�� B_��G���Ba(�G�� Bp�G���Bpp�G�� Bl=qG���Bg�\G�� BgffG���Bm��G�� Bo��G���Bk�G�� Bg{G���Bj  G�� Bip�G���Bf�RG�� Bn
=G���Bn
=G�� Bn�G���Bkp�G�� Bp�\G���Br�G�� Bk��G���Bf�
G�� Bg�HG���Bgp�G�� Bg�HG���Bo=qG�� Bq
=G���Bp�HG�� Bhz�G���Bf��G�� Bj(�G���BgffG�� BhQ�G���BgQ�G�� Bk�
G���Bk33G�� Bg�G���Bu�G�� Bxp�G���Bsp�G�� Bl�\G���Bl�RG�� Bup�G���Bt
=G�� Bt��G���Bp��G�� Bo=qG���Bm  G�� Bh�RG���Bj�G�� Bm�G���Bi��G�� Bl��G���Bt��G�� Bn��G���Bg�\G�� Bm�G���BlG�G�� Bj(�G���Bnp�G�� Bp�\G���Bk�G�� Bi�G���Bn33G�� Boz�G���Br  G�� BmQ�G���Bk��G�� Bi��G���BjG�G�� Bt�\G���Bu
=G�� Bk(�G���BiG�G�� Bn(�G���Bn(�G�� Bv{G���Br�RG�� Bi(�G���Bk�
G�� Bt�G���Bv=qG�� Bf��G���B]z�G�� Bg(�G���Bc�RG�� Bi�G���Bp  G�� Bn
=G���Boz�G�� Bj�G���Bi
=G�� Bf�G���Ba��G�� BjG�G���Bl�RG�� BbQ�G���B]��G�� BmG���Bqp�G�� BjffG���BmG�� Bn�G���Bq��G�� BhffG���BbG�G�� Bf�\G���Be�G�� Bg=qG���Bf�HG�� Bf{G���Bi{G�� Bh�
G���Bj��G�� Bo�RG���Bj��G�� Bg�G���Bk��G�� Bl�\G���Bj��G�� Bjz�G���Bm�G�� Bn�G���Blz�G�� Bmp�G���BfG�G�� Bc�G�Bj�
G�� Bb{G�ÀBe��G�� Bk�
G�ĀBb�G�� BjG�ŀBn\)G�� Bh�G�ƀBk\)G�� Bl�HG�ǀBjp�G�� Be{G�ȀBmffG�� Bn�RG�ɀBg=qG�� Br��G�ʀBs��G�� Bk�HG�ˀBh\)G�� Bi�RG�̀Bj�RG�� Bm�G�̀Bt(�G�� Bp��G�΀Bm�G�� Bq�G�πBt{G�� Bw33G�ЀBv\)G�� Bs  G�рBu\)G�� Bv�HG�ҀBj�G�� Bf��G�ӀBn��G�� Bi  G�ԀB`�G�� Bf�G�ՀBl��G�� Bf�G�րBhG�� BjG�G�׀Blz�G�� Bm(�G�؀Bh�G�� Bk�\G�ـBrQ�G�� Bm=qG�ڀBkz�G�� Bn�G�ۀBo
=G�� Bq�\G�܀Br{G�� BpQ�G�݀Bf�G�� Bj  G�ހBm�G�� Bg  G�߀Bo�\G�� Bw�HG���B�ǮG�� By  G��Bdz�G�� BfQ�G��BqG�� Br�G��BqG�G�� Bo��G��Bh�HG�� Bi�RG��Bq�G�� BiQ�G��Bg33G�� Bi
=G��BoG�G�� Bs�G��Bm��G�� BjffG��BpffG�� Bl��G��B`�
G�� Bb�G��B_(�G�� Bf=qG��BrG�G�� Bi�G��BnQ�G�� Bq�G��Bg�G�� Bg��G��Bh��G�� BnQ�G���Bn�
G�� Bc{G��B[ffG�� Br�G��Bl  G�� Bf��G��Bg�G�� Bj�G��Bv�G�� Bt�G���Bj(�G�� Bk�G���BqG�� Bv�HG���BxffG�� Bo�\G���BiG�� BiG���Bnz�G�� Bu�\G���Bp  G�� Bd(�G���Bj
=G�� Bq��G���Bn�G�� Bt  G���Bu�
G�� B|{G���Bx�G�� Bop�G���Bnp�G�  BcQ�G� �BlG� B~��G��Bw�\G� Bq{G��BuG�G� Bx{G��Bv��G� B}z�G��Bz�
G� Bq�
G��Bq33G� Bo  G��BpG�G� Bvz�G��Bt�RG� Bn  G��Bl(�G�	 Bd(�G�	�Bd{G�
 Bo�G�
�Bqz�G� Bh�\G��Bhz�G� Bo��G��Bup�G� Bt�G��Bu��G� Bp{G��Bf33G� Bh=qG��Bi(�G� Bp=qG��Brp�G� Bp��G��Bl�HG� Bfz�G��Bk\)G� Bp��G��Br\)G� Bk�G��Bk(�G� Bw�HG��Bu�G� BsG��Bxz�G� B�B�G��B�(�G� B~�G��Bm�G� BhffG��Bm��G� Bo��G��Bs�
G� Br\)G��BfffG� Bk\)G��Bq�G� Bq  G��Bn(�G� Bj��G��Bn  G� Bj�HG��Bm=qG�  Bj��G� �Bk�G�! Bk=qG�!�Bk��G�" Bm�G�"�Bi�\G�# BcG�#�Bc�G�$ Boz�G�$�BrffG�% Bl33G�%�Bh�RG�& Bfp�G�&�Bi�G�' BpffG�'�Bt�G�( Bu�G�(�Bp=qG�) Bh  G�)�Bf=qG�* Bo��G�*�BoG�+ Bl�HG�+�Bl�\G�, Bh�
G�,�BlQ�G�- Br�G�-�Bm�G�. BdQ�G�.�Bg�G�/ Bj(�G�/�Bkz�G�0 Bu33G�0�Brz�G�1 Bg\)G�1�Bj{G�2 BjG�2�Bf33G�3 Bh=qG�3�Bk��G�4 Bn��G�4�Bq��G�5 BoQ�G�5�Bi�G�6 Be�G�6�BcG�G�7 BT�HG�7�B^G�8 Bmz�G�8�Bmz�G�9 Bh��G�9�BhG�G�: Bi��G�:�Bn�G�; Bs{G�;�BtG�G�< Bm  G�<�Bf��G�= Bh�\G�=�Bk�G�> Ba�RG�>�B`
=G�? Bi�
G�?�Bi�G�@ Bf��G�@�Be��G�A Be��G�A�Bd�G�B Bk�\G�B�Bk33G�C Be�G�C�Bh\)G�D Bi
=G�D�Bcz�G�E Bb��G�E�Bh�HG�F BjQ�G�F�BcffG�G B`�G�G�Bd�
G�H BfG�H�BpQ�G�I BrQ�G�I�BjffG�J Bfp�G�J�Biz�G�K BiffG�K�Bnz�G�L Bm��G�L�Bl
=G�M Bj��G�M�Bj�G�N Bi�
G�N�Bl�HG�O Bk�\G�O�Bb\)G�P Bd�\G�P�Bf=qG�Q B^z�G�Q�B[G�G�R Bb�RG�R�Bh��G�S Bd{G�S�BcQ�G�T BlQ�G�T�Bi
=G�U B`�RG�U�Bd�G�V BdG�V�B]=qG�W B[  G�W�Ba=qG�X Bf��G�X�Be��G�Y Bd��G�Y�Bf��G�Z Ba��G�Z�Bd��G�[ Bfz�G�[�Bc��G�\ Bjp�G�\�Bk
=G�] Bk�HG�]�Bn�
G�^ BeQ�G�^�B`G�G�_ Bf(�G�_�BiffG�` Bi�RG�`�Bc��G�a Ba�HG�a�Bj�\G�b Bt33G�b�Bk=qG�c B^p�G�c�Bb(�G�d B_��G�d�B`��G�e BiG�e�Bf�RG�f Bjp�G�f�Be�\G�g Bb�
G�g�Bg{G�h Bi�G�h�Bj��G�i Bj�G�i�Bg�G�j B_Q�G�j�BbG�G�k Bg  G�k�Bfp�G�l Bl  G�l�Bu�G�m Bq=qG�m�Bhz�G�n Bk  G�n�Bo�\G�o Bi=qG�o�Bh
=G�p BcffG�p�B[��G�q B\=qG�q�Bf��G�r Bq��G�r�Bj33G�s Bc�G�s�Bi33G�t Bj
=G�t�Ba(�G�u B_ffG�u�Ba�G�v BbG�G�v�Bc�\G�w BcffG�w�BhG�G�x Bg�HG�x�Ba��G�y B]G�y�BcQ�G�z Bi
=G�z�Bmp�G�{ Bg�HG�{�B^{G�| B_�G�|�Bh�G�} Br�G�}�Bm�G�~ Bi=qG�~�Bf�\G� BgG��Bj�HG�� Ba�G���B\�G�� B^�G���B^��G�� Bg�
G���BmG�� Bip�G���Bg33G�� Bap�G���B](�G�� B]z�G���Bap�G�� Bg��G���Biz�G�� BfG�G���Bi��G�� Bj�G���Bc��G�� Be��G���Bd�G�� B[�
G���Bb��G�� Bh(�G���Bez�G�� Bdp�G���B`G�� Bj�\G���Bj�\G�� Br��G���Bj
=G�� Ba��G���Bi�HG�� Bi�RG���Bf\)G�� BkG���Bi�HG�� Bf33G���BiffG�� Bg�\G���Bf33G�� Bc�
G���Bh=qG�� Bo  G���Bm��G�� BhQ�G���Bc  G�� Ba(�G���B^��G�� B`��G���Bc\)G�� Bb�
G���Bj�\G�� Bi33G���Bb\)G�� BZ{G���BX�HG�� Bc33G���Bc
=G�� Bl�G���BxG�� Bi
=G���Bi
=G�� B_p�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـIt#�G�� It#�G�ڀIt#�G�� It#�G�ۀIt#�G�� It#�G�܀It#�G�� It#�G�݀It#�G�� It#�G�ހIt#�G�� It#�G�߀It#�G�� It#�G���It#�G�� Bo{G��Bh{G�� Bi  G��Bhp�G�� Bf�G��Bup�G�� BtG��Bq
=G�� Bj��G��BkQ�G�� Bh(�G��Bf�
G�� Bb\)G��B^�RG�� Bp�\G��Bv��G�� BrG��Bq=qG�� BoG�G��Bi��G�� Bl�G��Bt�G�� Bn
=G��Bp{G�� Bw  G��Bn�\G�� Bi33G��Bm�HG�� BsG�G��Bz=qG�� Bn�RG���BhG�G�� Bl�G��Br��G�� Bu  G��Bo(�G�� BuQ�G��B���G�� B���G��Bu�G�� Brp�G���Bsz�G�� Bp�G���Biz�G�� Brp�G���BrG�G�� Bs
=G���Bv  G�� Br�HG���B}G�G�� Bz{G���Bp��G�� Bn�G���Bo=qG�� Bpp�G���Bt  G�� BqG�G���Bf��G�� Bf��G���Bi=qG�� Bq��G���Bu��G�  Bn�G� �Bs(�G� Bs(�G��Bu�HG� Bu�HG��Bc�G� B_�HG��Bfz�G� Bez�G��Bb��G� Bd\)G��Bf=qG� Bb=qG��BkffG� Bo�G��BiG�G� BlQ�G��Bn
=G�	 Bl{G�	�Bk�\G�
 Bh�HG�
�Bd�HG� Bd�G��Bi\)G� Bi�HG��Bo�HG� Bs��G��Be(�G� B`�
G��Bf�G� BjffG��Bl�G� Bi�RG��BeQ�G� Bh�HG��Bq\)G� Bx�G��Bp�
G� Boz�G��By��G� Byz�G��B|33G� B}\)G��Bz(�G� Br�\G��Bk�
G� Bo=qG��Bk{G� BiffG��Bb�G� B`(�G��Bs=qG� B|z�G��Bs(�G� B|�RG��B��G� By(�G��Bv��G� B{�G��B}�\G� B{
=G��Bv��G� Bp�G��B�B�G�  B�(�G� �B��
G�! B�ǮG�!�B��3G�" B�\G�"�B}�
G�# B��G�#�B��fG�$ B���G�$�B�aHG�% B��G�%�B��G�& B��G�&�B}=qG�' B��G�'�B���G�( B�  G�(�B�  G�) B��G�)�B��G�* B�33G�*�B���G�+ B��)G�+�B���G�, B�  G�,�B�z�G�- B�aHG�-�B��G�. B�p�G�.�B�{G�/ B�  G�/�B�\G�0 B��G�0�B���G�1 B���G�1�B�Q�G�2 B���G�2�B��fG�3 B�ffG�3�B��qG�4 B��
G�4�B��HG�5 B}�G�5�B�  G�6 B�Q�G�6�B�(�G�7 B�{G�7�B�k�G�8 B��3G�8�B��HG�9 B��HG�9�B��
G�: B�ffG�:�B��=G�; B���G�;�B�W
G�< B��3G�<�B�u�G�= B�G�=�B��\G�> B�ffG�>�B�G�? B��G�?�B��G�@ B�W
G�@�B��
G�A B�p�G�A�B��G�B B��
G�B�B�#�G�C B�=qG�C�B��)G�D B�  G�D�B�B�G�E B��
G�E�B��G�F B�\)G�F�B�u�G�G B�W
G�G�B��fG�H B�.G�H�B�� G�I B��fG�I�B�W
G�J B��G�J�B���G�K B���G�K�B�#�G�L B�\G�L�B�ǮG�M B�aHG�M�B�Q�G�N B�ǮG�N�B��3G�O B��3G�O�B��)G�P B���G�P�B�B�G�Q B���G�Q�B�G�R B�aHG�R�B�(�G�S B��RG�S�B�#�G�T B���G�T�B���G�U B�.G�U�B���G�V B�z�G�V�B�aHG�W B��fG�W�B���G�X B���G�X�B��3G�Y B�\)G�Y�B�W
G�Z B���G�Z�B�k�G�[ B�#�G�[�B�=qG�\ B���G�\�B��fG�] B�G�G�]�B��=G�^ B���G�^�B�L�G�_ B�33G�_�B�=qG�` B���G�`�B��HG�a B���G�a�B�L�G�b B�33G�b�B�.G�c B�\G�c�B���G�d B�.G�d�B�G�e B�\)G�e�B���G�f B���G�f�B��G�g B���G�g�B���G�h B�u�G�h�B���G�i B�.G�i�B�� G�j B�#�G�j�B�
=G�k B�u�G�k�B��qG�l B��G�l�B���G�m B��\G�m�B�B�G�n B��G�n�B���G�o B�=qG�o�B�� G�p B��RG�p�B�G�q B�ffG�q�B��
G�r B�z�G�r�B�\G�s B��3G�s�B���G�t B���G�t�B��)G�u B��{G�u�B��G�v B�L�G�v�B���G�w B�L�G�w�B�
=G�x B�{G�x�B���G�y B��G�y�B�B�G�z B��G�z�B��fG�{ B���G�{�B��G�| B���G�|�B���G�} B���G�}�B�p�G�~ B�{G�~�B���G� B��\G��B���G�� B�  G���B��fG�� B�Q�G���B�ffG�� B�G���B�Q�G�� B�W
G���B���G�� B��fG���B�\)G�� B�\)G���B�G�� B��G���B��HG�� B�p�G���B�\G�� B�Q�G���B��RG�� B��qG���B�G�� B��G���B�G�� B�z�G���B��HG�� B��qG���B�ffG�� B���G���B��3G�� B�L�G���B�.G�� B��)G���B�� G�� B�B�G���B�� G�� B��G���B�z�G�� B��G���B���G�� B�G�G���B��{G�� B�=qG���B���G�� B�\)G���B�aHG�� B��3G���B�  G�� B�G�G���B�aHG�� B��
G���B�=qG�� B�ffG���B���G�� B���G���B���G�� B��
G���B��G�� B�
=G���B�.G�� B�  G���B�#�G�� B�Q�G���B�G�� B�ǮG���B��3G�� B��3G���B��3G�� B��G���B��
G�� B��G���B�G�G�� B��G���B��)G�� B���G���B���G�� B�p�G���B��RG�� B�u�G���B�aHG�� B��G���B��RG�� B�8RG���B�\G�� B�u�G���B�Q�G�� B���G���B�(�G�� B��HG���B��3G�� B���G���B�G�G�� B��G���B�33G�� B�\)G���B��G�� B�Q�G���B��RG�� B�G���B��G�� B�z�G���B��\G�� B�k�G���B�k�G�� B���G���B�u�G�� B�(�G���B���G�� B���G���B�  G�� B�(�G���B��G�� B��HG���B��{G�� B�\G���B��HG�� B���G���B�{G�� B�  G���B�.G�� B��G���B���G�� B��HG���B���G�� B��qG���B��G�� B��G���B��G�� B��RG���B�W
G�� B��3G���B���G�� B�k�G���B���G�� B��G�B�p�G�� B��
G�ÀB�z�G�� B��G�ĀB�\)G�� B�
=G�ŀB�33G�� B�k�G�ƀB��G�� B�G�ǀB�z�G�� B�33G�ȀB��\G�� B��qG�ɀB�{G�� B���G�ʀB�Q�G�� B��{G�ˀB�G�G�� B��G�̀B�B�G�� B�L�G�̀B�  G�� B�(�G�΀B�ǮG�� B�
=G�πB��G�� B��G�ЀB���G�� B��{G�рB�L�G�� B�p�G�ҀB�8RG�� B��
G�ӀB��G�� B��G�ԀB�u�G�� B�� G�ՀB��=G�� B�  G�րB��G�� B�W
G�׀B���G�� B��G�؀B�Q�G�� B���G�ـB�=qG�� B�G�ڀB��qG�� B��G�ۀB��G�� B�#�G�܀B�8RG�� B�\)G�݀B�W
G�� B���G�ހB�33G�� B�u�G�߀B�\)G�� B���G���B��=G�� B��G��B�u�G�� B�p�G��B��G�� B�(�G��B�L�G�� B��qG��B�G�� B���G��B�Q�G�� B�
=G��B�Q�G�� B�\)G��B�(�G�� B�{G��B���G�� B��
G��B�8RG�� B�#�G��B�k�G�� B���G��B�k�G�� B���G��B���G�� B�G�G��B��G�� B���G��B���G�� B���G��B��G�� B�G���B��G�� B�G�G��B��G�� B��
G��B�
=G�� B��G��B���G�� B���G��B�k�G�� B��{G���B���G�� B�B�G���B��G�� B�� G���B���G�� B��G���B�8RG�� B�{G���B��\G�� B���G���B�8RG�� B��RG���B�{G�� B�W
G���B��\G�� B�Q�G���B��G�� B��G���B�G�� B�� G���B��G�  B�#�G� �B��G� B���G��B���G� B��
G��B��{G� B��{G��B�  G� B�G�G��B��G� B�G��B��G� B�G�G��B��G� B��G��B�B�G� B�k�G��B�33G�	 B�G�	�B��
G�
 B��3G�
�B�L�G� B��HG��B�(�G� B���G��B�
=G� B���G��B�(�G� B��RG��B�p�G� B�� G��B�=qG� B��G��B�L�G� B��G��B���G� B��3G��B�u�G� B���G��B��G� B�Q�G��B�G� B�L�G��B��G� B�aHG��B�.G� B���G��B�\G� B�(�G��B��G� B��G��B�B�G� B��G��B��G� B���G��B���G� B�k�G��B�Q�G� B�aHG��B��RG� B��)G��B�ǮG� B�=qG��B�u�G�  B�aHG� �B��RG�! B�{G�!�B�W
G�" B��{G�"�B�L�G�# B�\)G�#�B�.G�$ B��\G�$�B��\G�% B��)G�%�B�G�& B�ffG�&�B��G�' B���G�'�B��
G�( B��G�(�B��3G�) B�u�G�)�B�G�* B��\G�*�B�� G�+ B��G�+�B��G�, B���G�,�B�p�G�- B�B�G�-�B�ǮG�. B�z�G�.�B��)G�/ B���G�/�B��G�0 B�
=G�0�B�
=G�1 B��G�1�B�W
G�2 B���G�2�B�  G�3 B���G�3�B��=G�4 B��HG�4�B�k�G�5 B�Q�G�5�B�33G�6 B�W
G�6�B��=G�7 B��=G�7�B�G�G�8 B��{G�8�B���G�9 B�8RG�9�B���G�: B�Q�G�:�B�W
G�; B�  G�;�B�\)G�< B�L�G�<�B�L�G�= B��)G�=�B�� G�> B�� G�>�B���G�? B�.G�?�B���G�@ B��qG�@�B��{G�A B�\)G�A�B�\G�B B�{G�B�B��G�C B�p�G�C�B�  G�D B���G�D�B��G�E B��HG�E�B���G�F B�=qG�F�B�G�G�G B�#�G�G�B�L�G�H B�8RG�H�B�� G�I B�B�G�I�B�(�G�J B�=qG�J�B��3G�K B��3G�K�B���G�L B��G�L�B�u�G�M B�W
G�M�B��)G�N B��G�N�B���G�O B��G�O�B��G�P B���G�P�B�ffG�Q B��
G�Q�B��G�R B�  G�R�B�W
G�S B��HG�S�B��G�T B��HG�T�B���G�U B���G�U�B���G�V B�8RG�V�B��RG�W B�#�G�W�B�u�G�X B��HG�X�B�G�Y B�aHG�Y�B��qG�Z B���G�Z�B���G�[ B�  G�[�B��)G�\ B�ǮG�\�B��G�] B�k�G�]�B�8RG�^ B��3G�^�B���G�_ B���G�_�B��G�` B�G�`�B��G�a B���G�a�B��G�b B���G�b�B�\G�c B��G�c�B�8RG�d B���G�d�B�L�G�e B�\G�e�B�8RG�f B���G�f�B���G�g B�ffG�g�B���G�h B��G�h�B��=G�i B�#�G�i�B�  G�j B�u�G�j�B��G�k B�z�G�k�B��=G�l B��)G�l�B�W
G�m B��G�m�B�G�n B��3G�n�B���G�o B�L�G�o�B��G�p B�G�p�B�k�G�q B�aHG�q�B��G�r B��)G�r�B�.G�s B��qG�s�B�(�G�t B�\)G�t�B���G�u B�u�G�u�B��G�v B�#�G�v�B��G�w B�G�w�B�.G�x B���G�x�B�u�G�y B��)G�y�B�B�G�z B�=qG�z�B�� G�{ B�
=G�{�B���G�| B��)G�|�B�p�G�} B���G�}�B���G�~ B�Q�G�~�B���G� B���G��B���G�� B��G���B��HG�� B�\)G���B�z�G�� B�z�G���B��3G�� B�Q�G���B��HG�� B�{G���B��fG�� B�B�G���B��\G���B�aHG�� B�#�G���B���G�� B��G���B��RG�� B���G���B�ffG�� B��fG���B��
G�� B��
G���B�B�G�� B���G���B��3G�� B���G���B���G�� B�
=G���B��G�� B��G���B��\G�� B�33G���B��fG�� B��G���B�ffG�� B�#�G���B�(�G�� B�  G���B���G�� B�=qG���B�B�G�� B���G���B�G�� B��qG���B��G�� B���G���B��HG�� B��
G���B���G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀIt#�G�� It#�G�ӀIt#�G�� It#�G�ԀIt#�G�� It#�G�ՀIt#�G�� It#�G�րIt#�G�� It#�G�׀It#�G�� It#�G�؀It#�G�� It#�G�ـB�  G�� B�#�G�ڀB��3G�� B�(�G�ۀB�G�� B�{G�܀B��3G�� B�u�G�݀B�G�G�� B��{G�ހB���G�� B���G�߀B��G�� B��3G���B��G�� B���G��B���G�� B���G��B�G�G�� B��3G��B�  G�� B��G��B���G�� B���G��B�\)G�� B�
=G��B�G�� B�L�G��B���G�� B���G��B�{G�� B���G��B�p�G�� B��=G��B��G�� B���G��B��fG�� B��qG��B��G�� B���G��B���G�� B���G��B�W
G�� B�L�G��B�B�G�� B�u�G���B�  G�� B��\G��B�G�G�� B�B�G��B�aHG�� B�  G��B��G�� B�p�G��B��RG�� B��)G���B�k�G�� B���G���B��fG�� B�{G���B��G�� B���G���B���G�� B��HG���B��\G�� B���G���B��G�� B�L�G���B��3G�� B�z�G���B�ǮG�� B���G���B�{G�� B�=qG���B��HG�� B�=qG���B���G�  B��G� �B��G� B���G��B��HG� B�G��B��G� B�#�G��B�\)G� B��G��B�z�G� B�\G��B�k�G� B��G��B��
G� B��=G��B���G� B��=G��B�p�G�	 B��=G�	�B�ffG�
 B�k�G�
�B��RG� B���G��B�
=G� B�aHG��B�Q�G� B�L�G��B���G� B��G��B��G� B�p�G��B���G� B��G��B�(�G� B��G��B�8RG� B�G�G��B�u�G� B��=G��B���G� B�ǮG��B�B�G� B�� G��B��qG� B���G��B���G� B�p�G��B�.G� B���G��B��RG� B���G��B�aHG� B�aHG��B���G� B��G��B�aHG� B��\G��B�p�G� B�p�G��B���G� B�33G��B�aHG� B�G��B��{G�  B�G� �B�ǮG�! B��\G�!�B��G�" B���G�"�B��\G�# B�u�G�#�B��G�$ B��)G�$�B�ǮG�% B��
G�%�B�z�G�& B�z�G�&�B��HG�' B��G�'�B�G�( B�ffG�(�B��{G�) B��)G�)�B�#�G�* B�{G�*�B�Q�G�+ B�G�G�+�B�� G�, B�{G�,�B�L�G�- B�B�G�-�B��G�. B�33G�.�B��G�/ B��G�/�B���G�0 B�L�G�0�B���G�1 B���G�1�B��\G�2 B�8RG�2�B�u�G�3 B���G�3�B�W
G�4 B�B�G�4�B���G�5 B�B�G�5�B��G�6 B��3G�6�B�u�G�7 B��=G�7�B��G�8 B�  G�8�B�=qG�9 B�ffG�9�B��HG�: B��G�:�B��G�; B��G�;�B�\G�< B��
G�<�B���G�= B���G�=�B��G�> B��RG�>�B�{G�? B�u�G�?�B�u�G�@ B��fG�@�B�ǮG�A B�ǮG�A�B�ǮG�B B���G�B�B��\G�C B�B�G�C�B��\G�D B��fG�D�B��G�E B�G�G�E�B�\)G�F B�Q�G�F�B�� G�G B��G�G�B��{G�H B�G�G�H�B�G�G�I B�L�G�I�B��G�J B�u�G�J�B�Q�G�K B�33G�K�B��{G�L B��\G�L�B��RG�M B�.G�M�B�G�N B�k�G�N�B�{G�O B�8RG�O�B�aHG�P B�aHG�P�B��G�Q B��RG�Q�B���G�R B�ffG�R�B�\)G�S B�.G�S�B�\G�T B�z�G�T�B�� G�U B��fG�U�B�8RG�V B��)G�V�B�u�G�W B�z�G�W�B�G�G�X B�8RG�X�B��=G�Y B�ǮG�Y�B��
G�Z B���G�Z�B��RG�[ B��qG�[�B��G�\ B�B�G�\�B��RG�] B�aHG�]�B��G�^ B���G�^�B�� G�_ B��3G�_�B�Q�G�` B�G�G�`�B�aHG�a B�Q�G�a�B���G�b B�=qG�b�B�ǮG�c B�z�G�c�B���G�d B��G�d�B��HG�e B�33G�e�B��G�f B��{G�f�B��G�g B��G�g�B��RG�h B��G�h�B�aHG�i B�  G�i�B��=G�j B�u�G�j�B�
=G�k B�ǮG�k�B�G�l B��G�l�B�G�m B�
=G�m�B��qG�n B���G�n�B�ffG�o B�Q�G�o�B�W
G�p B���G�p�B��G�q B�aHG�q�B�G�r B�Q�G�r�B�.G�s B�G�s�B�p�G�t B�8RG�t�B��qG�u B���G�u�B�G�G�v B��G�v�B��fG�w B��G�w�B��G�x B��HG�x�B�  G�y B�aHG�y�B��)G�z B�(�G�z�B��qG�{ B�\)G�{�B�B�G�| B���G�|�B�u�G�} B��G�}�B��HG�~ B���G�~�B�\)G� B�ffG��B�W
G�� B�aHG���B���G�� B�.G���B��{G�� B�#�G���B�33G�� B��G���B�
=G�� B�k�G���B���G�� B�33G���B��G�� B�(�G���B�G�G�� B���G���B��G�� B���G���B�\)G�� B��G���B��G�� B��fG���B��G�� B�\)G���B���G�� B�� G���B�u�G�� B���G���B��G�� B��HG���B�z�G�� B��fG���B�ffG�� B�8RG���B�{G�� B�ffG���B�  G�� B�p�G���B���G�� B�z�G���B��
G�� B���G���B��G�� B���G���B�\G�� B���G���B�.G�� B�\G���B���G�� B�z�G���B�33G�� B���G���B�G�G�� B�#�G���B�
=G�� B�z�G���B�z�G�� B��
G���B�z�G�� B�G���B�\)G�� B�{G���B��{G�� B���G���B�ǮG�� B�(�G���B���G�� B�k�G���B�z�G�� B��G���B��G�� B�\)G���B���G�� B�� G���B�G�� B�ǮG���B���G�� B�
=G���B��)G�� B�=qG���B�33G�� B��G���B�B�G�� B�G���B��HG�� B��{G���B���G�� B���G���B��=G�� B�Q�G���B�B�G�� B�
=G���B�
=G�� B�z�G���B���G�� B�(�G���B�B�G�� B���G���B��
G�� B��qG���B�Q�G�� B�aHG���B�G�� B�=qG���B�=qG�� B�
=G���B��G�� B�#�G���B�
=G�� B���G���B���G�� B�#�G���B�#�G�� B��qG���B��qG�� B���G���B��
G�� B���G���B��qG�� B��RG���B���G�� B�ffG���B��{G�� B�L�G���B�G�� B�8RG���B�u�G�� B��{G���B�=qG�� B���G���B��\G�� B��RG���B�=qG�� B�G�B��\G�� B�G�ÀB��G�� B��fG�ĀB��HG�� B��G�ŀB�L�G�� B��G�ƀB�� G�� B��
G�ǀB���G�� B�\G�ȀB���G�� B���G�ɀB��G�� B�ffG�ʀB��qG�� B�(�G�ˀB���G�� B�{G�̀B��G�� B�W
G�̀B�u�G�� B��G�΀B�.G�� B�33G�πB�#�G�� B�\G�ЀB�aHG�� B�\)G�рB�p�G�� B�� G�ҀB���G�� B�aHG�ӀB�\)G�� B��G�ԀB�{G�� B�{G�ՀB�B�G�� B��3G�րB�  G�� B��G�׀B��G�� B�G�؀B��
G�� B��
G�ـB��G�� B��G�ڀB��\G�� B�8RG�ۀB���G�� B��G�܀B�8RG�� B�Q�G�݀B�k�G�� B���G�ހB���G�� B�(�G�߀B��3G�� B���G���B��G�� B���G��B��fG�� B�.G��B�L�G�� B�ǮG��B�{G�� B��qG��B�ffG�� B�W
G��B���G�� B�
=G��B��\G�� B�(�G��B���G�� B�L�G��B�k�G�� B��HG��B�.G�� B�  G��B��G�� B��\G��B�  G�� B��RG��B��HG�� B��G��B�u�G�� B��G��B���G�� B���G��B�
=G�� B��\G���B���G�� B���G��B��=G�� B��G��B��G�� B�� G��B��HG�� B��G��B���G�� B�8RG���B���G�� B�aHG���B���G�� B�Q�G���B�W
G�� B�\G���B��
G�� B�\)G���B�
=G�� B��HG���B���G�� B���G���B�p�G�� B�ffG���B��)G�� B�k�G���B�L�G�� B�W
G���B�p�G�� B�  G���B�k�G�  B��qG� �B��G� B��{G��B��\G� B�L�G��B��G� B���G��B��=G� B�G��B��\G� B��qG��B�u�G� B��G��B��=G� B��G��B���G� B��G��B�=qG�	 B�\)G�	�B�#�G�
 B�\)G�
�B�  G� B�p�G��B�=qG� B�#�G��B��qG� B��
G��B�W
G� B���G��B��qG� B�p�G��B�G� B�L�G��B��G� B�k�G��B�33G� B�G��B��G� B�W
G��B�#�G� B�#�G��B��=G� B��fG��B���G� B��qG��B��)G� B�(�G��B��G� B��=G��B��G� B�B�G��B��G� B�k�G��B��G� B�33G��B�  G� B�z�G��B�  G� B��G��B��
G� B���G��B�\)G� B�u�G��B���G�  B�Q�G� �B�
=G�! B�.G�!�B�=qG�" B���G�"�B���G�# B��)G�#�B�{G�$ B��3G�$�B���G�% B�k�G�%�B���G�& B��{G�&�B�33G�' B�Q�G�'�B�z�G�( B�k�G�(�B�#�G�) B�k�G�)�B�ǮG�* B��3G�*�B��G�+ B�(�G�+�B���G�, B��\G�,�B��=G�- B�aHG�-�B���G�. B��G�.�B���G�/ B�B�G�/�B�aHG�0 B��\G�0�B��HG�1 B��G�1�B���G�2 B��\G�2�B�
=G�3 B�ffG�3�B�(�G�4 B�L�G�4�B�G�5 B�8RG�5�B�8RG�6 B�� G�6�B�(�G�7 B�\)G�7�B�aHG�8 B��{G�8�B��qG�9 B��qG�9�B��{G�: B�� G�:�B�  G�; B��fG�;�B��G�< B�\)G�<�B���G�= B���G�=�B�ffG�> B��G�>�B��G�? B�ffG�?�B���G�@ B��)G�@�B�L�G�A B�� G�A�B�33G�B B��)G�B�B�p�G�C B��3G�C�B���G�D B��{G�D�B�� G�E B�G�E�B�  G�F B�8RG�F�B��=G�G B�L�G�G�B�ffG�H B�ffG�H�B�p�G�I B�\)G�I�B��)G�J B��G�J�B��3G�K B�ǮG�K�B���G�L B��RG�L�B�Q�G�M B���G�M�B��qG�N B���G�N�B�W
G�O B�aHG�O�B���G�P B��{G�P�B���G�Q B���G�Q�B��\G�R B���G�R�B��G�S B��HG�S�B��G�T B��RG�T�B��G�U B�\G�U�B�G�G�V B�G�V�B�G�W B�W
G�W�B��)G�X B��RG�X�B�G�Y B��{G�Y�B��G�Z B�33G�Z�B��qG�[ B��G�[�B���G�\ B�z�G�\�B�\)G�] B�k�G�]�B��fG�^ B��qG�^�B�
=G�_ B�  G�_�B�L�G�` B���G�`�B�aHG�a B���G�a�B�B�G�b B�G�b�B�G�c B�p�G�c�B�p�G�d B�p�G�d�B�(�G�e B���G�e�B�{G�f B�B�G�f�B���G�g B�B�G�g�B��)G�h B��qG�h�B��3G�i B�k�G�i�B�8RG�j B�Q�G�j�B�\)G�k B��qG�k�B���G�l B��3G�l�B���G�m B��G�m�B��RG�n B���G�n�B�G�o B�33G�o�B��fG�p B�G�p�B�#�G�q B��HG�q�B�#�G�r B�G�r�B�\)G�s B��\G�s�B���G�t B��\G�t�B�#�G�u B�W
G�u�B���G�v B��qG�v�B�
=G�w B��=G�w�B�ǮG�x B�=qG�x�B���G�y B�G�y�B�p�G�z B�(�G�z�B�G�G�{ B��=G�{�B�
=G�| B�8RG�|�B�z�G�} B��{G�}�B��qG�~ B��3G�~�B�ǮG� B�z�G��B��=G�� B�L�G���B�k�G�� B��qG���B��=G�� B�#�G���B���G�� B�G���B�G�G�� B�{G���B��G�� B���G���B�.G�� B�ǮG���B�� G�� B�\G���B���G�� B��{G���B��3G�� B�Q�G���B��
G�� B��=G���B�L�G�� B�  G���B��G�� B�k�G���B��\G�� B�ǮG���B�L�G�� B��{G���B�{G�� B�G�G���B�\G�� B�G���B��RG�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� It#�G�ˀIt#�G�� It#�G�̀It#�G�� It#�G�̀It#�G�� It#�G�΀It#�G�� It#�G�πIt#�G�� It#�G�ЀIt#�G�� It#�G�рIt#�G�� It#�G�ҀB���G�� B��
G�ӀB�G�� B�  G�ԀB�#�G�� B�z�G�ՀB��)G�� B���G�րB�L�G�� B��3G�׀B��\G�� B���G�؀B���G�� B�Q�G�ـB��\G�� B�u�G�ڀB��\G�� B�W
G�ۀB�.G�� B��=G�܀B���G�� B�aHG�݀B��G�� B��3G�ހB���G�� B��)G�߀B��G�� B�G�G���B��
G�� B�#�G��B��\G�� B�
=G��B���G�� B�=qG��B�W
G�� B��\G��B��qG�� B�  G��B�ffG�� B�G�G��B�G�G�� B��G��B�k�G�� B�\G��B�ǮG�� B��G��B��qG�� B�G��B�G�� B��G��B���G�� B���G��B�B�G�� B�.G��B�� G�� B�� G��B��{G�� B��{G��B��fG�� B�u�G���B�G�� B��
G��B��G�� B��
G��B��{G�� B��)G��B���G�� B���G��B��G�� B�
=G���B�.G�� B�L�G���B�� G�� B��G���B�G�G�� B�u�G���B�ǮG�� B�
=G���B�aHG�� B�u�G���B���G�� B���G���B�p�G�� B��G���B��=G�� B���G���B�� G�� B�L�G���B�u�G�� B�ǮG���B�\G�  B�p�G� �B��qG� B��qG��B��G� B���G��B��qG� B�\G��B�\G� B��G��B�u�G� B��
G��B�aHG� B�=qG��B�=qG� B�  G��B�8RG� B��3G��B�u�G�	 B��G�	�B���G�
 B�33G�
�B�B�G� B�  G��B�
=G� B�(�G��B��fG� B�L�G��B��fG� B��HG��B��\G� B�G��B�ǮG� B�(�G��B�33G� B���G��B��G� B���G��B��fG� B�Q�G��B��HG� B���G��B���G� B�B�G��B��3G� B�ffG��B��)G� B��
G��B��fG� B��qG��B��\G� B�G��B���G� B��3G��B�G�G� B��RG��B�ǮG� B�ǮG��B�ffG� B��RG��B��qG� B�{G��B��G� B�aHG��B��RG�  B�33G� �B���G�! B�ffG�!�B�L�G�" B�
=G�"�B�#�G�# B��G�#�B���G�$ B�p�G�$�B��G�% B�.G�%�B��G�& B��)G�&�B��3G�' B�  G�'�B��\G�( B��G�(�B�ǮG�) B�p�G�)�B��G�* B��G�*�B�=qG�+ B�z�G�+�B�33G�, B�B�G�,�B���G�- B��3G�-�B���G�. B�ǮG�.�B��G�/ B��HG�/�B�{G�0 B�=qG�0�B��=G�1 B�k�G�1�B�B�G�2 B�(�G�2�B���G�3 B�u�G�3�B���G�4 B��G�4�B�k�G�5 B�ffG�5�B��RG�6 B��G�6�B�ffG�7 B��\G�7�B��qG�8 B��G�8�B��G�9 B�k�G�9�B�#�G�: B�{G�:�B��G�; B��)G�;�B��\G�< B�#�G�<�B�L�G�= B�Q�G�=�B�L�G�> B��G�>�B��)G�? B��
G�?�B��G�@ B�=qG�@�B���G�A B�L�G�A�B��qG�B B�G�G�B�B���G�C B��=G�C�B��HG�D B���G�D�B�� G�E B�u�G�E�B���G�F B��3G�F�B�W
G�G B���G�G�B��G�H B�z�G�H�B�G�G�I B�\G�I�B�G�G�J B�#�G�J�B��3G�K B�#�G�K�B�aHG�L B���G�L�B��\G�M B��G�M�B�(�G�N B��qG�N�B��HG�O B��G�O�B��
G�P B��
G�P�B�ffG�Q B�8RG�Q�B�(�G�R B�G�R�B�ǮG�S B��G�S�B�  G�T B��G�T�B�z�G�U B�\G�U�B�{G�V B��3G�V�B��G�W B�#�G�W�B�ǮG�X B�u�G�X�B�p�G�Y B�=qG�Y�B��\G�Z B��qG�Z�B�z�G�[ B���G�[�B�.G�\ B���G�\�B��G�] B���G�]�B��G�^ B�
=G�^�B�\)G�_ B�8RG�_�B�u�G�` B�=qG�`�B�
=G�a B���G�a�B�G�G�b B��qG�b�B�
=G�c B�
=G�c�B�L�G�d B�ffG�d�B�� G�e B�B�G�e�B�� G�f B�(�G�f�B�.G�g B�  G�g�B�W
G�h B�G�h�B���G�i B���G�i�B�k�G�j B�ǮG�j�B���G�k B��qG�k�B�Q�G�l B�k�G�l�B�L�G�m B��
G�m�B��=G�n B�
=G�n�B��\G�o B��G�o�B��fG�p B�=qG�p�B�.G�q B���G�q�B��RG�r B���G�r�B�B�G�s B�G�s�B���G�t B�\)G�t�B��G�u B��\G�u�B�  G�v B�\G�v�B���G�w B�aHG�w�B��G�x B��G�x�B���G�y B���G�y�B�G�z B�33G�z�B���G�{ B��)G�{�B�  G�| B�{G�|�B�ffG�} B�ffG�}�B��G�~ B���G�~�B��G� B���G��B��RG�� B�33G���B�ǮG�� B��fG���B���G�� B�u�G���B��G�� B���G���B�  G�� B��)G���B�\)G�� B�ffG���B��HG�� B��G���B���G�� B�\G���B��{G�� B��HG���B��\G�� B�L�G���B�
=G�� B�\)G���B�G�G�� B��G���B��=G�� B��\G���B���G�� B�\)G���B�k�G�� B�
=G���B�.G�� B���G���B��\G�� B�
=G���B�u�G�� B�p�G���B��\G�� B�8RG���B�Q�G�� B���G���B�{G�� B�p�G���B�G�G�� B��qG���B�\)G�� B�L�G���B�33G�� B�
=G���B���G�� B�ǮG���B�.G�� B��G���B�aHG�� B��\G���B�
=G�� B�Q�G���B�aHG�� B�p�G���B���G�� B�ǮG���B���G�� B�  G���B�\)G�� B�aHG���B�L�G�� B�z�G���B���G�� B��G���B�\G�� B��RG���B�33G�� B��3G���B�ǮG�� B�ffG���B��3G�� B�B�G���B��{G�� B�.G���B�B�G�� B��=G���B�Q�G�� B�(�G���B�{G�� B�.G���B��qG�� B�#�G���B���G�� B�33G���B��G�� B�� G���B�k�G�� B�B�G���B�Q�G�� B���G���B��G�� B���G���B�aHG�� B��RG���B��G�� B��fG���B���G�� B���G���B��=G�� B�W
G���B�8RG�� B�z�G���B�.G�� B��=G���B�8RG�� B���G���B�.G�� B�G���B�ffG�� B�=qG���B�
=G�� B�  G���B�G�� B��HG���B���G�� B�\G���B�
=G�� B��G���B���G�� B��HG���B�ǮG�� B�B�G���B��)G�� B�ǮG���B�Q�G�� B�#�G���B�aHG�� B�=qG���B�#�G�� B�.G�B��G�� B�8RG�ÀB�u�G�� B�8RG�ĀB��G�� B��=G�ŀB��fG�� B��G�ƀB��G�� B��G�ǀB��qG�� B�B�G�ȀB��G�� B���G�ɀB�L�G�� B�p�G�ʀB��3G�� B���G�ˀB���G�� B�#�G�̀B���G�� B��\G�̀B�  G�� B�B�G�΀B��G�� B���G�πB��G�� B��G�ЀB�W
G�� B�L�G�рB��)G�� B�33G�ҀB�{G�� B���G�ӀB�#�G�� B��HG�ԀB�\)G�� B��G�ՀB�\G�� B��G�րB��G�� B�u�G�׀B���G�� B�B�G�؀B�W
G�� B�k�G�ـB�{G�� B�ffG�ڀB��)G�� B��fG�ۀB��RG�� B�z�G�܀B��3G�� B���G�݀B�#�G�� B�G�ހB���G�� B��G�߀B�L�G�� B��G���B���G�� B�=qG��B�8RG�� B���G��B���G�� B�aHG��B���G�� B��RG��B�G�� B���G��B�� G�� B�8RG��B�z�G�� B�u�G��B���G�� B���G��B�8RG�� B�ffG��B�\G�� B�.G��B�.G�� B�(�G��B�\)G�� B�(�G��B�\)G�� B�=qG��B��G�� B�u�G��B�L�G�� B�ǮG��B�L�G�� B�G�G���B�\G�� B�u�G��B��qG�� B��HG��B�(�G�� B�aHG��B��RG�� B�Q�G��B�G�� B�u�G���B���G�� B��G���B��G�� B�G���B�ffG�� B��G���B�#�G�� B��{G���B�#�G�� B��G���B�.G�� B��G���B�aHG�� B�  G���B��G�� B��HG���B�z�G�� B��HG���B�L�G�� B�p�G���B�ǮG�  B�ffG� �B�� G� B�\)G��B�\)G� B�\)G��B��G� B���G��B�ǮG� B�G��B�8RG� B��3G��B�\G� B�(�G��B��G� B��HG��B�aHG� B��qG��B��{G�	 B�\G�	�B�p�G�
 B��G�
�B�u�G� B�\G��B���G� B��\G��B��G� B�G��B�Q�G� B�Q�G��B�� G� B�\)G��B���G� B�aHG��B��G� B��fG��B�{G� B�{G��B�Q�G� B��=G��B�L�G� B���G��B�B�G� B��G��B���G� B��G��B�  G� B�ǮG��B��=G� B�
=G��B���G� B���G��B�8RG� B�B�G��B�u�G� B�u�G��B�=qG� B��)G��B���G� B�8RG��B�8RG� B�B�G��B�u�G� B�G��B�ǮG�  B�\)G� �B�G�! B�W
G�!�B�B�G�" B���G�"�B���G�# B�G�G�#�B�ǮG�$ B�ffG�$�B��G�% B��)G�%�B�=qG�& B�=qG�&�B�8RG�' B��G�'�B�.G�( B�#�G�(�B��\G�) B��{G�)�B�p�G�* B�ffG�*�B���G�+ B��{G�+�B�Q�G�, B�G�,�B��3G�- B��
G�-�B���G�. B��3G�.�B�(�G�/ B��G�/�B�u�G�0 B�33G�0�B�L�G�1 B�Q�G�1�B��RG�2 B���G�2�B���G�3 B�33G�3�B��G�4 B��qG�4�B���G�5 B���G�5�B��\G�6 B�\)G�6�B���G�7 B�
=G�7�B�k�G�8 B��\G�8�B��\G�9 B���G�9�B�
=G�: B�(�G�:�B�Q�G�; B�\G�;�B�L�G�< B��3G�<�B�G�= B��G�=�B��G�> B��G�>�B���G�? B�#�G�?�B�33G�@ B�33G�@�B�
=G�A B��
G�A�B�  G�B B��3G�B�B�\G�C B�B�G�C�B���G�D B�\G�D�B�=qG�E B�u�G�E�B�ffG�F B�(�G�F�B�� G�G B��{G�G�B�{G�H B�k�G�H�B�G�G�I B�k�G�I�B���G�J B��)G�J�B�k�G�K B��HG�K�B�aHG�L B�
=G�L�B��G�M B��G�M�B���G�N B��=G�N�B�� G�O B��G�O�B�G�P B�{G�P�B��G�Q B�=qG�Q�B��G�R B�\)G�R�B�z�G�S B��=G�S�B��HG�T B��G�T�B���G�U B���G�U�B�  G�V B��HG�V�B�� G�W B�ffG�W�B�p�G�X B��G�X�B��G�Y B��=G�Y�B�#�G�Z B���G�Z�B��\G�[ B�{G�[�B��qG�\ B�u�G�\�B�G�G�] B���G�]�B��=G�^ B��fG�^�B�G�_ B��
G�_�B�(�G�` B�{G�`�B�G�a B�ffG�a�B�L�G�b B�B�G�b�B�p�G�c B��
G�c�B��HG�d B�p�G�d�B�B�G�e B��\G�e�B��3G�f B���G�f�B���G�g B�ffG�g�B��G�h B��fG�h�B�z�G�i B�W
G�i�B��G�j B���G�j�B�33G�k B�W
G�k�B���G�l B��3G�l�B��=G�m B�W
G�m�B�p�G�n B���G�n�B�k�G�o B��G�o�B�G�G�p B�33G�p�B�#�G�q B�Q�G�q�B�(�G�r B���G�r�B���G�s B��{G�s�B�\G�t B�u�G�t�B��{G�u B��G�u�B��qG�v B�=qG�v�B�\)G�w B�ffG�w�B�
=G�x B�#�G�x�B��RG�y B�ffG�y�B�=qG�z B�ǮG�z�B�(�G�{ B�\)G�{�B�L�G�| B�z�G�|�B�#�G�} B�(�G�}�B�z�G�~ B�{G�~�B�#�G� B��
G��B���G�� B�{G���B��G�� B�B�G���B��3G�� B���G���B��)G�� B���G���B��
G�� B�Q�G���B���G�� B��RG���B�W
G�� B���G���B��qG�� B��G���B���G�� B��=G���B��{G�� B���G���B�33G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� It#�G�ĀIt#�G�� It#�G�ŀIt#�G�� It#�G�ƀIt#�G�� It#�G�ǀIt#�G�� It#�G�ȀIt#�G�� It#�G�ɀIt#�G�� It#�G�ʀIt#�G�� B��{G�ˀB��)G�� B�\G�̀B��G�� B��G�̀B���G�� B��G�΀B�\)G�� B��3G�πB��G�� B�ffG�ЀB��{G�� B��
G�рB��{G�� B�#�G�ҀB�33G�� B�Q�G�ӀB��=G�� B�\G�ԀB��G�� B�33G�ՀB�W
G�� B�\G�րB��G�� B��G�׀B�#�G�� B��RG�؀B�.G�� B�.G�ـB��G�� B�=qG�ڀB��3G�� B��\G�ۀB��)G�� B��G�܀B�W
G�� B�W
G�݀B�W
G�� B�aHG�ހB���G�� B��HG�߀B��3G�� B�33G���B��RG�� B��RG��B��{G�� B�� G��B��G�� B�G��B��fG�� B�
=G��B��G�� B�W
G��B���G�� B�\)G��B���G�� B��G��B�  G�� B�8RG��B��=G�� B�\)G��B�G�� B��\G��B���G�� B�k�G��B��G�� B�B�G��B���G�� B�  G��B�\)G�� B�p�G��B�G�� B���G��B�k�G�� B�aHG���B�=qG�� B�
=G��B��G�� B��)G��B���G�� B��G��B�k�G�� B�33G��B�8RG�� B�u�G���B�ǮG�� B�#�G���B�B�G�� B��)G���B��=G�� B�{G���B�ffG�� B��G���B�\)G�� B�B�G���B���G�� B�p�G���B��G�� B�ffG���B�\G�� B�z�G���B�8RG�� B��G���B��
G�� B��=G���B���G�  B��qG� �B���G� B��HG��B�ffG� B�8RG��B���G� B���G��B�G�G� B�
=G��B��\G� B���G��B�p�G� B�  G��B��\G� B��HG��B�G�G� B��HG��B��G�	 B���G�	�B��)G�
 B��
G�
�B�G�G� B��qG��B���G� B�33G��B�p�G� B��G��B���G� B�k�G��B��)G� B�G�G��B�
=G� B��3G��B�L�G� B��=G��B�� G� B�W
G��B��=G� B�W
G��B��G� B�=qG��B��G� B���G��B���G� B�L�G��B��G� B�8RG��B��{G� B�B�G��B�G�G� B�\)G��B��3G� B�33G��B��qG� B���G��B�G� B���G��B��
G� B�{G��B�B�G� B�  G��B�Q�G� B��RG��B��qG�  B���G� �B�{G�! B���G�!�B�� G�" B��qG�"�B�u�G�# B�u�G�#�B��)G�$ B�W
G�$�B�z�G�% B�aHG�%�B��G�& B�
=G�&�B���G�' B��=G�'�B�\)G�( B��G�(�B�� G�) B��G�)�B���G�* B��G�*�B�k�G�+ B�G�+�B��G�, B���G�,�B�\G�- B��=G�-�B�
=G�. B��fG�.�B�ffG�/ B��G�/�B��HG�0 B���G�0�B���G�1 B��3G�1�B�aHG�2 B��\G�2�B��
G�3 B���G�3�B�ǮG�4 B��=G�4�B��3G�5 B��qG�5�B��\G�6 B��G�6�B���G�7 B�aHG�7�B�W
G�8 B���G�8�B�\)G�9 B��G�9�B�G�G�: B��G�:�B�u�G�; B���G�;�B��
G�< B���G�<�B���G�= B�\G�=�B�33G�> B���G�>�B�Q�G�? B��G�?�B�B�G�@ B�B�G�@�B�B�G�A B���G�A�B��G�B B���G�B�B�B�G�C B�\G�C�B��\G�D B�ǮG�D�B��=G�E B�u�G�E�B�Q�G�F B���G�F�B�B�G�G B��HG�G�B�  G�H B��\G�H�B�p�G�I B�\G�I�B�z�G�J B��)G�J�B��G�K B��G�K�B���G�L B��fG�L�B���G�M B�ǮG�M�B��qG�N B��=G�N�B�33G�O B�B�G�O�B�(�G�P B�8RG�P�B�z�G�Q B��G�Q�B��3G�R B���G�R�B��qG�S B�p�G�S�B��G�T B���G�T�B�p�G�U B�ǮG�U�B��fG�V B���G�V�B��qG�W B���G�W�B���G�X B��=G�X�B��RG�Y B�aHG�Y�B��G�Z B�k�G�Z�B���G�[ B�  G�[�B�Q�G�\ B��G�\�B�  G�] B���G�]�B�W
G�^ B�z�G�^�B�=qG�_ B���G�_�B�G�` B�aHG�`�B�
=G�a B��{G�a�B��G�b B�L�G�b�B���G�c B��)G�c�B���G�d B�#�G�d�B���G�e B��G�e�B�G�f B�.G�f�B��\G�g B���G�g�B�\)G�h B���G�h�B��HG�i B��G�i�B��=G�j B��G�j�B�=qG�k B��RG�k�B��G�l B�=qG�l�B�8RG�m B��fG�m�B��RG�n B��=G�n�B�  G�o B��=G�o�B�33G�p B�{G�p�B��G�q B�{G�q�B���G�r B��G�r�B�p�G�s B�ffG�s�B�B�G�t B�ǮG�t�B��fG�u B���G�u�B�z�G�v B���G�v�B�
=G�w B�
=G�w�B��G�x B�8RG�x�B���G�y B��G�y�B���G�z B�k�G�z�B�\)G�{ B��G�{�B�G�| B�ǮG�|�B�z�G�} B���G�}�B�(�G�~ B��\G�~�B���G� B�G�G��B�ffG�� B�8RG���B�Q�G�� B�
=G���B�ffG�� B�
=G���B��qG�� B��G���B��)G�� B�ffG���B�
=G�� B�Q�G���B�p�G�� B�z�G���B�33G�� B��G���B�  G�� B�L�G���B��)G�� B��3G���B��G�� B�G���B��
G�� B�  G���B�#�G�� B�#�G���B�Q�G�� B��fG���B�L�G�� B�L�G���B��)G�� B���G���B��G�� B�W
G���B�W
G�� B�.G���B�#�G�� B�Q�G���B�G�G�� B��3G���B��RG�� B��G���B�
=G�� B�#�G���B�\)G�� B�aHG���B�� G�� B��HG���B�G�� B�\G���B��G�� B�aHG���B��G�� B���G���B�.G�� B��G���B�z�G�� B�33G���B�aHG�� B���G���B��G�� B�L�G���B��G�� B���G���B���G�� B�� G���B�#�G�� B�ffG���B��RG�� B��qG���B��G�� B�B�G���B��G�� B�Q�G���B�G�� B��qG���B��G�� B��G���B�u�G�� B��
G���B���G�� B�k�G���B���G�� B��G���B�.G�� B�8RG���B��fG�� B��\G���B�L�G�� B��{G���B�33G�� B��\G���B�B�G�� B�\)G���B��qG�� B��G���B�33G�� B��RG���B��G�� B�aHG���B�G�� B�aHG���B�ǮG�� B�ǮG���B�� G�� B�k�G���B���G�� B�aHG���B���G�� B�ffG���B��G�� B��\G���B��
G�� B��3G���B�8RG�� B�ǮG���B�z�G�� B�G�G���B�Q�G�� B�
=G���B��G�� B���G���B�ffG�� B��G���B�W
G�� B�  G���B���G�� B��G���B��RG�� B��G���B��G�� B��\G���B���G�� B�\G�B�u�G�� B�G�ÀB�G�� B�ffG�ĀB�
=G�� B��\G�ŀB�Q�G�� B�{G�ƀB���G�� B���G�ǀB�#�G�� B�  G�ȀB��RG�� B�33G�ɀB��)G�� B�L�G�ʀB�aHG�� B�#�G�ˀB��fG�� B���G�̀B�ǮG�� B�\G�̀B�
=G�� B�� G�΀B�\)G�� B�aHG�πB��fG�� B���G�ЀB��G�� B�W
G�рB��=G�� B�.G�ҀB�z�G�� B�
=G�ӀB�{G�� B�  G�ԀB��HG�� B�aHG�ՀB��G�� B���G�րB�
=G�� B�u�G�׀B��)G�� B�W
G�؀B�G�� B��G�ـB�#�G�� B��3G�ڀB���G�� B��G�ۀB�G�� B�G�܀B�#�G�� B���G�݀B�G�G�� B���G�ހB�ffG�� B���G�߀B��G�� B�z�G���B�� G�� B�G�G��B�ǮG�� B��G��B�u�G�� B�
=G��B�ǮG�� B�G�G��B��
G�� B��fG��B�=qG�� B�(�G��B�#�G�� B���G��B�k�G�� B�  G��B��HG�� B�G�G��B�=qG�� B�.G��B�33G�� B��G��B�ffG�� B��HG��B��\G�� B���G��B�8RG�� B�  G��B���G�� B�u�G��B��)G�� B�\)G���B�\)G�� B���G��B�  G�� B�u�G��B��G�� B��qG��B��
G�� B�.G��B�\)G�� B��qG���B�=qG�� B��fG���B���G�� B�  G���B�  G�� B�p�G���B��G�� B���G���B��G�� B�{G���B�{G�� B�  G���B�u�G�� B�L�G���B��G�� B��\G���B�(�G�� B�G���B��G�� B���G���B��3G�  B��=G� �B��G� B�k�G��B�z�G� B�(�G��B�Q�G� B��G��B��HG� B��G��B�  G� B��G��B���G� B�G��B�#�G� B��HG��B�� G� B�z�G��B��qG�	 B���G�	�B��\G�
 B���G�
�B���G� B���G��B�G�G� B�k�G��B���G� B�#�G��B���G� B��G��B��\G� B�B�G��B�G�G� B��G��B�
=G� B��)G��B�k�G� B�G��B��RG� B���G��B��=G� B��qG��B�{G� B��3G��B�B�G� B��G��B��
G� B��G��B���G� B�=qG��B���G� B�G��B�aHG� B�L�G��B�L�G� B��RG��B�(�G� B���G��B��G� B�G��B��{G� B�z�G��B��G� B���G��B���G�  B�aHG� �B�p�G�! B��RG�!�B���G�" B���G�"�B�k�G�# B�G�#�B��RG�$ B�ǮG�$�B��G�% B��RG�%�B��RG�& B�ǮG�&�B�W
G�' B��
G�'�B�=qG�( B���G�(�B��=G�) B�33G�)�B�8RG�* B��HG�*�B��G�+ B��G�+�B�.G�, B�\G�,�B��G�- B��G�-�B���G�. B�{G�.�B�z�G�/ B��
G�/�B���G�0 B�ǮG�0�B�ffG�1 B�aHG�1�B�aHG�2 B�33G�2�B�\)G�3 B���G�3�B�.G�4 B�k�G�4�B��HG�5 B��3G�5�B���G�6 B��\G�6�B���G�7 B�\G�7�B���G�8 B�W
G�8�B���G�9 B��RG�9�B��G�: B��{G�:�B��\G�; B��G�;�B�u�G�< B�.G�<�B�B�G�= B��G�=�B��\G�> B��G�>�B�G�G�? B��G�?�B�ffG�@ B�k�G�@�B��G�A B�u�G�A�B��)G�B B��G�B�B���G�C B�{G�C�B��{G�D B�B�G�D�B��G�E B�� G�E�B��G�F B��fG�F�B�B�G�G B�{G�G�B��{G�H B��\G�H�B�ǮG�I B�
=G�I�B��G�J B�Q�G�J�B��G�K B�=qG�K�B�aHG�L B���G�L�B��G�M B�W
G�M�B�aHG�N B�\G�N�B�G�G�O B�=qG�O�B�aHG�P B���G�P�B�G�Q B�G�Q�B��G�R B��G�R�B��3G�S B�z�G�S�B��)G�T B�z�G�T�B�G�U B�
=G�U�B��G�V B�
=G�V�B��qG�W B���G�W�B�W
G�X B�\)G�X�B�p�G�Y B��G�Y�B�ǮG�Z B��{G�Z�B�k�G�[ B�W
G�[�B��3G�\ B��fG�\�B��G�] B�8RG�]�B��G�^ B��G�^�B���G�_ B�u�G�_�B�z�G�` B��)G�`�B�aHG�a B��HG�a�B�{G�b B��=G�b�B�=qG�c B��RG�c�B��RG�d B���G�d�B�p�G�e B���G�e�B���G�f B���G�f�B�
=G�g B��\G�g�B��RG�h B�\)G�h�B�
=G�i B�� G�i�B�B�G�j B���G�j�B�ffG�k B���G�k�B�aHG�l B�=qG�l�B�B�G�m B��HG�m�B�8RG�n B��G�n�B�\)G�o B��
G�o�B��{G�p B���G�p�B��G�q B���G�q�B���G�r B�\)G�r�B�=qG�s B�aHG�s�B���G�t B��G�t�B�G�G�u B���G�u�B�� G�v B�� G�v�B��fG�w B�{G�w�B�W
G�x B���G�x�B��=G�y B�.G�y�B�p�G�z B�B�G�z�B��RG�{ B�p�G�{�B�k�G�| B�=qG�|�B���G�} B��\G�}�B�G�~ B�#�G�~�B��G� B���G��B���G�� B���G���B��RG�� B�8RG���B�G�G�� B���G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G���It#�G�� It#�G�It#�G�� It#�G�ÀIt#�G�� B�#�G�ĀB��qG�� B�8RG�ŀB�\)G�� B��)G�ƀB�  G�� B�k�G�ǀB���G�� B�
=G�ȀB���G�� B���G�ɀB�{G�� B��fG�ʀB�aHG�� B���G�ˀB�\)G�� B��G�̀B�G�� B�k�G�̀B�p�G�� B�p�G�΀B�.G�� B�33G�πB��G�� B�B�G�ЀB��
G�� B��G�рB�z�G�� B�L�G�ҀB�k�G�� B��G�ӀB�(�G�� B��3G�ԀB�.G�� B�\)G�ՀB�(�G�� B��fG�րB��\G�� B��G�׀B��G�� B��RG�؀B��fG�� B�\)G�ـB���G�� B�aHG�ڀB�G�� B��G�ۀB��G�� B�8RG�܀B��G�� B��)G�݀B�  G�� B���G�ހB��qG�� B��RG�߀B��{G�� B�  G���B�=qG�� B�ǮG��B�33G�� B�L�G��B��3G�� B���G��B�#�G�� B�Q�G��B�8RG�� B�#�G��B��3G�� B�8RG��B�33G�� B���G��B���G�� B��qG��B�z�G�� B�  G��B�Q�G�� B��
G��B�ǮG�� B��G��B�L�G�� B�� G��B��G�� B�(�G��B��fG�� B���G��B�ffG�� B�k�G��B��G�� B��G���B��G�� B�=qG��B��fG�� B���G��B��RG�� B�k�G��B��G�� B�B�G��B���G�� B�ffG���B�Q�G�� B�W
G���B��G�� B���G���B��G�� B�G���B���G�� B���G���B�.G�� B�{G���B�� G�� B���G���B�ǮG�� B�{G���B��=G�� B�33G���B��G�� B���G���B�� G�� B���G���B�8RG�  B��qG� �B��G� B�G��B���G� B��=G��B�ffG� B���G��B��G� B�\)G��B��
G� B�#�G��B�.G� B���G��B��G� B��G��B�\G� B���G��B�k�G�	 B�{G�	�B�  G�
 B�k�G�
�B�W
G� B�\G��B�ffG� B��qG��B�8RG� B��RG��B�k�G� B�u�G��B��=G� B��RG��B��{G� B��G��B��G� B��G��B�33G� B��)G��B��3G� B���G��B��G� B��qG��B�L�G� B�aHG��B���G� B�=qG��B��G� B�Q�G��B���G� B��G��B���G� B���G��B��
G� B�8RG��B���G� B��RG��B��G� B�L�G��B�L�G� B�G�G��B��)G� B���G��B�k�G� B��G��B�ffG�  B�ffG� �B�� G�! B�u�G�!�B���G�" B��G�"�B�G�# B���G�#�B���G�$ B�.G�$�B�aHG�% B��qG�%�B��{G�& B���G�&�B�\)G�' B���G�'�B�ffG�( B�\G�(�B�
=G�) B��)G�)�B�Q�G�* B�G�*�B�33G�+ B��HG�+�B�#�G�, B��G�,�B�ffG�- B�� G�-�B�u�G�. B�z�G�.�B��\G�/ B��G�/�B�aHG�0 B�G�0�B���G�1 B��{G�1�B�k�G�2 B�\G�2�B�u�G�3 B���G�3�B�33G�4 B���G�4�B�B�G�5 B���G�5�B�#�G�6 B�#�G�6�B��RG�7 B�aHG�7�B�G�8 B�(�G�8�B�
=G�9 B��G�9�B��G�: B�33G�:�B���G�; B�{G�;�B��)G�< B�k�G�<�B��3G�= B�#�G�=�B�#�G�> B�W
G�>�B��{G�? B�ǮG�?�B�{G�@ B�ffG�@�B��)G�A B�{G�A�B���G�B B���G�B�B�� G�C B�W
G�C�B��HG�D B�8RG�D�B��G�E B�Q�G�E�B�\)G�F B�(�G�F�B�z�G�G B�#�G�G�B�z�G�H B��G�H�B�� G�I B�p�G�I�B��)G�J B��G�J�B�p�G�K B�G�K�B��3G�L B��)G�L�B�W
G�M B�.G�M�B�33G�N B�G�N�B�aHG�O B�=qG�O�B�p�G�P B�{G�P�B�33G�Q B�aHG�Q�B�  G�R B�k�G�R�B��
G�S B�u�G�S�B�G�T B�p�G�T�B�(�G�U B�  G�U�B�=qG�V B��
G�V�B��G�W B�#�G�W�B���G�X B��\G�X�B�\G�Y B��fG�Y�B�33G�Z B�p�G�Z�B�z�G�[ B���G�[�B��
G�\ B�� G�\�B��G�] B��G�]�B�� G�^ B���G�^�B�  G�_ B��G�_�B��3G�` B�� G�`�B�=qG�a B��)G�a�B�z�G�b B��G�b�B��fG�c B��G�c�B���G�d B���G�d�B��G�e B�ffG�e�B�G�f B�G�f�B�k�G�g B��
G�g�B���G�h B�33G�h�B��G�i B�G�G�i�B��G�j B���G�j�B���G�k B��HG�k�B�� G�l B�33G�l�B��3G�m B�33G�m�B�aHG�n B��G�n�B�(�G�o B�.G�o�B�aHG�p B�{G�p�B��HG�q B�33G�q�B�u�G�r B��RG�r�B�aHG�s B��G�s�B���G�t B�\)G�t�B�=qG�u B�
=G�u�B���G�v B�(�G�v�B���G�w B�\G�w�B���G�x B�k�G�x�B�z�G�y B�aHG�y�B�ffG�z B�p�G�z�B�G�{ B�� G�{�B��HG�| B�(�G�|�B���G�} B��\G�}�B�\)G�~ B��fG�~�B�B�G� B���G��B�u�G B�ffG�B��{G B�W
G�B���G B�
=G�B�{G B��G�B�ǮG B�(�G�B��HG B�\)G�B���G B�p�G�B���G B�(�G�B�u�G B�G�G�B��\G B�33G�B��\G B�ǮG�B���G B�  G�B��\G B�.G�B��
G B�� G�B��HG B�ǮG�B��RG B���G�B�k�G B��G�B��{G B�  G�B�Q�G B���G�B�  G B���G�B�u�G B���G�B��=G B��)G�B��3G B��fG�B��RG B�G�G�B�ffG B��fG�B�{G B�#�G�B�.G B���G�B���G B��RG�B���G B�=qG�B��G B�z�G�B��G B���G�B�G B�.G�B��=G  B���G �B��qG¡ B�33G¡�B�G�G¢ B��3G¢�B���G£ B�aHG£�B�=qG¤ B�33G¤�B��G¥ B�aHG¥�B��G¦ B�L�G¦�B���G§ B��3G§�B��G¨ B���G¨�B�G© B��{G©�B���Gª B�u�Gª�B��RG« B��3G«�B�p�G¬ B�Q�G¬�B�G­ B�#�G­�B��G® B���G®�B���G¯ B�  G¯�B���G° B��RG°�B�
=G± B�Q�G±�B��RG² B��G²�B�ǮG³ B�B�G³�B���G´ B��RG´�B��HGµ B���Gµ�B�ffG¶ B�W
G¶�B�Q�G· B�u�G·�B��=G¸ B�� G¸�B�L�G¹ B���G¹�B�� Gº B�p�Gº�B�  G» B�(�G»�B�G�G¼ B��{G¼�B�ǮG½ B���G½�B���G¾ B�#�G¾�B�
=G¿ B�L�G¿�B�L�G�� B��G���B�{G�� B��RG���B��G�� B��G�B���G�� B��G�ÀB�ffG�� B�\)G�ĀB�\)G�� B��G�ŀB��G�� B�ǮG�ƀB�u�G�� B�B�G�ǀB�B�G�� B�W
G�ȀB��G�� B��)G�ɀB���G�� B���G�ʀB�aHG�� B�u�G�ˀB���G�� B�p�G�̀B�#�G�� B���G�̀B��{G�� B��\G�΀B��fG�� B�L�G�πB��G�� B�.G�ЀB���G�� B���G�рB��qG�� B��G�ҀB�{G�� B���G�ӀB�(�G�� B���G�ԀB��qG�� B���G�ՀB�#�G�� B�ffG�րB���G�� B�z�G�׀B���G�� B�u�G�؀B�� G�� B��G�ـB�(�G�� B�=qG�ڀB��fG�� B�k�G�ۀB��fG�� B���G�܀B�G�� B���G�݀B���G�� B�Q�G�ހB��\G�� B�(�G�߀B���G�� B��HG���B�33G�� B�B�G��B���G�� B��G��B�(�G�� B���G��B�ǮG�� B��G��B�(�G�� B�k�G��B�#�G�� B���G��B�W
G�� B�  G��B�33G�� B�(�G��B��=G�� B���G��B�G�� B��{G��B��\G�� B��G��B�aHG�� B�8RG��B���G�� B��qG��B�(�G�� B���G��B��G�� B�B�G��B��G�� B��3G���B�W
G�� B�8RG��B�G�� B��G��B���G�� B�B�G��B���G�� B�  G��B���G�� B���G���B��G�� B���G���B��)G�� B��RG���B�8RG�� B�\)G���B�B�G�� B�=qG���B�=qG�� B�p�G���B��G�� B��fG���B�{G�� B�  G���B�G�G�� B�Q�G���B��\G�� B�L�G���B�aHG�� B�z�G���B��G�  B�B�G� �B�\G� B�G��B�#�G� B�#�G��B�L�G� BkffG��Bh(�G� Bx
=G��By�\G� B}z�G��Bx
=G� B��RG��B��fG� B~(�G��By�G� B{{G��B|33G�	 Bz��G�	�B{G�G�
 B{�G�
�B|��G� B��G��B~Q�G� Bvz�G��Bp(�G� Bx33G��B��
G� B  G��Bw=qG� BwQ�G��Bw�G� Bs�G��Bx��G� B�G��B|��G� B|
=G��Bv�
G� Bu�G��By�G� ByffG��Bv��G� Bx\)G��Bz�G� Bvz�G��Bx�HG� B�G��B��qG� B{��G��Bt��G� B�HG��B���G� B|��G��B}�G� B�z�G��B�ǮG� Bz(�G��Bw(�G� Bx\)G��B��RG� B}��G��By�HG� By�\G��B|\)G�  B�p�G� �B�G�! B���G�!�Bx�G�" BvQ�G�"�Bvz�G�# Bv��G�#�Bv�G�$ Br
=G�$�Bw�G�% B\)G�%�By��G�& B{�G�&�B��G�' B���G�'�B�B�G�( B�8RG�(�B~z�G�) B{��G�)�B~�G�* B�
=G�*�B}z�G�+ B~(�G�+�B�ffG�, B��G�,�B(�G�- B}Q�G�-�B(�G�. B}z�G�.�B}�HG�/ Bx��G�/�B��G�0 B��)G�0�B���G�1 B�  G�1�B��\G�2 Bu�HG�2�B}z�G�3 B�ffG�3�B��G�4 B�\)G�4�B��qG�5 B�8RG�5�B|�G�6 B��3G�6�Bp�G�7 B{Q�G�7�B~z�G�8 B|��G�8�By�RG�9 B~�
G�9�B�G�: B|��G�:�B��fG�; B���G�;�Bz�RG�< By�HG�<�B�p�G�= B�  G�=�B|��G�> Bx�HG�>�B~(�G�? B{�G�?�Bu��G�@ B��G�@�B�\)G�A B�aHG�A�B���G�B B}��G�B�B|�G�C B���G�C�B~(�G�D B|��G�D�B�{G�E B�p�G�E�B�G�F B|��G�F�B|{G�G B~�G�G�B|{G�H Bv33G�H�B}�G�I B��3G�I�B�.G�J B|G�G�J�B�G�K B�ǮG�K�B���G�L B��RG�L�B��G�M B���G�M�B�G�N B��fG�N�B~��G�O B  G�O�Bz��G�P B}\)G�P�B�\G�Q Bx��G�Q�Bx�G�R B��3G�R�B�
=G�S B��qG�S�B��qG�T B�(�G�T�B~�RG�U Byp�G�U�B{ffG�V B�33G�V�B��=G�W Bz�RG�W�By�HG�X B  G�X�B��
G�Y B�8RG�Y�Bs=qG�Z Bz�RG�Z�B�Q�G�[ B���G�[�B~z�G�\ B��RG�\�B���G�] B}�G�]�B|G�G�^ B}�G�^�B~�G�_ B�B�G�_�B�=qG�` B��\G�`�B��qG�a B��)G�a�B��G�b B��RG�b�B�
=G�c B��3G�c�B�33G�d B�� G�d�B��fG�e B  G�e�B~�\G�f B�ǮG�f�B|��G�g B���G�g�B�8RG�h B��G�h�B{ffG�i B�aHG�i�B�
=G�j B��)G�j�BzG�G�k B|��G�k�B��HG�l B=qG�l�By33G�m B}�G�m�B��qG�n B~ffG�n�B|�G�o B�
=G�o�B��G�p B�W
G�p�Bx�G�q Bz��G�q�B���G�r B�u�G�r�B�\G�s B���G�s�B��G�t B�G�t�B���G�u B�ffG�u�B���G�v B�=qG�v�B���G�w B{�\G�w�Bz�G�x B|�G�x�By�G�y Bz�\G�y�By33G�z Bp(�G�z�Bz33G�{ B�z�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�GÀ It#�GÀ�It#�GÁ It#�GÁ�It#�GÂ It#�GÂ�It#�GÃ It#�GÃ�It#�GÄ It#�GÄ�It#�GÅ It#�GÅ�It#�GÆ It#�GÆ�It#�GÇ It#�GÇ�It#�GÈ It#�GÈ�It#�GÉ It#�GÉ�It#�GÊ It#�GÊ�It#�GË It#�GË�It#�GÌ It#�GÌ�It#�GÍ It#�GÍ�It#�GÎ It#�GÎ�It#�GÏ It#�GÏ�It#�GÐ It#�GÐ�It#�GÑ It#�GÑ�It#�GÒ It#�GÒ�It#�GÓ It#�GÓ�It#�GÔ It#�GÔ�It#�GÕ It#�GÕ�It#�GÖ It#�GÖ�It#�G× It#�G×�It#�GØ It#�GØ�It#�GÙ It#�GÙ�It#�GÚ It#�GÚ�It#�GÛ It#�GÛ�It#�GÜ It#�GÜ�It#�GÝ It#�GÝ�It#�GÞ It#�GÞ�It#�Gß It#�Gß�It#�Gà It#�Gà�It#�Gá It#�Gá�It#�Gâ It#�Gâ�It#�Gã It#�Gã�It#�Gä It#�Gä�It#�Gå It#�Gå�It#�Gæ It#�Gæ�It#�Gç It#�Gç�It#�Gè It#�Gè�It#�Gé It#�Gé�It#�Gê It#�Gê�It#�Gë It#�Gë�It#�Gì It#�Gì�It#�Gí It#�Gí�It#�Gî It#�Gî�It#�Gï It#�Gï�It#�Gð It#�Gð�It#�Gñ It#�Gñ�It#�Gò It#�Gò�It#�Gó It#�Gó�It#�Gô It#�Gô�It#�Gõ It#�Gõ�It#�Gö It#�Gö�It#�G÷ It#�G÷�It#�Gø It#�Gø�It#�Gù It#�Gù�It#�Gú It#�Gú�It#�Gû It#�Gû�It#�Gü It#�Gü�It#�Gý B��{Gý�B��RGþ B�Gþ�B���Gÿ BzG�Gÿ�By{G�� By��G���B��G�� B�z�G���B�G�G�� ByG�B~�RG�� B��G�ÀBzp�G�� B|ffG�ĀB�8RG�� B}�G�ŀBw�G�� B{��G�ƀB~\)G�� B~33G�ǀBz�HG�� Bx��G�ȀB���G�� B�{G�ɀB��3G�� B���G�ʀB���G�� B��3G�ˀB�p�G�� B~p�G�̀B�33G�� B��3G�̀B���G�� B��\G�΀B�#�G�� B�33G�� B|��G�πB�{G�� B�
=G�ЀB��G�� B�{G�рB�33G�� B��qG�ҀB�8RG�� B|�\G�ӀBx�G�� B��HG�ԀB�\G�� B�k�G�ՀB��\G�� B��qG�րB��G�� B~33G�׀B}(�G�� B�u�G�؀B�z�G�� B{G�G�ـB|(�G�� B}p�G�ڀB�33G�� B�33G�ۀBr�G�� Bq=qG�܀BvG�� B��\G�݀B�\G�� B}\)G�ހB�u�G�� B{�\G�߀B{�RG�� Bz�G���Bv\)G�� B}  G��B�W
G�� B�p�G��B~G�G�� Bz  G��B~��G�� Bw�G��BwG�G�� Bx�RG��B{z�G�� B�  G��B���G�� B�\G��B~
=G�� B�� G��B���G�� B{z�G��B�
=G�� B��{G��B�G�� B~�RG��B}�
G�� B�ffG��Bz�G�� Bv�
G��BsG�G�� Bnz�G��Bt��G�� B�RG��B���G�� Bw�RG���BnQ�G�� B|��G��B�W
G�� Bv�
G��Bw�RG�� B�
=G��B��G�� B
=G��B|�
G�� B���G���B��qG�� B�aHG���B}(�G�� B��G���B}G�� Bx
=G���B�G�� B�ǮG���B�33G�� B�.G���B��G�� ByG���B~\)G�� B��G���Bz{G�� B{z�G���B}p�G�� B|G���B
=G�� B{�RG���Bx�\G�  BffG� �B�\G� By(�G��B}=qG� B���G��B}�G� Bu��G��Bw\)G� BzG��B|�G� BzG�G��B
=G� BffG��B{G�G� B|�\G��Bv(�G� B�L�G��B���G�	 B~�G�	�B�
=G�
 B���G�
�B~G�G� B{\)G��Bv  G� BwG�G��B|z�G� Bvp�G��BsG�G� B{G�G��B�\G� Bx33G��BzG� B33G��B��G� B}  G��B�Q�G� B�{G��B��G� B���G��Bwp�G� Bw��G��B~�G� B��G��B��qG� B}(�G��Bv�G� B{z�G��Bu��G� Buz�G��B�=qG� Bz{G��By�G� B�#�G��B}(�G� Bx�RG��B{z�G� B}�G��Bz��G� B�(�G��Bz�G� B�L�G��B�k�G� B�  G��B���G�  B}G� �B��fG�! B{��G�!�By{G�" B��G�"�B���G�# Bz��G�#�BuQ�G�$ BxffG�$�B��G�% B�(�G�%�B��=G�& B}�G�&�Bz�G�' B��
G�'�B�#�G�( B�k�G�(�B
=G�) B~�G�)�B��G�* B{��G�*�B}(�G�+ B��G�+�B�33G�, B��
G�,�B�k�G�- B��=G�-�Bv�
G�. B���G�.�B�  G�/ B�B�G�/�B��HG�0 B��HG�0�B�(�G�1 B�G�G�1�B���G�2 B��RG�2�B���G�3 B��
G�3�B�B�G�4 B�ffG�4�B�L�G�5 B�ǮG�5�B�ǮG�6 B�z�G�6�B���G�7 B��G�7�B��3G�8 B�Q�G�8�B�.G�9 B�p�G�9�B���G�: B��3G�:�B�#�G�; B��RG�;�B���G�< B�#�G�<�B�ffG�= B��3G�=�B�� G�> B���G�>�B���G�? B��{G�?�B���G�@ B��
G�@�B�L�G�A B�G�A�B��3G�B B�� G�B�B�  G�C B�W
G�C�B��{G�D B��
G�D�B��G�E B��=G�E�B�� G�F B���G�F�B�L�G�G B��)G�G�B��)G�H B�\)G�H�B��G�I B�G�I�B���G�J B�\G�J�B��RG�K B�{G�K�B�  G�L B�ǮG�L�B�  G�M B�G�M�B�� G�N B��
G�N�B�k�G�O B���G�O�B���G�P B���G�P�B�  G�Q B���G�Q�B��=G�R B�u�G�R�B���G�S B�=qG�S�B�G�G�T B��\G�T�B�Q�G�U B��G�U�B�ffG�V B�  G�V�B��\G�W B���G�W�B�#�G�X B���G�X�B��qG�Y B�L�G�Y�B��RG�Z B�p�G�Z�B�� G�[ B��G�[�B�aHG�\ B�
=G�\�B��qG�] B���G�]�B���G�^ B���G�^�B��HG�_ B�\G�_�B�8RG�` B���G�`�B�=qG�a B��G�a�B�  G�b B�p�G�b�B�ǮG�c B�.G�c�B�� G�d B��RG�d�BQ�G�e BQ�G�e�B�k�G�f B�(�G�f�B�.G�g B�W
G�g�B�L�G�h B���G�h�B�\G�i B�.G�i�B��HG�j B�ǮG�j�B���G�k B��RG�k�B�.G�l B�{G�l�B�(�G�m B��fG�m�B���G�n B��fG�n�B��)G�o B�k�G�o�B�p�G�p B��)G�p�B�L�G�q B�B�G�q�B�ffG�r B���G�r�B���G�s B��G�s�B��G�t B�aHG�t�B��\G�u B�ǮG�u�B��3G�v B�k�G�v�B���G�w B���G�w�B�L�G�x B�z�G�x�B�G�y B�k�G�y�B�=qG�z B��fG�z�B��\G�{ B�=qG�{�B���G�| B�aHG�|�B��\G�} B�\G�}�B�u�G�~ B���G�~�B�B�G� B�ǮG��B���GĀ B���GĀ�B�L�Gā B��3Gā�B�\)GĂ B���GĂ�B�(�Gă B�(�Gă�B�=qGĄ B�� GĄ�B�z�Gą B�\)Gą�B��fGĆ B�8RGĆ�B�{Gć B��
Gć�B���GĈ B���GĈ�B��Gĉ B�p�Gĉ�B�
=GĊ B���GĊ�B�ǮGċ B���Gċ�B�\)GČ B�8RGČ�B�L�Gč B�z�Gč�B�.GĎ B�u�GĎ�B��Gď B���Gď�B�u�GĐ B�#�GĐ�B�ffGđ B�z�Gđ�B�u�GĒ B��RGĒ�B��=Gē B��3Gē�B�=qGĔ B�� GĔ�B��\Gĕ B��=Gĕ�B�z�GĖ B�Q�GĖ�B��Gė B���Gė�B���GĘ B�#�GĘ�B�\)Gę B��3Gę�B�z�GĚ B�(�GĚ�B���Gě B���Gě�B�ǮGĜ B�\)GĜ�B��
Gĝ B��Gĝ�B�k�GĞ B�GĞ�B���Gğ B�L�Gğ�B�� GĠ B���GĠ�B��Gġ B�(�Gġ�B�k�GĢ B�ǮGĢ�B���Gģ B���Gģ�B�\GĤ B�p�GĤ�B��Gĥ B�aHGĥ�B�(�GĦ B��fGĦ�B�(�Għ B���Għ�B���GĨ B���GĨ�B�aHGĩ B���Gĩ�B�  GĪ B�GĪ�B��3Gī B�33Gī�B���GĬ B�aHGĬ�B���Gĭ B���Gĭ�B�{GĮ B���GĮ�B���Gį B���Gį�B���Gİ B��Gİ�B��fGı B�.Gı�B��GĲ B�G�GĲ�B�33Gĳ B���Gĳ�B�u�GĴ B��GĴ�B��=Gĵ B�W
Gĵ�B�.GĶ B��
GĶ�B�ffGķ B�33Gķ�B��Gĸ B�Gĸ�B���GĹ B�W
GĹ�B���Gĺ B�Gĺ�B���GĻ B��)GĻ�B��)Gļ B�L�Gļ�B�k�GĽ B��{GĽ�B��Gľ B�Q�Gľ�B��{GĿ B�ǮGĿ�B��G�� B�u�G���B�\)G�� B�G���B�G�� B�  G�B��G�� B�=qG�ÀB�G�� B���G�ĀB�B�G�� B�  G�ŀB�
=G�� B���G�ƀB�#�G�� B�W
G�ǀB�\G�� B�=qG�ȀB��HG�� B�ǮG�ɀB��qG�� B�33G�ʀB�� G�� B�ǮG�ˀB�G�� B�\)G�̀B�G�� B��G�̀B��G�� B�L�G�΀B�G�� B��=G�πB�� G�� B�ffG�ЀB�.G�� B�
=G�рB�G�� B�(�G�ҀB�8RG�� B�u�G�ӀB�ffG�� B�Q�G�ԀB�� G�� B�k�G�ՀB��G�� B�ffG�րB��G�� B�{G�׀B�\G�� B�aHG�؀B�Q�G�� B�
=G�ـB��
G�� B�(�G�ڀB���G�� B���G�ۀB���G�� B��)G�܀B���G�� B�G�݀B��\G�� B��)G�ހB�aHG�� B��
G�߀B�� G�� B�ffG���B�(�G�� B�.G��B�G�G�� B��G��B��=G�� B�G�G��B��G�� B���G��B�8RG�� B��HG��B�k�G�� B�{G��B��RG�� B���G��B�33G�� B��G��B�8RG�� B�� G��B��G�� B�k�G��B���G�� B�  G��B��G�� B�G�G��B��HG�� B�{G��B�ffG�� B�(�G��B�#�G�� B�#�G��B�Q�G�� B��G���B�8RG�� B��RG��B�k�G�� B�� G��B�W
G�� B��qG��B��G�� B�k�G��B��G�� B��G���B�B�G�� B�(�G���B�=qG�� B��qG���B��HG�� B��{G���B�ǮG�� B���G���B�ffG�� B�ffG���B��=G�� B�8RG���B���G�� B���G���B�L�G�� B��3G���B�  G�� B�G�G���B�  G�� B��{G���B�z�G�  B�� G� �B���G� B���G��B��G� B���G��B�(�G� B���G��B�{G� B��G��B���G� B�W
G��B��G� B��HG��B�L�G� B�p�G��B��G� B�#�G��B�{G�	 B�� G�	�B�\G�
 B��G�
�B��\G� B�(�G��B�ffG� B��qG��B�33G� B��3G��B�z�G� B���G��B���G� B��=G��B�z�G� B�p�G��B���G� B���G��B�ǮG� B��G��B��=G� B�ffG��B��3G� B�aHG��B���G� B�aHG��B��G� B���G��B�=qG� B�Q�G��B��G� B�
=G��B�  G� B���G��B��G� B���G��B��qG� B�
=G��B��)G� B�{G��B�B�G� B��G��B�u�G� B��)G��B�� G� B��G��B��3G�  B�aHG� �B��G�! B�ǮG�!�B���G�" B��qG�"�B��
G�# B�Q�G�#�B��G�$ B���G�$�B��G�% B�.G�%�B�
=G�& B�=qG�&�B���G�' B��qG�'�B�{G�( B�ǮG�(�B�ffG�) B�ǮG�)�B���G�* B�  G�*�B���G�+ B��qG�+�B�=qG�, B��=G�,�B���G�- B��G�-�B��G�. B���G�.�B��fG�/ B�.G�/�B��
G�0 B��RG�0�B���G�1 B��qG�1�B��=G�2 B��3G�2�B���G�3 B�33G�3�B�(�G�4 B�W
G�4�B��qG�5 B�#�G�5�B�L�G�6 B���G�6�B�=qG�7 B�ǮG�7�B�B�G�8 B�=qG�8�B��RG�9 B�(�G�9�B�  G�: B��G�:�B�ffG�; B�33G�;�B�8RG�< B���G�<�B�
=G�= B�33G�=�B�
=G�> B��G�>�B�u�G�? B��)G�?�B��
G�@ B�p�G�@�B��G�A B��RG�A�B��)G�B B���G�B�B�\G�C B��)G�C�B�Q�G�D B��\G�D�B�\G�E B�\G�E�B��)G�F B��)G�F�B���G�G B�W
G�G�B��fG�H B���G�H�B��\G�I B�aHG�I�B�=qG�J B��G�J�B�ǮG�K B�L�G�K�B��3G�L B���G�L�B��fG�M B�G�M�B���G�N B��qG�N�B��G�O B�W
G�O�B�8RG�P B�L�G�P�B��G�Q B�8RG�Q�B��RG�R B��RG�R�B�8RG�S B�.G�S�B�k�G�T B��G�T�B�B�G�U B��
G�U�B��)G�V B�u�G�V�B���G�W B���G�W�B�k�G�X B�Q�G�X�B���G�Y B��qG�Y�B�k�G�Z B�k�G�Z�B�.G�[ B�G�G�[�B�G�\ B���G�\�B�G�] B�u�G�]�B�aHG�^ B�\G�^�B��
G�_ B��G�_�B�u�G�` B��RG�`�B�aHG�a B��G�a�B��3G�b B�L�G�b�B���G�c B�.G�c�B��
G�d B��G�d�B�ǮG�e B��
G�e�B���G�f B�G�f�B��G�g B���G�g�B���G�h B�\)G�h�B���G�i B�aHG�i�B��qG�j B�.G�j�B�Q�G�k B�\G�k�B��\G�l B�G�G�l�B��G�m B�8RG�m�B���G�n B��G�n�B�ǮG�o B��3G�o�B�#�G�p B��{G�p�B��fG�q B�W
G�q�B��G�r B�G�G�r�B���G�s B�p�G�s�B�.G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�Gŀ It#�Gŀ�It#�GŁ It#�GŁ�It#�Gł It#�Gł�It#�GŃ It#�GŃ�It#�Gń It#�Gń�It#�GŅ It#�GŅ�It#�Gņ It#�Gņ�It#�GŇ It#�GŇ�It#�Gň It#�Gň�It#�Gŉ It#�Gŉ�It#�GŊ It#�GŊ�It#�Gŋ It#�Gŋ�It#�GŌ It#�GŌ�It#�Gō It#�Gō�It#�GŎ It#�GŎ�It#�Gŏ It#�Gŏ�It#�GŐ It#�GŐ�It#�Gő It#�Gő�It#�GŒ It#�GŒ�It#�Gœ It#�Gœ�It#�GŔ It#�GŔ�It#�Gŕ It#�Gŕ�It#�GŖ It#�GŖ�It#�Gŗ It#�Gŗ�It#�GŘ It#�GŘ�It#�Gř It#�Gř�It#�GŚ It#�GŚ�It#�Gś It#�Gś�It#�GŜ It#�GŜ�It#�Gŝ It#�Gŝ�It#�GŞ It#�GŞ�It#�Gş It#�Gş�It#�GŠ It#�GŠ�It#�Gš It#�Gš�It#�GŢ It#�GŢ�It#�Gţ It#�Gţ�It#�GŤ It#�GŤ�It#�Gť It#�Gť�It#�GŦ It#�GŦ�It#�Gŧ It#�Gŧ�It#�GŨ It#�GŨ�It#�Gũ It#�Gũ�It#�GŪ It#�GŪ�It#�Gū It#�Gū�It#�GŬ It#�GŬ�It#�Gŭ It#�Gŭ�It#�GŮ It#�GŮ�It#�Gů It#�Gů�It#�GŰ It#�GŰ�It#�Gű It#�Gű�It#�GŲ It#�GŲ�It#�Gų It#�Gų�It#�GŴ It#�GŴ�It#�Gŵ It#�Gŵ�B�ffGŶ B���GŶ�B���Gŷ B���Gŷ�B��GŸ B�B�GŸ�B��qGŹ B��GŹ�B��Gź B�Gź�B�L�GŻ B�ffGŻ�B��qGż B��RGż�B��GŽ B�\)GŽ�B��HGž B�ffGž�B��HGſ B���Gſ�B���G�� B�L�G���B��G�� B�  G���B�\)G�� B���G�B��G�� B���G�ÀB���G�� B��G�ĀB���G�� B�33G�ŀB��)G�� B�(�G�ƀB�\)G�� B���G�ǀB�(�G�� B�G�ȀB�  G�� B��RG�ɀB�33G�� B��=G�ʀB�G�� B�L�G�ˀB�33G�� B��HG�̀B��\G�� B�\)G�̀B�ffG�� B��HG�΀B�.G�� B�k�G�πB�ffG�� B�W
G�ЀB���G�� B�#�G�рB�.G�� B�u�G�ҀB���G�� B��fG�ӀB���G�� B�8RG�ԀB�ffG�� B��RG�ՀB�{G�� B�33G�րB�=qG�� B�\G�׀B�G�� B�.G�؀B��HG�� B�ffG�ـB�G�� B�p�G�ڀB��G�� B��=G�ۀB���G�� B�L�G�܀B��{G�� B��qG�݀B�33G�� B���G�ހB�\)G�� B�z�G�߀B��HG�� B�(�G���B��G�� B��G��B�z�G�� B�u�G��B��G�� B�u�G��B���G�� B��G��B���G�� B�33G��B�{G�� B�8RG��B��qG�� B�Q�G��B��
G�� B�=qG��B�G�G�� B�W
G��B�8RG�� B�ffG��B�k�G�� B��
G��B�G�� B��fG��B��G�� B�\G��B�� G�� B�=qG��B�8RG�� B�8RG��B�G�� B��HG���B�ǮG�� B��qG��B�z�G�� B��HG��B�\G�� B�L�G��B���G�� B�B�G��B�Q�G�� B��{G���B�ǮG�� B�(�G���B��G�� B��G���B��
G�� B��G���B���G�� B�z�G���B�k�G�� B�p�G���B��G�� B��qG���B���G�� B�L�G���B�(�G�� B���G���B�B�G�� B��{G���B���G�� B�\)G���B��G�  B�z�G� �B��G� B�.G��B��G� B���G��B�Q�G� B�#�G��B�8RG� B��G��B���G� B�k�G��B�p�G� B�ffG��B�L�G� B�ffG��B�G� B�Q�G��B�.G�	 B�B�G�	�B���G�
 B�(�G�
�B�� G� B��3G��B�k�G� B�33G��B�33G� B�8RG��B�(�G� B��G��B�G� B�� G��B��\G� B��RG��B�8RG� B�(�G��B��{G� B���G��B��G� B�p�G��B��qG� B���G��B�\G� B��=G��B�ǮG� B��)G��B�\)G� B�\G��B���G� B�k�G��B�=qG� B�#�G��B�p�G� B��G��B��HG� B��HG��B�Q�G� B�z�G��B��RG� B�33G��B��{G� B��{G��B��=G� B�  G��B��3G�  B�33G� �B�\)G�! B�8RG�!�B��
G�" B��G�"�B�p�G�# B��G�#�B��G�$ B��)G�$�B���G�% B�\)G�%�B�33G�& B�� G�&�B�.G�' B�� G�'�B��\G�( B���G�(�B�{G�) B�B�G�)�B��G�* B�W
G�*�B��G�+ B��HG�+�B�G�G�, B���G�,�B�k�G�- B�G�-�B�� G�. B��G�.�B�aHG�/ B�Q�G�/�B�=qG�0 B�
=G�0�B��G�1 B��qG�1�B��
G�2 B��
G�2�B��RG�3 B��G�3�B��qG�4 B�{G�4�B�u�G�5 B�� G�5�B��G�6 B�(�G�6�B��=G�7 B���G�7�B��fG�8 B��G�8�B�\)G�9 B��G�9�B���G�: B���G�:�B���G�; B��\G�;�B��
G�< B��G�<�B��3G�= B���G�=�B��G�> B��G�>�B�aHG�? B��
G�?�B��qG�@ B�L�G�@�B�B�G�A B�(�G�A�B�z�G�B B��
G�B�B�\G�C B�ffG�C�B��3G�D B��G�D�B�ffG�E B��HG�E�B�z�G�F B��3G�F�B�W
G�G B��{G�G�B��RG�H B��fG�H�B��G�I B�  G�I�B��3G�J B���G�J�B���G�K B��=G�K�B���G�L B�(�G�L�B�  G�M B��G�M�B��G�N B�8RG�N�B��3G�O B�8RG�O�B�Q�G�P B�L�G�P�B�z�G�Q B�33G�Q�B���G�R B��3G�R�B�k�G�S B�\G�S�B�.G�T B�=qG�T�B�33G�U B�G�G�U�B�k�G�V B�k�G�V�B���G�W B�aHG�W�B���G�X B���G�X�B�p�G�Y B���G�Y�B�k�G�Z B�ǮG�Z�B��G�[ B�Q�G�[�B���G�\ B�G�\�B�B�G�] B��)G�]�B�.G�^ B���G�^�B�(�G�_ B��RG�_�B��RG�` B��{G�`�B�aHG�a B�{G�a�B��
G�b B���G�b�B�33G�c B��RG�c�B�aHG�d B��G�d�B��3G�e B�\)G�e�B��G�f B�{G�f�B��G�g B�G�g�B�p�G�h B�L�G�h�B��\G�i B�#�G�i�B�G�j B��G�j�B�{G�k B�\)G�k�B�G�l B�L�G�l�B��G�m B�G�m�B�{G�n B��G�n�B��{G�o B�\)G�o�B��fG�p B�L�G�p�B�G�q B���G�q�B�G�r B�=qG�r�B�p�G�s B�\G�s�B��G�t B�p�G�t�B���G�u B�{G�u�B�Q�G�v B�#�G�v�B���G�w B�(�G�w�B�W
G�x B��
G�x�B���G�y B��G�y�B�k�G�z B�8RG�z�B��\G�{ B�8RG�{�B�8RG�| B�p�G�|�B�33G�} B�p�G�}�B���G�~ B�u�G�~�B���G� B���G��B�u�Gƀ B�ffGƀ�B���GƁ B��fGƁ�B�aHGƂ B��GƂ�B�33Gƃ B�  Gƃ�B�.GƄ B��=GƄ�B��Gƅ B�G�Gƅ�B�  GƆ B��HGƆ�B�p�GƇ B��GƇ�B��Gƈ B��Gƈ�B��GƉ B���GƉ�B��GƊ B��
GƊ�B��fGƋ B�\)GƋ�B��\Gƌ B��Gƌ�B��fGƍ B�G�Gƍ�B�Q�GƎ B��qGƎ�B��GƏ B�Q�GƏ�B��)GƐ B�\GƐ�B�
=GƑ B���GƑ�B�33Gƒ B�ffGƒ�B�8RGƓ B��GƓ�B���GƔ B���GƔ�B���Gƕ B�.Gƕ�B�ffGƖ B���GƖ�B�33GƗ B�\)GƗ�B�W
GƘ B�B�GƘ�B�33Gƙ B��fGƙ�B���Gƚ B�.Gƚ�B���Gƛ B��
Gƛ�B�� GƜ B�� GƜ�B�  GƝ B�L�GƝ�B��fGƞ B�8RGƞ�B��GƟ B�\GƟ�B��
GƠ B�L�GƠ�B��\Gơ B���Gơ�B��fGƢ B��qGƢ�B��Gƣ B�u�Gƣ�B�ǮGƤ B��fGƤ�B��Gƥ B��Gƥ�B�#�GƦ B�#�GƦ�B��{GƧ B���GƧ�B�  Gƨ B���Gƨ�B�\GƩ B�{GƩ�B��{Gƪ B��)Gƪ�B��)Gƫ B���Gƫ�B��\GƬ B��qGƬ�B�Q�Gƭ B�.Gƭ�B��GƮ B���GƮ�B�\)GƯ B�ffGƯ�B��3Gư B���Gư�B�=qGƱ B��RGƱ�B���GƲ B���GƲ�B�� GƳ B�k�GƳ�B��RGƴ B��qGƴ�B��GƵ B��\GƵ�B�\)Gƶ B�(�Gƶ�B�B�GƷ B�Q�GƷ�B�� GƸ B�33GƸ�B�=qGƹ B��qGƹ�B��Gƺ B���Gƺ�B��{Gƻ B��Gƻ�B��)GƼ B�(�GƼ�B���Gƽ B��Gƽ�B���Gƾ B�G�Gƾ�B�\)Gƿ B��fGƿ�B�8RG�� B�#�G���B���G�� B�aHG���B��G�� B��3G�B�G�G�� B��)G�ÀB�8RG�� B���G�ĀB�(�G�� B�u�G�ŀB��
G�� B���G�ƀB�G�� B��G�ǀB�z�G�� B�#�G�ȀB�
=G�� B�#�G�ɀB��HG�� B��=G�ʀB�(�G�� B���G�ˀB�z�G�� B�u�G�̀B��G�� B��)G�̀B�G�� B���G�΀B�33G�� B�\G�πB�{G�� B�� G�ЀB�G�� B��=G�рB�k�G�� B�ffG�ҀB��G�� B��G�ӀB�\)G�� B�=qG�ԀB�ǮG�� B�p�G�ՀB�  G�� B��RG�րB��G�� B��
G�׀B��G�� B���G�؀B�G�� B��=G�ـB�p�G�� B��G�ڀB�8RG�� B�aHG�ۀB���G�� B�.G�܀B��G�� B�W
G�݀B���G�� B���G�ހB��G�� B���G�߀B�=qG�� B�G�G���B�Q�G�� B��G��B�G�� B�33G��B�ffG�� B���G��B���G�� B�.G��B���G�� B��fG��B��3G�� B�
=G��B��3G�� B���G��B�(�G�� B��=G��B�\G�� B�\G��B�u�G�� B�.G��B�=qG�� B��G��B���G�� B�L�G��B��G�� B�  G��B�p�G�� B��\G��B��{G�� B�\)G��B��G�� B�aHG���B��G�� B�p�G��B��3G�� B�
=G��B��HG�� B�8RG��B��fG�� B�\)G��B��fG�� B���G���B���G�� B�\G���B�\G�� B�
=G���B�p�G�� B�.G���B�W
G�� B�ffG���B�G�G�� B�8RG���B��G�� B�B�G���B�=qG�� B�=qG���B��G�� B���G���B�p�G�� B��qG���B���G�� B�=qG���B�33G�  B�k�G� �B�Q�G� B���G��B��qG� B�u�G��B��\G� B�z�G��B�33G� B�Q�G��B�z�G� B�p�G��B��
G� B�G�G��B�B�G� B��qG��B��G� B�#�G��B���G�	 B�G�	�B�{G�
 B�� G�
�B��G� B��G��B�.G� B��
G��B��{G� B��G��B�ǮG� B�33G��B���G� B�\)G��B�G�G� B��G��B�B�G� B��G��B�8RG� B��HG��B�
=G� B�{G��B�33G� B��{G��B�G� B���G��B�  G� B�G�G��B�  G� B��HG��B�
=G� B�p�G��B��\G� B��qG��B�G� B�B�G��B�  G� B�(�G��B�� G� B�� G��B���G� B��G��B�aHG� B��
G��B�G�G� B��=G��B��fG�  B�\G� �B��G�! B�33G�!�B�ffG�" B��HG�"�B��HG�# B���G�#�B���G�$ B��
G�$�B�L�G�% B�(�G�%�B�aHG�& B�{G�&�B�p�G�' B��G�'�B�\)G�( B��G�(�B�33G�) B��)G�)�B�W
G�* B��HG�*�B���G�+ B���G�+�B��RG�, B��)G�,�B�ffG�- B��
G�-�B���G�. B�  G�.�B�8RG�/ B���G�/�B��G�0 B���G�0�B�8RG�1 B�� G�1�B���G�2 B��)G�2�B��=G�3 B�� G�3�B��RG�4 B���G�4�B�{G�5 B�Q�G�5�B���G�6 B�Q�G�6�B�G�7 B�aHG�7�B�=qG�8 B��G�8�B�.G�9 B�L�G�9�B�33G�: B�Q�G�:�B�{G�; B�aHG�;�B�G�< B�W
G�<�B�ffG�= B�{G�=�B�33G�> B�ffG�>�B�Q�G�? B�ffG�?�B�
=G�@ B�(�G�@�B�k�G�A B�33G�A�B�W
G�B B�  G�B�B��=G�C B��G�C�B�aHG�D B�W
G�D�B�(�G�E B��G�E�B��G�F B�(�G�F�B�{G�G B���G�G�B�G�G�H B���G�H�B��G�I B��\G�I�B�ǮG�J B�ǮG�J�B�Q�G�K B��G�K�B��{G�L B���G�L�B��{G�M B�ǮG�M�B��G�N B�33G�N�B��G�O B�(�G�O�B���G�P B���G�P�B�W
G�Q B�G�G�Q�B���G�R B��RG�R�B�B�G�S B���G�S�B�G�G�T B���G�T�B�=qG�U B�ǮG�U�B�\G�V B�p�G�V�B�#�G�W B�ǮG�W�B�L�G�X B��G�X�B��G�Y B��G�Y�B�� G�Z B���G�Z�B���G�[ B�z�G�[�B�33G�\ B�L�G�\�B�(�G�] B��qG�]�B��{G�^ B���G�^�B�ffG�_ B��{G�_�B��3G�` B��G�`�B�W
G�a B�k�G�a�B�\G�b B�ffG�b�B���G�c B��G�c�B�.G�d B��)G�d�B���G�e B��G�e�B�L�G�f B|�HG�f�B��G�g B�W
G�g�B�k�G�h B�G�h�B�B�G�i B��G�i�B�u�G�j B��
G�j�B�Q�G�k B���G�k�B���G�l B��\G�l�B�33G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�Gǀ It#�Gǀ�It#�Gǁ It#�Gǁ�It#�Gǂ It#�Gǂ�It#�Gǃ It#�Gǃ�It#�GǄ It#�GǄ�It#�Gǅ It#�Gǅ�It#�Gǆ It#�Gǆ�It#�GǇ It#�GǇ�It#�Gǈ It#�Gǈ�It#�Gǉ It#�Gǉ�It#�GǊ It#�GǊ�It#�Gǋ It#�Gǋ�It#�Gǌ It#�Gǌ�It#�GǍ It#�GǍ�It#�Gǎ It#�Gǎ�It#�GǏ It#�GǏ�It#�Gǐ It#�Gǐ�It#�GǑ It#�GǑ�It#�Gǒ It#�Gǒ�It#�GǓ It#�GǓ�It#�Gǔ It#�Gǔ�It#�GǕ It#�GǕ�It#�Gǖ It#�Gǖ�It#�GǗ It#�GǗ�It#�Gǘ It#�Gǘ�It#�GǙ It#�GǙ�It#�Gǚ It#�Gǚ�It#�GǛ It#�GǛ�It#�Gǜ It#�Gǜ�It#�Gǝ It#�Gǝ�It#�GǞ It#�GǞ�It#�Gǟ It#�Gǟ�It#�GǠ It#�GǠ�It#�Gǡ It#�Gǡ�It#�GǢ It#�GǢ�It#�Gǣ It#�Gǣ�It#�GǤ It#�GǤ�It#�Gǥ It#�Gǥ�It#�GǦ It#�GǦ�It#�Gǧ It#�Gǧ�It#�GǨ It#�GǨ�It#�Gǩ It#�Gǩ�It#�GǪ It#�GǪ�It#�Gǫ It#�Gǫ�It#�GǬ It#�GǬ�It#�Gǭ It#�Gǭ�It#�GǮ It#�GǮ�B�Q�Gǯ B�ǮGǯ�B��fGǰ B���Gǰ�B���GǱ B�u�GǱ�B��Gǲ B�W
Gǲ�B�
=Gǳ B��HGǳ�B�k�GǴ B���GǴ�B��Gǵ B�\Gǵ�B���GǶ B���GǶ�B�\GǷ B�u�GǷ�B��\GǸ B�GǸ�B�L�Gǹ B��Gǹ�B�aHGǺ B��GǺ�B��{Gǻ B�B�Gǻ�B���GǼ B�=qGǼ�B��Gǽ B���Gǽ�B�8RGǾ B���GǾ�B�p�Gǿ B�\)Gǿ�B�\G�� B���G���B�.G�� B���G���B�p�G�� B���G�B�{G�� B�p�G�ÀB���G�� B�ffG�ĀB�8RG�� B��G�ŀB�{G�� B�G�G�ƀB�ffG�� B�
=G�ǀB���G�� B�.G�ȀB��{G�� B�L�G�ɀB�33G�� B�{G�ʀB�ǮG�� B��G�ˀB�#�G�� B�G�̀B�G�� B�� G�̀B��
G�� B�  G�΀B�Q�G�� B�ǮG�πB��fG�� B�ffG�ЀB}��G�� B��G�рB���G�� B���G�ҀB��{G�� B���G�ӀB��
G�� B��G�ԀB�ǮG�� B�=qG�ՀB�\G�� B�G�G�րB�{G�� B��G�׀B��G�� B�G�G�؀B��)G�� B�.G�ـB��fG�� B�Q�G�ڀB�33G�� B�G�G�ۀB���G�� B��3G�܀B�p�G�� B��fG�݀B��3G�� B�
=G�ހB�L�G�� B�=qG�߀B��3G�� B�G���B�#�G�� B��{G��B�L�G�� B�
=G��B�.G�� B�ffG��B���G�� B���G��B��G�� B���G��B��qG�� B��
G��B��fG�� B�  G��B�\G�� B��3G��B���G�� B�ffG��B��fG�� B�\)G��B��G�� B�8RG��B��G�� B��3G��B��G�� B�8RG��B��G�� B�(�G��B��G�� B�=qG��B���G�� B�ffG���B�G�G�� B��G��B��G�� B��3G��B�z�G�� B�  G��B�\G�� B�8RG�� B��{G��B�k�G�� B��
G���B�8RG�� B�33G���B���G�� B�aHG���B���G�� B�Q�G���B��)G�� B�#�G���B�
=G�� B�k�G���B��G�� B�ǮG���B���G�� B�z�G���B�
=G�� B�{G���B�G�� B��qG���B��HG�� B�\)G���B�� G�  B��{G� �B�.G� B���G��B�p�G� B���G��B��G� B�  G��B��=G� B��{G��B�G� B�G��B���G� B�\)G��B�z�G� B�L�G��B���G� B���G��B�B�G�	 B�G�	�B�k�G�
 B�� G�
�B�{G� B�ffG��B�W
G� B�(�G��B�B�G� B�33G��B�#�G� B�Q�G��B���G� B��G��B���G� B��)G��B��G� B�#�G��B�ffG� B�
=G��B�G� B���G��B�
=G� B��G��B�L�G� B��G��B��3G� B��\G��B��G� B�z�G��B��G� B�B�G��B�� G� B�G�G��B��G� B���G��B�ǮG� B�G��B��G� B�W
G��B�k�G� B�G�G��B��{G� B��=G��B�L�G� B���G��B��qG�  B��3G� �B�u�G�! B�p�G�!�B�33G�" B�\G�"�B���G�# B�G�#�B��G�$ B���G�$�B��3G�% B�{G�%�B�z�G�& B�{G�&�B�=qG�' B�  G�'�B���G�( B��3G�(�B���G�) B�k�G�)�B��3G�* B�u�G�*�B��3G�+ B�B�G�+�B��G�, B�.G�,�B�p�G�- B��)G�-�B��)G�. B�(�G�.�B���G�/ B�� G�/�B��HG�0 B�u�G�0�B��G�1 B�W
G�1�B�B�G�2 B�  G�2�B�G�G�3 B��=G�3�B�ffG�4 B��G�4�B�k�G�5 B�=qG�5�B���G�6 B�k�G�6�B�G�G�7 B�L�G�7�B�=qG�8 B�(�G�8�B���G�9 B�u�G�9�B�=qG�: B�aHG�:�B�k�G�; B��G�;�B�#�G�< B��HG�<�B�W
G�= B��fG�=�B�G�> B�z�G�>�B��{G�? B�p�G�?�B���G�@ B�k�G�@�B��=G�A B�L�G�A�B�8RG�B B�G�G�B�B�G�C B���G�C�B��G�D B��G�D�B�B�G�E B�B�G�E�B�  G�F B�ffG�F�B��G�G B��G�G�B�G�H B�Q�G�H�B��=G�I B�aHG�I�B���G�J B�B�G�J�B��=G�K B��HG�K�B���G�L B��\G�L�B�
=G�M B�=qG�M�B��G�N B��=G�N�B��3G�O B���G�O�B�
=G�P B���G�P�B��G�Q B�  G�Q�B�(�G�R B�8RG�R�B��RG�S B�u�G�S�B�u�G�T B���G�T�B�G�U B���G�U�B�ffG�V B�Q�G�V�B�.G�W B��qG�W�B�.G�X B�G�G�X�B�Q�G�Y B�u�G�Y�B��HG�Z B��)G�Z�B�L�G�[ B�B�G�[�B�(�G�\ B�ffG�\�B�  G�] B�{G�]�B�L�G�^ B��
G�^�B��qG�_ B��G�_�B�#�G�` B��=G�`�B�ffG�a B�  G�a�B���G�b B��HG�b�B�  G�c B��G�c�B�W
G�d B�\G�d�B���G�e B�G�G�e�B��G�f B�=qG�f�B��G�g B�aHG�g�B��)G�h B�z�G�h�B�� G�i B��G�i�B���G�j B��G�j�B��3G�k B�z�G�k�B���G�l B��fG�l�B�8RG�m B�aHG�m�B��G�n B��G�n�B�33G�o B���G�o�B�=qG�p B��qG�p�B�  G�q B�ǮG�q�B�B�G�r B��G�r�B���G�s B�=qG�s�B�G�t B��\G�t�B��G�u B���G�u�B�=qG�v B��G�v�B�G�G�w B�z�G�w�B�G�x B��G�x�B��)G�y B��qG�y�B�8RG�z B��{G�z�B�B�G�{ B��G�{�B�ffG�| B��3G�|�B�{G�} B�ǮG�}�B��HG�~ B�k�G�~�B��G� B��RG��B��\GȀ B�p�GȀ�B�{Gȁ B�u�Gȁ�B���GȂ B�\)GȂ�B�W
Gȃ B�#�Gȃ�B��fGȄ B��3GȄ�B��Gȅ B��Gȅ�B��
GȆ B��3GȆ�B��Gȇ B�{Gȇ�B���GȈ B��{GȈ�B��fGȉ B�Gȉ�B�{GȊ B��GȊ�B�aHGȋ B�p�Gȋ�B�33GȌ B���GȌ�B��RGȍ B��Gȍ�B��GȎ B��GȎ�B�  Gȏ B��Gȏ�B�\GȐ B��)GȐ�B�\)Gȑ B�33Gȑ�B}�GȒ B��GȒ�B��Gȓ B���Gȓ�B��GȔ B�z�GȔ�B�ǮGȕ B��Gȕ�B�u�GȖ B�=qGȖ�B���Gȗ B��qGȗ�B�p�GȘ B���GȘ�B�B�Gș B��
Gș�B���GȚ B��
GȚ�B��=Gț B���Gț�B��{GȜ B�\GȜ�B�#�Gȝ B��=Gȝ�B�{GȞ B���GȞ�B�aHGȟ B�Gȟ�B��GȠ B�ffGȠ�B�u�Gȡ B�33Gȡ�B�ffGȢ B���GȢ�B�Gȣ B���Gȣ�B�� GȤ B���GȤ�B��Gȥ B�p�Gȥ�B��GȦ B�33GȦ�B�z�Gȧ B�8RGȧ�B�GȨ B�  GȨ�B���Gȩ B��Gȩ�B���GȪ B�
=GȪ�B���Gȫ B���Gȫ�B��)GȬ B�ǮGȬ�B��
Gȭ B�u�Gȭ�B���GȮ B�\GȮ�B�.Gȯ B�p�Gȯ�B�u�GȰ B�aHGȰ�B��Gȱ B�=qGȱ�B�ǮGȲ B�L�GȲ�B���Gȳ B��)Gȳ�B��=Gȴ B��Gȴ�B���Gȵ B�Gȵ�B�G�Gȶ B��=Gȶ�B�W
Gȷ B�{Gȷ�B���Gȸ B�#�Gȸ�B�.Gȹ B�.Gȹ�B�#�GȺ B�k�GȺ�B�u�GȻ B�k�GȻ�B�B�Gȼ B�ǮGȼ�B��GȽ B�Q�GȽ�B�p�GȾ B���GȾ�B�� Gȿ B��Gȿ�B���G�� B���G���B��RG�� B�
=G���B�G�� B�k�G�B���G�� B��G�ÀB���G�� B�k�G�ĀB��G�� B���G�ŀB��G�� B�ǮG�ƀB��{G�� B���G�ǀB�  G�� B���G�ȀB��{G�� B�� G�ɀB���G�� B��HG�ʀB���G�� B��=G�ˀB�ffG�� B��{G�̀B��HG�� B�p�G�̀B�G�G�� B���G�΀B�k�G�� B���G�πB��\G�� B�L�G�ЀB��fG�� B�
=G�рB�  G�� B��RG�ҀB��\G�� B�� G�ӀB��\G�� B��{G�ԀB�8RG�� B�8RG�ՀB�u�G�� B��=G�րB���G�� B�#�G�׀B�k�G�� B��=G�؀B���G�� B�W
G�ـB�\G�� B��\G�ڀB�aHG�� B�L�G�ۀB�z�G�� B��
G�܀B�G�G�� B��G�݀B�W
G�� B��G�ހB�33G�� B���G�߀B�L�G�� B�u�G���B�G�� B���G��B��)G�� B�u�G��B�� G�� B�(�G��B�\)G�� B���G��B��HG�� B�p�G��B�u�G�� B��G��B�G�G�� B���G��B�  G�� B�G��B��qG�� B��3G��B�#�G�� B���G��B��{G�� B���G��B��G�� B�ffG��B�z�G�� B�W
G��B�\)G�� B��\G��B�p�G�� B�� G��B�.G�� B��G���B�L�G�� B���G��B�L�G�� B��G��B��\G�� B�k�G��B�G�� B��G��B�u�G�� B�33G���B�p�G�� B�.G���B�aHG�� B�8RG���B���G�� B��G���B��G�� B�8RG���B���G�� B�ffG���B�L�G�� B��G���B�33G�� B�ǮG���B�B�G�� B��=G���B���G�� B��=G���B�ffG�� B�.G���B���G�  B��G� �B�aHG� B�.G��B��\G� B�=qG��B��G� B��{G��B���G� B�\G��B��G� B�ǮG��B�B�G� B���G��B��RG� B��G��B��G� B�B�G��B�.G�	 B
=G�	�B��
G�
 B���G�
�B�ǮG� B��fG��B�\G� B��G��B�8RG� B���G��B�k�G� B�p�G��B�(�G� B�
=G��B��
G� B�8RG��B��
G� B���G��B��RG� B�k�G��B�z�G� B�#�G��B�ffG� B�
=G��B�L�G� B���G��B�G� B�\)G��B��{G� B�k�G��B�(�G� B��G��B���G� B�{G��B�z�G� B�L�G��B�ǮG� B�{G��B�ffG� B�W
G��B��G� B�p�G��B�W
G� B�\G��B��RG� B�
G��B��G�  B�8RG� �B�G�! B��G�!�B��
G�" B�B�G�"�B��G�# B��
G�#�B�u�G�$ B���G�$�B���G�% B�aHG�%�B���G�& B�B�G�&�B���G�' B��)G�'�B�=qG�( B�L�G�(�B�8RG�) B�#�G�)�B��G�* B�L�G�*�B��HG�+ B��G�+�B��G�, B���G�,�B��G�- B���G�-�B�W
G�. B�\)G�.�B��{G�/ B��3G�/�B�u�G�0 B���G�0�B�B�G�1 B��G�1�B�z�G�2 B���G�2�B�G�3 B�W
G�3�B�{G�4 B�8RG�4�B�aHG�5 B��G�5�B�� G�6 B���G�6�B���G�7 B��3G�7�B�Q�G�8 B�
=G�8�B�#�G�9 B���G�9�B��HG�: B�=qG�:�B�aHG�; B���G�;�B|  G�< B���G�<�B��G�= B�ffG�=�B�(�G�> B�  G�>�B�k�G�? B��
G�?�B��3G�@ B��G�@�B��G�A B�(�G�A�B���G�B B��3G�B�B��
G�C B�aHG�C�B��G�D B��G�D�B��3G�E B��{G�E�B���G�F B��)G�F�B�G�G B��fG�G�B�
=G�H B���G�H�B��
G�I B���G�I�B�� G�J B���G�J�B�G�K B���G�K�B��G�L B��HG�L�B�B�G�M B���G�M�B�ffG�N B�G�N�B�G�O B}=qG�O�B��G�P B��G�P�B�  G�Q B�#�G�Q�B�� G�R B���G�R�B|�RG�S Bx�G�S�B~G�G�T B�33G�T�B�.G�U B�Q�G�U�B�ffG�V B�\G�V�B��fG�W B�� G�W�BQ�G�X B�#�G�X�B�z�G�Y Bz�G�Y�B�33G�Z B���G�Z�B�p�G�[ B��fG�[�B��G�\ B�.G�\�B�p�G�] B{G�]�B��G�^ B��{G�^�B�33G�_ B���G�_�B~��G�` B�#�G�`�B�k�G�a B�k�G�a�B�G�b B��RG�b�B�8RG�c B�\)G�c�B�\G�d B��G�d�B�L�G�e B���G�e�It#�G�f It#�G�f�It#�G�g It#�G�g�It#�G�h It#�G�h�It#�G�i It#�G�i�It#�G�j It#�G�j�It#�G�k It#�G�k�It#�G�l It#�G�l�It#�G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�Gɀ It#�Gɀ�It#�GɁ It#�GɁ�It#�Gɂ It#�Gɂ�It#�GɃ It#�GɃ�It#�GɄ It#�GɄ�It#�GɅ It#�GɅ�It#�GɆ It#�GɆ�It#�Gɇ It#�Gɇ�It#�GɈ It#�GɈ�It#�Gɉ It#�Gɉ�It#�GɊ It#�GɊ�It#�Gɋ It#�Gɋ�It#�GɌ It#�GɌ�It#�Gɍ It#�Gɍ�It#�GɎ It#�GɎ�It#�Gɏ It#�Gɏ�It#�Gɐ It#�Gɐ�It#�Gɑ It#�Gɑ�It#�Gɒ It#�Gɒ�It#�Gɓ It#�Gɓ�It#�Gɔ It#�Gɔ�It#�Gɕ It#�Gɕ�It#�Gɖ It#�Gɖ�It#�Gɗ It#�Gɗ�It#�Gɘ It#�Gɘ�It#�Gə It#�Gə�It#�Gɚ It#�Gɚ�It#�Gɛ It#�Gɛ�It#�Gɜ It#�Gɜ�It#�Gɝ It#�Gɝ�It#�Gɞ It#�Gɞ�It#�Gɟ It#�Gɟ�It#�Gɠ It#�Gɠ�It#�Gɡ It#�Gɡ�It#�Gɢ It#�Gɢ�It#�Gɣ It#�Gɣ�It#�Gɤ It#�Gɤ�It#�Gɥ It#�Gɥ�It#�Gɦ It#�Gɦ�It#�Gɧ Bs{Gɧ�Bk�Gɨ Btz�Gɨ�Bxp�Gɩ By�Gɩ�Bw��Gɪ Bt=qGɪ�Bs��Gɫ Bs�Gɫ�Bu�Gɬ Bx�Gɬ�BrGɭ Biz�Gɭ�Bl�\Gɮ Bx��Gɮ�Bv�RGɯ Bo�
Gɯ�Bs�HGɰ Bs33Gɰ�Bu
=Gɱ BvffGɱ�Bx�\Gɲ Bo  Gɲ�Bf33Gɳ Bv��Gɳ�Bw��Gɴ Bo�Gɴ�BsGɵ Bv��Gɵ�Bt  Gɶ Bx(�Gɶ�Bz�Gɷ Bo�Gɷ�BkGɸ Bo\)Gɸ�BuffGɹ BuffGɹ�Bw��Gɺ Bp�\Gɺ�Bo�RGɻ Bu{Gɻ�Bo�HGɼ Bl��Gɼ�Bl��Gɽ Bn��Gɽ�B|(�Gɾ B}�Gɾ�Bx33Gɿ Bt33Gɿ�Bm{G�� Bm  G���Bv
=G�� B|�G���BqffG�� Bs�\G�Bu\)G�� Br��G�ÀBs33G�� Bqz�G�ĀBtG�� Bp�G�ŀBm  G�� Bo{G�ƀBup�G�� Bq�G�ǀBi��G�� Bk��G�ȀBb�G�� Bi�\G�ɀBoG�G�� Bx�G�ʀB33G�� BwG�G�ˀBpffG�� Bp�RG�̀Br33G�� Bq  G�̀Bn��G�� Bw\)G�΀Bw�G�� Bl�G�πBt�G�� B~�HG�ЀB�B�G�� B~�RG�рBG�� B�G�ҀBv\)G�� Bnp�G�ӀBmffG�� Bp  G�ԀB{��G�� Bv33G�ՀBr{G�� Bu(�G�րBt�
G�� Bw{G�׀Bs33G�� BvffG�؀Bz�G�� B{ffG�ـBs�G�� Bo  G�ڀBvffG�� By�
G�ۀBr�G�� BtQ�G�܀Bt{G�� BlffG�݀BlffG�� Btp�G�ހBs=qG�� Bm�G�߀BnffG�� Bj  G���Be  G�� Bdz�G��BcG�G�� Bk�RG��Blp�G�� Bn�G��BlG�� Bf�\G��Bn(�G�� BpG��Bk=qG�� Bk=qG��Bd\)G�� Bb�HG��Bo�HG�� Bl\)G��Bk  G�� Bl�G��Bj�RG�� Bo�G��B_�RG�� B_�RG��Bb�\G�� Bl��G��Br  G�� Be
=G��B[G�� BY�G��B\��G�� Ba\)G��B\�G�� B_�\G���Bh(�G�� Ba��G��B`�
G�� Ba��G��Bcp�G�� B`��G��BX=qG�� BX�G��B_�G�� Bd
=G���B`��G�� B_=qG���Ba33G�� BdG���BbQ�G�� B]  G���Bc�G�� Bbp�G���B\�
G�� B]{G���B\�G�� Bb��G���Bg\)G�� Be33G���B`\)G�� B\Q�G���BY�\G�� B[ffG���Ba=qG�� Bb\)G���B`��G�  Bg\)G� �Be�RG� B]\)G��BeQ�G� Bi  G��Ba�HG� Bf\)G��Bg��G� Be�\G��B`(�G� BY��G��B`  G� Bc=qG��B`�G� Bf  G��Bf33G� B\�\G��BZ�G�	 Ba��G�	�Bf�HG�
 B_��G�
�BY��G� B[��G��B\G�G� BVp�G��BZQ�G� Bc�HG��BP�
G� BUQ�G��BZ(�G� BZG��Ba=qG� Bd�G��Bc��G� B]p�G��B]G�G� Bc�RG��B`�HG� BaG��BZ�G� BXG�G��BW=qG� BQ�RG��BQ�\G� BO(�G��BN�RG� BK�HG��BQ
=G� BSG�G��BM��G� BK33G��BH33G� BG��G��BN�G� BS��G��BR�G� BT�G��BNG� BJ�G��BU33G� BU�G��BP�\G� BY��G��BY33G�  BRffG� �BR��G�! BZ�G�!�BT�HG�" BL��G�"�BK=qG�# BP{G�#�BSp�G�$ BNp�G�$�BI�G�% BL�RG�%�BN��G�& BO�\G�&�BS  G�' BR�G�'�BPp�G�( BN��G�(�BL�RG�) BFp�G�)�BGG�G�* BHQ�G�*�BD��G�+ BBQ�G�+�B;��G�, B9�G�,�BCp�G�- BD
=G�-�B>�G�. B@G�.�B>�G�/ B<��G�/�B>��G�0 BI�
G�0�BB�G�1 B<�G�1�B;p�G�2 B=�G�2�BB{G�3 B@p�G�3�BDffG�4 BE��G�4�BB�G�5 BF=qG�5�BK=qG�6 BNG�6�BI�G�7 BE33G�7�BFG�8 BNp�G�8�BSG�9 BTp�G�9�BU(�G�: BL�HG�:�BG�RG�; B@�G�;�B=��G�< BGp�G�<�BE�RG�= B:��G�=�B<  G�> B>��G�>�B?�HG�? B<�HG�?�B=
=G�@ BD�RG�@�BC(�G�A B7�HG�A�B<33G�B B7�HG�B�B2
=G�C B-
=G�C�B033G�D B7
=G�D�B1�
G�E B433G�E�B=�G�F BC��G�F�BG�G�G BOG�G�BQ=qG�H BO(�G�H�BO(�G�I BO�HG�I�BL=qG�J BCG�G�J�BF�G�K BE�G�K�B?Q�G�L BD  G�L�B=��G�M B7�G�M�B<ffG�N BEp�G�N�BA�G�O B>�G�O�BE
=G�P BD(�G�P�BB��G�Q BA=qG�Q�B=G�G�R B9�G�R�BE��G�S BGG�G�S�BA��G�T B?  G�T�BAp�G�U B?�HG�U�B?33G�V B<p�G�V�B:
=G�W B3z�G�W�B.�G�X B+�HG�X�B/�G�Y B3z�G�Y�B-��G�Z B+�\G�Z�B,��G�[ B1=qG�[�B.Q�G�\ B2�G�\�B5�G�] B6��G�]�B3�G�^ B1\)G�^�B3Q�G�_ B(Q�G�_�B$�G�` B(�\G�`�B-��G�a B1�HG�a�B.�G�b B,p�G�b�B/
=G�c B0  G�c�B4�
G�d B2�
G�d�B-z�G�e B0{G�e�B3�G�f B2�G�f�B0��G�g B/z�G�g�B1�RG�h B2�G�h�B1Q�G�i B6Q�G�i�B:�G�j B3ffG�j�B3
=G�k B9�G�k�B:�RG�l B7  G�l�B3�RG�m B/��G�m�B8ffG�n B>=qG�n�B<�HG�o B:ffG�o�B8ffG�p B2\)G�p�B0z�G�q B1\)G�q�B0�\G�r B2��G�r�B(��G�s B%\)G�s�B)�G�t B)�
G�t�B)(�G�u B)(�G�u�B2�RG�v B-ffG�v�B-�
G�w B-ffG�w�B'33G�x B&{G�x�B-z�G�y B)p�G�y�B ��G�z B%�
G�z�B Q�G�{ BffG�{�B$G�G�| B%z�G�|�B!�G�} B�G�}�BQ�G�~ B=qG�~�BQ�G� B�G��B��Gʀ B!
=Gʀ�B%�
Gʁ B#��Gʁ�B�Gʂ B��Gʂ�B{Gʃ B\)Gʃ�B�Gʄ B�Gʄ�B(�Gʅ B�RGʅ�BffGʆ B{Gʆ�B"{Gʇ B!�RGʇ�B�\Gʈ B\)Gʈ�B\)Gʉ B{Gʉ�B��Gʊ B�HGʊ�BQ�Gʋ BGʋ�B�
Gʌ B{Gʌ�B��Gʍ B(�Gʍ�B�Gʎ B=qGʎ�BQ�Gʏ B�
Gʏ�B  Gʐ B��Gʐ�B�Gʑ B�Gʑ�BQ�Gʒ B�Gʒ�B��Gʓ B�RGʓ�BG�Gʔ B�
Gʔ�B ��Gʕ B!��Gʕ�B�Gʖ B(�Gʖ�B��Gʗ B(�Gʗ�B�Gʘ B�RGʘ�B(�Gʙ B�RGʙ�BGʚ B�Gʚ�Bp�Gʛ B�HGʛ�B��Gʜ B  Gʜ�B�
Gʝ B�
Gʝ�B\)Gʞ B
=Gʞ�B(�Gʟ B{Gʟ�B��Gʠ B	  Gʠ�B��Gʡ B��Gʡ�B��Gʢ Bz�Gʢ�B33Gʣ BG�Gʣ�B�Gʤ Bz�Gʤ�B�\Gʥ B	�HGʥ�BffGʦ B�
Gʦ�B(�Gʧ B�Gʧ�Bp�Gʨ B
��Gʨ�B{Gʩ B
=Gʩ�B  Gʪ B{Gʪ�B��Gʫ B!  Gʫ�B*�Gʬ B1ffGʬ�B.z�Gʭ B1�Gʭ�B2�Gʮ B)�
Gʮ�B&z�Gʯ B%Gʯ�B!
=Gʰ B��Gʰ�B"��Gʱ B"Q�Gʱ�B&z�Gʲ B(��Gʲ�B�Gʳ B��Gʳ�B$z�Gʴ B%33Gʴ�B#G�Gʵ B+=qGʵ�B,G�Gʶ B*
=Gʶ�B'G�Gʷ B�
Gʷ�B�Gʸ B�\Gʸ�B  Gʹ B�Gʹ�B�Gʺ B��Gʺ�B Q�Gʻ B�Gʻ�B  Gʼ B�Gʼ�B   Gʽ B�Gʽ�B��Gʾ B �Gʾ�BQ�Gʿ BGʿ�B��G�� B�HG���BG�G�� A�=qG���B \)G�� B�G�B��G�� B��G�ÀB�HG�� B�\G�ĀB��G�� B(�G�ŀB{G�� B	  G�ƀB�\G�� Bz�G�ǀB�
G�� B{G�ȀB�\G�� B	��G�ɀB�G�� B�
G�ʀA�(�G�� BG�ˀB��G�� B=qG�̀B33G�� B{G�̀BG�G�� B�
G�΀B(�G�� A�33G�πB  G�� BQ�G�ЀB �G�� A���G�рA���G�� A�Q�G�ҀBG�G�� B �G�ӀA�G�G�� B��G�ԀB��G�� B	�\G�ՀBG�G�� B�G�րB  G�� B  G�׀B�HG�� B�G�؀B(�G�� B�
G�ـA�
=G�� B{G�ڀB�
G�� Bp�G�ۀB	G�� B�G�܀B�HG�� B=qG�݀B=qG�� B	p�G�ހB=qG�� B33G�߀B	��G�� B
33G���Bz�G�� B�G��B  G�� B�RG��B Q�G�� A�33G��B(�G�� B�G��B(�G�� B\)G��A�  G�� B=qG��A��RG�� A�{G��A�G�G�� B�HG��A�=qG�� B �G��A�=qG�� A�G��A�(�G�� A��
G��A���G�� A���G��B��G�� B	=qG��B
=G�� B(�G��A��G�� A��G��A���G�� B�G���B��G�� B��G��B=qG�� B�\G��B	Q�G�� B��G��A��G�� B �G��B 
=G�� B  G���B�HG�� B�G���A�Q�G�� B ffG���B��G�� B�RG���B�G�� A�33G���A�  G�� BG���B�G�� B�G���BQ�G�� B��G���A��G�� A�ffG���A�=qG�� B��G���B�
G�� B�
G���A�{G�  A�ffG� �A��G� A��HG��A�=qG� B =qG��A�(�G� A�{G��A�p�G� A��G��B�G� B��G��B�
G� B
{G��B�RG� A�(�G��A��HG� A�33G��A��RG�	 A�{G�	�A�(�G�
 A�Q�G�
�A��RG� A�
=G��B
=G� BG��B�
G� B�RG��A�ffG� A�G��A��G� A�\)G��A�ffG� A���G��A�G�G� A�Q�G��A��
G� A���G��A�z�G� A�{G��A�33G� A�p�G��A�G� A�z�G��A��HG� A���G��A�p�G� Aԏ\G��AӅG� A��G��A��G� A�33G��A�=qG� A�ffG��A�
=G� A�{G��A�33G� A�{G��A�
=G� A��
G��A�{G� A��G��A�\)G� A�
=G��A�33G�  A�G� �A�\)G�! A�=qG�!�AУ�G�" A�\)G�"�A��HG�# A��HG�#�A�Q�G�$ A���G�$�A܏\G�% A�  G�%�A��G�& A�G�&�A��G�' A�Q�G�'�A�G�( A�RG�(�A��
G�) A�  G�)�A�RG�* A�=qG�*�A�G�G�+ A��HG�+�A�G�G�, A�ffG�,�A噚G�- A��G�-�A�G�. A�33G�.�AمG�/ A��G�/�A��G�0 A���G�0�AݮG�1 A�z�G�1�AȸRG�2 A�G�2�A���G�3 AθRG�3�A��
G�4 A�z�G�4�A��G�5 A�\)G�5�A��HG�6 A�33G�6�A���G�7 A�  G�7�A�RG�8 A�ffG�8�A�
=G�9 A�
=G�9�Aģ�G�: A��G�:�A�  G�; A�  G�;�A���G�< A���G�<�A��G�= AڸRG�=�A��HG�> A��G�>�Aљ�G�? AиRG�?�A�{G�@ A�=qG�@�A�z�G�A A�{G�A�A�\)G�B Aۙ�G�B�Aԏ\G�C A�(�G�C�A�p�G�D A�33G�D�A�RG�E A��HG�E�A�\)G�F A�
=G�F�A��
G�G A�{G�G�A�G�G�H A��HG�H�A��HG�I A�Q�G�I�A�
=G�J A�p�G�J�A陚G�K A�{G�K�A���G�L A�RG�L�A�33G�M B��G�M�BQ�G�N Bz�G�N�B(�G�O B{G�O�A��G�P A�33G�P�A���G�Q A�=qG�Q�A��
G�R BG�R�A�G�G�S A�G�S�B��G�T B�G�T�Bp�G�U B	��G�U�A��G�V A�p�G�V�A�{G�W Bp�G�W�B G�X B(�G�X�B �G�Y A�ffG�Y�A�Q�G�Z B {G�Z�BQ�G�[ A�z�G�[�A�33G�\ A�  G�\�A���G�] A��G�]�A��G�^ A���G�^�It#�G�_ It#�G�_�It#�G�` It#�G�`�It#�G�a It#�G�a�It#�G�b It#�G�b�It#�G�c It#�G�c�It#�G�d It#�G�d�It#�G�e It#�G�e�It#�G�f It#�G�f�It#�G�g It#�G�g�It#�G�h It#�G�h�It#�G�i It#�G�i�It#�G�j It#�G�j�It#�G�k It#�G�k�It#�G�l It#�G�l�It#�G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�Gˀ It#�Gˀ�It#�Gˁ It#�Gˁ�It#�G˂ It#�G˂�It#�G˃ It#�G˃�It#�G˄ It#�G˄�It#�G˅ It#�G˅�It#�Gˆ It#�Gˆ�It#�Gˇ It#�Gˇ�It#�Gˈ It#�Gˈ�It#�Gˉ It#�Gˉ�It#�Gˊ It#�Gˊ�It#�Gˋ It#�Gˋ�It#�Gˌ It#�Gˌ�It#�Gˍ It#�Gˍ�It#�Gˎ It#�Gˎ�It#�Gˏ It#�Gˏ�It#�Gː It#�Gː�It#�Gˑ It#�Gˑ�It#�G˒ It#�G˒�It#�G˓ It#�G˓�It#�G˔ It#�G˔�It#�G˕ It#�G˕�It#�G˖ It#�G˖�It#�G˗ It#�G˗�It#�G˘ It#�G˘�It#�G˙ It#�G˙�It#�G˚ It#�G˚�It#�G˛ It#�G˛�It#�G˜ It#�G˜�It#�G˝ It#�G˝�It#�G˞ It#�G˞�It#�G˟ It#�G˟�It#�Gˠ A��HGˠ�A�p�Gˡ A���Gˡ�BQ�Gˢ B	�HGˢ�B�RGˣ B{Gˣ�Bz�Gˤ B�\Gˤ�B��G˥ B�\G˥�B
33G˦ B�G˦�B	(�G˧ B�G˧�B	�G˨ B	G�G˨�B��G˩ B \)G˩�A�G˪ B��G˪�B  G˫ B{G˫�B�\Gˬ B 
=Gˬ�A�(�G˭ A�G˭�A�z�Gˮ A�Q�Gˮ�A��G˯ A�G˯�A�G˰ A�
=G˰�A�RG˱ A��G˱�A�z�G˲ A�z�G˲�A��G˳ A�  G˳�A�G˴ A��
G˴�A���G˵ A�p�G˵�A�p�G˶ A��G˶�A�\)G˷ A��G˷�A�\G˸ A���G˸�A�
=G˹ A�=qG˹�A�  G˺ A�\G˺�A��G˻ A�(�G˻�A��G˼ A�(�G˼�A��G˽ A�\)G˽�A�G�G˾ A��G˾�A�\)G˿ A�33G˿�A�33G�� A�p�G���A���G�� B  G���BffG�� A�{G�B �
G�� B=qG�ÀA�G�G�� A�=qG�ĀA�  G�� A��G�ŀA�{G�� A��G�ƀA��HG�� A�\)G�ǀA�  G�� A��HG�ȀA��
G�� A�(�G�ɀA�  G�� A�33G�ʀA���G�� A�33G�ˀA�G�G�� A�33G�̀B Q�G�� A���G�̀A���G�� A��G�΀A�  G�� A�z�G�πA��
G�� A�(�G�ЀBG�G�� A���G�рA뙚G�� A��G�ҀA��G�� A�G�G�ӀA�Q�G�� A���G�ԀB ffG�� A�{G�ՀA�ffG�� A�G�G�րA��G�� A�
=G�׀A�=qG�� B\)G�؀A�  G�� A�33G�ـBQ�G�� B�G�ڀA�\)G�� A�(�G�ۀA��G�� A�G�܀A�p�G�� A�\G�݀A��
G�� A��G�ހA�z�G�� A���G�߀A�(�G�� A�G���A���G�� A���G��A�{G�� A�{G��A�Q�G�� A�G��A�33G�� A�\G��A�z�G�� A�G��A���G�� A�  G��A�ffG�� A�ffG��A�\)G�� A�
=G��A��HG�� A��
G��A�\G�� A��G��AݮG�� A�(�G��A��\G�� A���G��A�
=G�� A�
=G��A���G�� A��HG��A�z�G�� AG��A�\G�� A�33G���A��G�� A�G�G��A���G�� A���G��Bz�G�� A�{G��A�p�G�� A���G��A�  G�� A�G���A��G�� A��G���A��
G�� A�  G���A���G�� A�Q�G���B G�� A�=qG���A�\)G�� A�{G���A�{G�� B��G���B��G�� B ��G���A�ffG�� A�(�G���A�{G�� B�
G���B�G�� BG�G���B �\G�  A��\G� �A�RG� A�G��A�z�G� A�p�G��A���G� A��HG��A�(�G� A�Q�G��A�p�G� A�\)G��B ffG� A��G��A��G� B\)G��A�{G� A�
=G��A�33G�	 A�p�G�	�A�(�G�
 A���G�
�Bp�G� A�G�G��A�
=G� A�ffG��Aߙ�G� A�G��A�G� A�G��A�{G� A�Q�G��A�G�G� BG�G��A��G� A�z�G��A�G� A�RG��A�=qG� A�=qG��A�33G� A��
G��A��G� A��G��A��RG� A�=qG��A��G� A��G��A�33G� A�ffG��A���G� A���G� A���G��A�
=G� A�G�G��A�\)G� A���G��A�z�G� A���G��A�G� AӅG��A݅G� A�(�G��A陚G� A�G�G��A���G�  A���G� �A�\G�! A�z�G�!�A�p�G�" A�
=G�"�A�G�G�# A�RG�#�A�\)G�$ A�p�G�$�B�G�% A��HG�%�Aޣ�G�& A�p�G�&�A���G�' A�
=G�'�A���G�( A��G�(�A�G�) A�G�G�)�A�ffG�* A�
=G�*�A�33G�+ A�G�+�A�  G�, A�RG�,�A�z�G�- A�G�-�A�33G�. A��G�.�A��RG�/ A��\G�/�B �G�0 A�ffG�0�A�\)G�1 A�
=G�1�A��
G�2 B�G�2�B��G�3 A�G�G�3�A�p�G�4 B ��G�4�B Q�G�5 A�33G�5�A�ffG�6 BQ�G�6�A�z�G�7 A�G�7�A��HG�8 B�HG�8�A��G�9 A홚G�9�A�G�: A�
=G�:�A�z�G�; A��G�;�A�
=G�< A�z�G�<�A�\)G�= A�G�G�=�A�33G�> A�G�>�B ��G�? A�\)G�?�B�
G�@ A��G�@�A��HG�A B G�G�A�A��G�B A�=qG�B�A��
G�C B�G�C�B�RG�D B\)G�D�BG�E A���G�E�A��G�F B�\G�F�A�G�G A���G�G�A�p�G�H BG�H�B(�G�I B�
G�I�B �G�J B�
G�J�A��
G�K A��G�K�A�z�G�L BG�L�B��G�M B ��G�M�A�z�G�N A��\G�N�A��\G�O A�ffG�O�A��G�P A�z�G�P�A�
=G�Q A�G�Q�A�p�G�R A���G�R�A��HG�S A��G�S�B�G�T A�G�T�A�RG�U B ��G�U�BG�V B�\G�V�A�G�W A�  G�W�B�
G�X B  G�X�B
=G�Y A�G�G�Y�A�=qG�Z A��G�Z�A��G�[ A��G�[�B\)G�\ B{G�\�A�
=G�] A�
=G�]�B ��G�^ B��G�^�B�
G�_ B��G�_�BG�` B  G�`�B=qG�a A�33G�a�A���G�b A��G�b�A�  G�c A�(�G�c�A��RG�d B{G�d�A���G�e B (�G�e�B��G�f B�G�f�B��G�g B �\G�g�B�\G�h B�\G�h�A���G�i A��\G�i�B\)G�j A�z�G�j�A���G�k B 
=G�k�B�G�l A���G�l�BG�G�m B �RG�m�A�G�G�n A��G�n�A���G�o A���G�o�A��HG�p A�G�G�p�A�ffG�q A�G�q�A�(�G�r B��G�r�B�
G�s BG�G�s�B��G�t BG�G�t�A�G�G�u B�G�u�Bp�G�v B	�G�v�A�G�w A��
G�w�A�ffG�x B \)G�x�Bz�G�y B\)G�y�B�\G�z B�G�z�B�G�{ B{G�{�Bz�G�| B�G�|�B�G�} B �G�}�Bz�G�~ B{G�~�BG� B�
G��B
=G̀ A�ffG̀�A�33Ǵ A�Ǵ�A�=qĜ B33Ĝ�B{G̃ B��G̃�B��Ḡ B�\Ḡ�B=qG̅ B�
G̅�BG�Ğ B�
Ğ�B�
Ġ B�Ġ�B�G̈ B
�RG̈�B�RG̉ Bp�G̉�B\)G̊ B(�G̊�BQ�G̋ Bp�G̋�B�HǦ B\)Ǧ�B��G̍ B\)G̍�B{G̎ Bz�G̎�Bp�G̏ B�G̏�B(�G̐ B
\)G̐�B
�HG̑ B�G̑�B�HG̒ A�p�G̒�B��G̓ B�G̓�B��G̔ B=qG̔�Bz�G̕ BG̕�BffG̖ A�G�G̖�A�G�G̗ B\)G̗�BG�G̘ BG̘�B	��G̙ BG̙�B(�G̚ B
(�G̚�B{G̛ A�G̛�A��HG̜ BG̜�BG̝ A�z�G̝�A��RG̞ BG̞�B
  G̟ B	G�G̟�B��G̠ B�\G̠�B�HG̡ B33G̡�Bp�G̢ Bp�G̢�B��G̣ B z�G̣�BffG̤ B�G̤�B{G̥ B
=G̥�B z�G̦ B=qG̦�A�33Ģ B�Ģ�BffG̨ B��G̨�B
ffG̩ B=qG̩�B  G̪ B�G̪�B �\G̫ B��G̫�B	�G̬ B
=G̬�BG�G̭ B(�G̭�B�
G̮ A�G̮�A�z�G̯ A�p�G̯�A�33G̰ B  G̰�B	G̱ B��G̱�BffG̲ B�G̲�B��G̳ B�\G̳�B�HG̴ BffG̴�BQ�G̵ B��G̵�B33G̶ Bz�G̶�B��G̷ B �G̷�B
=G̸ BG̸�B��G̹ B	  G̹�B�G̺ B	�G̺�B	G̻ B��G̻�B
��G̼ B�G̼�B
G�G̽ B=qG̽�BffG̾ B��G̾�Bz�G̿ BQ�G̿�B(�G�� B(�G���B  G�� B�G���B�G�� B�HG�B��G�� B�G�ÀBz�G�� A�p�G�ĀA�ffG�� A�  G�ŀA�p�G�� B��G�ƀBffG�� BG�G�ǀA�33G�� A��RG�ȀA���G�� A�G�ɀA�
=G�� A�  G�ʀA�
=G�� Aߙ�G�ˀA�Q�G�� A���G�̀A�33G�� A��RG�̀A�RG�� A�33G�΀A���G�� A���G�πA�(�G�� A�(�G�ЀA陚G�� A��G�рB
=G�� Bz�G�ҀB  G�� B�G�ӀB�G�� B��G�ԀB�G�� B=qG�ՀBG�G�� B{G�րB�G�� Bp�G�׀Bz�G�� B��G�؀B(�G�� B \)G�ـB�G�� B�G�ڀA�(�G�� A��
G�ۀB33G�� B�HG�܀B{G�� BG�݀B	\)G�� B	  G�ހBG�G�� A�Q�G�߀A���G�� B(�G���B(�G�� B��G��Bp�G�� B�G��A�\)G�� B
=G��B{G�� BQ�G��B�G�� BG�G��B	�HG�� A�G�G��B�G�� B��G��B��G�� B	\)G��B�\G�� B
=G��B�G�� B��G��BG�� B  G��B�HG�� B�RG��B��G�� B
\)G��B�RG�� B=qG��B(�G�� B�G��B��G�� B�\G���B{G�� B��G��B
�HG�� B33G��B�G�� B
=G��Bz�G�� B�G��B{G�� Bz�G���B{G�� B�G���B  G�� B��G���B�\G�� B��G���B  G�� B\)G���B�HG�� B
��G���B�G�� B
�HG���B��G�� B=qG���BG�G�� B��G���BffG�� Bp�G���BffG�� B�G���B(�G�  Bp�G� �B�RG� B�G��B��G� B
�
G��B33G� B
  G��B��G� B{G��B33G� Bp�G��BffG� B  G��Bp�G� B�G��B
=G� Bp�G��B�G�	 B�HG�	�B�HG�
 B
=G�
�B�\G� B��G��B�RG� B33G��B(�G� B  G��Bp�G� BffG��B�G� A���G��A�ffG� B�G��B�G� B33G��B p�G� A��G��A���G� B=qG��B
=G� BG��B��G� B	��G��BQ�G� B
=G��A���G� Bp�G��B33G� B��G��BQ�G� B�RG��B
�\G� B�G��B
�\G� B	�G��B  G� BQ�G��Bp�G� Bz�G��B  G� B  G��B {G� BffG��B
z�G�  B	{G� �B	{G�! B�HG�!�B	{G�" BffG�"�B�RG�# B��G�#�B33G�$ B�G�$�B=qG�% Bp�G�%�A�
=G�& B33G�&�B33G�' B�\G�'�BffG�( Bp�G�(�BG�G�) BQ�G�)�B  G�* B��G�*�A�(�G�+ A�{G�+�A�\)G�, A��
G�,�A��G�- BffG�-�B	�G�. B�\G�.�A�G�/ A�(�G�/�B(�G�0 B  G�0�B��G�1 A��G�1�B��G�2 BG�2�B��G�3 B
=qG�3�B�HG�4 A���G�4�B
=G�5 B\)G�5�B\)G�6 BG�G�6�B �\G�7 B 33G�7�B
=G�8 B\)G�8�Bp�G�9 B
�G�9�B	�G�: B�G�:�B�G�; A�Q�G�;�B33G�< B�G�<�B�
G�= B �RG�=�A�(�G�> BffG�>�A�=qG�? A�z�G�?�A���G�@ A�  G�@�A��G�A A��\G�A�A�G�G�B A���G�B�A�(�G�C A�{G�C�B�G�D B��G�D�Bz�G�E B\)G�E�A�=qG�F A��G�F�A�{G�G A�z�G�G�A��HG�H A��G�H�A���G�I A�ffG�I�A��G�J A��G�J�A�
=G�K A�RG�K�A�G�L A��G�L�B�
G�M B G�G�M�B�G�N A���G�N�A�G�G�O B�G�O�B��G�P Bz�G�P�A���G�Q A��HG�Q�B p�G�R BffG�R�Bz�G�S A�=qG�S�A���G�T B�G�T�B��G�U B�G�U�Bp�G�V Bz�G�V�B�G�W It#�G�W�It#�G�X It#�G�X�It#�G�Y It#�G�Y�It#�G�Z It#�G�Z�It#�G�[ It#�G�[�It#�G�\ It#�G�\�It#�G�] It#�G�]�It#�G�^ It#�G�^�It#�G�_ It#�G�_�It#�G�` It#�G�`�It#�G�a It#�G�a�It#�G�b It#�G�b�It#�G�c It#�G�c�It#�G�d It#�G�d�It#�G�e It#�G�e�It#�G�f It#�G�f�It#�G�g It#�G�g�It#�G�h It#�G�h�It#�G�i It#�G�i�It#�G�j It#�G�j�It#�G�k It#�G�k�It#�G�l It#�G�l�It#�G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�G̀ It#�G̀�It#�Ǵ It#�Ǵ�It#�G͂ It#�G͂�It#�G̓ It#�G̓�It#�G̈́ It#�G̈́�It#�Gͅ It#�Gͅ�It#�G͆ It#�G͆�It#�G͇ It#�G͇�It#�G͈ It#�G͈�It#�G͉ It#�G͉�It#�G͊ It#�G͊�It#�G͋ It#�G͋�It#�G͌ It#�G͌�It#�G͍ It#�G͍�It#�G͎ It#�G͎�It#�G͏ It#�G͏�It#�G͐ It#�G͐�It#�G͑ It#�G͑�It#�G͒ It#�G͒�It#�G͓ It#�G͓�It#�G͔ It#�G͔�It#�G͕ It#�G͕�It#�G͖ It#�G͖�It#�G͗ It#�G͗�It#�G͘ It#�G͘�B�HG͙ B��G͙�BG͚ B\)G͚�B=qG͛ B
=G͛�BQ�G͜ B�G͜�B��G͝ B�G͝�B
=G͞ B33G͞�B\)G͟ B(�G͟�B�G͠ B�G͠�B(�G͡ B�G͡�B�G͢ B�RG͢�B�Gͣ B��Gͣ�B
=Gͤ B��Gͤ�B�\Gͥ B�HGͥ�B�Gͦ B��Gͦ�Bz�Gͧ BGͧ�B=qGͨ B�Gͨ�B\)Gͩ BGͩ�BG�Gͪ BG�Gͪ�B�Gͫ B
��Gͫ�B{Gͬ B�Gͬ�B��Gͭ B33Gͭ�B�Gͮ B
=Gͮ�Bp�Gͯ B�Gͯ�B ��GͰ Bz�GͰ�Bz�Gͱ B\)Gͱ�B�
GͲ B�\GͲ�B  Gͳ B�Gͳ�B��Gʹ BGʹ�B�RG͵ B�G͵�B(�GͶ B�\GͶ�B�HGͷ B�Gͷ�B��G͸ BG͸�B��G͹ B��G͹�BGͺ B\)Gͺ�BffGͻ Bz�Gͻ�BG�Gͼ BG�Gͼ�B�Gͽ B�
Gͽ�B33G; B�G;�B  GͿ B��GͿ�B  G�� B33G���BG�G�� BG�G���B�G�� B�G�BQ�G�� B{G�ÀB��G�� B�G�ĀB�RG�� B��G�ŀB{G�� B�
G�ƀB�G�� B�G�ǀB�G�� B
=G�ȀBQ�G�� B
=G�ɀB�G�� B�G�ʀB�
G�� Bz�G�ˀB��G�� B=qG�̀Bz�G�� BQ�G�̀B
=G�� Bp�G�΀B\)G�� B
=G�πB  G�� BG�ЀB�RG�� B33G�рB��G�� B�HG�ҀB��G�� BffG�ӀB�
G�� B�G�ԀB�\G�� B�G�ՀB��G�� B�G�րB{G�� B�\G�׀B(�G�� B�G�؀B��G�� BG�ـB��G�� B�\G�ڀBz�G�� BffG�ۀB��G�� B33G�܀Bp�G�� B ��G�݀B�G�� B�RG�ހB�G�� B�HG�߀B
=G�� Bz�G���BG�G�� B!�G��BffG�� B
G��B��G�� B=qG��Bz�G�� B��G��BG�� BffG��B�
G�� B{G��B	��G�� BG�G��Bp�G�� B33G��B��G�� BffG��B�G�� Bz�G��B�G�� B(�G��B  G�� BG�G��B�\G�� B��G��B�G�� B  G��Bz�G�� B{G��B�\G�� BffG���B�G�� B!(�G��BG�� Bp�G��BG�G�� B�HG��Bz�G�� B
=G��B�G�� B{G���B(�G�� B�RG���B��G�� B��G���B��G�� B��G���B�G�� B33G���B�G�� B��G���B��G�� B��G���B�G�� B=qG���BQ�G�� B�RG���B��G�� B\)G���B��G�� B=qG���B�G�  B33G� �BG� B�HG��BG�G� B�G��BffG� B{G��B
=G� B{G��B��G� B�HG��Bz�G� B
�G��B
=G� Bz�G��Bz�G� B  G��B�HG�	 B�\G�	�B�G�
 B=qG�
�B  G� B�G��B(�G� B��G��B(�G� B=qG��B�G� B��G��B
ffG� B33G��Bp�G� BG��B�RG� B�HG��B�G� B  G��B{G� BG��B�
G� B��G��BG�G� B��G��B�
G� B(�G��B�
G� B�G��B�HG� B��G��B=qG� B�G��B�HG� B�G��B�\G� B��G��Bz�G� B�\G��B{G� BffG��B  G� B  G��B��G� B��G��B��G�  B{G� �B��G�! B��G�!�B��G�" BffG�"�BG�G�# B�G�#�B�G�$ B��G�$�B
=G�% B�
G�%�B�G�& B�RG�&�B�\G�' B�G�'�B�G�( BffG�(�BG�) B�G�)�B�G�* B��G�*�B�G�+ Bp�G�+�B�G�, B\)G�,�BG�G�- B��G�-�B�G�. B�G�.�BQ�G�/ B{G�/�B��G�0 B��G�0�B{G�1 B��G�1�B�G�2 B�HG�2�B�G�3 B�HG�3�B�G�4 BffG�4�B(�G�5 BQ�G�5�B
=G�6 B=qG�6�BQ�G�7 B��G�7�BG�G�8 B�
G�8�B  G�9 B=qG�9�Bz�G�: B
G�:�B
G�; B��G�;�B�
G�< B�
G�<�B  G�= B�G�=�B=qG�> B{G�>�B�
G�? B
�G�?�B�HG�@ B��G�@�B�HG�A Bz�G�A�B�G�B BG�G�B�BG�G�C B��G�C�B��G�D B��G�D�B�\G�E B�
G�E�B��G�F B�
G�F�B
�HG�G B  G�G�B  G�H BG�H�B
=G�I B{G�I�BG�J B	=qG�J�B�RG�K B33G�K�B\)G�L BQ�G�L�BG�G�M B
=G�M�B ��G�N A�{G�N�B��G�O B33G�O�B	�G�P B G�G�P�B�HG�Q B�HG�Q�B ��G�R B\)G�R�B	  G�S BQ�G�S�B  G�T B
=G�T�B�G�U B��G�U�B�G�V B�G�V�B\)G�W B�HG�W�B(�G�X B=qG�X�BG�Y B
=G�Y�B�G�Z B�G�Z�B��G�[ B\)G�[�BffG�\ B�
G�\�B��G�] A�{G�]�B=qG�^ B��G�^�A�z�G�_ B
�\G�_�B�G�` B33G�`�B
=G�a B(�G�a�BG�G�b B
��G�b�B�G�c B{G�c�BQ�G�d B�G�d�B�G�e B	\)G�e�B��G�f Bz�G�f�Bz�G�g B
=G�g�A��G�h B{G�h�B�G�i B�RG�i�B��G�j B(�G�j�B��G�k B��G�k�B  G�l B	��G�l�B{G�m B
=G�m�B\)G�n B 
=G�n�B�\G�o B
=G�o�B�\G�p B=qG�p�B=qG�q B=qG�q�B{G�r BffG�r�B�G�s B33G�s�B�G�t A�Q�G�t�A��G�u A��
G�u�B	\)G�v B�G�v�B �
G�w A�ffG�w�A��G�x A�
=G�x�B��G�y Bp�G�y�A��G�z A�p�G�z�B{G�{ B=qG�{�B{G�| B�G�|�B\)G�} B�\G�}�BQ�G�~ B��G�~�B��G� B
=G��BffG΀ B	
=G΀�B	�\G΁ B�G΁�B�G΂ B�G΂�B��G΃ Bz�G΃�A��RG΄ BG΄�B�RG΅ B
�G΅�B\)GΆ B=qGΆ�B	�RG· BG�G·�A��GΈ A��GΈ�BQ�GΉ BGΉ�B��GΊ BG�GΊ�B�\G΋ A���G΋�B Q�GΌ B33GΌ�B
\)G΍ B{G΍�B33GΎ B=qGΎ�B�HGΏ B��GΏ�BG�Gΐ B��Gΐ�B�GΑ B
=GΑ�B�\GΒ B �GΒ�B�
GΓ B�GΓ�B��GΔ A�{GΔ�B��GΕ BffGΕ�B33GΖ BGΖ�A���GΗ B(�GΗ�Bp�GΘ B�\GΘ�B
\)GΙ B=qGΙ�B��GΚ B�GΚ�B�HGΛ BG�GΛ�BffGΜ A�p�GΜ�B �RGΝ B
�RGΝ�B	�GΞ B(�GΞ�B�\GΟ A���GΟ�B��GΠ B{GΠ�B�
GΡ Bp�GΡ�B�G΢ B\)G΢�A�(�GΣ A�  GΣ�B�RGΤ B	�
GΤ�B
=GΥ B(�GΥ�B\)GΦ B
=GΦ�B�\GΧ B(�GΧ�Bp�GΨ B�
GΨ�B	�
GΩ B��GΩ�B(�GΪ B�GΪ�B��GΫ B��GΫ�B��Gά B{Gά�B��Gέ B	{Gέ�B	Gή Bp�Gή�B�Gί B  Gί�BQ�Gΰ BffGΰ�B{Gα B��Gα�B��Gβ B
�HGβ�B(�Gγ B�
Gγ�B�Gδ B�Gδ�BQ�Gε B
�HGε�B�
Gζ B��Gζ�BQ�Gη B��Gη�B�
Gθ BG�Gθ�B33Gι B\)Gι�B��Gκ B�Gκ�B  Gλ Bz�Gλ�B{Gμ B{Gμ�B	\)Gν B�RGν�B��Gξ B
�RGξ�B{Gο B(�Gο�B=qG�� B
�\G���B�RG�� BG���B�G�� B	�RG�B=qG�� B=qG�ÀB�
G�� B{G�ĀB{G�� BffG�ŀB=qG�� B�G�ƀB�
G�� B33G�ǀBp�G�� B�RG�ȀB�G�� B�HG�ɀB�G�� B33G�ʀB��G�� B��G�ˀB�\G�� B	�G�̀B��G�� B	�HG�̀B�G�� B�\G�΀B
=G�� B
=G�πBG�� Bz�G�ЀB�\G�� BQ�G�рBG�� B��G�ҀB  G�� B(�G�ӀB{G�� Bp�G�ԀB{G�� B�RG�ՀB��G�� B	
=G�րB	��G�� B(�G�׀Bp�G�� B\)G�؀BG�� B�HG�ـB33G�� B��G�ڀBG�G�� B33G�ۀB{G�� B\)G�܀B�G�� B (�G�݀B��G�� B�G�ހB�RG�� B{G�߀B�HG�� B��G���B{G�� BQ�G��B��G�� BG�G��BG�G�� B
=G��B�
G�� B�HG��B{G�� B�RG��B(�G�� B��G��BG�� BQ�G��B
�HG�� B��G��B�RG�� B  G��B�G�� B\)G��BQ�G�� B�G��B�G�� B�RG��B{G�� BffG��B�G�� B(�G��B=qG�� B
�G��BQ�G�� B	=qG���B�G�� B�HG��B33G�� B�
G��B�
G�� B�\G��B��G�� B��G��BQ�G�� B p�G���B ��G�� BffG���B	{G�� B�RG���BG�� B�RG���B\)G�� B
=G���B��G�� B��G���B  G�� BG���B�\G�� B�RG���Bp�G�� B�HG���B
\)G�� B  G���B	�G�� B ��G���B�G�  B  G� �B��G� B�\G��B�\G� B	�G��BG�G� B\)G��BQ�G� B�HG��B  G� B ��G��B=qG� Bp�G��B�G� Bp�G��A��RG� B ��G��BffG�	 B Q�G�	�A���G�
 B�
G�
�BG� BQ�G��BG� B�G��B�G� B�G��B�G� Bz�G��B\)G� B�\G��B�G� B�HG��B�G� B�HG��B\)G� B
�G��B
��G� BQ�G��BG�G� B
ffG��B\)G� BQ�G��B��G� B(�G��B	��G� B(�G��B\)G� Bz�G��B=qG� B(�G��B��G� B�G��B
�\G� B	��G��B�G� B
=G��B�HG� B�G��B
=G� B�
G��B�HG� B\)G��B�HG�  B{G� �B��G�! B=qG�!�B�G�" B�G�"�B
Q�G�# B\)G�#�B(�G�$ B�RG�$�B��G�% B��G�%�Bz�G�& B�HG�&�B�G�' B�G�'�B\)G�( B  G�(�B�G�) B
=G�)�B�G�* BQ�G�*�B��G�+ B33G�+�Bz�G�, B  G�,�B=qG�- B	z�G�-�B�G�. BG�G�.�B\)G�/ B��G�/�B�
G�0 B\)G�0�B  G�1 B33G�1�Bz�G�2 B�HG�2�B\)G�3 B�
G�3�B�RG�4 BG�G�4�B\)G�5 B	
=G�5�B�HG�6 B�G�6�B�
G�7 BQ�G�7�B�\G�8 B�
G�8�B�HG�9 B�RG�9�B{G�: B\)G�:�BG�; B�G�;�B33G�< B�G�<�B
G�= B�G�=�B	=qG�> B	Q�G�>�B
  G�? B�RG�?�BffG�@ B�G�@�B�G�A BffG�A�B�G�B BG�G�B�B33G�C B
G�C�B=qG�D B�G�D�B{G�E B�RG�E�B�G�F B	Q�G�F�B�
G�G Bp�G�G�Bp�G�H B
\)G�H�B�G�I B\)G�I�B��G�J B��G�J�B�
G�K B��G�K�BG�L B	z�G�L�B	�
G�M B��G�M�B�\G�N B33G�N�B33G�O B\)G�O�B�HG�P It#�G�P�It#�G�Q It#�G�Q�It#�G�R It#�G�R�It#�G�S It#�G�S�It#�G�T It#�G�T�It#�G�U It#�G�U�It#�G�V It#�G�V�It#�G�W It#�G�W�It#�G�X It#�G�X�It#�G�Y It#�G�Y�It#�G�Z It#�G�Z�It#�G�[ It#�G�[�It#�G�\ It#�G�\�It#�G�] It#�G�]�It#�G�^ It#�G�^�It#�G�_ It#�G�_�It#�G�` It#�G�`�It#�G�a It#�G�a�It#�G�b It#�G�b�It#�G�c It#�G�c�It#�G�d It#�G�d�It#�G�e It#�G�e�It#�G�f It#�G�f�It#�G�g It#�G�g�It#�G�h It#�G�h�It#�G�i It#�G�i�It#�G�j It#�G�j�It#�G�k It#�G�k�It#�G�l It#�G�l�It#�G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�Gπ It#�Gπ�It#�Gρ It#�Gρ�It#�Gς It#�Gς�It#�Gσ It#�Gσ�It#�Gτ It#�Gτ�It#�Gυ It#�Gυ�It#�Gφ It#�Gφ�It#�Gχ It#�Gχ�It#�Gψ It#�Gψ�It#�Gω It#�Gω�It#�Gϊ It#�Gϊ�It#�Gϋ It#�Gϋ�It#�Gό It#�Gό�It#�Gύ It#�Gύ�It#�Gώ It#�Gώ�It#�GϏ It#�GϏ�It#�Gϐ It#�Gϐ�It#�Gϑ It#�Gϑ�B	�\Gϒ B�Gϒ�B��Gϓ B{Gϓ�B��Gϔ BGϔ�A�Q�Gϕ B=qGϕ�B�Gϖ B�Gϖ�B(�Gϗ B
  Gϗ�B	��GϘ BGϘ�BGϙ B��Gϙ�BQ�GϚ B��GϚ�B��Gϛ B��Gϛ�B��GϜ B	Q�GϜ�B�Gϝ B	Q�Gϝ�B�GϞ B�HGϞ�B��Gϟ Bp�Gϟ�B�GϠ BGϠ�B�Gϡ A�  Gϡ�BQ�GϢ B�GϢ�B�Gϣ BG�Gϣ�Bz�GϤ B��GϤ�B�Gϥ B�
Gϥ�B�
GϦ B��GϦ�B�HGϧ Bz�Gϧ�B�RGϨ B{GϨ�B�HGϩ B�HGϩ�B�GϪ Bp�GϪ�B�Gϫ B  Gϫ�B��GϬ B�GϬ�B��Gϭ BQ�Gϭ�B=qGϮ B\)GϮ�B�RGϯ B�Gϯ�B�Gϰ B  Gϰ�B	�\Gϱ B��Gϱ�BQ�Gϲ Bz�Gϲ�B�Gϳ B�\Gϳ�B��Gϴ B33Gϴ�A��Gϵ B��Gϵ�B33G϶ B{G϶�B�
GϷ BQ�GϷ�Bz�Gϸ B�HGϸ�B�\GϹ B�GϹ�B�GϺ B{GϺ�B  Gϻ B��Gϻ�B
��Gϼ B�\Gϼ�B  GϽ BQ�GϽ�B�RGϾ B33GϾ�B�HGϿ B=qGϿ�B{G�� B  G���B33G�� B�HG���B�
G�� B	\)G�BQ�G�� B
z�G�ÀB�G�� B{G�ĀB��G�� B\)G�ŀBG�� B(�G�ƀB�G�� B  G�ǀBffG�� B{G�ȀB�
G�� B�G�ɀB{G�� B(�G�ʀB=qG�� B�G�ˀBffG�� B	Q�G�̀B�\G�� B	�G�̀B
�
G�� B�G�΀BG�� BG�πB{G�� B�G�ЀB�RG�� BG�рB�G�� B�G�ҀB	��G�� B	�G�ӀB  G�� B
�HG�ԀB�\G�� B��G�ՀBp�G�� B�RG�րB	Q�G�� B
=G�׀B�HG�� B�G�؀B�G�� A��G�ـB��G�� B{G�ڀB��G�� B
�\G�ۀB��G�� B ffG�܀B��G�� B�\G�݀BQ�G�� Bz�G�ހBz�G�� B
=G�߀B�\G�� B	\)G���BQ�G�� B�G��B�RG�� B��G��Bp�G�� B(�G��BG�G�� Bz�G��B{G�� B(�G��B��G�� Bp�G��Bz�G�� BG�G��B=qG�� B\)G��B�HG�� B	ffG��B�G�� A�Q�G��B�G�� B��G��B�G�� B�G��BG�� B 33G��B�\G�� B�G��BG�G�� B�RG��BG�G�� B��G���B=qG�� B��G��B(�G�� BG��B
��G�� B	�G��B  G�� B33G��B=qG�� B
p�G���B	p�G�� B\)G���B��G�� B=qG���BG�G�� B(�G���B�G�� B(�G���B�RG�� BQ�G���A�\)G�� B
ffG���B\)G�� B��G���B�G�� B(�G���B�HG�� B
�\G���B�\G�� B�RG���B�
G�  B	��G� �B�G� B�G��B��G� B�G��B��G� B(�G��B�RG� Bp�G��B
ffG� B33G��B�RG� BG�G��B�
G� B�G��B
Q�G� BG�G��B�\G�	 B  G�	�B(�G�
 B  G�
�B\)G� B�\G��B��G� Bp�G��B�G� B��G��B
=G� B�
G��B33G� Bp�G��B{G� B�G��Bp�G� BQ�G��B�G� A�{G��A�{G� B��G��B�G� B	�G��B��G� B��G��BG�G� BG��B=qG� B��G��B�
G� B{G��B��G� B

=G��B(�G� B�\G��B33G� B
=qG��B��G� Bz�G��B��G� A�{G��BG� B�\G��B  G� B�G��B�\G�  Bp�G� �B
{G�! B
=qG�!�B
=G�" B	�\G�"�B	33G�# B�G�#�B
��G�$ B	��G�$�B(�G�% B��G�%�B	�RG�& B
��G�&�B	ffG�' BQ�G�'�Bp�G�( B��G�(�B	(�G�) BG�G�)�B��G�* BQ�G�*�B=qG�+ B��G�+�B
=G�, B��G�,�B\)G�- B�
G�-�B�RG�. B\)G�.�BG�G�/ Bz�G�/�B�RG�0 B�
G�0�B��G�1 Bp�G�1�B{G�2 B�G�2�B	p�G�3 B�RG�3�A�{G�4 B�G�4�B�\G�5 B  G�5�A��\G�6 A�\)G�6�A�=qG�7 Bz�G�7�BG�G�8 BQ�G�8�B�G�9 B�G�9�B�HG�: B�HG�:�BG�G�; B�\G�;�BQ�G�< B�HG�<�Bz�G�= B  G�=�B��G�> B	��G�> Bz�G�>�B��G�? B\)G�?�Bp�G�@ B �RG�@�B��G�A B�G�A�B�G�B B  G�B�BffG�C A�\)G�C�B(�G�D B�HG�D�BG�E B��G�E�B��G�F B��G�F�Bz�G�G B��G�G�B�HG�H B33G�H�B  G�I B��G�I�BQ�G�J B\)G�J�B�RG�K B

=G�K�B	(�G�L B�
G�L�B\)G�M B��G�M�B�G�N A��RG�N�A�G�G�O B	(�G�O�Bz�G�P B�HG�P�A�G�Q Bp�G�Q�B�G�R B�RG�R�B�G�S B��G�S�B	��G�T B	�HG�T�BG�U BG�U�B�HG�V B
=G�V�B��G�W B�G�W�B\)G�X B�RG�X�B  G�Y B33G�Y�B  G�Z B�
G�Z�B=qG�[ B
=G�[�B�HG�\ B�G�\�B��G�] B=qG�]�BG�^ B
{G�^�B  G�_ B  G�_�B�G�` B�HG�`�B�G�a B�HG�a�B	G�b B	G�b�B	=qG�c B�G�c�B�RG�d BffG�d�B�G�e BffG�e�B
��G�f B{G�f�B��G�g B�\G�g�B  G�h BG�G�h�B�
G�i B�G�i�B
=G�j B�\G�j�B�HG�k BffG�k�B�RG�l B	{G�l�BG�G�m A���G�m�BffG�n B��G�n�B=qG�o A��G�o�B G�p B�\G�p�B��G�q B=qG�q�B�HG�r B=qG�r�B	��G�s B��G�s�B�HG�t B�G�t�B�HG�u B�
G�u�B�G�v B�G�v�B��G�w B�G�w�B�G�x B�G�x�B
=G�y B
(�G�y�BffG�z B��G�z�Bp�G�{ A�ffG�{�B �G�| B�G�|�B
�G�} B
�
G�}�B��G�~ B	
=G�~�B�
G� B��G��B
GЀ B
=GЀ�B��GЁ B�RGЁ�B��GЂ B�GЂ�B	�
GЃ B
=GЃ�B�GЄ Bz�GЄ�B{GЅ B��GЅ�B�
GІ B	{GІ�B
\)GЇ B�GЇ�BG�GЈ B�
GЈ�BGЉ B��GЉ�BG�GЊ B�GЊ�B
�HGЋ B=qGЋ�Bp�GЌ A�GЌ�A�(�GЍ B
=GЍ�BffGЎ B	\)GЎ�B��GЏ BG�GЏ�A�{GА B	  GА�BQ�GБ B��GБ�BQ�GВ BffGВ�B��GГ B��GГ�Bp�GД B��GД�B�GЕ B  GЕ�A�=qGЖ B �HGЖ�A��
GЗ B ��GЗ�B
=GИ A�z�GИ�A��RGЙ B 33GЙ�B�GК Bz�GК�A�z�GЛ B�HGЛ�A�33GМ A�G�GМ�B  GН A���GН�BG�GО B	�\GО�B�\GП B
p�GП�B�GР B
=GР�B	GС B�RGС�BQ�GТ B=qGТ�B(�GУ A���GУ�B��GФ B�
GФ�B
33GХ BG�GХ�B�GЦ Bz�GЦ�B�GЧ B
=GЧ�B
z�GШ B	{GШ�B  GЩ B	��GЩ�B=qGЪ B��GЪ�B�GЫ B�GЫ�B  GЬ B��GЬ�B�
GЭ B33GЭ�B\)GЮ B�\GЮ�B
(�GЯ B	�GЯ�B	G�Gа B\)Gа�BffGб A�\)Gб�B=qGв BffGв�B
  Gг B��Gг�B(�Gд B33Gд�BG�Gе B(�Gе�B�Gж B�RGж�B
(�Gз B�
Gз�BG�Gи B\)Gи�B  Gй B ��Gй�Bz�Gк B�Gк�BffGл Bz�Gл�B�Gм BQ�Gм�BffGн B��Gн�BffGо B��Gо�B=qGп B�Gп�B��G�� B
�RG���BG�� B��G���B��G�� BQ�G�B��G�� B��G�ÀBp�G�� B��G�ĀB�\G�� B�\G�ŀBp�G�� B
33G�ƀB	�G�� B
=G�ǀB�\G�� B�RG�ȀB	G�G�� B��G�ɀB  G�� B  G�ʀB\)G�� B�G�ˀBQ�G�� B
�G�̀B  G�� B  G�̀B

=G�� BffG�΀Bp�G�� BG�G�πB(�G�� B=qG�ЀB�HG�� B33G�рB
=qG�� B{G�ҀB�\G�� B(�G�ӀBp�G�� B�HG�ԀB��G�� B	
=G�ՀB	�G�� B	ffG�րB\)G�� Bp�G�׀B
{G�� B
(�G�؀B�
G�� B33G�ـB �G�� B�G�ڀBG�� B�\G�ۀB{G�� B
��G�܀B�G�� B��G�݀B
=G�� B
��G�ހB
G�G�� B{G�߀A�=qG�� B
=G���B
33G�� B�G��B
G�G�� B
��G��Bp�G�� B(�G��B�G�� B�G��B��G�� Bz�G��B	{G�� B�G��B�HG�� B �G��B\)G�� B�G��B��G�� BG��B
=G�� B��G��BffG�� Bz�G��A��HG�� A��HG��BffG�� B�\G��B(�G�� BG��BQ�G�� B��G��B33G�� B�HG���B  G�� B�G��B�HG�� B�G��B��G�� B�G��B�G�� B	z�G��B
�HG�� B��G���B
=G�� B	z�G���B=qG�� B�RG���B�G�� Bp�G���B
�G�� B(�G���BG�G�� B�G���B  G�� B	  G���BffG�� B�G���B�
G�� B
33G���B�G�� B=qG���B	=qG�� B�G���B =qG�  B =qG� �B=qG� B��G��B�G� B�G��B  G� B�\G��B�RG� B{G��B	  G� BG��B
=G� B
=qG��B��G� B�HG��B  G� B G�G��B	�G�	 BG�	�B	�RG�
 B	�\G�
�Bp�G� B��G��B	�HG� B��G��Bz�G� B�\G��BffG� B�G��B��G� B�G��B�HG� B��G��B  G� B�G��BQ�G� Bz�G��B	�\G� B
(�G��B	z�G� B�HG��B�G� B\)G��B33G� B	�G��B�G� B
��G��BffG� B=qG��Bz�G� B	p�G��B
��G� B
��G��B�G� B��G��B�HG� B
=G��B
=G� BQ�G��B�
G� B	G��B	�G� BQ�G��A���G�  B�G� �B��G�! BG�!�B�G�" B�G�"�B �HG�# B��G�#�B
��G�$ B
=qG�$�B	��G�% B\)G�%�B  G�& B\)G�&�Bp�G�' B�\G�'�B
{G�( B��G�(�B	ffG�) A��G�)�A�  G�* B�HG�*�B{G�+ BQ�G�+�B�
G�, BffG�,�BG�G�- B�HG�-�BG�G�. BQ�G�.�Bp�G�/ A�z�G�/�BffG�0 B�HG�0�B�HG�1 B�RG�1�BG�2 B�G�2�B�\G�3 B	{G�3�B33G�4 B�RG�4�B��G�5 B
�G�5�B{G�6 Bz�G�6�Bp�G�7 Bp�G�7�B
��G�8 B��G�8�B�G�9 B
�RG�9�B	(�G�: B�G�:�B33G�; B{G�;�BffG�< A�p�G�<�B�G�= B�
G�=�B	�HG�> B��G�>�B
Q�G�? B�
G�?�B	\)G�@ B{G�@�B(�G�A B33G�A�A��G�B B{G�B�B33G�C B
(�G�C�B=qG�D B33G�D�B�G�E B
=G�E�B��G�F B�G�F�B�
G�G B	�G�G�B
=G�H A�G�H�It#�G�I It#�G�I�It#�G�J It#�G�J�It#�G�K It#�G�K�It#�G�L It#�G�L�It#�G�M It#�G�M�It#�G�N It#�G�N�It#�G�O It#�G�O�It#�G�P It#�G�P�It#�G�Q It#�G�Q�It#�G�R It#�G�R�It#�G�S It#�G�S�It#�G�T It#�G�T�It#�G�U It#�G�U�It#�G�V It#�G�V�It#�G�W It#�G�W�It#�G�X It#�G�X�It#�G�Y It#�G�Y�It#�G�Z It#�G�Z�It#�G�[ It#�G�[�It#�G�\ It#�G�\�It#�G�] It#�G�]�It#�G�^ It#�G�^�It#�G�_ It#�G�_�It#�G�` It#�G�`�It#�G�a It#�G�a�It#�G�b It#�G�b�It#�G�c It#�G�c�It#�G�d It#�G�d�It#�G�e It#�G�e�It#�G�f It#�G�f�It#�G�g It#�G�g�It#�G�h It#�G�h�It#�G�i It#�G�i�It#�G�j It#�G�j�It#�G�k It#�G�k�It#�G�l It#�G�l�It#�G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�Gр It#�Gр�It#�Gс It#�Gс�It#�Gт It#�Gт�It#�Gу It#�Gу�It#�Gф It#�Gф�It#�Gх It#�Gх�It#�Gц It#�Gц�It#�Gч It#�Gч�It#�Gш It#�Gш�It#�Gщ It#�Gщ�It#�Gъ Bp�Gъ�BQ�Gы B33Gы�B\)Gь B��Gь�B\)Gэ B

=Gэ�B
�HGю B��Gю�B
=Gя B
�Gя�B
�Gѐ B��Gѐ�B�Gё B{Gё�BffGђ B\)Gђ�BffGѓ B\)Gѓ�Bp�Gє BQ�Gє�B�\Gѕ B
Gѕ�B�
Gі B

=Gі�BGї B

=Gї�B�RGј B�\Gј�B 33Gљ BGљ�B
�
Gњ Bp�Gњ�B
�Gћ B	��Gћ�B�Gќ BG�Gќ�B��Gѝ B
Gѝ�BG�Gў B(�Gў�A�p�Gџ A�ffGџ�A��GѠ B	p�GѠ�B
33Gѡ BG�Gѡ�B33GѢ B
(�GѢ�A�\)Gѣ Bp�Gѣ�B  GѤ B33GѤ�BG�Gѥ B�Gѥ�B
=GѦ B
\)GѦ�B	�\Gѧ B��Gѧ�B=qGѨ B

=GѨ�B�
Gѩ B
ffGѩ�B{GѪ B
p�GѪ�B��Gѫ B��Gѫ�B�GѬ B  GѬ�B	��Gѭ B�Gѭ�B
G�GѮ B��GѮ�Bp�Gѯ B\)Gѯ�B  GѰ B��GѰ�B
{Gѱ B\)Gѱ�B(�GѲ B��GѲ�B�
Gѳ B�Gѳ�B{GѴ B��GѴ�BQ�Gѵ B
�Gѵ�B�
GѶ B�GѶ�BffGѷ B=qGѷ�B�\GѸ B\)GѸ�B	��Gѹ B�\Gѹ�B�GѺ Bp�GѺ�B�HGѻ B�Gѻ�BGѼ B�GѼ�B��Gѽ B
G�Gѽ�B
=GѾ B=qGѾ�B�Gѿ B�HGѿ�B�G�� BffG���BQ�G�� BffG���B	�G�� B��G�B��G�� B\)G�ÀB��G�� Bp�G�ĀBffG�� B(�G�ŀB��G�� B�G�ƀB
�G�� BQ�G�ǀB�
G�� B	�RG�ȀB=qG�� B	�G�ɀB�G�� B  G�ʀB�\G�� B��G�ˀA���G�� B�G�̀B(�G�� BG�G�̀B�
G�� B 
=G�΀A�33G�� A�z�G�πB��G�� B	�G�ЀB
z�G�� B(�G�рB�G�� B  G�ҀB�RG�� B��G�ӀBp�G�� B�G�ԀB	G�G�� B
=G�ՀB�\G�� B
�G�րB�G�� B��G�׀B�G�� B	G�؀B\)G�� B�G�ـB=qG�� B��G�ڀB(�G�� B  G�ۀB�G�� B�G�܀B	(�G�� B33G�݀B�
G�� B�G�ހBG�G�� B�G�߀B�
G�� B
=G���B��G�� B\)G��B{G�� BG�G��B�G�� A�z�G��B �\G�� B	{G��B\)G�� B�G��BQ�G�� B
{G��B�G�� B33G��B��G�� B{G��B  G�� B
\)G��B�RG�� B�G��A��G�� B�G��B{G�� B�\G��A���G�� B�RG��BffG�� B�HG��A��G�� B�G��B(�G�� B��G���B��G�� B=qG��B��G�� Bz�G��B��G�� B(�G��B �G�� BG��B  G�� B��G���B	\)G�� B
��G���B�\G�� B�\G���B
�HG�� B
(�G���B{G�� Bz�G���B �G�� B�
G���BG�� B�G���A���G�� A�\)G���Bp�G�� B33G���B(�G�� B	�G���B��G�� A�Q�G���B  G�  B
��G� �B
G�G� B
=G��BffG� BffG��B�G� Bp�G��BG�G� B=qG��B�\G� B  G��BffG� B�
G��B{G� B\)G��BffG� BffG��BG�	 B�G�	�B
=G�
 B
=G�
�B�G� B��G��B
��G� B
=G��B�G� B��G��B�RG� Bp�G��B�RG� B=qG��B   G� BQ�G��BffG� A��HG��B	��G� B
\)G��Bz�G� BffG��B33G� B�G��B=qG� B��G��BG� B{G��B�G� B��G��B33G� B�G��B=qG� B�G��B{G� B�G��BG� B��G��B��G� BG��BQ�G� B�G��B �
G� A��G��A��HG� B�RG��A�\)G�  B \)G� �B	�G�! B	Q�G�!�BG�G�" Bp�G�"�A���G�# A��G�#�B��G�$ B{G�$�B��G�% B ��G�%�B (�G�& B�G�&�Bz�G�' B��G�'�A��HG�( B��G�(�B��G�) B��G�)�B=qG�* B
�HG�*�B=qG�+ B�G�+�B�G�, B  G�,�B�G�- B�\G�-�BQ�G�. BG�G�.�B
��G�/ B��G�/�A�  G�0 B�
G�0�B�\G�1 B�G�1�B
�G�2 B(�G�2�A���G�3 B��G�3�B	�RG�4 B	��G�4�B33G�5 A�ffG�5�A�G�G�6 A��RG�6�B��G�7 B�\G�7�A�Q�G�8 A�p�G�8�B 
=G�9 B�HG�9�B�G�: BG�G�:�B
�RG�; B=qG�;�B �G�< A���G�<�A�{G�= Bz�G�=�BG�G�> B
�\G�>�B��G�? B �G�?�A��G�@ A��G�@�A�=qG�A A�z�G�A�B33G�B B
�G�B�B�G�C B(�G�C�BG�G�D B
�\G�D�B�HG�E BG�E�B
=G�F B33G�F�B
=G�G A�{G�G�A���G�H B{G�H�B	(�G�I B�RG�I�B�G�J B {G�J�BG�G�K B��G�K�A��HG�L B \)G�L�B��G�M B(�G�M�BG�G�N B
=G�N�B=qG�O B��G�O�B33G�P Bp�G�P�Bp�G�Q BffG�Q�BQ�G�R B�\G�R�BG�S B�G�S�B�
G�T B��G�T�BG�U B�G�U�B��G�V B!��G�V�B�RG�W B�G�W�B�HG�X B  G�X�BffG�Y B$��G�Y�B��G�Z B\)G�Z�B��G�[ BG�[�B$33G�\ B,�
G�\�B,(�G�] B.�HG�]�B*��G�^ B*ffG�^�B,��G�_ B$Q�G�_�B G�` B)��G�`�B(��G�a B ��G�a�B$p�G�b B${G�b�B,�RG�c B7(�G�c�B=Q�G�d B:�HG�d�B1  G�e B*��G�e�B*=qG�f B8��G�f�B?(�G�g B;=qG�g�BE��G�h BE33G�h�BF�RG�i BL�G�i�BF33G�j B@��G�j�BJ�
G�k BIp�G�k�BF��G�l BH=qG�l�BI{G�m BK�G�m�BOz�G�n BU(�G�n�BZ(�G�o BV��G�o�BG�G�p B@G�G�p�BF=qG�q BM=qG�q�BO�
G�r BTG�r�BU�G�s BW  G�s�BX�G�t BO�G�t�BP�G�u BQ{G�u�BHffG�v BG�HG�v�BMG�w BS�\G�w�BQ\)G�x BM
=G�x�BO�G�y BO{G�y�BU�G�z BT��G�z�BR\)G�{ BQ�
G�{�BJ�\G�| BK{G�|�BS�G�} BP�G�}�BE=qG�~ BL=qG�~�BRG�G� BK�G��BN\)GҀ BR{GҀ�BK�Gҁ BNp�Gҁ�BWG҂ BYG҂�BWG҃ BQ{G҃�BR\)G҄ BWz�G҄�B\�\G҅ B\�G҅�BZ�RG҆ B]=qG҆�Ba�\G҇ Bg��G҇�B_G�G҈ Bd33G҈�Bi�G҉ B]�G҉�B[\)GҊ B[\)GҊ�Bf  Gҋ Bd  Gҋ�BfffGҌ B`GҌ�B[�Gҍ BZ�Gҍ�B_\)GҎ Bk��GҎ�BjGҏ Bk��Gҏ�Bgz�GҐ Ba=qGҐ�Bg��Gґ BhGґ�Bi�
GҒ Bi�
GҒ�Bc��Gғ BiGғ�Bk�\GҔ Bj�
GҔ�Bp
=Gҕ Bo�Gҕ�BnG�GҖ BsQ�GҖ�Bl33Gҗ BdGҗ�Bf��GҘ BpQ�GҘ�Bt33Gҙ Bop�Gҙ�Bx\)GҚ Bwz�GҚ�Bu�\Gқ By�RGқ�Bi  GҜ Bh�GҜ�Bgp�Gҝ Bhp�Gҝ�Bkz�GҞ Bs(�GҞ�Bs\)Gҟ Bq{Gҟ�Bs�
GҠ BvG�GҠ�B{�RGҡ Bz�\Gҡ�B���GҢ B���GҢ�B�L�Gң B���Gң�B~(�GҤ B~�GҤ�B}��Gҥ Bu�Gҥ�BzGҦ Bxp�GҦ�Bp��Gҧ B{�Gҧ�B��GҨ By��GҨ�B���Gҩ B���Gҩ�B|��GҪ B���GҪ�B���Gҫ B�G�Gҫ�B��\GҬ B�\GҬ�B��\Gҭ B�L�Gҭ�B��GҮ B�  GҮ�B���Gү B�Gү�B��qGҰ B��)GҰ�B�\)Gұ B�� Gұ�B��RGҲ B�33GҲ�B��3Gҳ B�L�Gҳ�B�  GҴ B�W
GҴ�B�ǮGҵ B��\Gҵ�B�W
GҶ B���GҶ�B��{Gҷ B�z�Gҷ�B�z�GҸ B�  GҸ�B���Gҹ B���Gҹ�B�z�GҺ B��GҺ�B��HGһ B�Gһ�B�#�GҼ B�k�GҼ�B�Q�Gҽ B�#�Gҽ�B���GҾ B�W
GҾ�B���Gҿ B�.Gҿ�B�.G�� B��)G���B�L�G�� B�\)G���B�  G�� B���G�B�z�G�� B�8RG�ÀB�(�G�� B��G�ĀB�k�G�� B���G�ŀB���G�� B��G�ƀB��=G�� B�.G�ǀB���G�� B��\G�ȀB���G�� B���G�ɀB�\)G�� B�
=G�ʀB�{G�� B�{G�ˀB��RG�� B���G�̀B�u�G�� B�ffG�̀B��RG�� B�u�G�΀B�{G�� B�B�G�πB�B�G�� B���G�ЀB�p�G�� B�\G�рB�ffG�� B�  G�ҀB���G�� B��fG�ӀB�Q�G�� B���G�ԀB�#�G�� B�8RG�ՀB���G�� B�ǮG�րB�L�G�� B��
G�׀B��{G�� B�aHG�؀B��fG�� B�.G�ـB���G�� B�ffG�ڀB�#�G�� B�W
G�ۀB�p�G�� B���G�܀B�(�G�� B��fG�݀B�(�G�� B�k�G�ހB�� G�� B�k�G�߀B�� G�� B�G���B�\)G�� B�.G��B�  G�� B�ǮG��B���G�� B��RG��B��G�� B�Q�G��B��G�� B��G��B�\G�� B�\)G��B�ǮG�� B�z�G��B�aHG�� B��qG��B�z�G�� B��G��B�Q�G�� B���G��B���G�� B�G�G��B��HG�� B�p�G��B�{G�� B��3G��B���G�� B���G��B��\G�� B��RG��B��RG�� B�u�G���B��\G�� B���G��B�#�G�� B���G��B�=qG�� B��HG��B�ǮG�� B�ffG��B��\G�� B��G���B���G�� B�8RG���B�aHG�� B�33G���B�W
G�� B���G���B�G�G�� B��qG���B���G�� B��
G���B�.G�� B��\G���B�ǮG�� B��qG���B��G�� B��RG���B��G�� B�\G���B��G�� B���G���B��=G�  B�\G� �B�ffG� B�  G��B�B�G� B�L�G��B�=qG� B�=qG��B�(�G� B�\G��B�G�G� B���G��B�� G� B�\)G��B�ǮG� B�ǮG��B��G� B�G��B�(�G�	 B���G�	�B�� G�
 B�\G�
�B���G� B�ffG��B���G� B��G��B�W
G� B�.G��B��)G� B�ǮG��B��\G� B��)G��B�(�G� B�aHG��B���G� B�p�G��B��3G� B�.G��B�#�G� B���G��B�.G� B�(�G��B�ǮG� B�33G��B�u�G� B��G��B��HG� B��G��B���G� B�z�G��B�#�G� B�z�G��B��G� B��G��B�=qG� B�aHG��B�L�G� B�\G��B�k�G� B��G��B�ffG� B�\)G��B�L�G� B�8RG��B�p�G�  B�L�G� �B��G�! B��\G�!�B��fG�" B�G�G�"�B���G�# B���G�#�B�G�G�$ B���G�$�B��3G�% B�#�G�%�B��G�& B�p�G�&�B���G�' B��G�'�B�=qG�( B�{G�(�B�\G�) B�=qG�)�B��G�* B�p�G�*�B�.G�+ B���G�+�B���G�, B��HG�,�B�8RG�- B��\G�-�B��\G�. B���G�.�B�33G�/ B��
G�/�B�� G�0 B���G�0�B�#�G�1 B�.G�1�B�ǮG�2 B�\)G�2�B��qG�3 B���G�3�B�W
G�4 B���G�4�B�=qG�5 B�\G�5�B��G�6 B���G�6�B��=G�7 B�p�G�7�B��{G�8 B���G�8�B�B�G�9 B�  G�9�B�L�G�: B�aHG�:�B��\G�; B�aHG�;�B�� G�< B��{G�<�B�G�= B�� G�=�B��=G�> B��G�>�B�z�G�? B��G�?�B���G�@ B��G�@�B�{G�A B�k�G�A�It#�G�B It#�G�B�It#�G�C It#�G�C�It#�G�D It#�G�D�It#�G�E It#�G�E�It#�G�F It#�G�F�It#�G�G It#�G�G�It#�G�H It#�G�H�It#�G�I It#�G�I�It#�G�J It#�G�J�It#�G�K It#�G�K�It#�G�L It#�G�L�It#�G�M It#�G�M�It#�G�N It#�G�N�It#�G�O It#�G�O�It#�G�P It#�G�P�It#�G�Q It#�G�Q�It#�G�R It#�G�R�It#�G�S It#�G�S�It#�G�T It#�G�T�It#�G�U It#�G�U�It#�G�V It#�G�V�It#�G�W It#�G�W�It#�G�X It#�G�X�It#�G�Y It#�G�Y�It#�G�Z It#�G�Z�It#�G�[ It#�G�[�It#�G�\ It#�G�\�It#�G�] It#�G�]�It#�G�^ It#�G�^�It#�G�_ It#�G�_�It#�G�` It#�G�`�It#�G�a It#�G�a�It#�G�b It#�G�b�It#�G�c It#�G�c�It#�G�d It#�G�d�It#�G�e It#�G�e�It#�G�f It#�G�f�It#�G�g It#�G�g�It#�G�h It#�G�h�It#�G�i It#�G�i�It#�G�j It#�G�j�It#�G�k It#�G�k�It#�G�l It#�G�l�It#�G�m It#�G�m�It#�G�n It#�G�n�It#�G�o It#�G�o�It#�G�p It#�G�p�It#�G�q It#�G�q�It#�G�r It#�G�r�It#�G�s It#�G�s�It#�G�t It#�G�t�It#�G�u It#�G�u�It#�G�v It#�G�v�It#�G�w It#�G�w�It#�G�x It#�G�x�It#�G�y It#�G�y�It#�G�z It#�G�z�It#�G�{ It#�G�{�It#�G�| It#�G�|�It#�G�} It#�G�}�It#�G�~ It#�G�~�It#�G� It#�G��It#�GӀ It#�GӀ�It#�GӁ It#�GӁ�It#�Gӂ It#�Gӂ�It#�GӃ B��GӃ�B�33Gӄ B��HGӄ�B���GӅ B�Q�GӅ�B��Gӆ B��HGӆ�B�8RGӇ B��GӇ�B�8RGӈ B���Gӈ�B��{GӉ B���GӉ�B��\Gӊ B�u�Gӊ�B�(�GӋ B�� GӋ�B�#�Gӌ B�p�Gӌ�B��qGӍ B��\GӍ�B���Gӎ B�ǮGӎ�B���Gӏ B�k�Gӏ�B�{GӐ B�#�GӐ�B�W
Gӑ B��)Gӑ�B��RGӒ B�z�GӒ�B�p�Gӓ B��3Gӓ�B��GӔ B���GӔ�B�L�Gӕ B��\Gӕ�B���GӖ B��\GӖ�B�B�Gӗ B��HGӗ�B��fGӘ B�� GӘ�B���Gә B�W
Gә�B��GӚ B���GӚ�B�\Gӛ B�=qGӛ�B��=GӜ B��
GӜ�B�ǮGӝ B���Gӝ�B�(�GӞ B�(�GӞ�B���Gӟ B��)Gӟ�B�u�GӠ B��{GӠ�B�=qGӡ B�33Gӡ�B��
GӢ B��
GӢ�B��Gӣ B�W
Gӣ�B�.GӤ B���GӤ�B��)Gӥ B�  Gӥ�B�aHGӦ B��GӦ�B���Gӧ B�Gӧ�B��HGӨ B��GӨ�B�L�Gө B�#�Gө�B�8RGӪ B�ǮGӪ�B�
=Gӫ B���Gӫ�B��3GӬ B�z�GӬ�B���Gӭ B��Gӭ�B�GӮ B��3GӮ�B�
=Gӯ B��3Gӯ�B�z�GӰ B�B�GӰ�B�aHGӱ B��Gӱ�B��=GӲ B�ffGӲ�B�  Gӳ B�aHGӳ�B��\GӴ B���GӴ�B�k�Gӵ B��3Gӵ�B��GӶ B�W
GӶ�B���Gӷ B�8RGӷ�B�k�GӸ B�(�GӸ�B���Gӹ B�u�Gӹ�B��fGӺ B��\GӺ�B��Gӻ B��fGӻ�B�(�GӼ B�aHGӼ�B��Gӽ B��{Gӽ�B���GӾ B�  GӾ�B��Gӿ B�  Gӿ�B�ffG�� BĸRG���B�W
G�� B��HG���B��
G�� B�.G�B�
=G�� B�G�ÀB��=G�� B�� G�ĀB�Q�G�� B�(�G�ŀB�G�G�� B�ffG�ƀB�ffG�� B���G�ǀB��
G�� B�ffG�ȀB�p�G�� B�G�G�ɀB�Q�G�� B�#�G�ʀB�B�G�� BŔ{G�ˀBĽqG�� B�(�G�̀B���G�� BƳ3G�̀Bʳ3G�� B�8RG�΀B��qG�� B��RG�πB���G�� B�G�ЀB��\G�� B�L�G�рB��G�� B���G�ҀB��G�� B�{G�ӀB�B�G�� B��HG�ԀB�8RG�� B��fG�ՀBŏ\G�� B{G�րB�p�G�� B�\)G�׀B�B�G�� B��\G�؀B�\)G�� B�  G�ـB�G�� B��G�ڀB�G�G�� B��=G�ۀB��\G�� B�B�G�܀B��)G�� B�W
G�݀B�B�G�� BÔ{G�ހB�.G�� B�=qG�߀B�
=G�� B��G���B�z�G�� B�ǮG��B���G�� B��G��B�ffG�� B{G��B=G�� B�B�G��B�33G�� B�  G��B�W
G�� B�G�G��B�ǮG�� B�u�G��B��
G�� B�p�G��B�=qG�� B���G��B��G�� B�ǮG��B�{G�� B�33G��B��fG�� B��G��B��G�� B�(�G��B�L�G�� B��
G��B�G�� B��\G��B��
G�� B�G�G���B��=G�� B�(�G��B��G�� B�k�G��B�\)G�� B��G��B���G�� B��)G��B�\G�� B���G���B�G�G�� B���G���B���G�� B��G���B�Q�G�� B��G���B�.G�� B�ǮG���B�G�G�� B���G���B��G�� B�G�G���B�{G�� B�p�G���B�W
G�� B��)G���B�L�G�� B�G�G���B��HG�� B�G���B���G�  B��{G� �B��fG� B�G��B��)G� B��G��B���G� B�aHG��B��G� B�\)G��B��G� B��RG��B�B�G� B�z�G��B�k�G� B��fG��B�(�G� B��3G��B�#�G�	 B���G�	�B�aHG�
 B�.G�
�B���G� B���G��B�G� B���G��B��)G� B�(�G��B�Q�G� B��G��B��3G� B�B�G��B�
=G� B�ǮG��B��G� B�G�G��B���G� B�W
G��B�aHG� B�W
G��B�p�G� B�G��B���G� B���G��B��HG� B�\)G��B���G� B���G��B�G�G� B��)G��B��qG� B�ǮG��B��{G� B���G��B�z�G� B��)G��B���G� B�Q�G��B�ǮG� B�
=G��B�8RG� B��
G��B�\)G� B�8RG��B���G�  B��
G� �B�G�! B�.G�!�B�p�G�" B��fG�"�B��fG�# B�ǮG�#�B�  G�$ B�L�G�$�B�#�G�% B�
=G�%�B��RG�& B��\G�&�B��=G�' B��\G�'�B�u�G�( B�u�G�(�B���G�) B��
G�)�B���G�* B��G�*�B��=G�+ B�=qG�+�B��G�, B��3G�,�B�z�G�- B�G�-�B��G�. B���G�.�B�ffG�/ B�33G�/�B��G�0 B�#�G�0�B�
=G�1 B��
G�1�B�ǮG�2 B��G�2�B��G�3 B��HG�3�B��G�4 B���G�4�B�G�5 B�{G�5�B�aHG�6 B��
G�6�B�.G�7 B���G�7�B�Q�G�8 B�Q�G�8�B�u�G�9 B��)G�9�B�L�G�: B�
=G�:�B���G�; B�33G�;�B�G�< B��G�<�B�(�G�= B�� G�=�B��G�> B�p�G�>�B���G�? B��\G�?�B�33G�@ B���G�@�B�(�G�A B�W
G�A�B��HG�B B���G�B�B�L�G�C B���G�C�B��G�D B���G�D�B��G�E B�33G�E�B��G�F B�aHG�F�B���G�G B��qG�G�B��)G�H B���G�H�B��RG�I B���G�I�B���G�J B�W
G�J�B�G�K B��=G�K�B���G�L B�8RG�L�B��{G�M B��G�M�B���G�N B�G�N�B�G�G�O B���G�O�B�{G�P B�� G�P�B�.G�Q B���G�Q�B���G�R B�k�G�R�B��RG�S B���G�S�B��)G�T B�#�G�T�B�\)G�U B��{G�U�B���G�V B�B�G�V�B��{G�W B�u�G�W�B�L�G�X B��G�X�B�.G�Y B��)G�Y�B��G�Z B�L�G�Z�B�
=G�[ B�G�[�B��HG�\ B���G�\�B��
G�] B�#�G�]�B��3G�^ B�G�^�B���G�_ B�W
G�_�B�p�G�` B��G�`�B��
G�a B�B�G�a�B���G�b B�\G�b�B�p�G�c B���G�c B�{G�c�B��\G�d B�=qG�d�B�=qG�e B�#�G�e�B���G�f B�z�G�f�B�L�G�g B���G�g�B���G�h B�#�G�h�B���G�i B�� G�i�B���G�j B���G�j�B�#�G�k B�L�G�k�B��G�l B��HG�l�B���G�m B�� G�m�B�z�G�n B��G�n�B���G�o B���G�o�B���G�p B��G�p�B�#�G�q B�W
G�q�B��fG�r B���G�r�B�z�G�s B���G�s�B��fG�t B��HG�t�B�G�u B���G�u�B�B�G�v B�W
G�v�B�� G�w B�\)G�w�B�33G�x B��G�x�B��
G�y B�ffG�y�B��G�z B���G�z�B�ffG�{ B���G�{�B��G�| B�k�G�|�B�=qG�} B��G�}�B�  G�~ B�  G�~�B��G� B��G��B�GԀ B��fGԀ�B�(�Gԁ B���Gԁ�B�
=GԂ B�ffGԂ�B��fGԃ B���Gԃ�B��3GԄ B�\)GԄ�B���Gԅ B��Gԅ�B���GԆ B�=qGԆ�B�p�Gԇ B��Gԇ�B�� GԈ B��=GԈ�B��RGԉ B��HGԉ�B���GԊ B��)GԊ�B��RGԋ B��=Gԋ�B�aHGԌ B�{GԌ�B�  Gԍ B�\Gԍ�B�ffGԎ B�G�GԎ�B�z�Gԏ B�� Gԏ�B��GԐ B���GԐ�B�Gԑ B��qGԑ�B��HGԒ B�Q�GԒ�B�Gԓ B�
=Gԓ�B�33GԔ B���GԔ�B��fGԕ B��Gԕ�B�aHGԖ B�\)GԖ�B���Gԗ B��Gԗ�B�B�GԘ B���GԘ�B�k�Gԙ B�  Gԙ�B�8RGԚ B��RGԚ�B��
Gԛ B��Gԛ�B�#�GԜ B��)GԜ�B���Gԝ B��{Gԝ�B���GԞ B�GԞ�B��Gԟ B�\Gԟ�B�z�GԠ B�(�GԠ�B�k�Gԡ B��
Gԡ�B�#�GԢ B�\GԢ�B��qGԣ B��Gԣ�B��qGԤ B�L�GԤ�B��Gԥ B��Gԥ�B���GԦ B��GԦ�B��fGԧ B���Gԧ�B�ǮGԨ B��GԨ�B�k�Gԩ B��Gԩ�B��)GԪ B��RGԪ�B�ffGԫ B�L�Gԫ�B�8RGԬ B��\GԬ�B�\)Gԭ B�aHGԭ�B���GԮ B�L�GԮ�B��qGԯ B��Gԯ�B�aHG԰ B�\)G԰�B��)GԱ B�p�GԱ�B�p�GԲ B�#�GԲ�B£�GԳ B��HGԳ�B��GԴ B�G�GԴ�B GԵ B�G�GԵ�B���GԶ B��GԶ�B�ǮGԷ B�W
GԷ�B�G�GԸ B�L�GԸ�B�GԹ B¸RGԹ�B���GԺ B�p�GԺ�B�8RGԻ B���GԻ�B��3GԼ B��GԼ�B�GԽ B�
=GԽ�B�.GԾ B��
GԾ�B��qGԿ B��fGԿ�B�{G�� B��G���B��)G�� B�33G���B��G�� B�Q�G�B�\)G�� B�G�ÀB��\G�� B��=G�ĀB��G�� B�  G�ŀB��G�� B�u�G�ƀB��
G�� B��
G�ǀBŊ=G�� B�B�G�ȀB�L�G�� B�G�ɀB�G�� B�{G�ʀB��\G�� B���G�ˀB��=G�� B��G�̀B�\G�� B�{G�̀BG�� B�aHG�΀B�aHG�� B�B�G�πB�Q�G�� B��fG�ЀB�8RG�� B��\G�рB��
G�� B���G�ҀB�aHG�� B�k�G�ӀBÀ G�� B³3G�ԀB�\G�� B�G�ՀB�G�G�� B�B�G�րBĞ�G�� B��G�׀B�\G�� B�\G�؀B�u�G�� B�L�G�ـB��
G�� B�8RG�ڀB�� G�� B��G�ۀB���G�� B�ǮG�܀B�#�G�� B�  G�݀B���G�� B�G�ހB�=qG�� B�  G�߀B�  G�� B�k�G���B�.G�� B�33G��B�W
G�� B�z�G��B�G�� B��qG��B��G�� B�\G��B�u�G�� B�\G��B���G�� BÙ�G��B��fG�� B�G��BƔ{G�� BȅG��Bș�G�� B�ffG��B���G�� B�ffG��B�\)G�� B�B�G��B���G�� B��G��B���G�� B��G��B�#�G�� B�.G��B���G�� B�\G��B�aHG�� B�\)G���BШ�G�� B�u�G��B̙�G�� B�k�G��B�u�G�� B̮G��Bə�G�� B�aHG��B�p�G�� B˞�G���B�ffG�� B��
G���B�\)G�� B��G���BȅG�� B�p�G���B�W
G�� Bˀ G���B���G�� B��G���B�  G�� B�p�G���B˔{G�� B͙�G���B�(�G�� B�aHG���B̅G�� B�  G���B�B�G�� B���G���Bˊ=G�  Bʔ{G� �B�ǮG� B�z�G��B��)G� BȸRG��B�\G� B���G��BʽqG� B��fG��BȞ�G� B˳3G��B��
G� Bș�G��B�k�G� B�aHG��B��
G� Bȣ�G��B���G�	 B�L�G�	�B��G�
 B��G�
�B�
=G� BƸRG��Bʣ�G� B�
=G��B��G� B��G��B�Q�G� Bҳ3G��BήG� Bʔ{G��B�8RG� B�u�G��B�(�G� B��G��B̮G� BƮG��Bɀ G� B�33G��B�ff