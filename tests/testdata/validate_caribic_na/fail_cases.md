OM_20200304_591_CPT_MUC_V01_invalid00 - MNAME incorrect
OM_20200304_591_CPT_MUC_V01_invalid01 - XNAME incorrect
OM_20200304_591_CPT_MUC_V01_invalid02 - VNAME incorrect
OM_20200304_591_CPT_MUC_V01_invalid03 - VNAME incorrect
OM_20200304_591_CPT_MUC_V01_invalid04 - SCOM first line incorrect
OM_20200304_591_CPT_MUC_V01_invalid05 - SCOM missing required label
OM_20200304_591_CPT_MUC_V01_invalid06 - SCOM FlightNo does not match that in filename 
OM_20200304_591_CPT_MUC_V01_invalid07 - date in filename does not match that in Ames header
OM_20200304_591_CPT_MUC_V01_invalid08 - SCOM invalid flight route
OM_20200304_591_CPT_XXX_V01_invalid09 - invalid destination airport (IATA code)
OM_20200304_591_CPT_MUC_V01_invalid10 - invalid DepartureAirport
OM_20200304_591_CPT_MUC_V01_invalid11 - invalid ArrivalAirport
OM_20200304_591_CPT_MUC_V01_invalid12 - invalid FileName specified in header
OM_20200304_591_CPT_MUC_V01_invalid13 - invalid flightnumber specified in header
OM_20200304_591_CPT_MUC_V01_invalid14 - changelog should not be "0" if version is > 1
OM_20200304_591_CPT_MUC_V01_invalid15 - NCOM only includes 7 lines
OM_20200304_591_CPT_MUC_V01_invalid16 - NCOM invalid description
OM_20200304_591_CPT_MUC_V01_invalid17 - NCOM invalid description
OM_20200304_591_CPT_MUC_V01_invalid18 - invalid NCOM[-4] (date, vscal)
OM_20200304_591_CPT_MUC_V01_invalid19 - invalid NCOM[-4] (date, vscal)
OM_20200304_591_CPT_MUC_V01_invalid20 - tag "Exceptions" missing in SCOM, cause error in NCOM[-3] check
OM_20200304_591_CPT_MUC_V01_invalid21 - Exceptions specified but line number not in NCOM[-3]
OM_20200304_591_CPT_MUC_V01_invalid22 - invalid VMISS specified in NCOM[-3]
OM_20200304_591_CPT_MUC_V01_invalid23 - invalid unit in NCOM[-2]
OM_20200304_591_CPT_MUC_V01_invalid24 - invalid column header line
